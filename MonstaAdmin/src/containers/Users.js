import React, { Component } from 'react';
import {Link} from 'react-router';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import Data from '../data';
import firebase from '../firebase';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';



  export default class Users extends Component {
    constructor(props) {
      super(props);
      this.state = {
        data:'',
        openDialog:false,
        selectedRowData:null,

        fullNameInput: '',
        fullNameInputError: '',
        userName:'',
        emailInput: '',
        emailInputError: '',
        passwordInput: '',
        passwordInputError: '',
        confirmInput: '',
        confirmInputError: '',
        role:"student",
        mobileNumber:'',
        university:'',
        graduationDate:'',
        course:'',
        major:'',
        careerInterest:'',
        describeYourself:'',
        summary:'',
  
        address:'',
        area:'',
        city:'',
        postcode:'',
        state:'',
        country:'Malaysia'
      };
      // Binding function to `this`
      this.showDetail = this.showDetail.bind(this);
      this.handleCloseDialog = this.handleCloseDialog.bind(this);
      this.renderDialog =this.renderDialog.bind(this);
    }

    handleInputChange = (evt) => {
      const target = evt.target
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name
      this.setState({
        [name]: value
      })
   
   
      switch (name) {
        case 'fullNameInput':
           this.setState({
             fullNameInputError: '',
           })
           break
         case 'emailInput':
          this.setState({
            emailInputError: ''
          })
          break
        case 'passwordInput':
         this.setState({
           confirmInputError: '',
           passwordInputError: ''
         })
         break
       case 'confirmInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
        case 'checkInput':
         this.setState({
           checkInputError: ''
         })
          break;
        default: 
      }
    }

    handleChangeDropdownMajor = (event, index, major) => this.setState({major});
    handleChangeuniversity = (event, index, university) => this.setState({university});
    handleChangeDate = (event, date) => {
     this.setState({ graduationDate: date, });
   };

   handleForm = () => {
    
        var error = false
         if (this.state.fullNameInput === '') {
           this.setState({
             fullNameInputError: 'This field is required'
           })
           error = true
         }
         if (this.state.emailInput === '') {
           this.setState({
             emailInputError: 'This field is required'
           })
           error = true
    
         }
         if (this.state.passwordInput === '') {
           this.setState({
             passwordInputError: 'This field is required'
           })
           error = true
    
         }
         if (this.state.confirmInput === '') {
           this.setState({
             confirmInputError: 'This field is required'
           })
           error = true
    
         }
         else if(this.state.confirmInput !== this.state.passwordInput){
           this.setState({
             passwordInputError: 'This field sould be equal to confirm password',
             confirmInputError: 'This field sould be equal to password'
           })
           error = true
    
         }
         if (!error) {
           this.props.register({
              email: this.state.emailInput,
              password: this.state.passwordInput,
              fullName: this.state.fullNameInput,
              userName:this.state.userName,
              role:this.state.role,
              mobileNumber:this.state.mobileNumber,
              university:this.state.university,
              course:this.state.course,
              major:this.state.major,
              graduationDate:this.state.graduationDate,
              careerInterest:this.state.careerInterest,
              describeYourself:this.state.describeYourself,
              summary:this.state.summary,
    
              address:this.state.address,
              area:this.state.area,
              city:this.state.city,
              postcode:this.state.postcode,
              state:this.state.state,
              country:this.state.country,     
              
            })
         }
    
      }

    componentWillMount() {
      let test ;
      let arr=[];
       firebase.database().ref().child('users/').once('value').then((snapshot)=>{
        test = snapshot.val();
        for(let a in test){
          if(test[a].info.role){
            if(test[a].info.role=='student'){
              test[a].info['id']=a;            
              arr.push(test[a].info);
            }
          }
        }
      },this);     
      //this.state.data=arr;
      this.setState({data:arr});
    }
    showDetail(index){
      this.setState({selectedRowData:this.state.data[index]});
      console.log(this.state.selectedRowData)
      this.setState({company:this.state.data[index].company})
      console.log(this.state.company+' '+"asd")
      this.setState({openDialog:true});
  }  
  handleCloseDialog(){
    this.setState({openDialog: false});
  }

  renderDialog(){
    console.log(this.state.selectedRowData)
    if(this.state.selectedRowData ==null||this.state.selectedRowData==undefined){
      return null;
    }
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleCloseDialog}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        keyboardFocused={true}
        onClick={this.handleClose}
      />,
    ];
    return(
      <Dialog
      title="Edit Info"
      modal={false}
      actions={actions}
      open={this.state.openDialog}
      onRequestClose={this.handleCloseDialog}
      autoScrollBodyContent={true}
      >{
      }
      <TextField
      onChange={this.handleInputChange}
      errorText={this.state.fullNameInputError}
      name="fullNameInput"
      value={this.state.selectedRowData.fullName}
      floatingLabelStyle={{fontSize:"15px"}}
     floatingLabelText="Full Name"
     type="text"
   /><br />
   <TextField
      onChange={this.handleInputChange}
      errorText={this.state.fullNameInputError}
      name="userName"
      value={this.state.selectedRowData.userName}      
      floatingLabelStyle={{fontSize:"15px"}}
     floatingLabelText="User Name"
     type="text"
   /><br />
   <TextField
     onChange={this.handleInputChange}
     errorText={this.state.emailInputError}
     value={this.state.selectedRowData.email}           
     name="emailInput"
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="Email"
    type="email"
  /><br />
  <TextField
   onChange={this.handleInputChange}
   name="mobileNumber"
   value={this.state.selectedRowData.mobileNumber}              
   floatingLabelStyle={{fontSize:"15px"}}
  floatingLabelText="Mobile Number"
  type="text"
/><br/>
  <TextField
    onChange={this.handleInputChange}
    value={this.state.selectedRowData.password}                  
    errorText={this.state.passwordInputError }
    name="passwordInput"
    floatingLabelStyle={{fontSize:"15px"}}
   floatingLabelText="Password"
   type="password"
 /><br />

<SelectField style={{width:'60%'}}  value={this.state.selectedRowData.university}  onChange={this.handleChangeuniversity} autoWidth={true}  floatingLabelText="University" >
<MenuItem value="MMU" primaryText="MMU" />
<MenuItem value="UNITEN" primaryText="UNITEN" />
<MenuItem value="Universiti Malaysia" primaryText="Universiti Malaysia" />
<MenuItem value="UTP" primaryText="UTP" />
</SelectField><br/>
<DatePicker hintText="Graduation Date"  value={this.state.selectedRowData.graduationDate} openToYearSelection={true} onChange={this.handleChangeDate} /><br/>
  <SelectField style={{width:'80%'}} value={this.state.course} labelStyle={{textAlign:'left',color:"gray"}} onChange={this.handleChangeDropdownCourse} autoWidth={true} floatingLabelText="Course" >
       <MenuItem value="Software Enginnering" primaryText="Software Enginnering" />
       <MenuItem value="Bussiness Management" primaryText="Bussiness Management"/>
       <MenuItem value="IT Security" primaryText="IT Security"/>
       <MenuItem value="Multimedia Design" primaryText="Multimedia Design"/>
       <MenuItem value="Electrical Engineering" primaryText="Electrical Engineering"/>
   </SelectField><br/>
 <SelectField style={{width:'80%'}}  value={this.state.selectedRowData.major} labelStyle={{textAlign:'left',color:"gray"}} onChange={this.handleChangeDropdownMajor} autoWidth={true} floatingLabelText="Major" >
   <MenuItem value="Computer Science" primaryText="Computer Science" />
   <MenuItem value="Bussiness " primaryText="Bussiness "/>
   <MenuItem value="Cyber Security" primaryText="Cyber Security"/>
   <MenuItem value="Multimedia" primaryText="Multimedia"/>
   <MenuItem value="Electrical" primaryText="Electrical"/>
</SelectField><br/>
<TextField 
 onChange={this.handleInputChange}  
 name="describeYourself"
 value={this.state.selectedRowData.describeYourself}
 floatingLabelStyle={{fontSize:"15px"}}
 floatingLabelText="Describe Yourself in One Sentence"
 type="text"
 /><br/>
 <TextField
  name="Summary"
  value={this.state.selectedRowData.Summary}  
  floatingLabelText="Write a Summary of Yourself"
  floatingLabelStyle={{fontSize:"15px"}}
  multiLine={true}
  rows={2}
  rowsMax={8}
  onChange={this.handleInputChange}
   /><br />
 <div>
   <h3 style={{
        textAlign: "left",
            paddingTop: "16px",
            fontWeight: 400,
            lineHeight: "32px",
            margin: 0}}> Address</h3>
        </div>
   <TextField
     onChange={this.handleInputChange}
     name="address"
     value={this.state.selectedRowData.address}       
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="Address"
    type="text"
  /><br />
  <TextField
     onChange={this.handleInputChange}
     name="area"
     value={this.state.selectedRowData.area}            
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="Area"
    type="text"
  /><br />
  <TextField
     onChange={this.handleInputChange}
     name="postcode"
     value={this.state.selectedRowData.postcode}                 
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="postcode"
    type="text"
  /><br />
  <TextField
     onChange={this.handleInputChange}
     name="city"
     value={this.state.selectedRowData.city}                      
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="City"
    type="text"
  /><br />
  <TextField
     onChange={this.handleInputChange}
     name="state"
     value={this.state.selectedRowData.state}                           
     floatingLabelStyle={{fontSize:"15px"}}
    floatingLabelText="State"
    type="text"
  /><br />
  <SelectField style={{width:'80%'}}      value={this.state.selectedRowData.country}                           
 labelStyle={{textAlign:'left',color:"gray"}} onChange={this.handleChangeDropdownCountry} autoWidth={true} floatingLabelText="Country" >
       <MenuItem value="Malaysia" primaryText="Malaysia" />
       <MenuItem value="Singapore" primaryText="Singapore" />
   </SelectField><br/>
<br />
</Dialog>

    )
  }

  render(){  
          
        const styles = {
          floatingActionButton: {
            margin: 0,
            top: 'auto',
            right: 20,
            bottom: 20,
            left: 'auto',
            position: 'fixed',
          },
          editButton: {
            fill: grey500
          },
          columns: {
            id: {
              width: 'auto'
            },
            name: {
              width: 'auto'
            },
            industry: {
              width: 'auto'
            },
            edit: {
              width: 'auto'
            }
          }
        };
    return (
      <PageBase title="Users"
                navigation="Application / Users">

        <div>
          {this.state.openDialog?this.renderDialog():null}

          <Table onRowSelection	={this.showDetail}>
            <TableHeader displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}>
              <TableRow>
                <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.fullName}>Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.fullName}>Email</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Mobile</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.company}>University</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.company}>Course</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.company}>Graduation Date</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Career Interest</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Description</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Summary</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Address</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.state.data.map(item =>
                <TableRow key={item.id}>
                  <TableRowColumn style={styles.columns.id}>{item.id}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.fullName}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.email}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.mobileNumber}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.university}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.course}</TableRowColumn>
                  <TableRowColumn style={styles.columns.industry}>{item.graduationDate}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.careerInterest}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.describeYourself}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.summary}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.address+","+item.area+","+item.city+","+item.postcode+","+item.state+","+item.country}</TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>    
        </div>
      </PageBase>
    );
  }
}
