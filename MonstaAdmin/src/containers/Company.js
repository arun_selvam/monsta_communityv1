import React, { Component } from 'react';
import {Link} from 'react-router';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import Data from '../data';
import firebase from '../firebase';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';

import SelectField from 'material-ui/SelectField';


  export default class Company extends Component {
    constructor(props) {
      super(props);
      this.state = {
        emailInput: '',
        emailInputError: '',
        passwordInput: '',
        passwordInputError: '',
        data:'',
        openDialog:false,
        selectedRowData:null,


        company:'',
        companyType:'',
        companySite:'',
        companySSM:'',
        companyFanPage:'',
        companyDesc:'',
        companyMobile:'',
        companyEmail:'',
        companyIndustry:'',
        otherCompanyIndustry:'',
        emailSubFrequency:'0',
  
        fullNameInput: '',
        fullNameInputError: '',
        emailInput: '',
        emailInputError: '',
        passwordInput: '',
        passwordInputError: '',
        confirmInput: '',
        confirmInputError: '',
        mobileNumber:'',
        role:'company',
       
        address:'',
        area:'',
        city:'',
        postcode:'',
        state:'',
        country:'Malaysia',
        validated:false
      }
      this.showDetail = this.showDetail.bind(this);
      this.handleCloseDialog = this.handleCloseDialog.bind(this);
      this.renderDialog =this.renderDialog.bind(this);
      this.gotoCampaigns = this.gotoCampaigns.bind(this)
      this.updateCheck = this.updateCheck.bind(this)
      // Binding function to `this`
    }

    updateCheck=() =>{
      this.setState((oldState) => {
        return {
          checked: !oldState.checked,
        };
      });
    }

    componentWillMount() {
        let test ;
        let arr=[];
         firebase.database().ref().child('users/').once('value').then((snapshot)=>{
          test = snapshot.val();
          for(let a in test){
            if(test[a].info.role){
              if(test[a].info.role=='company'){
                test[a].info['id']=a;            
                arr.push(test[a].info);
              }
            }
          }
        },this);     
        //this.state.data=arr;
        this.setState({data:arr});
      }

      componentDidMount(){
          let test ;
          let arr=[];
           firebase.database().ref().child('users/').once('value').then((snapshot)=>{
            test = snapshot.val();
            for(let a in test){
              if(test[a].info.role){
                if(test[a].info.role=='company'){
                  test[a].info['id']=a;            
                  arr.push(test[a].info);
                }
              }
            }
          },this);     
          //this.state.data=arr;
          this.setState({data:arr});
        
      }

    showDetail(index){
        this.setState({selectedRowData:this.state.data[index]});
        console.log(this.state.selectedRowData)
        this.setState({company:this.state.data[index].company})
        console.log(this.state.company+' '+"asd")
        this.setState({openDialog:true});
    }

    handleCloseDialog(){
      var updateData =   firebase.database().ref('users/' + this.state.selectedRowData +'/info')
      updateData.update({
        validated: true,
      })
      this.setState({openDialog: false});
    }

    handleInputChange = (evt) => {
      const target = evt.target
      const value = target.type === 'checkbox' ? target.checked : target.value
      const name = target.name
      this.setState({
        [name]: value
      })
   
   
      switch (name) {
        case 'fullNameInput':
           this.setState({
             fullNameInputError: '',
           })
           break
         case 'emailInput':
          this.setState({
            emailInputError: ''
          })
          break
        case 'passwordInput':
         this.setState({
           confirmInputError: '',
           passwordInputError: ''
         })
         break
       case 'confirmInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
        case 'checkInput':
         this.setState({
           checkInputError: ''
         })
          break;
        default:
   
   
      }
    }
    renderDialog(){
      console.log('hi')
      console.log(this.state.selectedRowData)
      if(this.state.selectedRowData ==null||this.state.selectedRowData==undefined){
        return null;
      }
      const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.handleCloseDialog}
        />,
        <FlatButton
          label="Submit"
          primary={true}
          keyboardFocused={true}
          onClick={this.handleClose}
        />,
      ];
      return(
      <Dialog
title="Edit Info"
modal={false}
actions={actions}
open={this.state.openDialog}
onRequestClose={this.handleCloseDialog}
autoScrollBodyContent={true}
>{
}
<TextField
onChange={this.handleInputChange}
errorText={this.state.confirmInputError }
name="company"
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Company"
value={this.state.selectedRowData.company}
type="text"
/><br/>
<TextField
onChange={this.handleInputChange}
errorText={this.state.confirmInputError }
name="companySite"
value={this.state.selectedRowData.companySite}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Company Website"
type="text"
/><br/>
<TextField
onChange={this.handleInputChange}
errorText={this.state.confirmInputError }
name="companySSM"
value={this.state.selectedRowData.companySSM}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="SSM Registration Number"
type="text"
/><br/>
<SelectField style={{width:'80%'}} value={this.state.selectedRowData.companyType}  onChange={this.handleChangeDropdownCompanyType} autoWidth={true}  floatingLabelText="Company Type" >
<MenuItem value="Recruitment Agency" primaryText="Recruitment Agency" />
<MenuItem value="SME" primaryText="SME" />
<MenuItem value="MNC" primaryText="MNC" />
<MenuItem value="Non-Profit Organisation" primaryText="Non-Profit Organisation" />
<MenuItem value="Government" primaryText="Government" />
</SelectField><br/>
<SelectField style={{width:'80%'}} value={this.state.selectedRowData.companyIndustry} onChange={this.handleChangeDropdownCompanyIndustry} autoWidth={true} floatingLabelText="Industry" >
<MenuItem value="Business Administrative" primaryText="Business Administrative" />
<MenuItem value="Culinary Arts" primaryText="Culinary Arts" />
<MenuItem value="Engineering" primaryText="Engineering" />
<MenuItem value="Event Management" primaryText="Event Management" />
<MenuItem value="Hospitality" primaryText="Hospitality" />
<MenuItem value="Information Technology" primaryText="Information Technology" />
<MenuItem value="Logistic" primaryText="Logistic" />
<MenuItem value="Marketing" primaryText="Marketing" />
<MenuItem value="Mass Communication" primaryText="Mass Communication" />
<MenuItem value="Science" primaryText="Science" />
<MenuItem value="Psychology" primaryText="Psychology" />
<MenuItem value="Others" primaryText="Others" />
</SelectField><br/>
<TextField style={this.state.companyIndustry!="Others"?{display:'none'}:{}}
onChange={this.handleInputChange}
errorText={this.state.emailInputError}
name="otherCompanyIndustry"
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Please Specify"
type="text"
/><br/>
<TextField
onChange={this.handleInputChange}
errorText={this.state.emailInputError}
name="companyFanPage"
value={this.state.selectedRowData.companyFanPage}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Company Social Media Page"
type="text"
/><br/>
<TextField
onChange={this.handleInputChange}
name="companyEmail"
value={this.state.selectedRowData.companyEmail}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Company Email"
type="text"
/><br/>
<TextField
onChange={this.handleInputChange}
name="companyMobile"
value={this.state.selectedRowData.companyMobile}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Company Mobile"
type="text"
/><br/>
<TextField
name="companyDesc"
value={this.state.selectedRowData.companyDesc}
floatingLabelText="Write a short description of your company"
floatingLabelStyle={{fontSize:"15px"}}
multiLine={true}
rows={2}
rowsMax={8}
onChange={this.handleInputChange}
/><br />
<SelectField style={{width:'80%'}} value={this.state.selectedRowData.emailSubFrequency} labelStyle={{textAlign:'left',color:"gray"}} onChange={this.handleChangeDropdownemailSubFrequency} autoWidth={true} floatingLabelText="Email Subscriptions Alert" >
<MenuItem value="0" primaryText="Real Time(As Soon as there's any updates" />
<MenuItem value="1" primaryText="Daily Summary" />
<MenuItem value="2" primaryText="No notification" />
</SelectField><br/>
{/*company Details*/}
{/*Address */}
<div>
<h3 style={{
textAlign: "left",
paddingTop: "16px",
fontWeight: 400,
lineHeight: "32px",
margin: 0}}>Company Address</h3>
</div>
<TextField
onChange={this.handleInputChange}
name="address"
floatingLabelStyle={{fontSize:"15px"}}
value={this.state.selectedRowData.address}
floatingLabelText="Address"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
name="area"
value={this.state.selectedRowData.area}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Area"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
name="postcode"
value={this.state.selectedRowData.postcode}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="postcode"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
name="city"
value={this.state.selectedRowData.city}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="City"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
name="state"
value={this.state.selectedRowData.state}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="State"
type="text"
/><br />
<SelectField style={{width:'80%'}} value={this.state.selectedRowData.country} labelStyle={{textAlign:'left',color:"gray"}} onChange={this.handleChangeDropdownCountry} autoWidth={true} floatingLabelText="Country" >
<MenuItem value="Malaysia" primaryText="Malaysia" />
</SelectField><br/>
<div>
{/*Address */}
{/*Contact Person Details */}
<h3 style={{
textAlign: "left",
paddingTop: "16px",
fontWeight: 400,
lineHeight: "32px",
margin: 0}}>Contact Person Details</h3>
</div>
<TextField
onChange={this.handleInputChange}
errorText={this.state.emailInputError}
name="fullNameInput"
value={this.state.selectedRowData.fullName}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Your Name"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
errorText={this.state.emailInputError}
name="emailInput"
value={this.state.selectedRowData.email}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Email"
type="email"
/><br />
<TextField
onChange={this.handleInputChange}
errorText={this.state.emailInputError}
name="mobileNumber"
value={this.state.selectedRowData.mobileNumber}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Your Mobile"
type="text"
/><br />
<TextField
onChange={this.handleInputChange}
errorText={this.state.passwordInputError }
name="passwordInput"
value={this.state.selectedRowData.passwordInput}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Password"
type="password"
/><br />
<TextField
onChange={this.handleInputChange}
errorText={this.state.confirmInputError }
name="confirmInput"
value={this.state.selectedRowData.confirmInput}
floatingLabelStyle={{fontSize:"15px"}}
floatingLabelText="Confirm Password"
type="password"
/><br/>
<Checkbox
          label={"Approve"}
          checked={this.state.validated}
          onCheck={this.updateCheck.bind(this)}
        />
</Dialog>
      )
    }

    gotoCampaigns(){
      console.log("campaigns initiataed")
    }

    handleChangeDropdownCompanyType = (event, index, companyType) => this.setState({companyType});
    handleChangeDropdownCompanyIndustry = (event, index, companyIndustry) => this.setState({companyIndustry});
    handleChangeDropdownemailSubFrequency =(event, index, emailSubFrequency) => this.setState({emailSubFrequency});
    handleChangeDropdownCountry  = (event, index, country) => 
    this.setState({country});

  render(){  

        const styles = {
          floatingActionButton: {
            margin: 0,
            top: 'auto',
            right: 20,
            bottom: 20,
            left: 'auto',
            position: 'fixed',
          },
          editButton: {
            fill: grey500
          },
          columns: {
            id: {
              width: 'auto'
            },
            name: {
              width: 'auto'
            },
            industry: {
              width: 'auto'
            },
            edit: {
              width: 'auto'
            }
          }
        }; 
        

        
    return (
      <PageBase title="Company"
                navigation="Application / Company">

        <div>
          {this.state.openDialog?this.renderDialog():null}

          <Table onRowSelection	={this.showDetail}>
            <TableHeader displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}>
              <TableRow>
                <TableHeaderColumn style={styles.columns.id}>ID</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.fullName}>Name</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.fullName}>Email</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.company}>Company</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.company}>Company SSM</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Email</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Site</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Mobile</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Industry</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Type</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>View Campaigns</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {this.state.data.map(item =>
                <TableRow key={item.id} >
                  <TableRowColumn style={styles.columns.id}>{item.id}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.fullName}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.email}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.company}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.companySSM}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.companyEmail}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.companySite}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.mobileNumber}</TableRowColumn>
                  <TableRowColumn style={styles.columns.industry}>{item.companyIndustry}</TableRowColumn>
                  <TableRowColumn style={styles.columns.industry}>{item.companyType}</TableRowColumn>
                  <TableRowColumn onClick={this.gotoCampaigns} style={styles.columns.industry}><FlatButton style={styles.flatButton} onClick={this.gotoCampaigns} style={{color: grey500}}  label="Campaigns"/></TableRowColumn>
                </TableRow>
              )}
            </TableBody>
          </Table>    
        </div>
      </PageBase>
    );  
  }
}
