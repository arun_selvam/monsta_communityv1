import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Checkbox from 'material-ui/Checkbox';
import {grey500, white} from 'material-ui/styles/colors';
import Help from 'material-ui/svg-icons/action/help';
import TextField from 'material-ui/TextField';
import {Link, Redirect,Router,withRouter} from 'react-router';
import ThemeDefault from '../theme-default';

 class LoginPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email:'',
      password:'',
      redirect:false
    };
   }
  
loginPress(){
  if(this.state.email=='admin' && this.state.password=='admin'){
    this.setState({redirect:true});
    }
  else{
  this.setState({redirect:false});      
    }
  }


emailChange(e){
  this.setState({email:e.target.value});
}

passwordChange(e){
  this.setState({password:e.target.value});
}

render(){
  /* if(this.state.redirect){
    console.log('test')
    return <Redirect to="/dashboard" />;
  } */
  const styles = {
    loginContainer: {
      minWidth: 320,
      maxWidth: 400,
      height: 'auto',
      position: 'absolute',
      top: '20%',
      left: 0,
      right: 0,
      margin: 'auto'
    },
    paper: {
      padding: 20,
      overflow: 'auto'
    },
    buttonsDiv: {
      textAlign: 'center',
      padding: 10
    },
    flatButton: {
      color: grey500
    },
    checkRemember: {
      style: {
        float: 'left',
        maxWidth: 180,
        paddingTop: 5
      },
      labelStyle: {
        color: grey500
      },
      iconStyle: {
        color: grey500,
        borderColor: grey500,
        fill: grey500
      }
    },
    loginBtn: {
      float: 'right'
    },
    btn: {
      background: '#4f81e9',
      color: white,
      padding: 7,
      borderRadius: 2,
      margin: 2,
      fontSize: 13
    },
    btnFacebook: {
      background: '#4f81e9'
    },
    btnGoogle: {
      background: '#e14441'
    },
    btnSpan: {
      marginLeft: 5
    },
  };
  return (
    <MuiThemeProvider muiTheme={ThemeDefault}>
      <div>
        <div style={styles.loginContainer}>

          <Paper style={styles.paper}>

            <form>
              <TextField
                hintText="E-mail"
                floatingLabelText="E-mail"
                fullWidth={true}
                onChange={this.emailChange.bind(this)}
              />
              <TextField
                hintText="Password"
                floatingLabelText="Password"
                fullWidth={true}
                type="password"
                onChange={this.passwordChange.bind(this)}                
              />

              <div>
                <Checkbox
                  label="Remember me"
                  style={styles.checkRemember.style}
                  labelStyle={styles.checkRemember.labelStyle}
                  iconStyle={styles.checkRemember.iconStyle}
                />
                  {this.state.redirect?<Link to="/dashboard">
                  <RaisedButton label="Login"
                                primary={true}
                                style={styles.loginBtn}/>
                </Link>: <RaisedButton label="verify"
                                primary={true}
                                style={styles.loginBtn}
                                onClick={this.loginPress.bind(this)}/>}
                 
              </div>
            </form>
          </Paper>

      {/*     <div style={styles.buttonsDiv}>
            <FlatButton
              label="Forgot Password?"
              href="/"
              style={styles.flatButton}
              icon={<Help />}
            />
          </div> */}
        </div>
      </div>
    </MuiThemeProvider>
  );
}
}
export default withRouter(LoginPage)