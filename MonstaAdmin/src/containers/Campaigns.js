import React, { Component } from 'react';
import {Link} from 'react-router';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import Dialog from 'material-ui/Dialog';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentCreate from 'material-ui/svg-icons/content/create';
import ContentAdd from 'material-ui/svg-icons/content/add';
import {pink500, grey200, grey500} from 'material-ui/styles/colors';
import PageBase from '../components/PageBase';
import Data from '../data';
import firebase from '../firebase';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';


  export default class Campaigns extends Component {
    constructor(props) {
      super(props);
      this.state = {
        emailInput: '',
        emailInputError: '',
        passwordInput: '',
        passwordInputError: '',
        data:'',
        openDialog:false,
        selectedRowData:null,
        campaignStateData:[],


        company:'',
        companyType:'',
        companySite:'',
        companySSM:'',
        companyFanPage:'',
        companyDesc:'',
        companyMobile:'',
        companyEmail:'',
        companyIndustry:'',
        otherCompanyIndustry:'',
        emailSubFrequency:'0',
  
        fullNameInput: '',
        fullNameInputError: '',
        emailInput: '',
        emailInputError: '',
        passwordInput: '',
        passwordInputError: '',
        confirmInput: '',
        confirmInputError: '',
        mobileNumber:'',
        role:'company',
       
        address:'',
        area:'',
        city:'',
        postcode:'',
        state:'',
        country:'Malaysia'
      }
      this.showDetail = this.showDetail.bind(this);
      this.handleCloseDialog = this.handleCloseDialog.bind(this);
      this.renderDialog =this.renderDialog.bind(this);
      // Binding function to `this`
    }

    componentWillMount() {
       
        let test ;
        let arr=[]
         firebase.database().ref().child('campaigns/').once('value').then((snapshot)=>{
          test = snapshot.val();
          for(var a in test){
            if(test){
                arr.push({'compkey': a ,'companyCampaign':test[a]})
            }
          }
        },this)
        console.log('test');
        
        this.state.data=arr;
      }
    showDetail(index){
      console.log(index)
        this.setState({selectedRowData:this.state.campaignStateData[index]});
        console.log(this.state.selectedRowData)
        this.setState({openDialog:true});
    }

    handleCloseDialog(){
      this.setState({openDialog: false});
      this.state.selectedRowData=[]
    }

    handleDelete(){
    firebase.database().ref().child('campaings').child(selectedRowData.compKey).child(selectedRowData.campaignKey).remove();
    alert("Deleted") 
    this.state.selectedRowData=[]
    }

  
  
    renderDialog(){
      console.log('hi')
      console.log(this.state.selectedRowData)
      if(this.state.selectedRowData ==null||this.state.selectedRowData==undefined){
        return null;
      }
      var dataTodisplay=[]
      var dataTodisplayhtml=[]
      var keyNames = Object.keys(this.state.selectedRowData);
      for (var i in keyNames) {
        dataTodisplay.push({"name":keyNames[i],"value":this.state.selectedRowData[keyNames[i]]});
        dataTodisplayhtml.push(<div><p>{keyNames[i]} :{this.state.selectedRowData[keyNames[i]]} </p><br /></div>)
      }

      const actions = [
        <FlatButton
          label="Cancel"
          primary={true}
          onClick={this.handleCloseDialog}
        />,
        <FlatButton
          label="Delete"
          primary={true}
          keyboardFocused={true}
          onClick={this.handleDelete}
        />,
      ];
      return(
      <Dialog
        title="View Info"
        modal={false}
        actions={actions}
        open={this.state.openDialog}
        onRequestClose={this.handleCloseDialog}
        autoScrollBodyContent={true}
        >
        <div>
           <div>
           {dataTodisplay.map(function(d, idx){
         return (<p key={idx}>{d.name} : {d.value.toString()}</p>)
       })} 
      </div>
      <p style={{color:'red'}}>Caution when deleting</p>
        </div>
      </Dialog>
      )
    }

    handleChangeDropdownCompanyType = (event, index, companyType) => this.setState({companyType});
    handleChangeDropdownCompanyIndustry = (event, index, companyIndustry) => this.setState({companyIndustry});
    handleChangeDropdownemailSubFrequency =(event, index, emailSubFrequency) => this.setState({emailSubFrequency});
    handleChangeDropdownCountry  = (event, index, country) => 
    this.setState({country});

    updateCampaignData(data){
      this.state.campaignStateData=data      
    }

  render(){  
    var CampaignData=[]
    this.state.data.forEach(function(comp){
      for(var campaign in comp.companyCampaign){
        comp.companyCampaign[campaign]['compKey']=comp.compkey
        comp.companyCampaign[campaign]['campaignKey']=campaign     
        CampaignData.push(comp.companyCampaign[campaign])
      }
    })   
      this.updateCampaignData(CampaignData)

        const styles = {
          floatingActionButton: {
            margin: 0,
            top: 'auto',
            right: 20,
            bottom: 20,
            left: 'auto',
            position: 'fixed',
          },
          editButton: {
            fill: grey500
          },
          columns: {
            id: {
              width: 'auto'
            },
            name: {
              width: 'auto'
            },
            industry: {
              width: 'auto'
            },
            edit: {
              width: 'auto'
            }
          }
        }; 
        

        
    return (
      <PageBase title="Campaigns"
                navigation="Application / Campaigns">

        <div>
          {this.state.openDialog?this.renderDialog():null}


          <Table onRowSelection	={this.showDetail}>
            <TableHeader displaySelectAll={false}
            adjustForCheckbox={false}
            enableSelectAll={false}>
              <TableRow>
                <TableHeaderColumn style={styles.columns.name}>Company</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.name}>Campaign Title</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.name}>Descriptions</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.name}>Date</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.name}>Experience Awarded</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.name}>Number of Participants</TableHeaderColumn>
               {/*  <TableHeaderColumn style={styles.columns.companyIndustry}>Company Site</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Mobile</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Industry</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>Company Type</TableHeaderColumn>
                <TableHeaderColumn style={styles.columns.companyIndustry}>View Campaigns</TableHeaderColumn> */}
              </TableRow>
            </TableHeader>
            <TableBody
            displayRowCheckbox={false}>
              {CampaignData.map((item,index) =>
                <TableRow key={index} >
                  <TableRowColumn style={styles.columns.name}>{item.Company}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.campaignTitle}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.description}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.date}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.monstaXP}</TableRowColumn>
                  <TableRowColumn style={styles.columns.name}>{item.ParticipantList? Object.keys(item.ParticipantList).length:0}</TableRowColumn>
                  {/*<TableRowColumn style={styles.columns.name}>{item.companySite}</TableRowColumn>
                   <TableRowColumn style={styles.columns.name}>{item.mobileNumber}</TableRowColumn>
                  <TableRowColumn style={styles.columns.industry}>{item.companyIndustry}</TableRowColumn>
                  <TableRowColumn style={styles.columns.industry}>{item.companyType}</TableRowColumn>
                  <TableRowColumn onClick={this.gotoCampaigns} style={styles.columns.industry}><FlatButton style={styles.flatButton} onClick={this.gotoCampaigns} style={{color: grey500}}  label="Campaigns"/></TableRowColumn>
                 */}</TableRow>
              )}
            </TableBody>
          </Table>    
        </div>
      </PageBase>
    );  
  }
}
