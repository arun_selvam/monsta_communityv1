import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import 'react-chat-elements/dist/main.css'
import {Input} from 'react-chat-elements'
import {Button} from 'react-chat-elements'
import trim from 'trim'

//import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Redirect} from 'react-router-dom'
import {
		Card,
		CardActions,
		CardHeader,
		CardMedia,
		CardTitle,
		CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {firebaseAuth, firebaseRef, firebaseApp} from 'app/firebase/'
import moment from 'moment'
import {
		ThemeProvider,
		Avatar,
		Subtitle,
		ChatListItem,
		ChatList,
		Row,
		Column,
		Title,
		MessageMedia,
		MessageTitle,
		MessageText,
		MessageButtons,
		MessageButton
} from '@livechat/ui-kit'
import {MessageList} from 'react-chat-elements'
import InstantMessengerRenderMessages from 'InstantMessengerRenderMessages'
import InstantMessengerInput from 'InstantMessengerInput'

export default class InstantMessengerMsgList extends Component {

		// Constructor
		constructor(props) {
				super(props)
				// Default state
				this.state = {
						Users: [],
						userData: [],
						userDataToRender: [],
						selectedChatRoom: '',
						message:[]
				}

		}

		componentWillMount() {

		}
  


		render() {
				return (
						<div style={{
								height: '100vh'
						}}>

						{this.props.selectedUserChatData!=undefined ? 
						<p>Chatting with {this.props.selectedUserChatData.data.fullName}
						</p>:<p>Select someone To Chat with</p>}
								<div
										style={{
										width: '100%',
										height: '80%',
										overflow: 'scroll',
										borderColor: 'orange',
										borderWidth: '1px'
								}}>
							<InstantMessengerRenderMessages chatRoomID={this.props.chatRoomID} />
								</div>
								<div
										style={{
										height: '10%',
										backgroundColor: 'grey'
								}}>
									<InstantMessengerInput chatRoomID={this.props.chatRoomID}/>
								</div>
						</div>
				)
		}
}