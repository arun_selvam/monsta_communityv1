// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter, Route, Switch, Redirect, NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Tabs, Tab } from 'material-ui/Tabs'
import Divider from 'material-ui/Divider'
import RaisedButton from 'material-ui/RaisedButton';
import { grey50, grey200, grey400, grey600, cyan500 } from 'material-ui/styles/colors'
import { push } from 'react-router-redux'
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import { firebaseRef } from 'app/firebase/'
import TextField from 'material-ui/TextField'
import moment from 'moment'
import Phone from 'react-icons/lib/md/local-phone'
import Email from 'react-icons/lib/md/email'
import Location from 'react-icons/lib/md/location-on'
import '../styles/app.scss'
import Dialog from 'material-ui/Dialog'
import DialogTitle from 'DialogTitle'
import ImageGallery from 'ImageGallery'
import CameraIcon from 'react-icons/lib/fa/camera'
import MenuItem from 'material-ui/MenuItem'
import SelectField from 'material-ui/SelectField'
import DatePicker from 'material-ui/DatePicker';
import Pagination from 'Pagination';
import jsPDF from 'jsPDF';
import CoverAvatar from 'CoverAvatar'
import '../styles/div.scss'

// - Import app components
import HomeHeader from 'HomeHeader'
import UserAvatar from 'UserAvatar'

// - Import API


// - Import actions
import * as globalActions from 'globalActions'

import Responsive from 'react-responsive';

/**
* Create component class
 */
var endorse = ''
var compCredit = 0
var skillList = []
var currentUserInfo

export class MyProfile extends Component {

  static propTypes = {

  }

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props)

    //Defaul state
    this.state = {
      userInfo: '',
      role: '',
      endorse: '',
      endorseArray: [],
      isInclude: false,
      skills: [],
      currentId: '',
      //skillList: [],
      unlockStudent: false,
      campaignIncluded: [],
      openAvatar: false,
      avatar: '',
      describeYourself: '',
      summary: '',
      isEdit: false,
      address: '',
      city: '',
      state: '',
      postcode: '',
      area: '',
      university: '',
      course: '',
      graduationDate: '',
      mobileNumber: '',
      email: '',
      monstaPer: '',
      monstaLvl: 1,
      divideBy: 1000,
      lookingFor: '',
      followers: '',
      pageOfItems: [],
      uid: '',
      isDirect: false,
      myRank: [],
      avatarType: '',
      coverAvatar: '',
      isProfile: false,
      compUid: '',
      pageOfCamp: [],
      companyUid: '',
      campaignUid: '',
      isCampaignView: false,
      fullName: '',
      title: ''
    }
    // Binding functions to `this`
    this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
    this.onChangeCamp = this.onChangeCamp.bind(this);
    this.onChangePage = this.onChangePage.bind(this);
    this.handleDownload = this.handleDownload.bind(this)
  }

  loadData() {
    var userId = this.props.match.params.userId
    var skills = []
    if (Object.keys(this.props.peopleInfo)[0]) {
      var campaignList = firebaseRef.child("campaigns")//.child(Object.keys(this.props.peopleInfo)[0])
      campaignList.on('value', snapshot => {
        var data = snapshot.val()
        var dataKey = Object.keys(data)
        var participantList = []
        var campaignIncluded = []
        dataKey.forEach(function (e) {
          var dataList = data[e]
          var dataListKey = Object.keys(data[e])
          dataListKey.forEach(function (i) {
            if (dataList[i].ParticipantList) {
              var plist = dataList[i].ParticipantList
              var pKeys = Object.keys(plist)
              if (e === Object.keys(this.props.peopleInfo)[0]) {
                pKeys.forEach(function (idx) {
                  participantList.includes(idx) === false ?
                    participantList.push(idx)
                    :
                    ''
                })
              }

              if (pKeys.includes(userId)) {
                if (dataList[i].typeOfCampaign === 1) {
                  if (plist[userId].isRate) {
                    campaignIncluded.push({
                      companyUid: e,
                      campaignUid: i,
                      campaignTitle: dataList[i].campaignTitle,
                      description: dataList[i].description,
                      companyAvatar: dataList[i].companyAvatar,
                      dateApply: plist[userId].dateApply,
                      compName: dataList[i].Company
                    })
                  }
                }
                else if (dataList[i].typeOfCampaign === 2) {
                  if (dataList[i].typeOfSubCampaign === 3 || dataList[i].typeOfSubCampaign === 4) {
                    if (plist[userId].isRate) {
                      campaignIncluded.push({
                        companyUid: e,
                        campaignUid: i,
                        campaignTitle: dataList[i].campaignTitle,
                        description: dataList[i].description,
                        companyAvatar: dataList[i].companyAvatar,
                        dateApply: plist[userId].dateApply,
                        compName: dataList[i].Company
                      })
                    }
                  }
                  else {
                    campaignIncluded.push({
                      companyUid: e,
                      campaignUid: i,
                      campaignTitle: dataList[i].campaignTitle,
                      description: dataList[i].description,
                      companyAvatar: dataList[i].companyAvatar,
                      dateApply: plist[userId].dateApply,
                      compName: dataList[i].Company
                    })
                  }
                }
                else if (dataList[i].typeOfCampaign === 3) {
                  if (dataList[i].typeOfSubCampaign === 1) {
                    if (plist[userId].isRate) {
                      campaignIncluded.push({
                        companyUid: e,
                        campaignUid: i,
                        campaignTitle: dataList[i].campaignTitle,
                        description: dataList[i].description,
                        companyAvatar: dataList[i].companyAvatar,
                        dateApply: plist[userId].dateApply,
                        compName: dataList[i].Company
                      })
                    }
                  }
                  else {
                    campaignIncluded.push({
                      companyUid: e,
                      campaignUid: i,
                      campaignTitle: dataList[i].campaignTitle,
                      description: dataList[i].description,
                      companyAvatar: dataList[i].companyAvatar,
                      dateApply: plist[userId].dateApply,
                      compName: dataList[i].Company
                    })
                  }
                }
                else {
                  campaignIncluded.push({
                    companyUid: e,
                    campaignUid: i,
                    campaignTitle: dataList[i].campaignTitle,
                    description: dataList[i].description,
                    companyAvatar: dataList[i].companyAvatar,
                    dateApply: plist[userId].dateApply,
                    compName: dataList[i].Company
                  })
                }
              }
            }
          }, this)

          campaignIncluded.sort(function (a, b) {
            return new Date(b.dateApply) - new Date(a.dateApply);
          });
          this.setState({ isInclude: participantList.includes(userId), campaignIncluded: campaignIncluded })
        }, this)
      })

      var info = firebaseRef.child(`users`)//.child(Object.keys(this.props.peopleInfo)[0]).child('info')
      var userCircle = firebaseRef.child("userCircles").child(`${userId}/-Followers/users/`)

      info.on('value', snapshot => {
        var allUserData = snapshot.val()
        var allUserKey = Object.keys(snapshot.val())
        var unlockStudent = false
        var myRank = []
        console.log("userId:" + userId)
        currentUserInfo = snapshot.val()[userId]

        userCircle.on('value', circleSanp => {
          var data = circleSanp.val()
          var followers = []
          if (data) {
            var dataKey = Object.keys(data)
            dataKey.forEach(e => {
              followers.push({
                uid: e,
                data: snapshot.val()[e].info
              })
            })
          }
          this.setState({ followers: followers })
        })

        allUserKey.forEach(e => {
          if (allUserData[e].info) {
            if (allUserData[e].info.role === 'student') {
              myRank.push(e)
            }

            myRank.sort(function (a, b) {
              var monstaXP_a = allUserData[a].info.monstaXP ? allUserData[a].info.monstaXP : 0
              var monstaXP_b = allUserData[b].info.monstaXP ? allUserData[b].info.monstaXP : 0
              return monstaXP_b - monstaXP_a
            });

            if (e === Object.keys(this.props.peopleInfo)[0]) {
              if (allUserData[e].info.role === 'company') {
                if (allUserData[e].info.unlockStudent) {
                  unlockStudent = Object.keys(allUserData[e].info.unlockStudent).includes(userId)
                }
                this.compCredit = allUserData[e].credit
              }
              this.setState({ role: allUserData[e].info.role, unlockStudent: unlockStudent })
            }
            this.setState({ myRank: myRank })
          }
        })

        if (userId) {
          var skillsData = []
          var endroseArray = []
          var endorseData = snapshot.val()[userId].info.Endorsements

          if (snapshot.val()[userId].info.skills) {
            snapshot.val()[userId].info.skills.forEach(e => {
              skillsData.push(this.skillList[e])
            }, this)
          }

          if (endorseData) {
            var endroseKey = Object.keys(snapshot.val()[userId].info.Endorsements)
            endroseKey.forEach(e => {
              var data = endorseData[e]
              var dataKey = Object.keys(endorseData[e])

              dataKey.forEach(key => {
                endroseArray.push({
                  uid: e,
                  avatar: snapshot.val()[e].info.avatar,
                  fullName: e === Object.keys(this.props.peopleInfo)[0] ? 'Me' : snapshot.val()[e].info.company,
                  comment: data[key].comment,
                  date: data[key].creationDate,
                  seq: data[key].unixDate
                })
              })
            })
            var sortedArray
            if (endroseArray.length > 1) {
              sortedArray = endroseArray.sort((a, b) => {
                if (a.seq > b.seq) {
                  return -1;
                }
                if (a.seq < b.seq) {
                  return 1;
                }
                return 0;
              })
            } else {
              sortedArray = endroseArray
            }
          }

          var monstaLvl = 1
          var divideBy = 500
          var monstaPer = 0
          var con = true

          if (snapshot.val()[userId].info.monstaXP) {
            if (snapshot.val()[userId].info.monstaXP <= 500) {
              divideBy = 500
              monstaPer = (snapshot.val()[userId].info.monstaXP / divideBy).toFixed(2) * 100
              if (monstaPer === 100) {
                monstaLvl = 2
              }
            }
            else if (snapshot.val()[userId].info.monstaXP <= 1200) {
              divideBy = 1200
              monstaLvl = 2
              monstaPer = (snapshot.val()[userId].info.monstaXP / divideBy).toFixed(2) * 100
              if (monstaPer === 100) {
                monstaLvl = 3
              }
            }
            else if (snapshot.val()[userId].info.monstaXP <= 2500) {
              divideBy = 2500
              monstaLvl = 3
              monstaPer = (snapshot.val()[userId].info.monstaXP / divideBy).toFixed(2) * 100
              if (monstaPer === 100) {
                monstaLvl = 4
              }
            }
            else if (snapshot.val()[userId].info.monstaXP <= 4000) {
              divideBy = 4000
              monstaLvl = 4
              monstaPer = (snapshot.val()[userId].info.monstaXP / divideBy).toFixed(2) * 100
              if (monstaPer === 100) {
                monstaLvl = 5
              }
            }
            else {
              divideBy = 4000
              monstaPer = 100
              monstaLvl = 5
            }
          }

          /*while (con) {
            if (snapshot.val()[userId].info.monstaXP) {
              if (snapshot.val()[userId].info.monstaXP > divideBy) {
                monstaLvl += 1
                divideBy += 1000
              } else {
                monstaPer = (snapshot.val()[userId].info.monstaXP / divideBy).toFixed(2) * 100
                con = false
              }
            } else {
              con = false
            }
          }*/

          this.setState({
            userInfo: snapshot.val()[userId].info, endorseArray: sortedArray, skills: skillsData, currentId: userId,
            avatar: snapshot.val()[userId].info.avatar, describeYourself: snapshot.val()[userId].info.describeYourself,
            summary: snapshot.val()[userId].info.summary, address: snapshot.val()[userId].info.address, area: snapshot.val()[userId].info.area,
            postcode: snapshot.val()[userId].info.postcode, city: snapshot.val()[userId].info.city,
            state: snapshot.val()[userId].info.state, course: snapshot.val()[userId].info.course, university: snapshot.val()[userId].info.university,
            graduationDate: snapshot.val()[userId].info.graduationDate, mobileNumber: snapshot.val()[userId].info.mobileNumber,
            email: snapshot.val()[userId].info.email, monstaPer: monstaPer, monstaLvl: monstaLvl, divideBy: divideBy,
            lookingFor: snapshot.val()[userId].info.lookingFor, coverAvatar: snapshot.val()[userId].info.coverAvatar,
            fullName: snapshot.val()[userId].info.fullName
          })
          this.forceUpdate()
        }
      }, this)
    }
  }

  componentWillMount() {
    var specialization = firebaseRef.child("staticData")
    specialization.on('value', snapshot => {
      this.skillList = snapshot.val().skills
      //this.setState({ skillList: snapshot.val().skills })
      this.loadData()
    })
  }

  /*
   var endorseUser = firebaseRef.child(`users`).child(e).child('info')
          
          */

  handleRequestSetAvatar = (fileName) => {
    if (this.state.avatarType === 'avatar') {
      this.setState({
        avatar: fileName
      })
    }
    else if (this.state.avatarType === 'cover') {
      this.setState({
        coverAvatar: fileName
      })
    }
  }

  onChangeCamp(pageOfCamp) {
    // update state with new page of items
    this.setState({ pageOfCamp: pageOfCamp });
  }

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }

  handleLookingFor = (event, index, value) => this.setState({ lookingFor: value });

  handleInputChange = (evt) => {
    const target = evt.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value
    })
  }

  handleOpenAvatarGallery = (type) => () => {
    var title = ''
    if (type === 'cover') {
      title = 'Choose an image, recommended size 1366 * 768'
    }
    else {
      title = 'Choose an image, recommended size 250 * 250'
    }
    this.setState({
      openAvatar: true, avatarType: type, title: title
    })
  }

  handleCloseAvatarGallery = () => {
    this.setState({
      openAvatar: false
    })
  }

  handleDesc = (event) => {
    this.setState({ describeYourself: event.target.value })
  };

  handleSummary = (event) => {
    this.setState({ summary: event.target.value })
  }

  handlefullName = (event) => {
    this.setState({
      fullName: event.target.value,
    });
  };

  handleChangeDropdownCourse = (event, index, course) => this.setState({ course })
  handleChangeuniversity = (event, index, university) => this.setState({ university });

  handleGraduationDate = (event, date) => {
    this.setState({ graduationDate: date, });
  };


  handleEndorse = (event) => {
    this.endorse = event.target.value
    this.setState({ endorse: event.target.value })
  };

  handleUnlock = (e) => () => {
    if (this.compCredit) {
      if (this.compCredit < 20) {
        alert('Your current credit is not sufficient to unlock the student')
      } else {
        var result = confirm('Confirm to unlock the student? If yes, 20 amount of credit will be deducted from the account.')
        if (result) {
          var compInfo = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}`)
          compInfo.update({
            credit: this.compCredit - 20
          })
          var unlockStudent = compInfo.child(`/info/unlockStudent/${e}`)
          unlockStudent.update({
            date: moment().format('LLL').toString()
          })
          alert('You have successfully unlock the student')
        }
      }
    }
    else {
      alert('Your current credit is not sufficient to unlock the student')
    }
  }

  handleSubmit = () => {
    var userId = this.props.match.params.userId
    var updateData = firebaseRef.child(`users`).child(userId).child('info').child(`Endorsements/${Object.keys(this.props.peopleInfo)[0]}`)
    updateData.push({
      creationDate: moment().format('LLL').toString(),
      unixDate: moment().unix(),
      comment: this.state.endorse
    })
    var notify = {
      description: `${this.props.fullName} has commented on your profile`,
      url: `/MyProfile/${userId}`,
      notifierUserId: Object.keys(this.props.peopleInfo)[0],
      isSeen: false,
      date: new Date().toString()
    }
    var notifyRef = firebaseRef.child(`userNotifies/${userId}`).push(notify)
    this.setState({ endorse: '' })
    //console.log(moment().format('LLL').toString())
  }

  handleUserProfile = (uid) => () => {
    this.setState({ uid: uid, isDirect: true })
  }

  handleCampaignView = (companyUid, campaignUid) => () => {
    this.setState({ companyUid: companyUid, campaignUid: campaignUid, isCampaignView: true })
  }

  handleDownload() {

    var doc = new jsPDF()
    var describeText = doc
      .setFont('times')
      .setFontSize(10)
      .splitTextToSize(this.state.describeYourself, 170);

    var summaryText = doc
      .setFont('times')
      .setFontSize(10)
      .splitTextToSize(this.state.summary, 170);

    // You can also calculate the height of the text very simply:
    var describeHeight = describeText.length * 10 * 1.2 / 2.5
    var summaryHeight = summaryText.length * 10 * 1.2 / 2.5;
    var nextHeight = 65 + describeHeight + summaryHeight

    var skills = ''

    this.state.skills.map((e, idx) => {
      idx === 0 ?
        skills = e
        :
        skills = skills + ' , ' + e
    })

    var skillsText = doc
      .setFont('times')
      .setFontSize(10)
      .splitTextToSize(skills, 170);

    var skillHeight = skillsText.length * 10 * 1.2 / 2.5;

    var experiences = ''

    this.state.campaignIncluded.map((e, idx) => {
      idx === 0 ?
        experiences = e.campaignTitle + `( ${e.compName} ) \n` +
        moment(new Date(e.dateApply)).format('MMMM Do YYYY') + '\n' +
        e.description + '\n\n'
        :
        experiences = experiences +
        e.campaignTitle + `( ${e.compName} ) \n` +
        moment(new Date(e.dateApply)).format('MMMM Do YYYY') + '\n' +
        e.description + '\n\n'
    })

    var expText = doc
      .setFont('times')
      .setFontSize(10)
      .splitTextToSize(experiences, 170);


    doc.setFont("times")
    doc.setFontSize(22);
    doc.setFontType("bold")
    doc.text(20, 20, this.state.userInfo.fullName)
    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, 28, (this.state.course + ' , ' + this.state.university))
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(20, 33, moment(new Date(this.state.graduationDate)).format('MMMM Do YYYY'))
    doc.line(15, 38, 200, 38)
    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, 45, 'Description')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(describeText, 20, 50);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, 55 + describeHeight, 'Summary')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(summaryText, 20, 60 + describeHeight);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, nextHeight, 'Phone Number')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(this.state.mobileNumber, 20, nextHeight + 5);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, nextHeight + 15, 'Email')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(this.state.email, 20, nextHeight + 20);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, nextHeight + 30, 'Address')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(this.state.address + ' , ' + this.state.area, 20, nextHeight + 35);
    doc.text(this.state.postcode + ' , ' + this.state.city, 20, nextHeight + 40);
    doc.text(this.state.state, 20, nextHeight + 45);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, nextHeight + 55, 'Skills')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(skillsText, 20, nextHeight + 60);

    doc.setFontSize(12);
    doc.setFontType("bold")
    doc.text(20, nextHeight + skillHeight + 70, 'Experiences')
    doc.setFontSize(10);
    doc.setFontType("normal")
    doc.text(expText, 20, nextHeight + skillHeight + 75);

    //doc.addPage('a4', '1')

    doc.save("download.pdf");

  }

  handleEdit = () => () => {

    if (this.state.isEdit) {
      firebaseRef.child(`users`).child(this.state.currentId).child('info').update({
        fullName: this.state.fullName,
        avatar: this.state.avatar,
        coverAvatar: this.state.coverAvatar,
        course: this.state.course,
        university: this.state.university,
        graduationDate: this.state.graduationDate,
        describeYourself: this.state.describeYourself,
        summary: this.state.summary,
        mobileNumber: this.state.mobileNumber,
        address: this.state.address,
        area: this.state.area,
        postcode: this.state.postcode,
        city: this.state.city,
        state: this.state.state,
        lookingFor: this.state.lookingFor
      })
    }
    this.setState({ isEdit: !this.state.isEdit })
  }

  handleUserCompProfile = (uid) => () => {
    this.setState({ compUid: uid, isProfile: true })
  }

  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {
    /**
   * Component styles
   */
    const styles = {
      people: {
        margin: '0 auto',
        width: '90%'
      },
      headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
      },
      slide: {
        padding: 10,
      },
      root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      },
      gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
      },
    }

    const { circlesLoaded } = this.props
    const Desktop = props => <Responsive {...props} minWidth={869} />;
/*     const Mid = props => <Responsive {...props} maxWidth={1292}/>;
 */    const Smaller = props => <Responsive {...props} maxWidth={868} />;
    var userId = this.props.match.params.userId

    if (this.state.isDirect) {
      return <Redirect to={`/MyProfile/${this.state.uid}`} />
    }

    if (this.state.isProfile) {
      return <Redirect to={`/companyinfo/${this.state.compUid}`} />
    }

    if (this.state.isCampaignView) {
      if (this.state.role === 'student') {
        return <Redirect to={`/CampaignInfo/${this.state.companyUid}/${this.state.campaignUid}`} />
      } else {
        if (Object.keys(this.props.peopleInfo)[0] === this.state.companyUid) {
          return <Redirect to={`/companymycampaigninfo/${this.state.campaignUid}`} />
        } else {
          return <Redirect to={`/compcampview/${this.state.companyUid}/${this.state.campaignUid}`} />
        }
      }
    }

    return (
      <div>
        <div>
          <Dialog
            title={<DialogTitle title={this.state.title} onRequestClose={this.handleCloseAvatarGallery} />}
            modal={false}
            open={this.state.openAvatar}
            onRequestClose={this.handleCloseAvatarGallery}
            overlayStyle={{ background: "rgba(0,0,0,0.12)" }}
            autoDetectWindowHeight={false}

          >
            <ImageGallery set={this.handleRequestSetAvatar} close={this.handleCloseAvatarGallery} />
          </Dialog>
          {
            this.state.role === 'company' ?
              this.state.isInclude ?
                <div style={{ width: '100%', display: 'flex' }}>
                  <div style={{ textAlign: 'left', marginLeft: '5%', width: '75%' }}>
                    <TextField
                      style={{ width: '100%', fontSize: 13 }}
                      floatingLabelText="Enter Endorsement"
                      multiLine={true}
                      value={this.state.endorse}
                      onChange={this.handleEndorse}
                    />
                  </div>
                  <div style={{ marginLeft: '5%', display: 'table', marginTop: 'auto', marginBottom: 'auto' }}>
                    <RaisedButton style={{ margin: '1%' }} label="Add" primary={true} onClick={this.handleSubmit} />
                  </div>
                </div>
                :
                false
              :
              false
          }
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            <div style={{
              margin: '1%', width: '65%',
              height: 'auto', position: 'relative'
            }}>
              <div>
                <div style={{ position: 'absolute', left: '5%', top: '250px' }}
                  onClick={this.state.isEdit ? this.handleOpenAvatarGallery('avatar') : null}>
                  <UserAvatar fileName={this.state.avatar}
                    size={90} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                </div>
                {
                  this.state.isEdit ?
                    <div style={{
                      position: 'absolute', left: '13%', top: '310px', backgroundColor: '#045e7a', borderRadius: '50%',
                      padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                    }} onClick={this.handleOpenAvatarGallery('avatar')}>
                      <CameraIcon size={15} color='white' />
                    </div>
                    :
                    false
                }
                <CoverAvatar fileName={this.state.coverAvatar} height={'300px'} />
                {
                  this.state.isEdit ?
                    <div style={{
                      position: 'absolute', right: '2%', top: '13px', backgroundColor: 'black', borderRadius: '50%',
                      padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                    }} onClick={this.handleOpenAvatarGallery('cover')}>
                      <CameraIcon size={15} color='white' />
                    </div>
                    :
                    false
                }
                <div style={{
                  backgroundColor: '#FFFFFF', width: '100%', height: 'auto',
                  padding: this.state.currentId === Object.keys(this.props.peopleInfo)[0] ? '0% 2% 2% 7%' : '7% 2% 2% 7%',
                  borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                }}>
                  {
                    this.state.currentId === Object.keys(this.props.peopleInfo)[0] ?
                      <div style={{ textAlign: 'right', padding: '0.5%' }}>
                        <RaisedButton label={this.state.isEdit ? "Save" : "Edit"} backgroundColor='#F5B041'
                          onClick={this.handleEdit()} />
                      </div>
                      :
                      false
                  }
                  {
                    this.state.currentId === Object.keys(this.props.peopleInfo)[0] ?
                      this.state.isEdit ? false
                        :
                        <div style={{ textAlign: 'right', padding: '1%' }}>
                          <RaisedButton label='Print PDF' backgroundColor='#F5B041'
                            onClick={this.handleDownload} />
                        </div>
                      : false
                  }
                  {
                    this.state.isEdit ?
                      <TextField
                        style={{ width: '100%', fontSize: 22 }}
                        multiLine={true}
                        value={this.state.fullName}
                        onChange={this.handlefullName}
                      />
                      :
                      <text style={{ fontSize: 22, fontWeight: 'bold' }}>{this.state.fullName}</text>
                  }
                  <br /><br />
                  <div style={{ width: '100%', backgroundColor: '#CACFD2', borderRadius: 20, height: '10px' }}>
                    <div style={{ width: this.state.monstaPer + '%', backgroundColor: '#045e7a', borderRadius: 20, height: '10px' }}></div>
                  </div>
                  <text style={{ fontSize: 13 }}>{`Monsta XP: ${this.state.userInfo.monstaXP ? this.state.userInfo.monstaXP : 0} / ${this.state.divideBy}`}</text><br />
                  {
                    this.state.isEdit ?
                      <div>
                        <div>
                          <SelectField style={{ width: '100%', fontSize: 13 }} value={this.state.course} labelStyle={{ textAlign: 'left', color: "gray" }} onChange={this.handleChangeDropdownCourse} autoWidth={true} floatingLabelText="Course" >
                            <MenuItem value="Software Enginnering" primaryText="Software Enginnering" />
                            <MenuItem value="Bussiness Management" primaryText="Bussiness Management" />
                            <MenuItem value="IT Security" primaryText="IT Security" />
                            <MenuItem value="Multimedia Design" primaryText="Multimedia Design" />
                            <MenuItem value="Electrical Engineering" primaryText="Electrical Engineering" />
                          </SelectField>
                        </div>
                        <div>
                          <SelectField style={{ width: '100%', fontSize: 13 }} value={this.state.university} onChange={this.handleChangeuniversity} autoWidth={true} floatingLabelText="University" >
                            <MenuItem value="MMU" primaryText="MMU" />
                            <MenuItem value="UNITEN" primaryText="UNITEN" />
                            <MenuItem value="Universiti Malaysia" primaryText="Universiti Malaysia" />
                            <MenuItem value="UTP" primaryText="UTP" />
                          </SelectField>
                        </div>
                        <div>
                          <DatePicker hintText="Graduation Date" floatingLabelText="Graduation Date" openToYearSelection={true} defaultDate={new Date(this.state.graduationDate)} onChange={this.handleGraduationDate} />
                        </div>
                      </div>
                      :
                      <div>
                        <br />
                        <text style={{ fontSize: 15, fontWeight: 'bold' }}>{this.state.course},{this.state.university}</text><br />
                        <text style={{ fontSize: 13 }}>{moment(new Date(this.state.graduationDate)).format('MMMM Do YYYY')}</text>
                      </div>
                  }
                  <br />
                  {
                    this.state.isEdit ?
                      <div>
                        <SelectField value={this.state.lookingFor} onChange={this.handleLookingFor} floatingLabelText="Looking For" >
                          <MenuItem value={1} primaryText="Part-time" />
                          <MenuItem value={2} primaryText="Full-time" />
                          <MenuItem value={3} primaryText="Internship" />
                          <MenuItem value={4} primaryText="Volunteers" />
                          <MenuItem value={5} primaryText="Freelance" />
                        </SelectField>
                      </div>
                      :
                      <div>
                        <text style={{ fontSize: 15, fontWeight: 'bold' }}>Looking For</text><br />
                        <text style={{ fontSize: 13 }}>
                          {
                            this.state.lookingFor ?
                              this.state.lookingFor === 1 ? "Part-time" :
                                this.state.lookingFor === 2 ? "Full-time" :
                                  this.state.lookingFor === 3 ? "Internship" :
                                    this.state.lookingFor === 4 ? "Volunteers" : "Freelance"
                              : ""
                          }
                        </text>
                      </div>
                  }
                  <hr className="hr-text" />
                  {
                    this.state.isEdit ?
                      <TextField
                        style={{ width: '100%', fontSize: 13 }}
                        floatingLabelText="Description"
                        multiLine={true}
                        value={this.state.describeYourself}
                        onChange={this.handleDesc}
                      />
                      :
                      <div>
                        <text style={{ fontSize: 15, fontWeight: 'bold' }}>Description</text>
                        <br />
                        <p style={{ wordWrap: 'break-word', fontSize: 13, margin: 0 }}>{this.state.describeYourself}</p>
                      </div>
                  }
                  <br />
                  {
                    this.state.isEdit ?
                      <div>
                        <TextField
                          style={{ width: '100%', fontSize: 13 }}
                          floatingLabelText="Summary"
                          multiLine={true}
                          value={this.state.summary}
                          onChange={this.handleSummary}
                        />
                      </div>
                      :
                      <div>
                        <text style={{ fontSize: 15, fontWeight: 'bold' }}>A little bit about myself</text><br />
                        <p style={{ wordWrap: 'break-word', fontSize: 13, margin: 0 }}>{this.state.summary}</p>
                      </div>
                  }

                  <hr className="hr-text" />
                  {
                    console.log(this.state.role, this.state.unlockStudent)
                  }
                  {
                    this.state.role === 'company' ?
                      this.state.unlockStudent ?
                        <table>
                          <tr style={{ paddingBottom: '2%' }}>
                            <td>
                              <Phone />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <text style={{ fontSize: 13 }}> {this.state.mobileNumber}</text><br />
                            </td>
                          </tr>
                          <tr style={{ paddingBottom: '2%' }}>
                            <td>
                              <Email />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <text style={{ fontSize: 13 }}> {this.state.email}</text><br />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <Location />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <p style={{ fontSize: 13, margin: 0, wordWrap: 'break-word' }}>
                                {this.state.address}<br />
                                {this.state.area}<br />
                                {this.state.postcode}<br />
                                {this.state.city}, {this.state.state}
                              </p>
                            </td>
                          </tr>
                        </table>
                        :
                        <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                          <div>
                            <text style={{ fontSize: 15, fontWeight: 'bold' }}> 175 CREDIT TO </text><br />
                            <RaisedButton label="Unlock" primary={true} onClick={this.handleUnlock(this.props.match.params.userId)} />
                          </div>
                        </div>
                      :
                      this.state.isEdit ?
                        <div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="Mobile Phone"
                              onChange={this.handleInputChange}
                              name='mobileNumber'
                              value={this.state.mobileNumber}
                            />
                          </div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="Address"
                              name='address'
                              value={this.state.address}
                              onChange={this.handleInputChange}
                            />
                          </div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="Area"
                              name='area'
                              value={this.state.area}
                              onChange={this.handleInputChange}
                            />
                          </div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="Post code"
                              name='postcode'
                              value={this.state.postcode}
                              onChange={this.handleInputChange}
                              type='integer'
                            />
                          </div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="City"
                              name='city'
                              value={this.state.city}
                              onChange={this.handleInputChange}
                            />
                          </div>
                          <div>
                            <TextField
                              style={{ width: '100%', fontSize: 13 }}
                              floatingLabelText="State"
                              name='state'
                              value={this.state.state}
                              onChange={this.handleInputChange}
                            />
                          </div>
                        </div>
                        :
                        <table>
                          <tr style={{ paddingBottom: '2%' }}>
                            <td>
                              <Phone />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <text style={{ fontSize: 13 }}> {this.state.mobileNumber}</text><br />
                            </td>
                          </tr>
                          <tr style={{ paddingBottom: '2%' }}>
                            <td>
                              <Email />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <text style={{ fontSize: 13 }}> {this.state.email}</text><br />
                            </td>
                          </tr>
                          <tr>
                            <td>
                              <Location />
                            </td>
                            <td style={{ paddingLeft: '5%' }}>
                              <p style={{ fontSize: 13, margin: 0, wordWrap: 'break-word' }}>
                                {this.state.address}, {this.state.area}, {this.state.postcode}, {this.state.city}, {this.state.state}
                              </p>
                            </td>
                          </tr>
                        </table>
                  }
                </div>

                <div style={{
                  marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                  height: 'auto', padding: '2% 2% 2% 4%'
                }}>
                  <text style={{ fontSize: 22, fontWeight: 'bold' }}>Skills</text><br />
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {
                      this.state.skills ?
                        this.state.skills.length === 0 ?
                          <div style={{ textAlign: 'center', backgroundColor: '#F2F3F4', borderRadius: 20 }}>
                            <text>No skills available</text>
                          </div>
                          :
                          this.state.skills.map(e => (
                            <div style={{
                              backgroundColor: '#CACFD2', padding: '1%', width: `${e.length * 8 + 20}px`,
                              margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                            }}>
                              <text style={{ fontSize: 13 }}>{e}</text>
                            </div>
                          ))
                        :
                        <div style={{ textAlign: 'center', width: '100%', backgroundColor: '#F2F3F4', borderRadius: 20 }}>
                          <text>No skills available</text>
                        </div>
                    }
                  </div>
                </div>

                <div style={{
                  marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                  height: 'auto', padding: '2% 2% 2% 4%', height: 'auto'
                }}>
                  <text style={{ fontSize: 22, fontWeight: 'bold' }}>Experiences</text><br />
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {
                      this.state.pageOfCamp.length > 0 ?
                        this.state.pageOfCamp.map(e => (
                          <div className={'clickDiv'} style={{
                            backgroundColor: '#F2F3F4', width: '100%', padding: '2%',
                            borderRadius: 20, margin: '1% 0 0 1%', display: 'flex'
                          }} onClick={this.handleCampaignView(e.companyUid, e.campaignUid)}>
                            <div style={{ width: '20%', display: 'table', marginTop: 'auto', marginBottom: 'auto' }}>
                              <UserAvatar
                                fileName={e.companyAvatar}
                                size={70}
                              />
                            </div>
                            <div style={{ width: '75%' }}>
                              <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.campaignTitle} ( {e.compName} )</text>
                              <br />
                              <div style={{ marginTop: '2%', height: 'auto' }}>
                                <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                  {
                                    e.description.length > 300 ?
                                      e.description.substring(0, 300) + '...'
                                      :
                                      e.description
                                  }
                                </p>
                              </div>
                            </div>
                          </div>
                        ))
                        :
                        <div style={{ padding: '1%', margin: '2%', textAlign: 'center', width: '100%', backgroundColor: '#F2F3F4', borderRadius: 20 }}>
                          <text style={{ fontSize: 13 }}>No experiences records in Monsta</text>
                        </div>
                    }
                  </div>
                  <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                    <Pagination items={this.state.campaignIncluded} onChangePage={this.onChangeCamp} numOfItem={4} numOfPage={2} />
                  </div>
                </div>

                <div style={{
                  marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                  height: 'auto', padding: '2% 2% 2% 4%'
                }}>
                  <text style={{ fontSize: 22, fontWeight: 'bold' }}>Endorsement</text><br />
                  <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    {
                      this.state.endorseArray ?
                        this.state.endorseArray.length === 0 ?
                          <div style={{ backgroundColor: '#F2F3F4', borderRadius: 20, width: '100%', padding: '1%', margin: '2%', }}>
                            <p style={{ textAlign: 'center', fontSize: 13 }}>No endorsements records in Monsta</p>
                          </div>
                          :
                          <div style={{
                            margin: '2%', padding: '1%', overflowY: 'auto', height: '300px',
                            borderRadius: 10, width: '100%'
                          }}>
                            {
                              this.state.endorseArray.map(e => (
                                <div style={{ padding: '1%', display: 'flex' }} onClick={this.handleUserCompProfile(e.uid)}>
                                  <div style={{ width: '5%' }}>
                                    <UserAvatar
                                      fileName={e.avatar}
                                      size={50}
                                    />
                                  </div>
                                  <div style={{ marginLeft: '5%', backgroundColor: '#F2F3F4', borderRadius: 20, width: '80%', padding: '1% 1% 1% 5%' }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 12 }}>{e.fullName}</text><br />
                                    <text style={{ fontSize: 15 }}>{e.comment}</text><br />
                                    <text style={{ fontSize: 11 }}>{moment(e.date).fromNow()}</text>
                                  </div>
                                </div>
                              ))
                            }
                          </div>
                        :
                        <div style={{ backgroundColor: '#F2F3F4', borderRadius: 20, width: '100%', padding: '1%', margin: '2%', }}>
                          <p style={{ textAlign: 'center', fontSize: 13 }}>No endorsements records in Monsta</p>
                        </div>
                    }
                  </div>
                </div>
              </div>
            </div>
            <div style={{ width: '30%' }}>
              <div style={{
                marginTop: '2%', borderRadius: 20,
                backgroundColor: 'white', height: 'auto', padding: '2% 2% 2% 4%', textAlign: 'center'
              }}>
                <text style={{ fontSize: 13 }}>My Level </text><br />
                <text style={{ fontSize: 22, fontWeight: 'bold' }}>Lvl {this.state.monstaLvl} </text><br /><br />
                <text style={{ fontSize: 13 }}>My XP </text><br />
                <text style={{ fontSize: 22, fontWeight: 'bold' }}>{this.state.userInfo.monstaXP} </text><br /><br />
                <text style={{ fontSize: 13 }}>My Rank </text><br />
                <text style={{ fontSize: 22, fontWeight: 'bold' }}> #{this.state.myRank.indexOf(this.props.match.params.userId) + 1} </text><br /><br />
                <RaisedButton label="Your Rewards" primary={true} />
              </div>
            {/*   <div style={{
                marginTop: '2%', borderRadius: 20,
                backgroundColor: 'white', height: 'auto', padding: '2% 2% 2% 4%'
              }}>
                <text style={{ fontSize: 18, fontWeight: 'bold' }}>Friends</text><br />
                <div style={{ display: 'flex', flexWrap: 'wrap', borderRadius: 10 }}>
                  {this.state.pageOfItems.map(e =>
                    <div className={'clickDiv'} style={{
                      width: '100%', backgroundColor: 'white',
                      margin: '0.5%', padding: '1%', borderRadius: 20
                    }}>
                      <hr className="hr-text" />
                      <table>
                        <tr>
                          <td style={{ width: '10%' }} onClick={this.handleUserProfile(e.uid)}>
                            <UserAvatar fileName={e.data.avatar} size={90} />
                          </td>
                          <td style={{ width: '20%' }}>
                            <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto' }}>
                              <text style={{ fontWeight: 'bold' }} onClick={this.handleUserProfile(e.uid)}>{e.data.fullName}</text>
                            </div>
                            <br />
                            <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto', marginLeft: 'auto', marginRight: 'auto' }}>
                              <RaisedButton label="Message" primary={true} />
                            </div>
                          </td>
                        </tr>
                      </table>
                    </div>
                  )}
                </div>
                <div style={{
                  marginTop: '2%', borderRadius: 20,
                  backgroundColor: 'white', height: 'auto', padding: '2% 2% 2% 4%'
                }}>
                  <text style={{ fontSize: 18, fontWeight: 'bold' }}>Followed Friends</text><br />
                  <div style={{ display: 'flex', flexWrap: 'wrap', borderRadius: 10 }}>
                    {this.state.pageOfItems.map(e =>
                      <div className={'clickDiv'} style={{
                        width: '100%', backgroundColor: 'white',
                        margin: '0.5%', padding: '1%', borderRadius: 20
                      }}>
                        <hr className="hr-text" />
                        <table>
                          <tr>
                            <td style={{ width: '10%' }} onClick={this.handleUserProfile(e.uid)}>
                              <UserAvatar fileName={e.data.avatar} size={90} />
                            </td>
                            <td style={{ width: '20%' }}>
                              <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto' }}>
                                <text style={{ fontWeight: 'bold' }} onClick={this.handleUserProfile(e.uid)}>{e.data.fullName}</text>
                              </div>
                              <br />
                              <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto', marginLeft: 'auto', marginRight: 'auto' }}>
                                <RaisedButton label="Message" primary={true} />
                              </div>
                            </td>
                          </tr>
                        </table>
                      </div>
                    )}
                  </div>
                  <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                    <Pagination items={this.state.followers} onChangePage={this.onChangePage} numOfItem={10} numOfPage={1} />
                  </div>
                </div>
              </div>
             */}</div>
          </div>
        </div>
      </div>
    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {

  return {
    goTo: (url) => dispatch(push(url)),
    setHeaderTitle: (title) => dispatch(globalActions.setHeaderTitle(title))


  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {

  return {
    uid: state.authorize.uid,
    circlesLoaded: state.circle.loaded,
    avatar: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].avatar : '',
    peopleInfo: state.user.info,
    fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MyProfile))