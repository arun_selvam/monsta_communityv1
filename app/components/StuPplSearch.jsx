import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import Avatar from 'material-ui/Avatar'
import { firebaseRef } from 'app/firebase/'
import Calendar from 'react-icons/lib/go/calendar'
//import Pin from 'react-icons/lib/fa/map-marker'
import Uni from 'react-icons/lib/go/home'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import Course from 'react-icons/lib/go/book'
import SelectField from 'material-ui/SelectField';
import UserAvatar from 'UserAvatar'
import '../styles/div.scss'
/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
const styles = {
    underlineStyle: {
        borderColor: '#70E0F2',
    }
};

class StuPplSearch extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            searchResult: [],
            allCompany: [],
            displayComp: [],
            fullNameKey: '',
            compIndustry: '',
            compType: '',
            stateMalaysia: 0,
            cityMalaysia: ['Selangor', 'Kuala Lumpur', 'Sarawak', 'Johor', 'Pulau Pinang', 'Sabah', 'Perak', 'Pahang', 'Negeri Sembilan', 'Kedah', 'Melaka', 'Terengganu', 'Kelantan', 'Perlis'],
            listMenuItem: [],
            uid: '',
            isDirect: false,
            yearKey: '',
            skillCanLearn: [],
            lookingFor: ''
        }
    }

    renderStu(uid) {
        var allUser = firebaseRef.child(`users`)
        var comps = []
        var allCompany = []
        var arrayData = []
        var credit
        allUser.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            dataKey.forEach(function (e) {
                if (data[e].info) {
                    if (data[e].info.role) {
                        if (data[e].info.role === 'company') {
                            allCompany.push({ uid: e, data: data[e].info })
                            if (uid) {
                                if (uid.includes(e)) {
                                    comps.push({ uid: e, data: data[e].info })
                                    arrayData.push(
                                        <div style={{
                                            width: (window.innerWidth < 1000 ? '100%' : (window.innerWidth < 1200 ? '48%' : '30%')), backgroundColor: 'white',
                                            margin: '1%', padding: '1%', borderRadius: 20
                                        }} onClick={this.handleUserProfile(e)}>
                                            <table>
                                                <tr>
                                                    <td style={{ width: '10%' }}>
                                                        <UserAvatar fileName={data[e].info.avatar}
                                                            size={90} />
                                                    </td>
                                                    <td style={{ width: '20%' }}>
                                                        <text style={{ fontWeight: 'bold' }}>{data[e].info.company}</text><br />
                                                        {
                                                            !data[e].info.companyIndustry ?
                                                                false
                                                                :
                                                                <div>
                                                                    <Course />
                                                                    <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.companyIndustry}</text><br />
                                                                </div>
                                                        }
                                                        {
                                                            !data[e].info.companyType ?
                                                                false
                                                                :
                                                                <div>
                                                                    <Uni />
                                                                    <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.companyType}</text><br />
                                                                </div>
                                                        }
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    )
                                }
                            }
                            else {
                                comps.push({ uid: e, data: data[e].info })
                                arrayData.push(
                                    <div className={'clickDiv'} style={{
                                        width: (window.innerWidth < 1000 ? '100%' : (window.innerWidth < 1200 ? '48%' : '30%')),
                                        backgroundColor: 'white', margin: '1%', padding: '1%', borderRadius: 20
                                    }} onClick={this.handleUserProfile(e)}>
                                        <table>
                                            <tr>
                                                <td style={{ width: '10%' }}>
                                                    <UserAvatar fileName={data[e].info.avatar}
                                                        size={90} />
                                                </td>
                                                <td style={{ width: '20%' }}>
                                                    <text style={{ fontWeight: 'bold' }}>{data[e].info.company}</text><br />
                                                    {
                                                        !data[e].info.companyIndustry ?
                                                            false
                                                            :
                                                            <div>
                                                                <Course />
                                                                <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.companyIndustry}</text><br />
                                                            </div>
                                                    }
                                                    {
                                                        !data[e].info.companyType ?
                                                            false
                                                            :
                                                            <div>
                                                                <Uni />
                                                                <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.companyType}</text><br />
                                                            </div>
                                                    }
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                )
                            }
                        }
                    }
                }
                this.setState({ displayComp: arrayData, allCompany: allCompany, searchResult: comps })
            }, this)
        })
    }

    componentWillMount() {
        this.props.loadPeople()
        this.renderStu('', '')
    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    handleCompIndChange = (event) => {
        this.setState({
            compIndustry: event.target.value,
        });
    };

    handleCompTypeChange = (event) => {
        this.setState({
            compType: event.target.value,
        });
    };

    handleSearch = () => {
        var uid = []
        var arrayData = []
        var compIndustry = this.state.compIndustry
        var a = this.state.allCompany
        var compType = this.state.compType
        var results = []
        if (compIndustry || compType) {
            results = this.state.allCompany.filter(function (e) {
                return (e.data.companyIndustry ? e.data.companyIndustry.toLowerCase().indexOf(compIndustry.toLowerCase()) !== -1 : 0)
                    && (e.data.companyType ? e.data.companyType.toLowerCase().indexOf(compType.toLowerCase()) !== -1 : 0)
            })
        }
        else {
            results = this.state.allCompany
        }
        results.forEach(function (e) {
            uid.push(e.uid)
        }, this)

        this.renderStu(uid)
    };

    render() {
        if (this.state.isDirect) {
            return <Redirect push to={`/companyinfo/${this.state.uid}`} />
        }

        return (
            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
            <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                    <table style={{ width: '100%' }}>
                        <tr>
                            <td style={{ width: '85%', display: 'flex', flexWrap: 'wrap' }}>
                                <div style={{ marginRight: '1%' }}>
                                    <TextField
                                        floatingLabelText="Company Industry"
                                        hintText="Company Industry"
                                        value={this.state.compIndustry}
                                        onChange={this.handleCompIndChange}
                                        underlineStyle={styles.underlineStyle}
                                    />
                                </div>
                                <div style={{ marginRight: '1%' }}>
                                    <TextField
                                        floatingLabelText="Company Type"
                                        hintText="Company Type"
                                        value={this.state.compType}
                                        onChange={this.handleCompTypeChange}
                                        underlineStyle={styles.underlineStyle}
                                    />
                                </div>
                            </td>
                            <td style={{ width: '15%' }}>
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <RaisedButton label="Search" primary={true} onClick={this.handleSearch} />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <hr style={{ width: '100%' }} />
                    <text style={{ width: '100%', paddingBottom: '1%', fontWeight: 'bold' }}>{this.state.searchResult.length} found</text>
                    {this.state.displayComp}
                </div>
            </div>
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loadPeople: () => dispatch(userActions.dbGetPeopleInfo()),
        getImage: (name) => dispatch(imageGalleryActions.dbDownloadImage(name))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(StuPplSearch)