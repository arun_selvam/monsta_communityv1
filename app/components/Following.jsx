// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Pagination from 'Pagination';
import { firebaseRef } from 'app/firebase/'
import { withRouter, Route, Switch, Redirect, NavLink } from 'react-router-dom'
import UserAvatar from 'UserAvatar'
import RaisedButton from 'material-ui/RaisedButton';
import '../styles/app.scss'

// - Import app components
import UserBoxList from 'UserBoxList'

// - Import API
import CircleAPI from 'CircleAPI'


// - Import actions

/**
* Create component class
 */
export class Following extends Component {

  static propTypes = {}

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props)

    //Defaul state
    this.state = {
      following: [],
      pageOfItems: [],
      uid: '',
      role: '',
      isDirect: false
    }

    // Binding functions to `this`
    this.onChangePage = this.onChangePage.bind(this);
  }

  componentWillMount() {
    var info = firebaseRef.child(`users`)//.child(Object.keys(this.props.peopleInfo)[0]).child('info')
    var userCircle = firebaseRef.child("userCircles").child(`${Object.keys(this.props.peopleInfo)[0]}/-Following/users/`)

    info.on('value', snapshot => {
      var allUserData = snapshot.val()
      var allUserKey = Object.keys(snapshot.val())

      userCircle.on('value', circleSanp => {
        var data = circleSanp.val()
        var following = []
        if (data) {
          var dataKey = Object.keys(data)
          dataKey.forEach(e => {
            following.push({
              uid: e,
              data: snapshot.val()[e].info
            })
          })
        }
        this.setState({ following: following })
      })
    })
  }

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }

  handleUserProfile = (uid, role) => () => {
    this.setState({ uid: uid, role: role, isDirect: true })
  }


  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {

    if (this.state.isDirect) {
      if (this.state.isDirect) {
        if (this.state.role === 'student') {
          return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }
        else {
          return <Redirect push to={`/companyinfo/${this.state.uid}`} />
        }
      }
    }

    return (
      <div style={{
        marginTop: '1%', borderRadius: 20,
        height: 'auto', padding: '2% 2% 2% 4%'
      }}>
        <text style={{ fontSize: 22, fontWeight: 'bold' }}>Following</text><br />
        <hr className="hr-text" />
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {this.state.pageOfItems.map(e =>
            <div className={'clickDiv'} style={{
              width: (window.innerWidth < 1000 ? '100%' : '32%'), backgroundColor: 'white',
              margin: '0.5%', padding: '1%', borderRadius: 20
            }}>
              <table>
                <tr>
                  <td style={{ width: '10%' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                    <UserAvatar fileName={e.data.avatar} size={90} />
                  </td>
                  <td style={{ width: '20%' }}>
                    <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                      <text style={{ fontWeight: 'bold' }} >{e.data.role === 'student' ? e.data.fullName : e.data.company}</text>
                    </div>
                    <br />
                    <div style={{ display: 'table', marginTop: 'auto', marginBottoq: 'auto', marginLeft: 'auto', marginRight: 'auto' }}>
                      <RaisedButton label="Message" primary={true} />
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          )}
        </div>
        <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
          <Pagination items={this.state.following} onChangePage={this.onChangePage} numOfItem={12} numOfPage={9} />
        </div>
      </div>
    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  return {

  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
  const { uid } = state.authorize
  const circles = state.circle ? state.circle.userCircles[uid] : {}
  return {
    //followers: circles ? (circles['-Followers'] ? circles['-Followers'].users || {} : {}) : {},
    peopleInfo: state.user.info,
  }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(Following)