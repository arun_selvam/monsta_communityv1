import React, { Component } from 'react';
import UserAvatar from 'UserAvatar'
import CoverAvatar from 'CoverAvatar'
import Call from 'react-icons/lib/md/call'
import Global from 'react-icons/lib/md/email'
import Location from 'react-icons/lib/md/place'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import Gear from 'react-icons/lib/go/gear'
import Square from 'react-icons/lib/fa/square'
import Modx from 'react-icons/lib/fa/modx'
import Radio from 'react-icons/lib/fa/dot-circle-o'
import Circle from 'react-icons/lib/fa/circle-o-notch'
import Quote from 'react-icons/lib/fa/quote-left'
import RaisedButton from 'material-ui/RaisedButton'
import Map from 'GoogleMap'
import { Redirect } from 'react-router-dom'
import Lock from 'react-icons/lib/md/lock'
import Pagination from 'Pagination';
import QuoteImg from 'QuoteImg'
import MapIcon from 'react-icons/lib/md/map'
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import TimePicker from 'material-ui/TimePicker';
import '../styles/div.scss'
//import InputMask from 'react-input-mask';  <InputMask mask="(0)999 999 99 99" maskChar=" " />

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
var companyInfo
var userCircles

class CompanyInfoView extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            companyInfo: '',
            role: '',
            endorse: '',
            voteCoreValues: false,
            voteFeedback: false,
            voteMeeting: false,
            voteReward: false,
            voteTeamWork: false,
            isEdit: false,
            displayQuotes: [],
            isCultureUnlock: false,
            credit: 0,
            cricleData: [],
            pageOfItems: [],
            pageOfCamp: [],
            uid: '',
            isDirect: false,
            campaignList: [],
            redirect: false,
            campaignKey: '',
            skill: [],
            myCampaignList: [],
            specialization: []
        }
        this.onChangeCamp = this.onChangeCamp.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        this.CampInfo = this.CampInfo.bind(this);
        this.genCampList = this.genCampList.bind(this)
    }

    genCampList = (campSelect) => () => {

        var dataSort = []
        var userID = this.props.selectedKey
        this.state.specialization.on('value', snapshotSpec => {
            this.state.myCampaignList.on('value', snapshot => {
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        if (data[e].startDate) {
                            if (new Date(data[e].startDate).toDateString() <= new Date().toDateString() && new Date(data[e].endDate).toDateString() >= new Date().toDateString()) {
                                if (data[e].typeOfCampaign) {
                                    if (campSelect === 'job') {
                                        if (data[e].typeOfCampaign == 1) {
                                            dataSort.push({
                                                compUid: userID,
                                                campUid: e,
                                                data: data[e],
                                                startDate: data[e].startDate
                                            })
                                        }
                                    }
                                    else if (campSelect === 'survey') {
                                        if (data[e].typeOfCampaign == 2) {
                                            dataSort.push({
                                                compUid: userID,
                                                campUid: e,
                                                data: data[e],
                                                startDate: data[e].startDate
                                            })
                                        }
                                    }
                                    else if (campSelect === 'event') {
                                        if (data[e].typeOfCampaign == 3) {
                                            dataSort.push({
                                                compUid: userID,
                                                campUid: e,
                                                data: data[e],
                                                startDate: data[e].startDate
                                            })
                                        }
                                    }
                                    else {
                                        dataSort.push({
                                            compUid: userID,
                                            campUid: e,
                                            data: data[e],
                                            startDate: data[e].startDate
                                        })
                                    }
                                }
                            }
                        } else {
                            if (data[e].typeOfCampaign) {
                                if (campSelect === 'job') {
                                    if (data[e].typeOfCampaign == 1) {
                                        dataSort.push({
                                            compUid: userID,
                                            campUid: e,
                                            data: data[e],
                                            startDate: new Date()
                                        })
                                    }
                                }
                                else if (campSelect === 'survey') {
                                    if (data[e].typeOfCampaign == 2) {
                                        dataSort.push({
                                            compUid: userID,
                                            campUid: e,
                                            data: data[e],
                                            startDate: new Date()
                                        })
                                    }
                                }
                                else if (campSelect === 'event') {
                                    if (data[e].typeOfCampaign == 3) {
                                        dataSort.push({
                                            compUid: userID,
                                            campUid: e,
                                            data: data[e],
                                            startDate: new Date()
                                        })
                                    }
                                }
                                else {
                                    dataSort.push({
                                        compUid: userID,
                                        campUid: e,
                                        data: data[e],
                                        startDate: new Date()
                                    })
                                }
                            }
                        }
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(b.startDate) - new Date(a.startDate);
                });

                dataSort.forEach(function (e, idx) {
                    var campaignPath = e.compUid + "/" + e.campUid;
                    var availableSlot = 0
                    e.data.campaignPath = campaignPath;

                    if (e.data.targetStudent) {
                        if (e.data.ParticipantList) {
                            var partKey = Object.keys(e.data.ParticipantList)
                            availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                        } else {
                            availableSlot = e.data.targetStudent
                        }
                    }
                    e.data.availableSlot = availableSlot
                }, this)

                this.setState({ campaignList: dataSort })
            })
        })
    }

    componentWillMount() {
        companyInfo = firebaseRef.child("users")
        userCircles = firebaseRef.child("userCircles").child(this.props.selectedKey)
        var cricleData = []
        companyInfo.on('value', snapshot => {
            var data = snapshot.val()

            if (data[this.props.selectedKey].endorse) {
                var cvData = data[this.props.selectedKey].endorse.coreValues
                if (cvData) {
                    var cvKeys = Object.keys(cvData)

                    cvKeys.forEach(function (e) {
                        if (cvData[e] === this.props.uid) {
                            this.setState({ voteCoreValues: true })
                        }
                    }, this)

                }

                var feedbackData = data[this.props.selectedKey].endorse.feedback
                if (feedbackData) {
                    var feedbackKeys = Object.keys(feedbackData)

                    feedbackKeys.forEach(function (e) {
                        if (feedbackData[e] === this.props.uid) {
                            this.setState({ voteFeedback: true })
                        }
                    }, this)
                }

                var meetingData = data[this.props.selectedKey].endorse.meeting
                if (meetingData) {
                    var meetingKeys = Object.keys(meetingData)

                    meetingKeys.forEach(function (e) {
                        if (meetingData[e] === this.props.uid) {
                            this.setState({ voteMeeting: true })
                        }
                    }, this)
                }

                var rewardData = data[this.props.selectedKey].endorse.reward
                if (rewardData) {
                    var rewardKeys = Object.keys(rewardData)

                    rewardKeys.forEach(function (e) {
                        if (rewardData[e] === this.props.uid) {
                            this.setState({ voteReward: true })
                        }
                    }, this)
                }

                var tmData = data[this.props.selectedKey].endorse.teamWork
                if (tmData) {
                    var tmKeys = Object.keys(tmData)

                    tmKeys.forEach(function (e) {
                        if (tmData[e] === this.props.uid) {
                            this.setState({ voteTeamWork: true })
                        }
                    }, this)
                }
            }

            var displayQuotes = []
            if (data[this.props.selectedKey].info.quotes) {
                var quotes = data[this.props.selectedKey].info.quotes
                quotes.map((quote, idx) => (
                    displayQuotes.push(
                        idx % 2 === 0 ?
                            <div style={{ width: '100%', display: 'flex' }}>
                                <div style={{ width: '48%', padding: '2%', backgroundColor: '#FFFFFF', borderRadius: 20, margin: '1%' }}>
                                    <Quote size={45} />
                                    <p style={{ wordWrap: 'break-word', fontSize: 20 }}>{quote.quote}</p>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <UserAvatar fileName={quote.avatar}
                                                        size={50} />
                                                </td>
                                                <td style={{ paddingLeft: '10%', paddingTop: '10%' }}>
                                                    <text style={{ fontWeight: 'bold', fontSize: 15 }}>{quote.name}</text><br />
                                                    <text style={{ fontSize: 12 }}>({quote.position})</text><br /><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div style={{ width: '50%', margin: '1%' }}>
                                    <QuoteImg fileName={quote.quoteImg} />
                                </div>
                            </div>
                            :
                            <div style={{ width: '100%', display: 'flex' }}>
                                <div style={{ width: '50%', margin: '1%' }}>
                                    <QuoteImg fileName={quote.quoteImg} />
                                </div>
                                <div style={{ width: '48%', padding: '2%', backgroundColor: '#FFFFFF', borderRadius: 20, margin: '1%' }}>
                                    <Quote size={45} />
                                    <p style={{ wordWrap: 'break-word', fontSize: 20 }}>{quote.quote}</p>
                                    <div>
                                        <table>
                                            <tr>
                                                <td>
                                                    <UserAvatar fileName={quote.avatar}
                                                        size={50} />
                                                </td>
                                                <td style={{ paddingLeft: '10%', paddingTop: '10%' }}>
                                                    <text style={{ fontWeight: 'bold', fontSize: 15 }}>{quote.name}</text><br />
                                                    <text style={{ fontSize: 12 }}>({quote.position})</text><br /><br />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                    )
                ))
                this.setState({ displayQuotes: displayQuotes })
            }
            this.setState({
                companyInfo: data[this.props.selectedKey].info, endorse: data[this.props.selectedKey].endorse,
                isCultureUnlock: data[this.props.selectedKey].isCultureUnlock ? data[this.props.selectedKey].isCultureUnlock : false,
                credit: data[this.props.selectedKey].credit, role: data[this.props.uid].info.role
            })

            userCircles.on('value', circleSnap => {
                if (circleSnap.val().circles) {
                    if (circleSnap.val().circles['-Followers']) {
                        if (circleSnap.val().circles['-Followers'].users) {
                            Object.keys(circleSnap.val().circles['-Followers'].users).forEach(e => {
                                cricleData.push({
                                    uid: e,
                                    data: data[e].info
                                })
                            })
                        }
                    }
                }
                this.setState({ cricleData: cricleData })
            })
        })

        var specialization = firebaseRef.child("staticData")
        var myCampaignList = firebaseRef.child(`campaigns/${this.props.selectedKey}`);
        var dataSort = []
        var userID = this.props.selectedKey
        specialization.on('value', snapshotSpec => {
            myCampaignList.on('value', snapshot => {
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        if (data[e].startDate) {
                            if (new Date(data[e].startDate).toDateString() <= new Date().toDateString() && new Date(data[e].endDate).toDateString() >= new Date().toDateString()) {
                                dataSort.push({
                                    compUid: userID,
                                    campUid: e,
                                    data: data[e],
                                    startDate: data[e].startDate
                                })
                            }
                        } else {
                            dataSort.push({
                                compUid: userID,
                                campUid: e,
                                data: data[e],
                                startDate: new Date()
                            })
                        }
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(b.startDate) - new Date(a.startDate);
                });

                dataSort.forEach(function (e, idx) {
                    var campaignPath = e.compUid + "/" + e.campUid;
                    var availableSlot = 0
                    e.data.campaignPath = campaignPath;

                    if (e.data.targetStudent) {
                        if (e.data.ParticipantList) {
                            var partKey = Object.keys(e.data.ParticipantList)
                            availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                        } else {
                            availableSlot = e.data.targetStudent
                        }
                    }
                    e.data.availableSlot = availableSlot
                }, this)
                this.setState({ campaignList: dataSort, skill: snapshotSpec.val().skills, myCampaignList: myCampaignList, specialization: specialization });
            })
        })
    }

    CampInfo = (campaignKey) => () => {
        this.setState({ redirect: true, campaignKey: campaignKey })
    }

    onChangeCamp(pageOfCamp) {
        // update state with new page of items
        this.setState({ pageOfCamp: pageOfCamp });
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    voteCoreValues = () => {
        if (this.state.role === 'student') {
            if (this.state.voteCoreValues === false) {
                var voteCoreValues = firebaseRef.child(`users/${this.props.selectedKey}/endorse/coreValues`)
                voteCoreValues.push(this.props.uid)
                var notify = {
                    description: `${this.props.fullName} has endorse for your core values`,
                    url: `/companyinfo/${this.props.selectedKey}`,
                    notifierUserId: this.props.uid,
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${this.props.selectedKey}`).push(notify)
                this.setState({ voteCoreValues: true })
            }
        }
    }

    voteReward = () => {
        if (this.state.role === 'student') {
            if (this.state.voteReward === false) {
                var voteReward = firebaseRef.child(`users/${this.props.selectedKey}/endorse/reward`)
                voteReward.push(this.props.uid)
                var notify = {
                    description: `${this.props.fullName} has endorse for your core Reward System`,
                    url: `/companyinfo/${this.props.selectedKey}`,
                    notifierUserId: this.props.uid,
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${this.props.selectedKey}`).push(notify)
                this.setState({ voteReward: true })
            }
        }
    }

    voteFeedback = () => {
        if (this.state.role === 'student') {
            if (this.state.voteFeedback === false) {
                var voteFeedback = firebaseRef.child(`users/${this.props.selectedKey}/endorse/feedback`)
                voteFeedback.push(this.props.uid)
                var notify = {
                    description: `${this.props.fullName} has endorse for your Feedback Practise`,
                    url: `/companyinfo/${this.props.selectedKey}`,
                    notifierUserId: this.props.uid,
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${this.props.selectedKey}`).push(notify)
                this.setState({ voteFeedback: true })
            }
        }
    }

    voteMeeting = () => {
        if (this.state.role === 'student') {
            if (this.state.voteMeeting === false) {
                var voteMeeting = firebaseRef.child(`users/${this.props.selectedKey}/endorse/meeting`)
                voteMeeting.push(this.props.uid)
                var notify = {
                    description: `${this.props.fullName} has endorse for your Meeting with Leaders`,
                    url: `/companyinfo/${this.props.selectedKey}`,
                    notifierUserId: this.props.uid,
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${this.props.selectedKey}`).push(notify)
                this.setState({ voteMeeting: true })
            }
        }
    }

    voteTeamWork = () => {
        if (this.state.role === 'student') {
            if (this.state.voteTeamWork === false) {
                var voteTeamWork = firebaseRef.child(`users/${this.props.selectedKey}/endorse/teamWork`)
                voteTeamWork.push(this.props.uid)
                var notify = {
                    description: `${this.props.fullName} has endorse for your Team Work & Fun Activities`,
                    url: `/companyinfo/${this.props.selectedKey}`,
                    notifierUserId: this.props.uid,
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${this.props.selectedKey}`).push(notify)
                this.setState({ voteTeamWork: true })
            }
        }
    }

    handleUnlock = () => () => {
        if (this.state.credit < 3000) {
            alert('Your current credit is not sufficient to unlock the culture page')
        }
        else {
            var confirmResult = confirm('Confirm to use 3000 credit to unlock the culture page? If yes, 3000 amount of credit will be deducted from the account')
            if (confirmResult) {
                var compInfo = firebaseRef.child(`users/${this.props.selectedKey}`)
                compInfo.update({
                    credit: this.state.credit - 3000,
                    isCultureUnlock: true
                })
                alert('Successfully unlock the culture page, now you are able to view the page')
            }
        }
    }

    handleEdit = () => () => {
        this.setState({ isEdit: true })
    }
    //                <img style={{width:'100px', height:'100px'}} src={'https://az616578.vo.msecnd.net/files/2016/08/12/636066249249209451-1974239933_college%20work.jpg'} />

    render() {
        var compLocation = this.state.companyInfo.address + this.state.companyInfo.area + this.state.companyInfo.postcode + ',' + this.state.companyInfo.state
        if (this.state.isEdit) {
            return <Redirect push to='/comppagebuilder' />
        }
        if (this.state.isDirect) {
            return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }
        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />
        }
        return (

            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                <div style={{ display: 'flex', flexWrap: 'wrap',justifyContent:'center',alignItems:'center', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                    {
                        this.props.selectedKey === Object.keys(this.props.peopleInfo)[0] ?
                            this.state.role === 'company' ?
                                <div style={{ width: '100%', textAlign: 'right', padding: '2%' }}>
                                    <RaisedButton label="Edit" primary={true} onClick={this.handleEdit()} />
                                </div>
                                :
                                false
                            :
                            false
                    }
                    {
                        this.state.isCultureUnlock ?
                            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                <div style={{
                                    margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '59%'),
                                    height: 'auto', position: 'relative'
                                }} >
                                    <div style={{ position: 'absolute', left: '5%', top: '250px' }}>
                                        <UserAvatar fileName={this.state.companyInfo.avatar}
                                            size={90} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                    </div>
                                    <CoverAvatar fileName={this.state.companyInfo.coverAvatar} height={'300px'} />
                                    <div style={{
                                        backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '7% 2% 2% 7%',
                                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                    }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 22 }}>{this.state.companyInfo.company}</text>
                                        <br />
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold' }}>About Us</text>
                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>{this.state.companyInfo.companyDesc}</p>
                                        </div>
                                    </div>
                                </div>
                                <div style={{
                                    margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '37%'),
                                    height: 'auto', position: 'relative'
                                }}>

                                    <div style={{ backgroundColor: '#FFFFFF', width: '100%', padding: '5%', borderRadius: 20 }}>
                                        <div style={{ overflow: 'hidden' }} >
                                            <Call size={20} style={{ color: 'black', float: 'left' }} />
                                            <text style={{ color: 'black', marginLeft: '12px' }}>{this.state.companyInfo.companyMobile}</text>
                                        </div>
                                        <br />
                                        <div style={{ overflow: 'hidden' }} >
                                            <Global size={20} style={{ color: 'black', float: 'left' }} />
                                            <text style={{ color: 'black', marginLeft: '12px' }}>{this.state.companyInfo.companySite}</text>
                                        </div>
                                        <br />
                                        <div style={{ overflow: 'hidden' }} >
                                            <Location size={20} style={{ color: 'black', float: 'left' }} />
                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                {this.state.companyInfo.address}, {this.state.companyInfo.area}, {this.state.companyInfo.postcode}, {this.state.companyInfo.state}
                                            </p>
                                        </div>
                                    </div>

                                    <div style={{ position: 'relative', marginTop: '1%', height: 300, overflow: 'hidden', borderRadius: 20 }}>
                                        <Map location={compLocation} />
                                    </div>
                                </div>

                                <div style={{
                                    margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '59%'),
                                    height: 'auto', position: 'relative'
                                }} >
                                    <div style={{ backgroundColor: '#FFFFFF', padding: '2% 1% 1% 5%', borderRadius: 20 }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 22 }}>Culture Enhancement Badges</text>
                                        <div style={{ overflowX: 'auto' }}>
                                            <table style={{ padding: '1%' }}>
                                                <tr>
                                                    <td className={'container'} onClick={() => this.voteCoreValues()}>
                                                        <div>
                                                            <div>
                                                                <text style={{ position: 'absolute', color: '#818078', paddingLeft: '1%', paddingRight: '1%', border: "solid 2px", borderRadius: 30, backgroundColor: 'white', right: '32%', top: '15px' }}>
                                                                    {this.state.endorse ?
                                                                        this.state.endorse.coreValues ?
                                                                            Object.keys(this.state.endorse.coreValues).length
                                                                            :
                                                                            0
                                                                        :
                                                                        0}
                                                                </text><br />
                                                            </div>
                                                            <div style={{ width: '50px', float: 'left' }}>
                                                                <div style={{
                                                                    width: '100px', height: '100px',
                                                                    borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2F05_icon_appreciation.png?alt=media&token=8b473127-6dd8-4d59-b91f-62dcf80613d4")`,
                                                                    WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                                                }}></div>
                                                            </div>
                                                            {
                                                                this.state.role === 'student' && !this.state.voteCoreValues ?
                                                                    <div className={'overlay'}>
                                                                        <div className={'text'}>Click to endrose</div>
                                                                    </div>
                                                                    : false
                                                            }
                                                            {/*<Gear size={(window.innerWidth < 1000 ? 50 : 85)} color={this.state.voteCoreValues ? 'green' : ''} />*/}
                                                        </div>
                                                    </td>
                                                    <td className={'container'} onClick={() => this.voteReward()}>
                                                        <div>
                                                            <text style={{ position: 'absolute', color: '#818078', paddingLeft: '1%', paddingRight: '1%', border: "solid 2px", borderRadius: 30, backgroundColor: 'white', right: '32%', top: '15px' }}>
                                                                {this.state.endorse ?
                                                                    this.state.endorse.reward ?
                                                                        Object.keys(this.state.endorse.reward).length
                                                                        :
                                                                        0
                                                                    :
                                                                    0}
                                                            </text><br />
                                                        </div>
                                                        <div style={{ width: '50px', float: 'left' }}>
                                                            <div style={{
                                                                width: '100px', height: '100px',
                                                                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2F04_icon_mentorship.png?alt=media&token=d18d4371-499e-4cb8-954e-02d1ef7f8a02")`,
                                                                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                                            }}></div>
                                                        </div>
                                                        {
                                                            this.state.role === 'student' && !this.state.voteReward ?
                                                                <div className={'overlay'}>
                                                                    <div className={'text'}>Click to endrose</div>
                                                                </div>
                                                                : false
                                                        }
                                                        {/*<Square size={(window.innerWidth < 1000 ? 50 : 85)} color={this.state.voteReward ? 'green' : ''} />*/}
                                                    </td>
                                                    <td className={'container'} onClick={() => this.voteFeedback()}>
                                                        <div>
                                                            <text style={{ position: 'absolute', color: '#818078', paddingLeft: '1%', paddingRight: '1%', border: "solid 2px", borderRadius: 30, backgroundColor: 'white', right: '32%', top: '15px' }}>
                                                                {this.state.endorse ?
                                                                    this.state.endorse.feedback ?
                                                                        Object.keys(this.state.endorse.feedback).length
                                                                        :
                                                                        0
                                                                    :
                                                                    0}
                                                            </text><br />
                                                        </div>
                                                        <div style={{ width: '50px', float: 'left' }}>
                                                            <div style={{
                                                                width: '100px', height: '100px',
                                                                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2F03_icon_environment.png?alt=media&token=e53385c5-487b-4186-b169-943ca99e0ea7")`,
                                                                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                                            }}></div>
                                                        </div>
                                                        {
                                                            this.state.role === 'student' && !this.state.voteFeedback ?
                                                                <div className={'overlay'}>
                                                                    <div className={'text'}>Click to endrose</div>
                                                                </div>
                                                                : false
                                                        }
                                                        {/*<Modx size={(window.innerWidth < 1000 ? 50 : 85)} color={this.state.voteFeedback ? 'green' : ''} />*/}
                                                    </td>
                                                    <td className={'container'} onClick={() => this.voteMeeting()}>
                                                        <div>
                                                            <text style={{ position: 'absolute', color: '#818078', paddingLeft: '1%', paddingRight: '1%', border: "solid 2px", borderRadius: 30, backgroundColor: 'white', right: '32%', top: '15px' }}>
                                                                {this.state.endorse ?
                                                                    this.state.endorse.meeting ?
                                                                        Object.keys(this.state.endorse.meeting).length
                                                                        :
                                                                        0
                                                                    :
                                                                    0}
                                                            </text><br />
                                                        </div>
                                                        <div style={{ width: '50px', float: 'left' }}>
                                                            <div style={{
                                                                width: '100px', height: '100px',
                                                                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2F02_icon_transparency.png?alt=media&token=8f1f9a15-1d42-483d-b4c6-c3a4a639f924")`,
                                                                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                                            }}></div>
                                                        </div>
                                                        {
                                                            this.state.role === 'student' && !this.state.voteMeeting ?
                                                                <div className={'overlay'}>
                                                                    <div className={'text'}>Click to endrose</div>
                                                                </div>
                                                                : false
                                                        }
                                                        {/*<Circle size={(window.innerWidth < 1000 ? 50 : 85)} color={this.state.voteMeeting ? 'green' : ''} />*/}
                                                    </td>
                                                    <td className={'container'} onClick={() => this.voteTeamWork()}>
                                                        <div>
                                                            <text style={{ position: 'absolute', color: '#818078', paddingLeft: '1%', paddingRight: '1%', border: "solid 2px", borderRadius: 30, backgroundColor: 'white', right: '32%', top: '15px' }}>
                                                                {this.state.endorse ?
                                                                    this.state.endorse.teamWork ?
                                                                        Object.keys(this.state.endorse.teamWork).length
                                                                        :
                                                                        0
                                                                    :
                                                                    0}
                                                            </text><br />
                                                        </div>
                                                        <div style={{ width: '50px', float: 'left' }}>
                                                            <div style={{
                                                                width: '100px', height: '100px',
                                                                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2F01_icon_teamwork.png?alt=media&token=ad8c90a5-e7bb-4acb-af0c-d3c0600db78b")`,
                                                                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                                            }}></div>
                                                        </div>
                                                        {
                                                            this.state.role === 'student' && !this.state.voteTeamWork ?
                                                                <div className={'overlay'}>
                                                                    <div className={'text'}>Click to endrose</div>
                                                                </div>
                                                                : false
                                                        }
                                                        {/*<Radio size={(window.innerWidth < 1000 ? 50 : 85)} color={this.state.voteTeamWork ? 'green' : ''} />*/}
                                                    </td>
                                                </tr>
                                                <tr style={{ textAlign: 'center' }}>
                                                    <td onClick={() => this.voteCoreValues()}><text style={{ fontWeight: 'bold', color: this.state.voteCoreValues ? 'green' : '', fontSize: 13 }}>Core Values</text></td>
                                                    <td onClick={() => this.voteReward()}><text style={{ fontWeight: 'bold', color: this.state.voteReward ? 'green' : '', fontSize: 13 }} >Reward System</text></td>
                                                    <td onClick={() => this.voteFeedback()}><text style={{ fontWeight: 'bold', color: this.state.voteFeedback ? 'green' : '', fontSize: 13 }} >Feedback Practise</text></td>
                                                    <td onClick={() => this.voteMeeting()}><text style={{ fontWeight: 'bold', color: this.state.voteMeeting ? 'green' : '', fontSize: 13 }} >Meeting with Leaders</text></td>
                                                    <td onClick={() => this.voteTeamWork()}><text style={{ fontWeight: 'bold', color: this.state.voteTeamWork ? 'green' : '', fontSize: 13 }} >{'Team Work & Fun Activities'}</text></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>

                                    <div style={{ backgroundColor: '#FFFFFF', padding: '2% 1% 2% 7%', marginTop: '1%', borderRadius: 20 }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 22 }}>Office Culture</text>
                                        {
                                            this.state.companyInfo.officeCulture ?
                                                <p style={{ wordWrap: 'break-word', fontSize: 13 }}>{this.state.companyInfo.officeCulture}</p>
                                                :
                                                false
                                        }
                                    </div>
                                </div>

                                <div style={{
                                    margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '37%'),
                                    height: 'auto', backgroundColor: '#FFFFFF', borderRadius: 20, padding: '1% 1% 1% 2%'
                                }}>
                                    <div style={{ paddingBottom: '5%' }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 22 }}>Who Has Joined</text>
                                    </div>
                                    {this.state.pageOfItems.map(e =>
                                        <div style={{ padding: '1%', position: 'relative', display: 'flex' }} onClick={this.handleUserProfile(e.uid)}>
                                            <div>
                                                <UserAvatar fileName={e.data.avatar}
                                                    size={60} />
                                            </div>
                                            <div style={{ paddingLeft: '5%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.fullName}</text><br />
                                                <text style={{ fontSize: 13 }}>{e.data.course}</text>
                                            </div>
                                            <div style={{ position: 'absolute', right: '1px', top: '5px' }}>
                                                <RaisedButton label="Message" primary={true} />
                                            </div>
                                        </div>
                                    )}
                                    <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                        <Pagination items={this.state.cricleData} onChangePage={this.onChangePage} numOfItem={5} numOfPage={1} />
                                    </div>
                                </div>
                                {this.state.displayQuotes}
                                {
                                    this.state.role === 'student' ?
                                        <div style={{ width: '100%', padding: '1% 1% 1% 5%' }}>
                                            <div style={{ textAlign: 'center' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 22 }}>Available Career &amp; Opportunities</text><br /><br />
                                                <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                                                    <div style={{ margin: '1%' }}>
                                                        <RaisedButton backgroundColor='#F5B041' label="All" onClick={this.genCampList('all')} />
                                                    </div>
                                                    <div style={{ margin: '1%' }}>
                                                        <RaisedButton backgroundColor='#F5B041' label="Online Events" onClick={this.genCampList('survey')} />
                                                    </div>
                                                    <div style={{ margin: '1%' }}>
                                                        <RaisedButton backgroundColor='#F5B041' label="Event" onClick={this.genCampList('event')} />
                                                    </div>
                                                    <div style={{ margin: '1%' }}>
                                                        <RaisedButton backgroundColor='#F5B041' label="Career &amp; Opportunities" onClick={this.genCampList('job')} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                {
                                                    this.state.campaignList.length ?
                                                        this.state.pageOfCamp.map(e => (
                                                            <div style={{
                                                                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                                                                height: 'auto', position: 'relative'
                                                            }} onClick={this.CampInfo(e.data.campaignPath)}>
                                                                <div className={'clickDiv'}>
                                                                    <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                                                                        <UserAvatar fileName={e.data.companyAvatar}
                                                                            size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                                                    </div>
                                                                    <div style={{
                                                                        position: 'absolute', right: '5%', top: '210px',
                                                                        backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                                                                    }}>
                                                                        <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                                                                    </div>
                                                                    <div style={{
                                                                        position: 'absolute', left: '40%', top: '210px', padding: '1%'
                                                                    }}>
                                                                        {
                                                                            e.data.targetStudent ?
                                                                                e.data.availableSlot === 0 ?
                                                                                    <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                                                                                    :
                                                                                    <text style={{ fontSize: '15', color: 'green' }}>{`${e.data.availableSlot}`}&nbsp;slot&nbsp;available</text>
                                                                                :
                                                                                <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                                                                        }
                                                                    </div>
                                                                    <CoverAvatar fileName={e.data.campAvatar} height={'200px'} />
                                                                    <div style={{
                                                                        backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                                                                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                                                    }}>
                                                                        <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                                                                        <br />
                                                                        <div style={{ marginTop: '2%', height: '90px' }}>
                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                                                                {
                                                                                    e.data.description.length > 200 ?
                                                                                        e.data.description.substring(0, 200) + '...'
                                                                                        :
                                                                                        e.data.description
                                                                                }
                                                                            </p>
                                                                        </div>
                                                                        <br />
                                                                        {e.data.typeOfCampaign === 1 ?
                                                                            <div>
                                                                                <text style={{ fontWeight: 'bold', fontSize: 13 }}>Alloance : </text><br />
                                                                                <text style={{ fontSize: 13 }}>RM {e.data.minPayout} - RM {e.data.maxPayout}</text>
                                                                            </div>
                                                                            :
                                                                            e.data.typeOfCampaign === 2 ?
                                                                                <div>
                                                                                    <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                                                                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                                                        {
                                                                                            e.data.skillCanLearn.length > 0 ?
                                                                                                e.data.skillCanLearn.map((skill, idx) => (
                                                                                                    idx < 4 ?
                                                                                                        <div style={{
                                                                                                            backgroundColor: '#CACFD2', padding: '1%', width: `${this.state.skill[skill].length * 8 + 20}px`,
                                                                                                            margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                                                                        }}>
                                                                                                            <text style={{ fontSize: 13 }}>{this.state.skill[skill]}</text>
                                                                                                        </div>
                                                                                                        : false
                                                                                                ))
                                                                                                : false
                                                                                        }
                                                                                    </div>
                                                                                </div>
                                                                                :
                                                                                e.data.typeOfCampaign === 3 ?
                                                                                    <div>
                                                                                        <table>
                                                                                            <tr>
                                                                                                <td><Calender /></td>
                                                                                                <td>
                                                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventDate}</text>
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><Time /></td>
                                                                                                <td>
                                                                                                    <TimePicker
                                                                                                        style={{ fontSize: 13, paddingLeft: '1%' }}
                                                                                                        disabled={true}
                                                                                                        format="ampm"
                                                                                                        value={new Date(e.data.eventTime)}
                                                                                                    />
                                                                                                </td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td><MapIcon /></td>
                                                                                                <td>
                                                                                                    {
                                                                                                        e.data.eventAdd2 ?
                                                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                                                {e.data.eventAdd1}, {e.data.eventAdd2}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                                                            </p>
                                                                                                            :
                                                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                                                {e.data.eventAdd1}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                                                            </p>
                                                                                                    }
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </div>
                                                                                    : false
                                                                        }
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        ))
                                                        : false
                                                }
                                            </div>
                                            {
                                                this.state.campaignList.length ?
                                                    <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                        <Pagination items={this.state.campaignList} onChangePage={this.onChangeCamp} numOfItem={6} numOfPage={9} />
                                                    </div>
                                                    : false
                                            }
                                        </div>
                                        :
                                        false
                                }
                            </div>
                            :
                            this.props.selectedKey === Object.keys(this.props.peopleInfo)[0] ?
                                this.state.role === 'company' ?
                                    <div style={{ textAlign: 'center', paddingTop: '10%' }}>
                                        <Lock size={250} color={'black'} /><br />
                                        <text>Culture Page is lock now. You have to use 3000 credit to unlock the page to view and edit the information</text><br /><br />
                                        <RaisedButton label="Unlock" primary={true} onClick={this.handleUnlock()} />
                                    </div>
                                    :
                                    <div style={{ textAlign: 'center', paddingTop: '10%' }}>
                                        <text>Company Culture  page is not available for viewing</text>
                                    </div>
                                :
                                <div style={{ textAlign: 'center', paddingTop: '10%' }}>
                                    <text>Company Culture  page is not available for viewing</text>
                                </div>
                    }
                </div >
            </div >

        );
    }
}

/**
 * Map dispatch to props
* @param  {func} dispatch is the function to dispatch action to reducers
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
        */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
* @param  {object} state is the obeject from redux store
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
*/
const mapStateToProps = (state, ownProps) => {
    return {
        uid: state.authorize.uid,
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList,
        fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompanyInfoView)