import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import { Table, Row, Cell } from 'react-responsive-table';
import { ChatFeed,ChatBubble, Message } from 'react-chat-ui'
import Avatar from 'material-ui/Avatar';
import FileFolder from 'material-ui/svg-icons/file/folder';
import FontIcon from 'material-ui/FontIcon';
import List from 'material-ui/List/List';
import ListItem from 'material-ui/List/ListItem';

const styles = {
    button: {
      backgroundColor: '#fff',
      borderColor: '#1D2129',
      borderStyle: 'solid',
      borderRadius: 20,
      borderWidth: 2,
      color: '#1D2129',
      fontSize: 18,
      fontWeight: '300',
      paddingTop: 8,
      paddingBottom: 8,
      paddingLeft: 16,
      paddingRight: 16,
    },
    selected: {
      color: '#fff',
      backgroundColor: '#0084FF',
      borderColor: '#0084FF',
    },
    body :{
        margin: 0,
      },
      container :{
        width: 850,
        margin: 0,
        marginTop: 100,
      },
      install: {
        margin:  0
      }
  };
 
  
  const users = {
    0: 'You',
    1: 'Mark',
    2: 'Evan',
  };
  
  const customBubble = props => (
    <div>
      <p>{`${props.message.senderName} ${props.message.id ? 'says' : 'said'}: ${
        props.message.message
      }`}</p>
    </div>
  );

class Messenger extends Component {

    constructor() {
        super();
        this.state = {
          messages: [
            new Message({ id: 1, message: 'Hi!', senderName: 'Arun' }),
            new Message({
              id: 0,
              message: 'Hi Arun',
              senderName: 'You',
            }),
          ],
          useCustomBubble: true,
          curr_user: 0,
        };
      }
    
      onPress(user) {
        this.setState({ curr_user: user });
      }
    
      onMessageSubmit(e) {
        const input = this.message;
        e.preventDefault();
        if (!input.value) {
          return false;
        }
        this.pushMessage(this.state.curr_user, input.value);
        input.value = '';
        return true;
      }
    
      pushMessage(recipient, message) {
        const prevState = this.state;
        const newMessage = new Message({
          id: recipient,
          message,
          senderName: users[recipient],
        });
        prevState.messages.push(newMessage);
        this.setState(this.state);
      }

      componentWillMount(){
        this.setState({ useCustomBubble: true })                
      }
    
      render() {
        return (
          <div className="container">
           <div style={{width:'30%' ,float:'left'}}>
           <ListItem
      disabled={true}
      leftAvatar={
        <Avatar icon={<FontIcon className="muidocs-icon-communication-voicemail" />} />
      }
    >
      Arun
    </ListItem>
            </div>
            <div className="chatfeed-wrapper" style={{width:'70%' ,float:'right'}}>
              <ChatFeed
                chatBubble={this.state.useCustomBubble && customBubble}
                maxHeight={250}
                messages={this.state.messages} // Boolean: list of message objects
                showSenderName
              />
    
              <form onSubmit={e => this.onMessageSubmit(e)}>
                <input
                  ref={m => {
                    this.message = m;
                  }}
                  placeholder="Type a message..."
                  className="message-input"
                style={{width:'90%', alignSelf:'center'}}/>
              </form>
    
       {/*        <div style={{ display: 'flex', justifyContent: 'space-around' }}>
                <button
                  style={{
                    ...styles.button,
                    ...(this.state.curr_user === 0 ? styles.selected : {}),
                  }}
                  onClick={() => this.onPress(0)}
                >
                  You
                </button>
                <button
                  style={{
                    ...styles.button,
                    ...(this.state.curr_user === 1 ? styles.selected : {}),
                  }}
                  onClick={() => this.onPress(1)}
                >
                  Arun
                </button>
                <button
                  style={{
                    ...styles.button,
                    ...(this.state.curr_user === 2 ? styles.selected : {}),
                  }}
                  onClick={() => this.onPress(2)}
                >
                  Max
                </button>
              </div> */}
              <div
                style={{ display: 'flex', justifyContent: 'center', marginTop: 10 }}
              >
                <button
                  style={{
                    ...styles.button,
                    ...(true? styles.selected : {}),
                  }}
                  onClick={() =>
                    this.setState({ useCustomBubble: !this.state.useCustomBubble })
                  }
                >
                  Change Css
                </button>
              </div>
            </div>
         
          </div>
        );
      }
    }

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(Messenger)