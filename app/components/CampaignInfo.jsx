import React, { Component } from 'react'
import Avatar from 'material-ui/Avatar'
import RaisedButton from 'material-ui/RaisedButton'
import { connect } from 'react-redux'
import '../styles/app.scss'
import TextField from 'material-ui/TextField'
import YouTube from 'react-youtube';
import TimePicker from 'material-ui/TimePicker';
import Profile from 'Profile'
import { Redirect } from 'react-router-dom'
import Map from 'GoogleMap'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import renderHTML from 'react-render-html';
import { firebaseAuth, firebaseRef, firebaseApp } from 'app/firebase/'
import sha1 from 'js-sha1'
import moment from 'moment'
import UserAvatar from 'UserAvatar'
import CoverAvatar from 'CoverAvatar'
import MapIcon from 'react-icons/lib/md/map'
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import '../styles/div.scss'

import {
    ShareButtons,
    generateShareIcon
} from 'react-share';

class CampaignInfo extends Component {

    constructor(props) {
        super(props)
        this.state = {
            monstaNetwork: [],
            redirect: false,
            avatarSize: window.innerWidth > 1000 ? window.innerWidth / 15 : (window.innerWidth > 700 ? window.innerWidth / 10 : window.innerWidth / 7),
            xpFontSize: window.innerWidth > 1000 ? 30 : 25,
            statusSize: window.innerWidth > 1000 ? 25 : 20,
            xpTitle: window.innerWidth > 1000 ? 25 : 15,
            width: window.innerWidth > 1000 ? '100%' : (window.innerWidth > 700 ? (window.innerWidth + 100) + 'px' : (window.innerWidth + 250) + 'px'),
            campaignList: [],
            specializationList: [],
            skillList: [],
            coursesList: [],
            skills: [],
            requiredCourses: [],
            requiredSkills: [],
            workLocation: [],
            oriQues: [],
            questionList: [],
            opts: {
                height: window.innerHeight / 2,
                width: '100%',
                playerVars: {
                    autoplay: 1
                }
            },
            userList: [],
            participantList: [],
            isDirect: false,
            uid: '',
            questionView: '',
            showDialog: false,
            availableSlot: '',
            isApply: false,
            interestedCamp: [],
            campaignPath: '',
            isEnroll: false,
            showCheckoutModal: false,
            firebaseQueyString: '',
            returnUrl: '',
            userInfo: [],
            companyDesc: '',
            submitLink: '',
            isCompany: false
        }
    }

    componentWillMount() {
        var check = firebaseApp.auth().currentUser
        firebaseApp.database().ref().child(`users/${check.uid}/info`).once('value').then((snapshot) => {
            this.state.userInfo = snapshot.val()
        })

        var campaignList = firebaseRef.child("campaigns").child(this.props.companyKey).child(this.props.userKey)
        var specialization = firebaseRef.child("staticData")
        var isApply = false
        var typeOfCampaign
        var requiredCourses = []
        var skill = []
        var requiredSkills = []
        var availableSlot = ''
        var questionList = []

        specialization.on('value', snapshotSpec => {
            campaignList.on('value', snapshot => {
                if (snapshot.val().skillCanLearn) {
                    snapshot.val().skillCanLearn.map((e) => (
                        skill.push(snapshotSpec.val().skills[e])
                    ))
                }

                if (snapshot.val().requiredCourses) {
                    if (snapshot.val().requiredCourses != '') {
                        snapshot.val().requiredCourses.map((e) => (
                            requiredCourses.push(snapshotSpec.val().courses[e])
                        ))
                    }
                }

                if (snapshot.val().requiredSkills) {
                    if (snapshot.val().requiredSkills != '') {
                        snapshot.val().requiredSkills.map((e) => (
                            requiredSkills.push(snapshotSpec.val().skills[e])
                        ))
                    }
                }

                typeOfCampaign = snapshot.val().typeOfCampaign
                if (snapshot.val().ParticipantList) {
                    if (snapshot.val().ParticipantList[Object.keys(this.props.peopleInfo)[0]]) {
                        isApply = true
                    }
                }

                if (snapshot.val().targetStudent) {
                    if (snapshot.val().ParticipantList) {
                        availableSlot = snapshot.val().targetStudent - Object.keys(snapshot.val().ParticipantList).length
                    } else {
                        availableSlot = snapshot.val().targetStudent
                    }
                }

                if (snapshot.val().questionList) {
                    if (snapshot.val().questionList != '') {
                        snapshot.val().questionList.forEach(e => {
                            if (e.answerList) {
                                e.answerList.forEach(ans => {
                                    ans.isAnswer = false
                                })
                            }
                            questionList.push(e)
                        })
                    }
                }

                this.setState({
                    campaignList: snapshot.val(), isApply: isApply, oriQues: snapshot.val().questionList, questionList: questionList,
                    skills: skill, requiredCourses: requiredCourses, requiredSkills: requiredSkills,
                    availableSlot: availableSlot
                })
            })
        })

        /* specialization.on('value', snapshot => {
             var data = snapshot.val()
             var dataKey = Object.keys(data.field)
             this.setState({ specializationList: dataKey, skillList: data.skills, coursesList: data.courses })
         })*/

        var user = firebaseRef.child("users")
        user.on('value', snapshot => {
            this.setState({ userList: snapshot.val(), companyDesc: snapshot.val()[this.props.companyKey].info.companyDesc })
        })
    }

    handleComp = () => () => {
        this.setState({ isCompany: true })
    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    handleClose = () => {
        this.setState({ showDialog: false });
    };

    handleCloseCheckoutModal = () => {
        this.setState({ showCheckoutModal: false });
    };

    applyCamp = () => {
        var totalQues = 0
        var isAns = 0
        var fullName = this.props.fullName
        var campTit = this.state.campaignList.campaignTitle
        var compUid = this.props.companyKey
        var stuUid = Object.keys(this.props.peopleInfo)[0]
        var subKey = this.props.userKey

        if (this.state.campaignList.aptitudeTest) {
            if (this.state.questionList.length > 0) {
                this.state.questionList.forEach((e, idx) => {
                    if (e.selectedGroup === 'onlyOne') {
                        if (e.answerList) {
                            totalQues++
                            e.answerList.forEach(ans => {
                                if (ans.isAnswer === true) {
                                    isAns++
                                }
                            })
                        }
                    }
                    else if (e.selectedGroup === 'multiSelect') {
                        var noAns = 0
                        if (e.answerList) {
                            totalQues++
                            e.answerList.forEach(ans => {
                                if (ans.isAnswer === true) {
                                    noAns++
                                }
                            })
                            if (noAns > 0) {
                                isAns++
                            }
                        }
                    }
                    else if (e.selectedGroup === 'ratingScale') {
                        if (e.answerList) {
                            e.answerList.forEach(ans => {
                                totalQues++
                                if (ans.isAnswer) {
                                    isAns++
                                }
                            })
                        }
                    }
                })

                var correctAns = 0
                var totCorrectAns = 0
                if (this.state.oriQues) {
                    if (this.state.oriQues.length > 0) {
                        this.state.oriQues.forEach(function (e, sidx) {
                            if (e.selectedGroup === 'multiSelect') {
                                e.answerList.forEach(function (ans, ansIdx) {
                                    if (ans.isAnswer && this.state.questionList[sidx].answerList[ansIdx].isAnswer) {
                                        correctAns += 1
                                        totCorrectAns += 1
                                    } else {
                                        correctAns -= 1
                                    }
                                }, this)
                            } else {
                                e.answerList.forEach(function (ans, ansIdx) {
                                    if (ans.isAnswer) {
                                        if (this.state.questionList[sidx].answerList[ansIdx].isAnswer) {
                                            correctAns += 1
                                        }
                                        totCorrectAns += 1
                                    }
                                }, this)
                            }
                            //userAnswer[sidx]
                        }, this)
                    }
                }
            }
        }
        else if (this.state.campaignList.typeOfCampaign === 2) {
            if (this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2) {
                this.state.questionList.forEach(e => {
                    if (e.selectedGroup === 'onlyOne' || e.selectedGroup === 'multiSelect' || e.selectedGroup === 'ratingScale') {
                        console.log(e)
                    }
                })
            }
        }

        if (totalQues !== isAns) {
            this.setState({ showDialog: true })
        } else {
            var participantData = firebaseRef.child('campaigns/' + this.props.companyKey + '/' + this.props.userKey + '/ParticipantList/' + Object.keys(this.props.peopleInfo)[0]);
            var addXP = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}/info`)
            var monstaXP = 0
            var skills = []

            if (this.state.campaignList.typeOfCampaign === 1) {
                if (this.state.campaignList.aptitudeTest) {
                    participantData.update({
                        dateApply: new Date().toString(),
                        totalScore: correctAns <= 0 ? 0 : ((correctAns / totCorrectAns) * 100),
                        userAnswer: this.state.questionList
                    });
                } else {
                    participantData.update({
                        dateApply: new Date().toString(),
                        username: this.props.peopleInfo[Object.keys(this.props.peopleInfo)[0]].fullName
                    });
                }
            }
            else if (this.state.campaignList.typeOfCampaign === 2) {
                if (this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2) {
                    if (this.state.questionList.length > 0) {
                        participantData.update({
                            dateApply: new Date().toString(),
                            userAnswer: this.state.questionList
                        });
                    }
                    else {
                        participantData.update({
                            dateApply: new Date().toString(),
                            username: this.props.peopleInfo[Object.keys(this.props.peopleInfo)[0]].fullName
                        });
                    }

                    addXP.on('value', snapshot => {
                        snapshot.val().monstaXP ?
                            monstaXP = snapshot.val().monstaXP
                            :
                            0

                        if (snapshot.val().skills) {
                            skills = snapshot.val().skills
                            this.state.campaignList.skillCanLearn.forEach(e => {
                                if (!snapshot.val().skills.includes(e)) {
                                    skills.push(e)
                                }
                            })
                        } else {
                            skills = this.state.campaignList.skillCanLearn
                        }
                    })

                    addXP.update({
                        skills: skills,
                        monstaXP: monstaXP + this.state.campaignList.monstaXP
                    })

                } else {
                    if (this.state.campaignList.submitMethod === 'submitLink') {
                        if (this.state.submitLink) {
                            participantData.update({
                                dateApply: new Date().toString(),
                                submitLink: this.state.submitLink,
                                username: this.props.peopleInfo[Object.keys(this.props.peopleInfo)[0]].fullName
                            });

                            addXP.on('value', snapshot => {
                                snapshot.val().monstaXP ?
                                    monstaXP = snapshot.val().monstaXP
                                    :
                                    0

                                if (snapshot.val().skills) {
                                    skills = snapshot.val().skills
                                    this.state.campaignList.skillCanLearn.forEach(e => {
                                        if (!snapshot.val().skills.includes(e)) {
                                            skills.push(e)
                                        }
                                    })
                                } else {
                                    skills = this.state.campaignList.skillCanLearn
                                }
                            })

                            addXP.update({
                                skills: skills,
                                monstaXP: monstaXP + this.state.campaignList.monstaXP
                            })
                        }
                        else {
                            alert('Submit link cannot be empty')
                        }
                    }
                    else {
                        participantData.update({
                            dateApply: new Date().toString(),
                            username: this.props.peopleInfo[Object.keys(this.props.peopleInfo)[0]].fullName
                        });
                    }
                }
            } else {
                if (!this.state.campaignList.isTicket) {
                    participantData.update({
                        dateApply: new Date().toString(),
                        username: this.props.peopleInfo[Object.keys(this.props.peopleInfo)[0]].fullName
                    });
                } else {
                    this.setState({ showCheckoutModal: true })
                }

                if (this.state.campaignList.typeOfSubCampaign === 2) {
                    addXP.on('value', snapshot => {
                        snapshot.val().monstaXP ?
                            monstaXP = snapshot.val().monstaXP
                            :
                            0

                        if (snapshot.val().skills) {
                            skills = snapshot.val().skills
                            this.state.campaignList.skillCanLearn.forEach(e => {
                                if (!snapshot.val().skills.includes(e)) {
                                    skills.push(e)
                                }
                            })
                        } else {
                            skills = this.state.campaignList.skillCanLearn
                        }
                    })

                    addXP.update({
                        skills: skills,
                        monstaXP: monstaXP + this.state.campaignList.monstaXP
                    })
                }
            }

            var notify = {
                description: `${fullName} has apply for ${campTit}`,
                url: `/companymycampaigninfo/${subKey}`,
                notifierUserId: stuUid,
                isSeen: false,
                date: new Date().toString()
            }
            var notifyRef = firebaseRef.child(`userNotifies/${compUid}`).push(notify)
        }
    }

    handleLinkChange = (event) => {
        this.setState({
            submitLink: event.target.value
        });
    };

    handleIsAnswer = (idx, ansIdx) => (event) => {

        const isAnswer = this.state.questionList
        var isAnswerData = isAnswer[idx].answerList
        if (isAnswer[idx].selectedGroup === "multiSelect") {
            isAnswerData[ansIdx].isAnswer = !isAnswerData[ansIdx].isAnswer
        }
        else if (isAnswer[idx].selectedGroup === 'textboxMultiLine' || isAnswer[idx].selectedGroup === 'textboxSingleLine') {
            console.log(event.target.value)
            isAnswer[idx].isAnswer = event.target.value
        }
        else if (isAnswer[idx].selectedGroup === 'ratingScale') {
            isAnswerData[ansIdx].isAnswer = event.target.value
        }
        else {
            isAnswerData.map((ans, sidx) => {
                if (ansIdx !== sidx) {
                    ans.isAnswer = false
                }
                else {
                    ans.isAnswer = true
                }
            });
        }
        this.setState({ userAnswer: isAnswer, questionList: isAnswer });
        this.setState({ submitBtn: true });

    }

    CampInfo = (campaignPath) => () => {
        this.setState({ redirect: true, campaignPath: campaignPath })
    }

    render() {
        var date = moment
        var merchantKey = "6iVOtqXMMh"
        var merchantCode = "M11114"
        var RefNo = moment().unix().toString()
        var amount = "1.00"
        var rawSign = merchantKey + merchantCode + RefNo + "100" + "MYR"
        const hex2bin = str => str.match(/.{1,2}/g).reduce((str, hex) => str += String.fromCharCode(parseInt(hex, 16)), '');
        var signature = btoa(hex2bin(sha1(rawSign)));
        //var signature = 'nbcIL4eKDHgHce/XyJ3AwCt3rHY='
        //        sha1( merchantKey + merchantCode + RefNo + "1" + "MYR")
        var remark = firebaseApp.auth().currentUser.uid + "," + this.state.campaignList.ticketPrice + ',' + window.location.pathname
        var formHtml = `<HTML><BODY><FORM method="post" name="ePayment" id="paymentForm" action="https://www.mobile88.com/ePayment/entry.asp"><INPUT type="hidden" name="MerchantCode" value=${merchantCode}><INPUT type="hidden" name="PaymentId" value=""><INPUT type="hidden" name="RefNo" value=${RefNo}><INPUT type="hidden" name="Amount" value=${amount}><INPUT type="hidden" name="Currency" value="MYR"><INPUT type="hidden" name="ProdDesc" value="Monsta Credit"><INPUT type="hidden" name="UserName" value=${this.state.userInfo.fullName}><INPUT type="hidden" name="UserEmail" value="${this.state.userInfo.email}"><INPUT type="hidden" name="UserContact" value=${this.state.userInfo.mobileNumber}><INPUT type="hidden" name="Remark" value=${remark}><INPUT type="hidden" name="Lang" value="UTF-8"><INPUT type="hidden" name="Signature" value=${signature}><INPUT type="hidden" name="ResponseURL" value="http://139.162.40.8:8080/api/studentpayment"><INPUT type="hidden" name="BackendURL"value="http://www.YourBackendURL.com/payment/backend_response.asp"><INPUT type="submit" value="Proceed with Payment" name="Submit"></FORM><script>document.getElementById("paymentForm").submit();</script></BODY></HTML><script>`

        const actions = [
            <FlatButton
                label="Ok"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleClose}
            />
        ];

        const actions2 = [
            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleCloseCheckoutModal}
            />,
            <div>{ReactHtmlParser(formHtml)}</div>
        ];

        const shareUrl = `http://monsta.appbros.com.my/Refferal/${Object.keys(this.props.peopleInfo)[0]}`
        const title = 'Monsta Registration';
        const {
            FacebookShareButton,
            GooglePlusShareButton,
            LinkedinShareButton,
            TwitterShareButton,
            TelegramShareButton,
            WhatsappShareButton,
            PinterestShareButton,
            VKShareButton,
            OKShareButton,
            RedditShareButton,
            TumblrShareButton,
            EmailShareButton,
        } = ShareButtons;

        const FacebookIcon = generateShareIcon('facebook');

        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignPath}`} />
        }

        if (this.state.isCompany) {
            return <Redirect push to={`/companyinfo/${this.props.companyKey}`} />
        }

        var eventLoc = this.state.campaignList.eventAdd1 + this.state.campaignList.eventAdd2 + this.state.campaignList.eventZip + ',' + this.state.campaignList.eventCity + this.state.campaignList.eventState
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                <Dialog
                    actions={actions2}
                    modal={true}
                    open={this.state.showCheckoutModal}
                    onRequestClose={this.handleCloseCheckoutModal}
                    autoScrollBodyContent={true}
                >
                    This Event Requires a payment of RM {this.state.campaignList.ticketPrice}
                    click the checkout Button to proceed to checkout
                </Dialog>
                <Dialog
                    actions={actions}
                    modal={true}
                    open={this.state.showDialog}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >
                    Please answer all the question , then only proceed with apply
                </Dialog>

                <div style={{
                    margin: '1%', width: '66%',
                    height: 'auto', position: 'relative'
                }}>
                    <div>
                        <div style={{ position: 'absolute', left: '5%', top: '350px' }}>
                            <UserAvatar fileName={this.state.campaignList.companyAvatar}
                                size={90} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                        </div>
                        <CoverAvatar fileName={this.state.campaignList.campAvatar} height={'400px'} />
                        <div style={{
                            backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '5% 2% 2% 7%',
                            borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                        }}>
                            <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.campaignList.campaignTitle}</text><br />
                            <div className={'textContainer'} onClick={this.handleComp()}>
                                {this.state.campaignList.Company}
                            </div>
                            <text style={{ fontSize: 13 }}>{`Published on ${this.state.campaignList.date}`} </text><br />
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <div style={{ marginTop: '2%' }}>
                                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN DETIALS </text><br />
                                        <table>
                                            <tr>
                                                <td><MapIcon /></td>
                                                <td style={{ display: 'flex' }}>
                                                    {
                                                        this.state.campaignList.workLocation ?
                                                            this.state.campaignList.workLocation.map((loc, idx) => (

                                                                <div style={{ backgroundColor: '#CACFD2', padding: '5%', borderRadius: 20, margin: '1%' }}>
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.add1}</text><br />
                                                                    {
                                                                        loc.add2 ?
                                                                            <div>
                                                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.add2}</text><br />
                                                                            </div>
                                                                            :
                                                                            false
                                                                    }
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.city}</text> <br />
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.zip}, {loc.state}</text>
                                                                </div>
                                                            ))
                                                            :
                                                            <text style={{ fontSize: 13, paddingLeft: '1%' }}>no&nbsp;define</text>
                                                    }
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    : this.state.campaignList.typeOfCampaign === 3 ?
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN&nbsp;DETIALS </text><br />
                                            <table>
                                                <tr>
                                                    <td><Calender /></td>
                                                    <td>
                                                        <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventDate}</text>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><Time /></td>
                                                    <td>
                                                        <TimePicker
                                                            style={{ fontSize: 13, paddingLeft: '1%' }}
                                                            disabled={true}
                                                            format="ampm"
                                                            value={new Date(this.state.campaignList.eventTime)}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><MapIcon /></td>
                                                    <td>
                                                        {
                                                            this.state.campaignList.eventAdd2 ?
                                                                <p style={{ fontSize: 13, paddingLeft: '1%', wordWrap: 'break-word', margin: 0 }}>{
                                                                    this.state.campaignList.eventAdd1 + ', ' + this.state.campaignList.eventAdd2 + ', ' + this.state.campaignList.eventCity + ', ' +
                                                                    this.state.campaignList.eventZip + ', ' + this.state.campaignList.eventState
                                                                }</p>
                                                                :
                                                                <p style={{ fontSize: 13, paddingLeft: '1%', wordWrap: 'break-word', margin: 0 }}>{
                                                                    this.state.campaignList.eventAdd1 + ', ' + this.state.campaignList.eventCity + ', ' +
                                                                    this.state.campaignList.eventZip + ', ' + this.state.campaignList.eventState
                                                                }</p>
                                                        }
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style={{ marginTop: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>TICKET</text><br />
                                                {
                                                    this.state.campaignList.isTicket ?
                                                        <text style={{ fontSize: 13 }}>RM&nbsp;{this.state.campaignList.ticketPrice}</text>
                                                        :
                                                        <text style={{ fontSize: 13 }}>Free</text>
                                                }
                                            </div>
                                        </div>
                                        : false
                            }
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN&nbsp;DESCRIPTION </text><br />
                                <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.description}</p>
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                {
                                    this.state.campaignList.typeOfCampaign === 1 ?
                                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;CAREER&amp;OPPORTUNITIES<br /></text>
                                        :
                                        this.state.campaignList.typeOfCampaign === 2 ?
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;DIGITAL&nbsp;CHALLENGE<br /></text>
                                            :
                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;EVENT<br /></text>
                                                : ''
                                }
                                {
                                    this.state.campaignList.typeOfCampaign === 1 ?
                                        this.state.campaignList.typeOfSubCampaign === 1 ?
                                            <text style={{ fontSize: 13 }}>Part-time</text>
                                            :
                                            this.state.campaignList.typeOfSubCampaign === 2 ?
                                                <text style={{ fontSize: 13 }}>Full-time</text>
                                                :
                                                this.state.campaignList.typeOfSubCampaign === 3 ?
                                                    <text style={{ fontSize: 13 }}>Internship</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 4 ?
                                                        <text style={{ fontSize: 13 }}>Volunteers</text>
                                                        :
                                                        this.state.campaignList.typeOfSubCampaign === 5 ?
                                                            <text style={{ fontSize: 13 }}>Freelance</text>
                                                            :
                                                            ''
                                        :
                                        this.state.campaignList.typeOfCampaign === 2 ?
                                            this.state.campaignList.typeOfSubCampaign === 1 ?
                                                <text style={{ fontSize: 13 }}>Survey</text>
                                                :
                                                this.state.campaignList.typeOfSubCampaign === 2 ?
                                                    <text style={{ fontSize: 13 }}>Feedback</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 3 ?
                                                        <text style={{ fontSize: 13 }}>Idea&nbsp;Bank</text>
                                                        :
                                                        this.state.campaignList.typeOfSubCampaign === 4 ?
                                                            <text style={{ fontSize: 13 }}>Projects</text>
                                                            :
                                                            ''
                                            :
                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                this.state.campaignList.typeOfSubCampaign === 1 ?
                                                    <text style={{ fontSize: 13 }}>With&nbsp;Attendance</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 2 ?
                                                        <text style={{ fontSize: 13 }}>Without&nbsp;Attendance</text>
                                                        :
                                                        ''
                                                :
                                                ''
                                }
                                <br />
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>ADDITIONAL&nbsp;REWARD</text><br />
                                <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>
                                    {this.state.campaignList.additionalReward ? this.state.campaignList.additionalReward : 'None'}
                                </p>
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>TARGET&nbsp;STUDENT</text><br />
                                <text style={{ fontSize: 13 }}>{this.state.campaignList.targetStudent ? this.state.campaignList.targetStudent : 'no define'}</text><br />
                            </div>
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>SPECIALIZATION</text><br />
                                            <text style={{ fontSize: 13 }}>{this.state.campaignList.specialization}</text>
                                        </div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIREMENT</text><br />
                                            <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.requirements}</p>
                                        </div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>RESPONSIBILITIES</text><br />
                                            <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.responsibilities}</p>
                                        </div>
                                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>BENEFITS</text><br />
                                                <text style={{ fontSize: 13 }}>{this.state.campaignList.benefits}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIRE COURSES</text><br />
                                                <text style={{ fontSize: 13 }}>{this.state.requiredCourses.join()}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>ALLOWANCE/ PAYOUT</text><br />
                                                <text style={{ fontSize: 13 }}>{`RM ${this.state.campaignList.minPayout} - RM ${this.state.campaignList.maxPayout}`}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIRE SKILLS</text><br />
                                                <text style={{ fontSize: 13 }}>
                                                    {this.state.requiredSkills ? this.state.requiredSkills.join() : 'None'}
                                                </text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>SALARY TYPE</text><br />
                                                {
                                                    this.state.campaignList.salaryType === 'PerHour' ?
                                                        <text style={{ fontSize: 13 }}>Per&nbsp;Hour</text>
                                                        :
                                                        this.state.campaignList.salaryType === 'PerDay' ?
                                                            <text style={{ fontSize: 13 }}>Per&nbsp;Day</text>
                                                            :
                                                            this.state.campaignList.salaryType === 'PerMonth' ?
                                                                <text style={{ fontSize: 13 }}>Per&nbsp;Month</text>
                                                                :
                                                                this.state.campaignList.salaryType === 'PerCompletion' ?
                                                                    <text style={{ fontSize: 13 }}>Per&nbsp;Completion/Project</text>
                                                                    :
                                                                    ''
                                                }
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>MINIMUM EDUCATION</text><br />
                                                {
                                                    this.state.campaignList.minEducation === 'PrimarySchool' ?
                                                        <text style={{ fontSize: 13 }}>Primary&nbsp;School</text>
                                                        :
                                                        this.state.campaignList.minEducation === 'HighSchool' ?
                                                            <text style={{ fontSize: 13 }}>High&nbsp;School</text>
                                                            :
                                                            this.state.campaignList.minEducation === 'Diploma' ?
                                                                <text style={{ fontSize: 13 }}>Certificates/Vocational/Diploma</text>
                                                                :
                                                                this.state.campaignList.minEducation === 'Degree' ?
                                                                    <text style={{ fontSize: 13 }}>Degree</text>
                                                                    :
                                                                    this.state.campaignList.minEducation === 'Master' ?
                                                                        <text style={{ fontSize: 13 }}>Master/PHD</text>
                                                                        :
                                                                        this.state.campaignList.minEducation === 'NoLimit' ?
                                                                            <text style={{ fontSize: 13 }}>No&nbsp;Limit</text>
                                                                            :
                                                                            ''
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    : false
                            }
                        </div>
                        <div style={{
                            marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                            height: 'auto', padding: '2% 2% 2% 4%'
                        }}>
                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>ABOUT&nbsp;THE&nbsp;BRAND</text><br />
                            <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.companyDesc}</p>
                        </div>
                        {
                            this.state.campaignList.isVideo ?
                                <div style={{
                                    marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                                    height: 'auto', padding: '2% 2% 2% 4%'
                                }}>
                                    <YouTube
                                        videoId={this.state.campaignList.videoId}
                                        opts={this.state.opts}
                                    />
                                </div>
                                :
                                false
                        }
                    </div>
                </div>
                <div style={{ margin: '1%', width: '30%', height: 'auto', position: 'relative' }}>
                    <div style={{ backgroundColor: 'white', borderRadius: 20, padding: '1% 1% 3% 1%' }}>
                        <div style={{ marginTop: '2%', textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Monsta&nbsp;Reward</text><br />
                            <text style={{ fontWeight: 'bold', fontSize: 25 }}>{`${this.state.campaignList.monstaXP ? this.state.campaignList.monstaXP : 0} XP`}</text>
                        </div>
                        <br />
                        <div style={{ textAlign: 'center' }}>
                            {
                                this.state.campaignList.typeOfCampaign === 2 ?
                                    this.state.isApply ?
                                        <RaisedButton backgroundColor='#F5B041' label="Submitted" disabled />
                                        :
                                        this.state.campaignList.startDate ?
                                            new Date(this.state.campaignList.startDate).toDateString() <= new Date().toDateString() && new Date(this.state.campaignList.endDate).toDateString() >= new Date().toDateString() ?
                                                this.state.availableSlot ?
                                                    <RaisedButton
                                                        backgroundColor='#F5B041'
                                                        label="Submit"
                                                        onClick={this.applyCamp}
                                                    />
                                                    : false
                                                : false
                                            :
                                            this.state.availableSlot ?
                                                <RaisedButton
                                                    backgroundColor='#F5B041'
                                                    label="Submit"
                                                    onClick={this.applyCamp}
                                                />
                                                :
                                                false
                                    :
                                    this.state.isApply ?
                                        <RaisedButton backgroundColor='#F5B041' label="Applied" disabled />
                                        :
                                        this.state.campaignList.startDate ?
                                            new Date(this.state.campaignList.startDate).toDateString() <= new Date().toDateString() && new Date(this.state.campaignList.endDate).toDateString() >= new Date().toDateString() ?
                                                this.state.availableSlot ?
                                                    <RaisedButton
                                                        backgroundColor='#F5B041'
                                                        label="Apply for this"
                                                        onClick={this.applyCamp}
                                                    />
                                                    : false
                                                : false
                                            :
                                            this.state.availableSlot ?
                                                <RaisedButton
                                                    backgroundColor='#F5B041'
                                                    label="Apply for this"
                                                    onClick={this.applyCamp}
                                                />
                                                :
                                                false

                            }
                        </div>
                        <br />
                        <div style={{ marginTop: '2%', textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>TYPE</text><br />
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <text style={{ fontSize: 14 }}>Carrer</text>
                                    :
                                    this.state.campaignList.typeOfCampaign === 2 ?
                                        this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                            <text style={{ fontSize: 14 }}>Survey/&nbsp;Feedback</text>
                                            :
                                            <text style={{ fontSize: 14 }}>Submit&nbsp;Link</text>
                                        :
                                        this.state.campaignList.typeOfCampaign === 3 ?
                                            <text style={{ fontSize: 14 }}>Event</text>
                                            :
                                            ''
                            }
                        </div>
                        <br />
                        <div style={{ marginTop: '2%', textAlign: 'center', padding: '1%' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Skills&nbsp;Offer</text><br />
                            <div>
                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                    {
                                        this.state.skills.length > 0 ?
                                            this.state.skills.map((e, idx) => (
                                                <div style={{
                                                    backgroundColor: '#CACFD2', padding: '1%', width: `${e.length * 8 + 20}px`,
                                                    margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                }}>
                                                    <text style={{ fontSize: 13 }}>{e}</text>
                                                </div>
                                            ))
                                            :
                                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                <text style={{ fontSize: 13 }}>None</text>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <br />
                        <div style={{ textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Latest&nbsp;Updates</text><br />
                        </div>
                        <div style={{
                            margin: '2% 5% 2% 5%', padding: '1%', overflowY: 'auto', height: '250px',
                            backgroundColor: '#E5E7E9', borderRadius: 10
                        }}>
                            {
                                this.state.campaignList.broadcast ?
                                    this.state.campaignList.broadcast.map(e => (
                                        <div style={{ backgroundColor: 'white', borderRadius: 10, padding: '2%', marginBottom: '2%' }}>
                                            <text style={{ fontSize: 11 }}>
                                                {moment(new Date(e.datePost)).format('MMMM Do YYYY, h:mm:ss a')}
                                            </text>
                                            <p style={{ margin: 0, fontSize: 13, paddingLeft: '2%', wordWrap: 'break-word' }}>{e.message}</p>
                                        </div>
                                    ))
                                    : false
                            }
                        </div>
                        <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                            <FacebookShareButton
                                url={shareUrl}
                                quote={title}
                                className="Demo__some-network__share-button">
                                <FacebookIcon
                                    size={32}
                                    round />
                            </FacebookShareButton>
                        </div>
                    </div>
                </div>
                {
                    this.state.campaignList.typeOfCampaign === 1 ?
                        this.state.campaignList.aptitudeTest ?
                            !this.state.isApply ?
                                this.state.campaignList.startDate ?
                                    new Date(this.state.campaignList.startDate).toDateString() <= new Date().toDateString() && new Date(this.state.campaignList.endDate).toDateString() >= new Date().toDateString() ?
                                        <div style={{
                                            margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                            height: 'auto', padding: '2% 2% 2% 4%'
                                        }}>
                                            <text style={{ fontSize: 22, fontWeight: 'bold' }}>Aptitude&nbsp;Test</text><br />
                                            <div>
                                                <br />
                                                {
                                                    this.state.questionList.length > 0 ?
                                                        this.state.questionList.map((quesList, idx) => (
                                                            <div style={{ paddingLeft: '2%' }}>
                                                                <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                                    {idx + 1 + '. ' + quesList.question}
                                                                </text><br />
                                                                <div style={{ paddingLeft: '1%' }}>
                                                                    {
                                                                        quesList.answerList ?
                                                                            quesList.answerList.map((ans, ansIdx) => (
                                                                                quesList.selectedGroup === 'multiSelect' ?
                                                                                    <div>
                                                                                        <input type="checkbox" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                        <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                                        <br />
                                                                                    </div>
                                                                                    :
                                                                                    <div>
                                                                                        <input type="radio" name={`radAnswer${idx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                        <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                                        <br />
                                                                                    </div>
                                                                            ))
                                                                            :
                                                                            false
                                                                    }
                                                                </div>
                                                                <br />
                                                            </div>
                                                        ))
                                                        :
                                                        false
                                                }
                                            </div>
                                            <br />
                                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                {

                                                    this.state.isApply ?
                                                        <RaisedButton backgroundColor='#F5B041' label="Applied" disabled />
                                                        :
                                                        this.state.availableSlot ?
                                                            <RaisedButton
                                                                backgroundColor='#F5B041'
                                                                label="Apply for this"
                                                                onClick={this.applyCamp}
                                                            />
                                                            :
                                                            false

                                                }
                                            </div>
                                        </div>
                                        : false
                                    :
                                    <div style={{
                                        margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                        height: 'auto', padding: '2% 2% 2% 4%'
                                    }}>
                                        <text style={{ fontSize: 22, fontWeight: 'bold' }}>Aptitude&nbsp;Test</text><br />
                                        <div>
                                            <br />
                                            {
                                                this.state.questionList.length > 0 ?
                                                    this.state.questionList.map((quesList, idx) => (
                                                        <div style={{ paddingLeft: '2%' }}>
                                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                                {idx + 1 + '. ' + quesList.question}
                                                            </text><br />
                                                            <div style={{ paddingLeft: '1%' }}>
                                                                {
                                                                    quesList.answerList ?
                                                                        quesList.answerList.map((ans, ansIdx) => (
                                                                            quesList.selectedGroup === 'multiSelect' ?
                                                                                <div>
                                                                                    <input type="checkbox" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                    <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                                    <br />
                                                                                </div>

                                                                                :
                                                                                <div>
                                                                                    <input type="radio" name={`radAnswer${idx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                    <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                                    <br />
                                                                                </div>
                                                                        ))
                                                                        :
                                                                        false
                                                                }
                                                            </div>
                                                            <br />
                                                        </div>
                                                    ))
                                                    :
                                                    false
                                            }
                                        </div>
                                        <br />
                                        <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                            {

                                                this.state.isApply ?
                                                    <RaisedButton backgroundColor='#F5B041' label="Applied" disabled />
                                                    :
                                                    this.state.availableSlot ?
                                                        <RaisedButton
                                                            backgroundColor='#F5B041'
                                                            label="Apply for this"
                                                            onClick={this.applyCamp}
                                                        />
                                                        :
                                                        false

                                            }
                                        </div>
                                    </div>
                                : false
                            : false
                        : this.state.campaignList.typeOfCampaign === 2 ?
                            this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                !this.state.isApply ?
                                    this.state.campaignList.startDate ?
                                        new Date(this.state.campaignList.startDate).toDateString() <= new Date().toDateString() && new Date(this.state.campaignList.endDate).toDateString() >= new Date().toDateString() ?
                                            <div style={{
                                                margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                                height: 'auto', padding: '2% 2% 2% 4%'
                                            }}>
                                                <text style={{ fontSize: 22, fontWeight: 'bold' }}>Question</text><br /><br />
                                                {
                                                    this.state.questionList.map((quesList, idx) => (
                                                        <div style={{ paddingLeft: '2%' }}>
                                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                                {idx + 1 + '. ' + quesList.question}
                                                            </text><br />
                                                            <div style={{ paddingLeft: '1%' }}>
                                                                {
                                                                    quesList.selectedGroup === 'ratingScale' ?
                                                                        <div>
                                                                            <br />
                                                                            <table style={{ borderCollapse: 'collapse' }}>
                                                                                <tr>
                                                                                    <th></th>
                                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Disagree&nbsp;</text></th>
                                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Disagree&nbsp;</text></th>
                                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Neutral&nbsp;</text></th>
                                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Agree&nbsp;</text></th>
                                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Agree&nbsp;</text></th>
                                                                                </tr>
                                                                                {
                                                                                    quesList.answerList ?
                                                                                        quesList.answerList.map((ans, ansIdx) => (
                                                                                            <tr style={{ textAlign: 'center' }}>
                                                                                                <td>
                                                                                                    <text style={{ padding: '2%', fontSize: 13 }}>&nbsp;{ans.answer.trim()}&nbsp;</text>
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input value="sd" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input value="d" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input value="n" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input value="a" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <input value="sa" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                            </tr>
                                                                                        ))
                                                                                        :
                                                                                        ''
                                                                                }
                                                                            </table>
                                                                        </div>
                                                                        :
                                                                        <table>
                                                                            {
                                                                                quesList.answerList ?
                                                                                    quesList.answerList.map((ans, ansIdx) => (
                                                                                        quesList.selectedGroup === 'multiSelect' ?
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <input type="checkbox" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                                </td>
                                                                                            </tr>
                                                                                            :
                                                                                            <tr>
                                                                                                <td>
                                                                                                    <input type="radio" name={`radAnswer${idx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                                </td>
                                                                                                <td>
                                                                                                    <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                                </td>
                                                                                            </tr>
                                                                                    ))
                                                                                    :
                                                                                    quesList.selectedGroup === 'textboxMultiLine' ?
                                                                                        <tr>
                                                                                            <TextField
                                                                                                style={{ width: '200%' }}
                                                                                                multiLine={true}
                                                                                                hintText="Enter your answer"
                                                                                                floatingLabelText="Enter your answer"
                                                                                                value={quesList.isAnswer ? quesList.isAnswer : quesList.isAnswer}
                                                                                                onChange={this.handleIsAnswer(idx, '')}
                                                                                            />
                                                                                        </tr>

                                                                                        :
                                                                                        <tr>
                                                                                            <TextField
                                                                                                style={{ width: '200%' }}
                                                                                                hintText="Enter your answer"
                                                                                                floatingLabelText="Enter your answer"
                                                                                                value={quesList.isAnswer ? quesList.isAnswer : quesList.isAnswer}
                                                                                                onChange={this.handleIsAnswer(idx, '')}
                                                                                            />
                                                                                        </tr>
                                                                            }
                                                                        </table>
                                                                }
                                                            </div>
                                                        </div>
                                                    ))}
                                                <br />
                                                <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                    {

                                                        this.state.isApply ?
                                                            <RaisedButton backgroundColor='#F5B041' label="Submitted" disabled />
                                                            :
                                                            this.state.availableSlot ?
                                                                <RaisedButton
                                                                    backgroundColor='#F5B041'
                                                                    label="Submit"
                                                                    onClick={this.applyCamp}
                                                                />
                                                                :
                                                                false

                                                    }
                                                </div>
                                            </div>
                                            : false
                                        :
                                        <div style={{
                                            margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                            height: 'auto', padding: '2% 2% 2% 4%'
                                        }}>
                                            <text style={{ fontSize: 22, fontWeight: 'bold' }}>Question</text><br /><br />
                                            {
                                                this.state.questionList.map((quesList, idx) => (
                                                    <div style={{ paddingLeft: '2%' }}>
                                                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                            {idx + 1 + '. ' + quesList.question}
                                                        </text><br />
                                                        <div style={{ paddingLeft: '1%' }}>
                                                            {
                                                                quesList.selectedGroup === 'ratingScale' ?
                                                                    <div>
                                                                        <br />
                                                                        <table style={{ borderCollapse: 'collapse' }}>
                                                                            <tr>
                                                                                <th></th>
                                                                                <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Disagree&nbsp;</text></th>
                                                                                <th><text style={{ fontSize: 13 }}>&nbsp;Disagree&nbsp;</text></th>
                                                                                <th><text style={{ fontSize: 13 }}>&nbsp;Neutral&nbsp;</text></th>
                                                                                <th><text style={{ fontSize: 13 }}>&nbsp;Agree&nbsp;</text></th>
                                                                                <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Agree&nbsp;</text></th>
                                                                            </tr>
                                                                            {
                                                                                quesList.answerList ?
                                                                                    quesList.answerList.map((ans, ansIdx) => (
                                                                                        <tr style={{ textAlign: 'center' }}>
                                                                                            <td>
                                                                                                <text style={{ padding: '2%', fontSize: 13 }}>&nbsp;{ans.answer.trim()}&nbsp;</text>
                                                                                            </td>
                                                                                            <td>
                                                                                                <input value="sd" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <input value="d" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <input value="n" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <input value="a" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <input value="sa" type="radio" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                        </tr>
                                                                                    ))
                                                                                    :
                                                                                    ''
                                                                            }
                                                                        </table>
                                                                    </div>
                                                                    :
                                                                    <table>
                                                                        {
                                                                            quesList.answerList ?
                                                                                quesList.answerList.map((ans, ansIdx) => (
                                                                                    quesList.selectedGroup === 'multiSelect' ?
                                                                                        <tr>
                                                                                            <td>
                                                                                                <input type="checkbox" name={`radAnswer${idx}${ansIdx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                            </td>
                                                                                        </tr>
                                                                                        :
                                                                                        <tr>
                                                                                            <td>
                                                                                                <input type="radio" name={`radAnswer${idx}`} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                            </td>
                                                                                            <td>
                                                                                                <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                            </td>
                                                                                        </tr>
                                                                                ))
                                                                                :
                                                                                quesList.selectedGroup === 'textboxMultiLine' ?
                                                                                    <tr>
                                                                                        <TextField
                                                                                            style={{ width: '200%' }}
                                                                                            multiLine={true}
                                                                                            hintText="Enter your answer"
                                                                                            floatingLabelText="Enter your answer"
                                                                                            value={quesList.isAnswer ? quesList.isAnswer : quesList.isAnswer}
                                                                                            onChange={this.handleIsAnswer(idx, '')}
                                                                                        />
                                                                                    </tr>

                                                                                    :
                                                                                    <tr>
                                                                                        <TextField
                                                                                            style={{ width: '200%' }}
                                                                                            hintText="Enter your answer"
                                                                                            floatingLabelText="Enter your answer"
                                                                                            value={quesList.isAnswer ? quesList.isAnswer : quesList.isAnswer}
                                                                                            onChange={this.handleIsAnswer(idx, '')}
                                                                                        />
                                                                                    </tr>
                                                                        }
                                                                    </table>
                                                            }
                                                        </div>
                                                    </div>
                                                ))}
                                            <br />
                                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                {

                                                    this.state.isApply ?
                                                        <RaisedButton backgroundColor='#F5B041' label="Submitted" disabled />
                                                        :
                                                        this.state.availableSlot ?
                                                            <RaisedButton
                                                                backgroundColor='#F5B041'
                                                                label="Submit"
                                                                onClick={this.applyCamp}
                                                            />
                                                            :
                                                            false

                                                }
                                            </div>
                                        </div>
                                    : false
                                :
                                this.state.campaignList.submitMethod === 'submitLink' ?
                                    !this.state.isApply ?
                                        this.state.campaignList.startDate ?
                                            new Date(this.state.campaignList.startDate).toDateString() <= new Date().toDateString() && new Date(this.state.campaignList.endDate).toDateString() >= new Date().toDateString() ?
                                                <div style={{
                                                    margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                                    height: 'auto', padding: '2% 2% 2% 4%'
                                                }}>
                                                    <text style={{ fontSize: 22, fontWeight: 'bold' }}>Submit</text>
                                                    <TextField
                                                        style={{ width: '100%', fontSize: 13 }}
                                                        hintText="Link"
                                                        floatingLabelText="Please submit by paste the link here:"
                                                        value={this.state.submitLink}
                                                        onChange={this.handleLinkChange}
                                                    />
                                                    <br />
                                                    <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                        {

                                                            this.state.isApply ?
                                                                <RaisedButton backgroundColor='#F5B041' label="Submitted" disabled />
                                                                :
                                                                this.state.availableSlot ?
                                                                    <RaisedButton
                                                                        backgroundColor='#F5B041'
                                                                        label="Submit"
                                                                        onClick={this.applyCamp}
                                                                    />
                                                                    :
                                                                    false

                                                        }
                                                    </div>
                                                </div>
                                                : false
                                            :
                                            <div style={{
                                                margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                                height: 'auto', padding: '2% 2% 2% 4%'
                                            }}>
                                                <text style={{ fontSize: 22, fontWeight: 'bold' }}>Submit</text>
                                                <TextField
                                                    style={{ width: '100%', fontSize: 13 }}
                                                    hintText="Link"
                                                    floatingLabelText="Please submit by paste the link here:"
                                                    value={this.state.submitLink}
                                                    onChange={this.handleLinkChange}
                                                />
                                                <br />
                                                <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                    {

                                                        this.state.isApply ?
                                                            <RaisedButton backgroundColor='#F5B041' label="Submitted" disabled />
                                                            :
                                                            this.state.availableSlot ?
                                                                <RaisedButton
                                                                    backgroundColor='#F5B041'
                                                                    label="Submit"
                                                                    onClick={this.applyCamp}
                                                                />
                                                                :
                                                                false

                                                    }
                                                </div>
                                            </div>
                                        : false
                                    : false
                            : false
                }
            </div>
        );
    }
}

/**
 * Map dispatch to props
* @param  {func} dispatch is the function to dispatch action to reducers
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
                */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
* @param  {object} state is the obeject from redux store
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
*/
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CampaignInfo)