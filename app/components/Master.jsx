// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Route, Switch, NavLink, withRouter, Redirect,BrowserRouter as BrowserRouter,BrowserRoute } from 'react-router-dom'
import { firebaseAuth, firebaseRef,firebaseApp } from 'app/firebase/'
import { push } from 'react-router-redux'
import Snackbar from 'material-ui/Snackbar';
import LinearProgress from 'material-ui/LinearProgress'


// - Import components
import Home from 'Home'
import Signup from 'Signup'
import Login from 'Login'
import Settings from 'Settings'
import SignupStudent from 'SignupStudent'
import Dashboard from 'Dashboard'
import Admin from 'Admin'
import AdminDashboard from 'AdminDashboard'
import MasterLoading from 'MasterLoading'
import MonstaChallenge from 'MonstaChallenge'
import MyProfile from 'MyProfile'
import Company from 'Company'
import RefferalSignup from 'RefferalSignup'
import Messenger from 'Messenger'
import PostPayment from 'PostPayment'
import PostPaymentPage from 'PostPaymentPage'


// - Import API
import { PrivateRoute, PublicRoute } from 'AuthRouterAPI'


// - Import actions
import * as authorizeActions from 'authorizeActions'
import * as imageGalleryActions from 'imageGalleryActions'
import * as postActions from 'postActions'
import * as commentActions from 'commentActions'
import * as voteActions from 'voteActions'
import * as userActions from 'userActions'
import * as globalActions from 'globalActions'
import * as circleActions from 'circleActions'
import * as notifyActions from 'notifyActions'


/* ------------------------------------ */


// - Create Master component class
export class Master extends Component {

  static isPrivate = true
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      authed: false,
      dataLoaded: false,
      role: 'C',
      verified:false
    };

    // Binding functions to `this`
    this.handleLoading = this.handleLoading.bind(this)
    this.handleMessage = this.handleMessage.bind(this)


  }

  // Handle click on message
  handleMessage = (evt) => {
    this.props.closeMessage()
  }

  // Handle loading
  handleLoading = (status) => {
    this.setState({
      loading: status,
      authed: false
    })
  }

  componentWillMount = () => {
/*     alert(" Error:browser auth/web-storage-unsupported")
 */
    firebaseAuth().onAuthStateChanged((user) => {

      if (user) {
        this.props.login(user)
        this.setState({
          loading: false
        })

        if (!this.props.global.defaultLoadDataStatus) {
          this.props.clearData()
          this.props.loadData()
          this.props.defaultDataEnable()
        }
      } else {
        this.props.logout()
        this.setState({
          loading: false
        })
        if (this.props.global.defaultLoadDataStatus) {
          this.props.defaultDataDisable()
          this.props.clearData()
        }
        this.props.loadDataGuest()
      }
      this.props.loadUserInfo(user)
      this.getUserRole(user)
      
    })
  }

getUserRole = (user) => {
  firebaseApp.database().ref().child(`users/${user.uid}/info`).once('value').then((snapshot)=>{
    console.log(snapshot.val())
    this.state.role = snapshot.val().role
  })
  var check = firebaseApp.auth().currentUser
  this.setState({verified:check.emailVerified})
    if (!check.emailVerified) {
      alert("Please verify your email")
   }
   this.setState({verified:true})
  }


  /**
   * Render app DOM component
   * 
   * @returns 
   * 
   * @memberof Master
   */
  render() {
    

    const { progress, global } = this.props
    var test = this.state.role
    console.log(test)
    return (
      <div id="master">

        <div className='master__progress' style={{ display: (progress.visible ? 'block' : 'none') }}>
          <LinearProgress mode="determinate" value={progress.percent} />
        </div>
        <div className='master__loading animate-fading2' style={{ display: (global.showTopLoading ? 'flex' : 'none') }}>
          <div className='title'>  Loading ... </div>
        </div>
        <MasterLoading activeLoading={this.state.loading || !(this.props.loaded || this.props.guest)} handleLoading={this.handleLoading} />

        {(!this.state.loading && (this.props.loaded || this.props.guest))
        ?(<Switch>
         <Route path="/Refferal/:uid" component={RefferalSignup}/>
         <Route path="/postPayment/:uid/:paymentpushkey" component={PostPayment}/>
          <Route path="/adminDashboard" component={AdminDashboard}/>
          <Route path="/postPaymentPage/:uid/:paymentpushkey" component={PostPaymentPage}/>
          <Route path="/supermonsta" component={Admin}/>
          <Route path="/signup" component={Signup} />
          <Route path ="/signupStudent" component ={SignupStudent}/>
          <Route path="/settings" component={Settings} />
          <Route path="/Messenger" component={Messenger} />
          <Route path="/login" render={() => {
            console.log('this.props.authed: ', this.props.authed, "this.props: ", this.props)
            return (
              this.props.authed 
                ? <Redirect to="/" />
                : <Login />
            )
          }
          } />
           {this.state.verified?this.state.role =='company'? 
           <Route render={() =><Company uid={this.props.uid}/>} />
           :<Route render={() =><Home uid={this.props.uid}/>} />:<Route render={() =><Login/>}/>}
        </Switch>) : ''
        }
        <Snackbar
          open={this.props.global.messageOpen}
          message={this.props.global.message}
          autoHideDuration={4000}
          style={{ left: '1%', transform: 'none' }}
        />
      </div>


    )
  }
}

// - Map dispatch to props
const mapDispatchToProps = (dispatch, ownProps) => {

  return {
    loadUserInfo: (user) =>{ 
      dispatch(userActions.dbGetUserInfoByUserId(user.uid, 'header'))
    },
    
    loadData: () => {
     // dispatch(commentActions.dbGetComments())
     dispatch(imageGalleryActions.downloadForImageGallery())
     // dispatch(postActions.dbGetPosts())
      dispatch(userActions.dbGetUserInfo())
     // dispatch(voteActions.dbGetVotes())
      dispatch(notifyActions.dbGetNotifies())
      //ådispatch(circleActions.dbGetCircles())

    },
    clearData: () => {
      dispatch(imageGalleryActions.clearAllData())
      dispatch(postActions.clearAllData())
      dispatch(userActions.clearAllData())
      dispatch(commentActions.clearAllData())
      dispatch(voteActions.clearAllvotes())
      dispatch(notifyActions.clearAllNotifications())
      dispatch(circleActions.clearAllCircles())

    },
    login: (user) => {
      dispatch(authorizeActions.login(user.uid))
    },
    logout: () => {
      dispatch(authorizeActions.logout())
    },
    defaultDataDisable: () => {
      dispatch(globalActions.defaultDataDisable())
    },
    defaultDataEnable: () => {
      dispatch(globalActions.defaultDataEnable())
    },
    closeMessage: () => {
      dispatch(globalActions.hideMessage())
    },
    loadDataGuest: () => {
      dispatch(globalActions.loadDataGuest())
    }
  }

}

// - Map state to props
const mapStateToProps = ({ authorize, global, user, post, comment, imageGallery, vote, notify, circle }) => {

  return {
    guest: authorize.guest,
    uid: authorize.uid,
    authed: authorize.authed,
    progress: global.progress,
    global: global,
    loaded: user.loaded  && imageGallery.loaded  && notify.loaded
    //loaded: user.loaded && post.loaded && comment.loaded && imageGallery.loaded && vote.loaded && notify.loaded && circle.loaded
  }

}
// - Connect commponent to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Master))
