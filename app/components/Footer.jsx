import React, {Component} from 'react'
import Ionicon from 'react-ionicons'

export default class Footer extends Component {

    constructor(props) {
        super(props)
        this.state = {}
    }

    componentDidMount() {}

    componentWillUnmount() {}

    /*     onClick={() => {this.handleFooterLinkClick('http://gomonsta.asia/contact/')}}
 */

    render() {
        var margin = this.props.sidebarStatus
            ? '20%'
            : '0%'
        return (

            <div
                style={{
                backgroundColor: '#313233',
                height: '100%',
                padding: '20px',
                width: '100%',
                flex: 1
            }}>
                <div
                    style={{
                    marginLeft: margin,
                    paddingLeft: 50
                }}>
                    <div
                        style={{
                        flexWrap: 'wrap',
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'flex-start'
                    }}>
                        <div style={{
                            padding: 0
                        }}>
                            <h1
                                className="line"
                                style={{
                                color: 'white',
                                position: 'relative'
                            }}>Monsta</h1>

                            <div
                                style={{
                                flex: 'row',
                                flexWrap: 'space-between'
                            }}>
                                <a
                                    href='http://gomonsta.asia/contact/'
                                    style={{
                                    color: 'white',
                                }}>Companies
                                </a>
                                <a
                                    href='http://gomonsta.asia/privacy-policy/'
                                    style={{
                                    color: 'white',
                                    margin: 10,
                                }}>Campaign/Jobs</a>
                                <a
                                    href='http://gomonsta.asia/faq/'
                                    style={{
                                    color: 'white',
                                    margin: 10,
                                }}>About Us
                                </a>
                            </div>
                            <div
                                style={{
                                flex: 'row',
                                flexWrap: 'space-between',
                                marginTop: '10px'
                            }}>
                                <a
                                    href='http://gomonsta.asia/contact/'
                                    style={{
                                    color: 'white',
                                }}>Contact Us
                                </a>
                                <a
                                    href='http://gomonsta.asia/privacy-policy/'
                                    style={{
                                    color: 'white',
                                    margin: 15,
                                }}>Privacy Policy</a>
                                <a
                                    href='http://gomonsta.asia/privacy-policy/'
                                    style={{
                                    color: 'white',
                                    margin: 10,
                                }}>FAQ</a>
                            </div>
                        </div>
                        <div
                            style={{
                            paddingTop: 10,
                        }}>
                            <p
                            className='line2'
                                style={{
                                color: 'white',
                                position:'relative',
                                fontSize:'20'
                            }}>
                                Follow Us</p>
                            <Ionicon
                                onClick={() => window.location = 'https://www.facebook.com/GoMonsta/'}
                                icon="logo-facebook"
                                fontSize="35px"
                                color="white"/> 
                                 <Ionicon
                                onClick={() => window.location = 'https://www.instagram.com/gomonsta/'}
                                icon="logo-instagram"
                                fontSize="35px"
                                color="white"/>{/*<Ionicon icon="logo-facebook" fontSize="35px" color="#3B5999"/>*/}

                        </div>
                        {/*   <div
                            style={{
                            padding: 20
                        }}>
                            <h4
                                style={{
                                color: 'white'
                            }}>
                                About Us</h4>
                            <p
                                style={{
                                color: 'white'
                            }}>We
                                are the first network for students to access REAL-WORLD opportunities through
                                experiential learning.</p>
                        </div> */}
                    </div>
                </div>
            </div>
        )
    }
}
