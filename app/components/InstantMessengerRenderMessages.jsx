import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import 'react-chat-elements/dist/main.css'
import {Input} from 'react-chat-elements'
import {Button} from 'react-chat-elements'
import trim from 'trim'

//import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Redirect} from 'react-router-dom'
import {
		Card,
		CardActions,
		CardHeader,
		CardMedia,
		CardTitle,
		CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {firebaseAuth, firebaseRef, firebaseApp} from 'app/firebase/'
import moment from 'moment'
import {
		ThemeProvider,
		Avatar,
		Subtitle,
		ChatListItem,
		ChatList,
		Row,
		Column,
		Title,
		MessageMedia,
		MessageTitle,
		MessageText,
		MessageButtons,
		MessageButton
} from '@livechat/ui-kit'
import {MessageList} from 'react-chat-elements'
import _ from 'lodash'

export default class InstantMessengerRenderMessages extends Component {

		// Constructor
		constructor(props) {
				super(props)
				this.state = {
						messages: [],
						chatRoomID:''
				}
		}

		componentWillMount(){
			this.setState({chatRoomID:this.props.chatRoomID})
		}
		componentDidMount() {
				//var app = firebaseApp.database().ref('conversations/'+this.props.chatRoomID.toString()+'/messages')
				var app2=firebaseRef.child(`conversations`)
				app2.on('value', snapshot => {
					var values= snapshot.val()
					let messagesVal = values[this.props.chatRoomID].messages
					let messages = _(messagesVal)
							.keys()
							.map(messageKey => {
									let cloned = _.clone(messagesVal[messageKey])
									cloned.key = messageKey
									return cloned
							})
							.value()
					this.setState({messages: messages})
				})
		}


		render() {
			var arr= []
			var uid = firebaseApp.auth().currentUser.uid
			this.state.messages.map((message) => {
				var m=moment
				var pos = message.sender == uid ?'right':'left'
							arr.push({
								text:message.message,
								type:'text',
								position:pos,
								date:new Date(message.date * 1000)

							})
						})
				return (
						<div>
								<MessageList
    className='message-list'
    lockable={true}
    toBottomHeight={'100%'}
    dataSource={arr} />
						</div>
				)
		}

}