import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar'
import Call from 'react-icons/lib/md/call'
import Global from 'react-icons/lib/fa/globe'
import Location from 'react-icons/lib/fa/male'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import Gear from 'react-icons/lib/go/gear'
import Square from 'react-icons/lib/fa/square'
import Modx from 'react-icons/lib/fa/modx'
import Radio from 'react-icons/lib/fa/dot-circle-o'
import Circle from 'react-icons/lib/fa/circle-o-notch'
import Quote from 'react-icons/lib/fa/quote-left'
import RaisedButton from 'material-ui/RaisedButton'
import Map from 'GoogleMap'
import { Redirect } from 'react-router-dom'
import '../styles/app.scss'
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField'
import YouTube from 'react-youtube';
import UserAvatar from 'UserAvatar'
import CoverAvatar from 'CoverAvatar'
import QuoteImg from 'QuoteImg'
import Dialog from 'material-ui/Dialog'
import DialogTitle from 'DialogTitle'
import ImageGallery from 'ImageGallery'
import CameraIcon from 'react-icons/lib/fa/camera'
//import InputMask from 'react-input-mask';  <InputMask mask="(0)999 999 99 99" maskChar=" " />

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
var company

class CompPageBuilder extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            companyName: '',
            quotes: [],
            isSave: false,
            companyInfo: [],
            webURL: '',
            companyMobile: '',
            address: '',
            area: '',
            postcode: '',
            state: '',
            companyDesc: '',
            endorse: '',
            officeCulture: '',
            avatar: '',
            coverAvatar: '',
            avatarType: '',
            quoteIdx: null,
            title: '',
            errorOfficeCulture: false,
            errorCompanyName: false,
            errorAboutUs: false,
            errorMobile: false,
            errorWebUrl: false,
            errorAddress: false,
            errorArea: false,
            errorPostCode: false,
            errorState: false
        }
        this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
    }

    componentWillMount() {
        company = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}`)
        company.on('value', snapshot => {
            this.setState({
                companyName: snapshot.val().info.company, webURL: snapshot.val().info.companySite, companyMobile: snapshot.val().info.companyMobile,
                address: snapshot.val().info.address, area: snapshot.val().info.area, postcode: snapshot.val().info.postcode, state: snapshot.val().info.state,
                companyDesc: snapshot.val().info.companyDesc, endorse: snapshot.val().endorse, officeCulture: snapshot.val().info.officeCulture,
                quotes: snapshot.val().info.quotes, avatar: snapshot.val().info.avatar, coverAvatar: snapshot.val().info.coverAvatar
            })
        })
    }

    componentDidMount() {

    }

    handleRequestSetAvatar = (fileName) => {
        if (this.state.avatarType === 'avatar') {
            this.setState({
                avatar: fileName
            })
        }
        else if (this.state.avatarType === 'cover') {
            this.setState({
                coverAvatar: fileName
            })
        }
        else if (this.state.avatarType === 'quotes') {
            const newQuotes = this.state.quotes.map((quote, sidx) => {
                if (this.state.quoteIdx !== sidx) return quote;
                return { ...quote, quoteImg: fileName };
            });
            this.setState({ quotes: newQuotes });
        }
        else {
            const newQuotesAvatar = this.state.quotes.map((quote, sidx) => {
                if (this.state.quoteIdx !== sidx) return quote;
                return { ...quote, avatar: fileName };
            });
            this.setState({ quotes: newQuotesAvatar });
        }
    }

    handlePositionChange = (idx) => (evt) => {
        const newPosition = this.state.quotes.map((quote, sidx) => {
            if (idx !== sidx) return quote;
            return { ...quote, position: evt.target.value };
        });

        this.setState({ quotes: newPosition });
    }

    handleNameChange = (idx) => (evt) => {
        const newName = this.state.quotes.map((quote, sidx) => {
            if (idx !== sidx) return quote;
            return { ...quote, name: evt.target.value };
        });

        this.setState({ quotes: newName });
    }

    handleQuoteChange = (idx) => (evt) => {
        const newQuote = this.state.quotes.map((quote, sidx) => {
            if (idx !== sidx) return quote;
            return { ...quote, quote: evt.target.value };
        });

        this.setState({ quotes: newQuote });
    }

    addNewQuotes() {
        if (this.state.quotes) {
            this.setState({
                quotes: this.state.quotes.concat([{ quote: '', name: '', position: '', quoteImg: '' }])
            });
        } else {
            this.setState({
                quotes: [{ quote: '', name: '', position: '', quoteImg: '' }]
            });
        }
    }

    handleRemoveQuote = (idx) => () => {
        this.setState({
            quotes: this.state.quotes.filter((s, sidx) => idx !== sidx),
        });
    }

    handleCancel = () => () => {
        this.setState({ isSave: true })
    }

    handleSave = () => () => {
        var errorOfficeCulture = false
        var errorCompanyName = false
        var errorAboutUs = false
        var errorMobile = false
        var errorWebUrl = false
        var errorAddress = false
        var errorArea = false
        var errorPostCode = false
        var errorState = false

        if (!this.state.companyName) {
            errorCompanyName: true
            this.setState({ errorCompanyName: true })
        }

        if (!this.state.companyDesc) {
            errorAboutUs: true
            this.setState({ errorAboutUs: true })
        }

        if (!this.state.companyMobile) {
            errorMobile: true
            this.setState({ errorMobile: true })
        }

        if (!this.state.webURL) {
            errorWebUrl: true
            this.setState({ errorWebUrl: true })
        }

        if (!this.state.address) {
            errorAddress: true
            this.setState({ errorAddress: true })
        }

        if (!this.state.area) {
            errorArea: true
            this.setState({ errorArea: true })
        }

        if (!this.state.postcode) {
            errorPostCode: true
            this.setState({ errorPostCode: true })
        }

        if (!this.state.state) {
            errorState: true
            this.setState({ errorState: true })
        }

        if (!this.state.officeCulture) {
            errorOfficeCulture: true
            this.setState({ errorOfficeCulture: true })
        }

        if (!errorOfficeCulture && !errorCompanyName && !errorAboutUs && !errorMobile && !errorWebUrl && !errorAddress &&
            !errorArea && !errorPostCode && !errorState) {
            var quotesList = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}`).child("buildPage")
            if (this.state.quotes) {
                company.child("info").update({
                    company: this.state.companyName,
                    companySite: this.state.webURL,
                    companyMobile: this.state.companyMobile,
                    address: this.state.address,
                    area: this.state.area,
                    postcode: this.state.postcode,
                    state: this.state.state,
                    companyDesc: this.state.companyDesc,
                    officeCulture: this.state.officeCulture,
                    quotes: this.state.quotes,
                    avatar: this.state.avatar ? this.state.avatar : '',
                    coverAvatar: this.state.coverAvatar ? this.state.coverAvatar : ''
                })
            }
            else {
                company.child("info").update({
                    company: this.state.companyName,
                    companySite: this.state.webURL,
                    companyMobile: this.state.companyMobile,
                    address: this.state.address,
                    area: this.state.area,
                    postcode: this.state.postcode,
                    state: this.state.state,
                    companyDesc: this.state.companyDesc,
                    officeCulture: this.state.officeCulture,
                    avatar: this.state.avatar ? this.state.avatar : '',
                    coverAvatar: this.state.coverAvatar ? this.state.coverAvatar : ''
                })
            }

            this.setState({ isSave: true })
        }
    }

    handleWebUrl = (event) => {
        this.setState({
            webURL: event.target.value, errorWebUrl: false
        });
    };

    handleCompMobile = (event) => {
        this.setState({
            companyMobile: event.target.value, errorMobile: false
        });
    };

    handleAddress = (event) => {
        this.setState({
            address: event.target.value, errorAddress: false
        });
    };

    handleArea = (event) => {
        this.setState({
            area: event.target.value, errorArea: false
        });
    };

    handlePostCode = (event) => {
        this.setState({
            postcode: event.target.value, errorPostCode: false
        });
    };

    handleState = (event) => {
        this.setState({
            state: event.target.value, errorState: false
        });
    };

    handleCompName = (event) => {
        this.setState({
            companyName: event.target.value, errorCompanyName: false
        });
    };

    handleCompDesc = (event) => {
        this.setState({
            companyDesc: event.target.value, errorAboutUs: false
        });
    };

    handleOfficeCulture = (event) => {
        this.setState({
            officeCulture: event.target.value, errorOfficeCulture: false
        });
    };

    handleOpenAvatarGallery = (type, idx) => () => {
        var title = ''
        if (type === 'cover' || type === 'quotes') {
            title = 'Choose an image, recommended size 1366 * 768'
        }
        else {
            title = 'Choose an image, recommended size 250 * 250'
        }
        this.setState({
            openAvatar: true, avatarType: type, quoteIdx: idx, title: title
        })
    }

    handleCloseAvatarGallery = () => {
        this.setState({
            openAvatar: false
        })
    }

    render() {
        if (this.state.isSave) {
            return <Redirect push to={`/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`} />
        }

        return (
            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                    <div style={{ padding: '2%' }}>
                        <Dialog
                            title={<DialogTitle title={this.state.title} onRequestClose={this.handleCloseAvatarGallery} />}
                            modal={false}
                            open={this.state.openAvatar}
                            onRequestClose={this.handleCloseAvatarGallery}
                            overlayStyle={{ background: "rgba(0,0,0,0.12)" }}
                            autoDetectWindowHeight={false}

                        >
                            <ImageGallery set={this.handleRequestSetAvatar} close={this.handleCloseAvatarGallery} />
                        </Dialog>
                        <table style={{ width: '100%' }}>
                            <tr>
                                <td style={{ width: '80%' }}><h1>Company Page Builder</h1></td>
                                <td style={{ width: '10%' }}>
                                    <RaisedButton
                                        style={{ fontWeight: 'bold', color: '#818078', float: 'right' }}
                                        label='Save'
                                        primary={true}
                                        onClick={this.handleSave()}
                                    />
                                </td>
                                <td style={{ width: '10%' }}>
                                    <RaisedButton
                                        style={{ fontWeight: 'bold', color: '#818078', float: 'right' }}
                                        label='Cancel'
                                        primary={true}
                                        onClick={this.handleCancel()}
                                    />
                                </td>
                            </tr>
                        </table>
                        <hr className="hr-text" />
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>

                            <div style={{
                                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '59%'),
                                height: 'auto', position: 'relative'
                            }} >
                                <div style={{ position: 'absolute', left: '5%', top: '250px' }} onClick={this.handleOpenAvatarGallery('avatar', null)}>
                                    <UserAvatar fileName={this.state.avatar}
                                        size={90} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                </div>
                                <div style={{
                                    position: 'absolute', left: '14%', top: '310px', backgroundColor: '#045e7a', borderRadius: '50%',
                                    padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                                }} onClick={this.handleOpenAvatarGallery('avatar', null)}>
                                    <CameraIcon size={15} color='white' />
                                </div>
                                <CoverAvatar fileName={this.state.coverAvatar} height={'300px'} />
                                <div style={{
                                    position: 'absolute', right: '2%', top: '13px', backgroundColor: 'black', borderRadius: '50%',
                                    padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                                }} onClick={this.handleOpenAvatarGallery('cover', null)}>
                                    <CameraIcon size={15} color='white' />
                                </div>
                                <div style={{
                                    backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '7% 2% 2% 7%',
                                    borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                }}>
                                    <TextField
                                        style={{ width: '100%', fontSize: 22 }}
                                        multiLine={true}
                                        value={this.state.companyName}
                                        onChange={this.handleCompName}
                                        errorText={this.state.errorCompanyName ? 'This field is required' : false}
                                    />
                                    <br />
                                    <div style={{ marginTop: '2%' }}>
                                        <text style={{ fontWeight: 'bold' }}>About Us</text>
                                        <TextField
                                            style={{ width: '100%', fontSize: 13 }}
                                            multiLine={true}
                                            value={this.state.companyDesc}
                                            onChange={this.handleCompDesc}
                                            errorText={this.state.errorAboutUs ? 'This field is required' : false}
                                        />
                                    </div>
                                </div>
                            </div>

                            <div style={{
                                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '37%'),
                                height: 'auto', position: 'relative'
                            }}>

                                <div style={{ backgroundColor: '#FFFFFF', width: '100%', padding: '5%', borderRadius: 20 }}>
                                    <div style={{ overflow: 'hidden' }} >
                                        <TextField
                                            style={{ width: '100%', fontSize: 13 }}
                                            floatingLabelText={'Company Mobile'}
                                            value={this.state.companyMobile}
                                            onChange={this.handleCompMobile}
                                            errorText={this.state.errorMobile ? 'This field is required' : false}
                                        />
                                    </div>
                                    <br />
                                    <div style={{ overflow: 'hidden' }} >
                                        <TextField
                                            style={{ width: '100%', fontSize: 13 }}
                                            floatingLabelText={'Company Site'}
                                            value={this.state.webURL}
                                            onChange={this.handleWebUrl}
                                            errorText={this.state.errorWebUrl ? 'This field is required' : false}
                                        />
                                    </div>
                                    <br />
                                    <div style={{ overflow: 'hidden' }} >
                                        <div>
                                            <TextField
                                                style={{ width: '100%', fontSize: 13 }}
                                                floatingLabelText={'Address'}
                                                value={this.state.address}
                                                onChange={this.handleAddress}
                                                errorText={this.state.errorAddress ? 'This field is required' : false}
                                            />

                                            <TextField
                                                style={{ width: '100%', fontSize: 13 }}
                                                floatingLabelText={'Area'}
                                                value={this.state.area}
                                                onChange={this.handleArea}
                                                errorText={this.state.errorArea ? 'This field is required' : false}
                                            />

                                            <TextField
                                                style={{ width: '100%', fontSize: 13 }}
                                                floatingLabelText={'Zip/Postal Code'}
                                                value={this.state.postcode}
                                                onChange={this.handlePostCode}
                                                errorText={this.state.errorPostCode ? 'This field is required' : false}
                                            />
                                            <TextField
                                                style={{ width: '100%', fontSize: 13 }}
                                                floatingLabelText={'state'}
                                                value={this.state.state}
                                                onChange={this.handleState}
                                                errorText={this.state.errorState ? 'This field is required' : false}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style={{
                                margin: '1%', width: '100%',
                                height: 'auto', position: 'relative'
                            }} >
                                <div style={{ backgroundColor: '#FFFFFF', padding: '2% 1% 2% 7%', marginTop: '1%', borderRadius: 20 }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 22 }}>Office Culture</text>
                                    <TextField
                                        style={{ width: '100%', fontSize: 13 }}
                                        multiLine={true}
                                        value={this.state.officeCulture}
                                        onChange={this.handleOfficeCulture}
                                        errorText={this.state.errorOfficeCulture ? 'This field is required' : false}
                                    />
                                </div>
                            </div>
                        </div>
                        {
                            this.state.quotes ?
                                this.state.quotes.map((quoteList, idx) => (

                                    idx % 2 === 0 ?
                                        <div style={{ width: '100%', display: 'flex' }}>
                                            <div style={{ width: '50%', backgroundColor: 'white', padding: '2%', borderRadius: 20, margin: '1%' }}>
                                                <div>
                                                    <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuote(idx)} />
                                                    <TextField
                                                        style={{ width: '80%' }}
                                                        multiLine={true}
                                                        hintText={`Enter Quote${idx + 1} here`}
                                                        floatingLabelText={`Quote${idx + 1}`}
                                                        value={quoteList.quote}
                                                        onChange={this.handleQuoteChange(idx)}
                                                    />
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td style={{ position: 'relative' }}>
                                                                    <div onClick={this.handleOpenAvatarGallery('quotesAvatar', idx)}>
                                                                        <UserAvatar fileName={quoteList.avatar} size={60} />
                                                                        <div style={{
                                                                            position: 'absolute', right: '0', top: '75px', backgroundColor: '#045e7a', borderRadius: '50%',
                                                                            height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex',
                                                                            paddingTop: '5px'
                                                                        }} onClick={this.handleOpenAvatarGallery('quotesAvatar', idx)}>
                                                                            <CameraIcon size={15} color='white' />
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style={{ paddingLeft: '10%' }}>
                                                                    <TextField
                                                                        style={{ width: '80%' }}
                                                                        hintText={`Enter Name${idx + 1} here`}
                                                                        floatingLabelText={`Name${idx + 1}`}
                                                                        value={quoteList.name}
                                                                        onChange={this.handleNameChange(idx)}
                                                                    />
                                                                    <TextField
                                                                        style={{ width: '80%' }}
                                                                        hintText={`Enter Position${idx + 1} here`}
                                                                        floatingLabelText={`Position${idx + 1}`}
                                                                        value={quoteList.position}
                                                                        onChange={this.handlePositionChange(idx)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style={{ width: '50%', margin: '1%', position: 'relative' }}>
                                                <QuoteImg fileName={quoteList.quoteImg} />
                                                <div style={{
                                                    position: 'absolute', right: '2%', top: '13px', backgroundColor: 'black', borderRadius: '50%',
                                                    padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                                                }} onClick={this.handleOpenAvatarGallery('quotes', idx)}>
                                                    <CameraIcon size={15} color='white' />
                                                </div>
                                            </div>
                                        </div>
                                        :
                                        <div style={{ width: '100%', display: 'flex' }}>
                                            <div style={{ width: '50%', margin: '1%', position: 'relative' }}>
                                                <QuoteImg fileName={quoteList.quoteImg} />
                                                <div style={{
                                                    position: 'absolute', right: '2%', top: '13px', backgroundColor: 'black', borderRadius: '50%',
                                                    padding: '1%', height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex'
                                                }} onClick={this.handleOpenAvatarGallery('quotes', idx)}>
                                                    <CameraIcon size={15} color='white' />
                                                </div>
                                            </div>
                                            <div style={{ width: '50%', backgroundColor: 'white', padding: '2%', borderRadius: 20, margin: '1%' }}>
                                                <div>
                                                    <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuote(idx)} />
                                                    <TextField
                                                        style={{ width: '80%' }}
                                                        multiLine={true}
                                                        hintText={`Enter Quote${idx + 1} here`}
                                                        floatingLabelText={`Quote${idx + 1}`}
                                                        value={quoteList.quote}
                                                        onChange={this.handleQuoteChange(idx)}
                                                    />
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td style={{ position: 'relative' }}>
                                                                    <div onClick={this.handleOpenAvatarGallery('quotesAvatar', idx)}>
                                                                        <UserAvatar fileName={quoteList.avatar} size={60} />
                                                                        <div style={{
                                                                            position: 'absolute', right: '0', top: '75px', backgroundColor: '#045e7a', borderRadius: '50%',
                                                                            height: '30px', width: '30px', justifyContent: 'center', alignContent: 'center', display: 'flex',
                                                                            paddingTop: '5px'
                                                                        }} onClick={this.handleOpenAvatarGallery('quotesAvatar', idx)}>
                                                                            <CameraIcon size={15} color='white' />
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                                <td style={{ paddingLeft: '10%' }}>
                                                                    <TextField
                                                                        style={{ width: '80%' }}
                                                                        hintText={`Enter Name${idx + 1} here`}
                                                                        floatingLabelText={`Name${idx + 1}`}
                                                                        value={quoteList.name}
                                                                        onChange={this.handleNameChange(idx)}
                                                                    />
                                                                    <TextField
                                                                        style={{ width: '80%' }}
                                                                        hintText={`Enter Position${idx + 1} here`}
                                                                        floatingLabelText={`Position${idx + 1}`}
                                                                        value={quoteList.position}
                                                                        onChange={this.handlePositionChange(idx)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                ))
                                : false
                        }
                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                            <FlatButton
                                label="Add Quotes"
                                onClick={this.addNewQuotes.bind(this)}
                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                            /><br />
                        </div>
                    </div >
                </div >
            </div >
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        uid: state.authorize.uid,
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompPageBuilder)