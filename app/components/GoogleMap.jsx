import React, { Component } from 'react';
import ReactDOM from 'react-dom'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react';

//import InputMask from 'react-input-mask';  <InputMask mask="(0)999 999 99 99" maskChar=" " />

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */

export class GoogleMap extends Component {

    constructor(props) {
        super(props);

        const { lat, lng } = this.props.initialCenter;
        this.state = {
            currentLocation: {
                lat: lat,
                lng: lng
            }
        }
    }

    componentWillMount() {

    }

    componentDidMount() {
        if (this.props.centerAroundCurrentLocation) {
            if (navigator && navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((pos) => {
                    const coords = pos.coords;
                    this.setState({
                        currentLocation: {
                            lat: coords.latitude,
                            lng: coords.longitude
                        }
                    })
                })
            }
        }
        this.loadMap();
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.google !== this.props.google) {
            this.loadMap();
        }
        if (prevState.currentLocation !== this.state.currentLocation) {
            this.recenterMap();
        }
    }

    recenterMap() {
        const map = this.map;
        const curr = this.state.currentLocation;

        const google = this.props.google;
        const maps = google.maps;

        if (map) {
            let center = new maps.LatLng(curr.lat, curr.lng)
            map.panTo(center)
        }
    }

    loadMap() {
        //console.log(this.props.location)
        if (this.props && this.props.google) {
            // google is available
            const { google } = this.props;
            const maps = google.maps;
            const mapRef = this.refs.map;
            const node = ReactDOM.findDOMNode(mapRef);

            let { initialCenter, zoom } = this.props;

            if (this.props.location) {
                const geocoder = new maps.Geocoder();
                if (geocoder) {
                    geocoder.geocode({
                        'address': this.props.location
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            var lat = results[0].geometry.location.lat()
                            var lng = results[0].geometry.location.lng()

                            const center = new maps.LatLng(lat, lng);
                            const mapConfig = Object.assign({}, {
                                center: center,
                                zoom: zoom
                            })
                            this.map = new maps.Map(node, mapConfig);

                            var marker = new maps.Marker({
                                position: { lat: lat, lng: lng }
                            });
                            marker.setMap(this.map)
                        }
                    });
                }
            } else {
                const { lat, lng } = this.state.currentLocation;
                const center = new maps.LatLng(lat, lng);
                const mapConfig = Object.assign({}, {
                    center: center,
                    zoom: zoom
                })
                this.map = new maps.Map(node, mapConfig);
            }
        }
        // ...
    }

    render() {
        return (
            <div style={{ height: 300, width: '100%' }} ref='map'>
                Loading map...
            </div>
        )
    }
}

GoogleMap.propTypes = {
    google: React.PropTypes.object,
    zoom: React.PropTypes.number,
    initialCenter: React.PropTypes.object,
    centerAroundCurrentLocation: React.PropTypes.bool
}

GoogleMap.defaultProps = {
    zoom: 13,
    // San Francisco, by default
    initialCenter: {
        lat: 3.1844813,
        lng: 101.64759549999997
    },
    centerAroundCurrentLocation: true
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyAyesbQMyKVVbBgKVi2g6VX7mop2z96jBo'
})(GoogleMap)