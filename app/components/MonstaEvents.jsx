import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import { Table, Row, Cell } from 'react-responsive-table';
import Avatar from 'material-ui/Avatar'
import UserAvatar from 'UserAvatar'
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import Map from 'react-icons/lib/md/map'
import TimePicker from 'material-ui/TimePicker';
import '../styles/app.scss'
import '../styles/div.scss'

class MonstaEvents extends Component {

    constructor(props) {
        super(props)
        this.state = {
            campaignList: [],
            redirect: false,
            selectedKey: '',
            campaignKey: '',
            arrayList2: [],
            avatarURL: 'http://gomonsta.asia/wp-content/uploads/2017/11/monsta_logo.png'
        }
        this.CampInfo = this.CampInfo.bind(this)
    }

    componentWillMount() {

        var specialization = firebaseRef.child("staticData")
        specialization.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data.field)
            this.setState({ specializationList: dataKey, skillList: data.skills, coursesList: data.courses })
        })

        var myCampaignList = firebaseRef.child("campaigns");
        var dataSort = []
        myCampaignList.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            var arrayList = []
            dataKey.forEach(function (e) {
                if (!data[e].isDraft) {
                    var dataCamp = data[e]
                    var dataCampKey = Object.keys(dataCamp)
                    dataCampKey.forEach(function (i) {
                        if (dataCamp[i].typeOfCampaign) {
                            if (dataCamp[i].typeOfCampaign == 3) {
                                dataCamp[i].compUid = e
                                dataCamp[i].campUid = i
                                dataSort.push({
                                    data: dataCamp[i]
                                })
                            }
                        }
                    })
                }
            })

            dataSort.sort(function (a, b) {
                return new Date(b.data.date) - new Date(a.data.date);
            });

            dataSort.forEach(function (e) {
                var campaignPath = e.data.compUid + "/" + e.data.campUid;
                var availableSlot = 0
                e.data.campaignPath = campaignPath;

                if (e.data.targetStudent) {
                    if (e.data.ParticipantList) {
                        var partKey = Object.keys(e.data.ParticipantList)
                        availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                    } else {
                        availableSlot = e.data.targetStudent
                    }
                }

                arrayList.push(
                    <div style={{
                        margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                        height: 'auto', position: 'relative'
                    }} onClick={this.CampInfo(e.data.campaignPath)}>
                        <div className={'clickDiv'}>
                            <div style={{ position: 'absolute', left: '5%', top: '38px' }}>
                                <UserAvatar fileName={e.data.campAvatar}
                                    size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                            </div>
                            <div style={{
                                position: 'absolute', right: '5%', top: '55px',
                                backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                            }}>
                                <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                            </div>
                            <div style={{
                                position: 'absolute', right: '5%', top: '100px', padding: '1%'
                            }}>
                                {
                                    e.data.targetStudent ?
                                        availableSlot === 0 ?
                                            <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                                            :
                                            <text style={{ fontSize: '15', color: 'green' }}>{`${availableSlot}`}&nbsp;slot&nbsp;available</text>
                                        :
                                        <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                                }
                            </div>
                            <div style={{
                                backgroundColor: '#d3d3d3', width: '100%', height: '100px',
                                borderTopLeftRadius: 20, borderTopRightRadius: 20
                            }} ></div>
                            <div style={{
                                backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '10% 2% 2% 7%',
                                borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                            }}>
                                <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                                <br />
                                <div style={{ marginTop: '2%' }}>
                                    <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                        {
                                            e.data.description.length > 500 ?
                                                e.data.description.substring(0, 500) + '...'
                                                :
                                                e.data.description
                                        }
                                    </p>
                                </div>
                                <br />
                                <div>
                                    <table>
                                        <tr>
                                            <td><Calender /></td>
                                            <td>
                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventDate}</text>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><Time /></td>
                                            <td>
                                                <TimePicker
                                                    style={{ fontSize: 13, paddingLeft: '1%' }}
                                                    disabled={true}
                                                    format="ampm"
                                                    value={new Date(e.data.eventTime)}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><Map /></td>
                                            <td>
                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventAdd1}</text><br />
                                                {
                                                    e.data.eventAdd2 ?
                                                        <div>
                                                            <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventAdd2}</text><br />
                                                        </div>
                                                        :
                                                        false
                                                }
                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventCity}</text> <br />
                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventZip}, {e.data.eventState}</text>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                )

            }, this)
            this.setState({ campaignList: arrayList });
        })
    }

    CampInfo = (abc) => () => {
        this.setState({ redirect: true, campaignKey: abc })
    }

    render() {

        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />;
        }

        return (
            <div>
                <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                    <text style={{ fontSize: 20, fontWeight: 'bold' }}>Monsta&amp;Events</text>
                    <hr className="hr-text" />
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {this.state.campaignList}
                    </div>
                </div>
            </div>
        );
    }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(MonstaEvents)