// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { NavLink, withRouter } from 'react-router-dom'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import MenuItem from 'material-ui/MenuItem';
import { firebaseRef } from 'app/firebase/'
import Checkbox from 'material-ui/Checkbox';





// - Import actions
import *  as authorizeActions from 'authorizeActions'
import * as globalActions from 'globalActions'
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import {
  Step,
  Stepper,
  StepButton,
  StepLabel
} from 'material-ui/Stepper';





// - Create Signup componet class
export class SignupStudent extends Component {

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props);

    this.state = {
      fullNameInput: '',
      fullNameInputError: '',
      userName: '',
      emailInput: '',
      emailInputError: '',
      passwordInput: '',
      passwordInputError: '',
      confirmInput: '',
      confirmInputError: '',
      role: "student",
      birthDate: '',
      mobileNumber: '',
      university: '',
      graduationDate: '',
      course: '',
      major: '',
      careerInterest: '',
      describeYourself: '',
      summary: '',
      lookingFor: '',
      address: '',
      area: '',
      city: '',
      postcode: '',
      state: '',
      country: 'Malaysia',
      loading: false,
      finished: false,
      stepIndex: 0,
      checked: false,
      nationality:'',
      gender:''

    }
    // Binding function to `this`
    this.handleForm = this.handleForm.bind(this)

  }

  /**
   * Handle data on input change
   * @param  {event} evt is an event of inputs of element on change
   */
  handleInputChange = (evt) => {
    const target = evt.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value
    })


    switch (name) {
      case 'fullNameInput':
        this.setState({
          fullNameInputError: '',
        })
        break
      case 'emailInput':
        this.setState({
          emailInputError: ''
        })
        break
      case 'passwordInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
      case 'confirmInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
      case 'checkInput':
        this.setState({
          checkInputError: ''
        })
        break;
      default:


    }
  }
  handleChangeDropdownCourse = (event, index, course) => this.setState({ course:course })
  handleChangeDropdownMajor = (event, index, major) => this.setState({ major:major });
  handleChangeuniversity = (event, index, university) => this.setState({ university:university });
  handleChangeGender = (event,index,gender)=>this.setState({gender:gender})
  handleLookingFor = (event, index, value) => this.setState({ lookingFor: value });
  handleChangeDate = (event, date) => {
    this.setState({ graduationDate: date, });
  };

  handleChangeBirthDate = (event, date) => {
    this.setState({ birthDate: date, });
  };

  dummyAsync = (cb) => {
    this.setState({loading: true}, () => {
      this.asyncTimer = setTimeout(cb, 500);
    });
  };

  updateCheck=() =>{
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }

  handleNext = () => {
    const {stepIndex} = this.state;
    if (!this.state.loading) {
      this.dummyAsync(() => this.setState({
        loading: false,
        stepIndex: stepIndex + 1,
        finished: stepIndex >= 2,
      }));
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (!this.state.loading) {
      this.dummyAsync(() => this.setState({
        loading: false,
        stepIndex: stepIndex - 1,
      }));
    } 
  };

  getStepContent(stepIndex) {
    console.log(stepIndex)
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <TextField
                onChange={this.handleInputChange}
                errorText={this.state.fullNameInputError}
                name="fullNameInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Full Name"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.fullNameInputError}
                name="userName"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="User Name"
                type="text"
              /><br />
                 <TextField
                onChange={this.handleInputChange}
                name="mobileNumber"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Mobile Number"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="emailInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Email"
                type="email"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                name="gender"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Gender"
                type="text"
              /><br />
               <SelectField style={{ width: '60%' }} value={this.state.gender} onChange={this.handleChangeGender} autoWidth={true} floatingLabelText="Gender" >
               <MenuItem value='' primaryText='' />
                <MenuItem value="female" primaryText="Female" />
                <MenuItem value="male" primaryText="Male" />

              </SelectField><br />
               <DatePicker hintText="Birth Date" openToYearSelection={true} onChange={this.handleChangeBirthDate} /><br />
               <SelectField style={{ width: '80%' }} value={this.state.country} labelStyle={{ textAlign: 'left', color: "gray" }} onChange={this.handleChangeDropdownCountry} autoWidth={true} floatingLabelText="Country" >
                <MenuItem value="Malaysia" primaryText="Malaysia" />
                <MenuItem value="Singapore" primaryText="Singapore" />
                <MenuItem value="China" primaryText="China" />
                <MenuItem value="Japan" primaryText="Japan" />
                <MenuItem value="South Korea" primaryText="South Korea" />
                <MenuItem value="Cambodia" primaryText="Cambodia" />
                <MenuItem value="Thailand" primaryText="Thailand" />
                <MenuItem value="Indonesia" primaryText="Indonesia" />
                <MenuItem value="Vietnam" primaryText="Vietnam" />
                <MenuItem value="Australia" primaryText="Australia" />
                <MenuItem value="New Zealand" primaryText="New Zealand"/>

              </SelectField><br />
             
          </div>
        );
      case 1:
        return (
          <div>
              <SelectField style={{ width: '60%' }} value={this.state.university} onChange={this.handleChangeuniversity} autoWidth={true} floatingLabelText="University" >
                <MenuItem value="MMU" primaryText="MMU" />
                <MenuItem value="UNITEN" primaryText="UNITEN" />
                <MenuItem value="Universiti Malaysia" primaryText="Universiti Malaysia" />
                <MenuItem value="UTP" primaryText="UTP" />
                <MenuItem value="KBU University College" primaryText="KBU University College" />
                <MenuItem value="First City University College" primaryText="First City University College" />
                <MenuItem value="HELP University" primaryText="HELP University" />
                <MenuItem value="Nilai University" primaryText="Nilai University" />
                <MenuItem value="INTI University" primaryText="INTI University" />
                <MenuItem value="Sunway University" primaryText="Sunway University" />
                <MenuItem value="Taylor's University" primaryText="Taylor's University" />
                <MenuItem value="UTAR" primaryText="UTAR" />
                <MenuItem value="TARC" primaryText="TARC" />
                <MenuItem value="University Malaya" primaryText="University Malaya" />
                <MenuItem value="Monash University" primaryText="Monash University" />
                <MenuItem value="Management and Science University" primaryText="Management and Science University" />
                <MenuItem value="UCSI University" primaryText="UCSI University" />
                <MenuItem value="Asia Pacific University of Technology and Innovation" primaryText="Asia Pacific University of Technology and Innovation" />
                <MenuItem value="International Medical University" primaryText="International Medical University" />
                <MenuItem value="Xiamen University" primaryText="Xiamen University" />
              </SelectField><br />

               <TextField
                onChange={this.handleInputChange}
                errorText={this.state.fullNameInputError}
                name="university"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="If your university is not in the list,please state here"
                type="text"
              /><br />

              <DatePicker hintText="Graduation Date" openToYearSelection={true} onChange={this.handleChangeDate} /><br />
              
          {/*<SelectField style={{ width: '80%' }} value={this.state.course} labelStyle={{ textAlign: 'left', color: "gray" }} onChange={this.handleChangeDropdownCourse} autoWidth={true} floatingLabelText="Course" >
                <MenuItem value="Software Enginnering" primaryText="Software Enginnering" />
                <MenuItem value="Bussiness Management" primaryText="Bussiness Management" />
                <MenuItem value="IT Security" primaryText="IT Security" />
                <MenuItem value="Multimedia Design" primaryText="Multimedia Design" />
                <MenuItem value="Electrical Engineering" primaryText="Electrical Engineering" />
              </SelectField><br /> */}
              <TextField
                onChange={this.handleInputChange}
                name="course"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Course"
                type="text"
              /><br />

{/*           <SelectField style={{ width: '80%' }} value={this.state.major} labelStyle={{ textAlign: 'left', color: "gray" }} onChange={this.handleChangeDropdownMajor} autoWidth={true} floatingLabelText="Major" >
                <MenuItem value="Accounting" primaryText="Accounting" />
                <MenuItem value="Agriculture" primaryText="Agriculture" />
                <MenuItem value="Arts" primaryText="Arts" />
                <MenuItem value="Biology " primaryText="Biology " />
                <MenuItem value="Biochemistry " primaryText="Biochemistry " />
                <MenuItem value="Bussiness " primaryText="Bussiness " />
                <MenuItem value="Computer Science" primaryText="Computer Science" />
                <MenuItem value="Bussiness Management" primaryText="Bussiness Management" />
                <MenuItem value="Cyber Security" primaryText="Cyber Security" />
                <MenuItem value="Multimedia" primaryText="Multimedia" />
                <MenuItem value="Electrical" primaryText="Electrical" />
                <MenuItem value="Ecology" primaryText="Ecology" />
                <MenuItem value="Economics" primaryText="Economics" />
                <MenuItem value="Environmental Sciences" primaryText="Economics" />
                <MenuItem value="Food Sciences" primaryText="Food Sciences" />
                <MenuItem value="Geography" primaryText="Geography" />
                <MenuItem value="History" primaryText="History" />
                <MenuItem value="Marketing" primaryText="Marketing" />
                <MenuItem value="Digital Marketing" primaryText="Digital Marketing" />
                <MenuItem value="Mathematics " primaryText="Mathematics" />
                <MenuItem value="Nutritional and Nutraceutical Sciences" primaryText="Nutritional and Nutraceutical Sciences" />
                <MenuItem value="Philosophy" primaryText="Philosophy" />
                <MenuItem value="Physics" primaryText="Physics" />
                <MenuItem value="Political Science" primaryText="Political Science" />
                <MenuItem value="Psychology" primaryText="Psychology" />
                <MenuItem value="Software Engineering" primaryText="Software Engineering" />
                <MenuItem value="Zoology" primaryText="Zoology" />
              </SelectField><br /> */}
  
          </div>
        );
        case 2:
              return(
              <div>
                 <TextField
                onChange={this.handleInputChange}
                name="describeYourself"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Describe Yourself in One Sentence"
                type="text"
              /><br />
                 <SelectField value={this.state.lookingFor} onChange={this.handleLookingFor} floatingLabelText="Looking For" >
                <MenuItem value={1} primaryText="Part-time" />
                <MenuItem value={2} primaryText="Full-time" />
                <MenuItem value={3} primaryText="Internship" />
                <MenuItem value={4} primaryText="Volunteers" />
                <MenuItem value={5} primaryText="Freelance" />
              </SelectField><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.passwordInputError}
                name="passwordInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Password"
                type="password"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.confirmInputError}
                name="confirmInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Confirm Password"
                type="password"
              /><br />
                      
                      <Checkbox
                label={<div><a href='http://gomonsta.asia/terms-and-conditions/'>Terms and Conditions</a></div>}
                checked={this.state.checked}
                onCheck={this.updateCheck.bind(this)}
              />
                </div>)
      default:
        return 'Somethings Wrong';
    }
  }

  renderContent() {
    const {finished, stepIndex} = this.state;
    const contentStyle = {margin: '0 16px', overflow: 'hidden'};

    if (finished) {
      return (
        <div style={contentStyle}>
          <p>
            <a
              href="#"
              onClick={(event) => {
                event.preventDefault();
                this.setState({stepIndex: 0, finished: false});
              }}
            >
            </a>
          </p>
        </div>
      );
    }

    return (
      <div style={contentStyle}>
        <div>{this.getStepContent(stepIndex)}</div>
        <div style={{marginTop: 24, marginBottom: 12}}>
          <FlatButton
            label="Back"
            onClick={stepIndex === 0?this.props.loginPage: this.handlePrev}
            style={{marginRight: 12}}
          />
          <RaisedButton
            label={stepIndex === 2 ? 'Create' : 'Next'}
            primary={true}
            onClick={stepIndex === 2 ? this.handleForm:this.handleNext}
          />
        </div>
      </div>
    );
  }
  /**
   * Handle register form
   */
  handleForm = () => {

    if(!this.state.checked){
      alert("Please agree to the Terms and Conditions")
    }
    var error = false
    if (this.state.fullNameInput === '') {
      this.setState({
        fullNameInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }
    if (this.state.emailInput === '') {
      this.setState({
        emailInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }
    if (this.state.passwordInput === '') {
      this.setState({
        passwordInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})


    }
    if (this.state.confirmInput === '') {
      this.setState({
        confirmInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})
    }
    else if (this.state.confirmInput !== this.state.passwordInput) {
      this.setState({
        passwordInputError: 'This field sould be equal to confirm password',
        confirmInputError: 'This field sould be equal to password'
      })

    }
    if (!error) {
      this.props.register({
        email: this.state.emailInput,
        password: this.state.passwordInput,
        fullName: this.state.fullNameInput,
        userName: this.state.userName,
        role: this.state.role,
        mobileNumber: this.state.mobileNumber,
        nationality:this.state.nationality,
        university: this.state.university,
        course: this.state.course,
        major: this.state.major,
        graduationDate: this.state.graduationDate.toString(),
        birthDate: this.state.birthDate.toString(),
        careerInterest: this.state.careerInterest,
        describeYourself: this.state.describeYourself,
        summary: this.state.summary,
        address: this.state.address,
        area: this.state.area,
        city: this.state.city,
        lookingFor: this.state.lookingFor,
        postcode: this.state.postcode,
        state: this.state.state,
        country: this.state.country,
        monstaXP: 200,
        coverAvatar:''
      })
    }
    if (this.props.refferalUid.length > 0) {
      var addXP = firebaseRef.child(`users/${this.props.refferalUid}/info`)
      var userXP = 0

      var addXP = firebaseRef.child(`users/${this.props.refferalUid}/info`)
      addXP.on('value', snapshot => {
        userXP = snapshot.val().monstaXP
      });
      addXP.update({
        monstaXP: userXP + 50
      })
    }

  }

  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {
    const paperStyle = {
      minHeight: 'auto',
      padding:'5%',
      width: '60%',
      margin: 20,
      textAlign: 'left',
      display: 'flex',
      margin: "auto"
    };


    const {loading, stepIndex} = this.state;

    const contentStyle = {margin: '0 16px'};

    return (

      <div>

        <h1 style={{
          textAlign: "center",
          padding: "20px",
          fontSize: "30px",
          fontWeight: 500,
          lineHeight: "32px",
          margin: "auto",
          color: "rgba(138, 148, 138, 0.2)"
        }}>Monsta</h1>

        <div className="animate-bottom">
          <Paper style={paperStyle} zDepth={1} rounded={false} >
             <div style={{width: '100%', margin: 'auto'}}>
                <Stepper activeStep={stepIndex}>
                <Step>
                  <StepLabel>Personal Details</StepLabel>
                </Step>
                <Step>
                  <StepLabel>University/College Details</StepLabel>
                </Step>
                <Step>
                  <StepLabel>About Yourself</StepLabel>
                </Step>
              </Stepper>
              <ExpandTransition loading={loading} open={true}>
                {this.renderContent()}
              </ExpandTransition>
            </div>
          </Paper>
        </div>
      </div>

    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    showError: (message) => {
      dispatch(action(types.SHOW_ERROR_MESSAGE_GLOBAL)(error.message))
    },
    register: (data) => {
      dispatch(authorizeActions.dbSignup(data))
    },
    loginPage: () => {
      dispatch(push("/login"))
    }
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
  return {

  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignupStudent))
