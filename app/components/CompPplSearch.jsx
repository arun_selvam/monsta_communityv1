import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import { firebaseRef } from 'app/firebase/'
import Calendar from 'react-icons/lib/go/calendar'
//import Pin from 'react-icons/lib/fa/map-marker'
import Uni from 'react-icons/lib/go/home'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router-dom'
import moment from 'moment'
import Course from 'react-icons/lib/go/book'
import SelectField from 'material-ui/SelectField';
import UserAvatar from 'UserAvatar'
import Pagination from 'Pagination';
import '../styles/div.scss'
/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
const styles = {
    underlineStyle: {
        borderColor: '#70E0F2',
    }
};

class CompPplSearch extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            searchResult: [],
            allStudent: [],
            displayStudent: [],
            fullNameKey: '',
            courseKey: '',
            uniKey: '',
            stateMalaysia: 0,
            cityMalaysia: ['Selangor', 'Kuala Lumpur', 'Sarawak', 'Johor', 'Pulau Pinang', 'Sabah', 'Perak', 'Pahang', 'Negeri Sembilan', 'Kedah', 'Melaka', 'Terengganu', 'Kelantan', 'Perlis'],
            listMenuItem: [],
            uid: '',
            isDirect: false,
            yearKey: '',
            skillList: [],
            skillCanLearn: [],
            lookingFor: '',
            pageOfItems: []
        }
        this.onChangePage = this.onChangePage.bind(this);
    }

    renderStu(uid) {
        var allUser = firebaseRef.child(`users`)
        var students = []
        var allStudent = []
        var arrayData = []
        var credit
        allUser.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            dataKey.forEach(function (e) {
                if (data[e].info) {
                    if (data[e].info.role) {
                        if (data[e].info.role === 'student') {
                            allStudent.push({ uid: e, data: data[e].info })
                            if (uid) {
                                if (uid.includes(e)) {
                                    students.push({ uid: e, data: data[e].info })
                                    /*arrayData.push(
                                        <div style={{ width: (window.innerWidth < 1000 ? '100%' : (window.innerWidth < 1200 ? '48%' : '30%')), backgroundColor: 'white', margin: '1%', padding: '1%' }} onClick={this.handleUserProfile(e)}>
                                            <table>
                                                <tr>
                                                    <td style={{ width: '10%' }}>
                                                        <UserAvatar fileName={data[e].info.avatar} size={90} />
                                                    </td>
                                                    <td style={{ width: '20%' }}>
                                                        <text style={{ fontWeight: 'bold' }}>{data[e].info.fullName}</text><br />
                                                        {
                                                            !data[e].info.course ?
                                                                false
                                                                :
                                                                <div>
                                                                    <Course />
                                                                    <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.course}</text><br />
                                                                </div>
                                                        }
                                                        {
                                                            !data[e].info.university ?
                                                                false
                                                                :
                                                                <div>
                                                                    <Uni />
                                                                    <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.university}</text><br />
                                                                </div>
                                                        }
                                                        {
                                                            !data[e].info.graduationDate ?
                                                                false
                                                                :
                                                                <div>
                                                                    <Calendar />
                                                                    <text style={{ marginLeft: '5%', fontSize: 11 }}>{moment(data[e].info.graduationDate).format('YYYY')}</text><br />
                                                                </div>
                                                        }
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    )*/
                                }
                            }
                            else {
                                students.push({ uid: e, data: data[e].info })
                                /*arrayData.push(
                                    <div style={{ width: (window.innerWidth < 1000 ? '100%' : (window.innerWidth < 1200 ? '48%' : '30%')), backgroundColor: 'white', margin: '1%', padding: '1%' }} onClick={this.handleUserProfile(e)}>
                                        <table>
                                            <tr>
                                                <td style={{ width: '10%' }}>
                                                    <UserAvatar fileName={data[e].info.avatar} size={90} />
                                                </td>
                                                <td style={{ width: '20%' }}>
                                                    <text style={{ fontWeight: 'bold' }}>{data[e].info.fullName}</text><br />
                                                    {
                                                        !data[e].info.course ?
                                                            false
                                                            :
                                                            <div>
                                                                <Course />
                                                                <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.course}</text><br />
                                                            </div>
                                                    }
                                                    {
                                                        !data[e].info.university ?
                                                            false
                                                            :
                                                            <div>
                                                                <Uni />
                                                                <text style={{ marginLeft: '5%', fontSize: 11 }}>{data[e].info.university}</text><br />
                                                            </div>
                                                    }
                                                    {
                                                        !data[e].info.graduationDate ?
                                                            false
                                                            :
                                                            <div>
                                                                <Calendar />
                                                                <text style={{ marginLeft: '5%', fontSize: 11 }}>{moment(data[e].info.graduationDate).format('YYYY')}</text><br />
                                                            </div>
                                                    }
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                )*/
                            }
                        }
                    }
                }
            }, this)
            this.setState({ displayStudent: arrayData, allStudent: allStudent, searchResult: students })
        })
    }

    componentWillMount() {
        this.props.loadPeople()
        this.renderStu('', '')

        var specialization = firebaseRef.child("staticData")
        specialization.on('value', snapshot => {
            var skills = snapshot.val().skills
            skills.unshift('All')
            this.setState({ skillList: skills })
        })

        /*var arrayState = []
        for (var i = 0; i < this.state.cityMalaysia.length; i++) {
            arrayState.push(
                <MenuItem value={i} primaryText={this.state.cityMalaysia[i]} />
            )
        }
        
        this.setState({ listMenuItem: arrayState })*/
    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    handleLookingFor = (event, index, value) => this.setState({ lookingFor: value });

    handleChange = (event) => {
        this.setState({
            fullNameKey: event.target.value,
        });
    };

    handleCourseChange = (event) => {
        this.setState({
            courseKey: event.target.value,
        });
    };

    handleUniChange = (event) => {
        this.setState({
            uniKey: event.target.value,
        });
    };

    handleGraYear = (event) => {
        if (!isNaN(event.target.value)) {
            this.setState({
                yearKey: event.target.value,
            });
        }
    }

    handleMonstaXP = (event, index, value) => this.setState({ stateMalaysia: value });

    updateSkillCanLearn = (event, index, values) => {
        if (values.includes(-1)) {
            if (values.indexOf(-1) === 0 && values.length > 1) {
                values.splice(0, 1)
                this.setState({ skillCanLearn: values })
            } else {
                this.setState({ skillCanLearn: [-1] })
            }
        }
        else {
            this.setState({ skillCanLearn: values })
        }
    };

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handleSearch = () => {
        var uid = []
        var arrayData = []
        var course = this.state.courseKey
        var uni = this.state.uniKey
        var year = this.state.yearKey
        var skills = this.state.skillCanLearn
        var lookingFor = this.state.lookingFor
        var a = this.state.allStudent
        var results = []
        if (course || uni || year || skills.length > 0 || lookingFor) {
            results = this.state.allStudent.filter(function (e) {
                return (e.data.university ? e.data.university.toLowerCase().indexOf(uni.toLowerCase()) !== -1 : 0)
                    && (e.data.course ? e.data.course.toLowerCase().indexOf(course.toLowerCase()) !== -1 : 0)
                    && (year ? (e.data.graduationDate ? moment(e.data.graduationDate).format('YYYY').indexOf(year) !== -1 : 0) : 1)
                    && (lookingFor ? (e.data.lookingFor ? (e.data.lookingFor === lookingFor ? true : false) : 0) : 1)
                    && (skills.length > 0 ?
                        !skills.includes(-1) ?
                            (e.data.skills ?
                                e.data.skills.length > 0 ?
                                    e.data.skills.some(function (v) {
                                        return skills.indexOf(v) >= 0;
                                    })
                                    : 0
                                : 0)
                            : 1
                        : 1)
                //(e.data.fullName.toLowerCase().indexOf(fullName.toLowerCase()) !== -1) && 
            })
        }
        else {
            results = this.state.allStudent
        }
        results.forEach(function (e) {
            uid.push(e.uid)
        }, this)

        this.renderStu(uid)
    };

    render() {
        if (this.state.isDirect) {
            return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }

        return (
            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
            <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 800 ? '0%' : '0%') }}>
            <div style={{ padding: '1%' }}>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                    <table style={{ width: '100%' }}>
                        <tr>
                            <td style={{ width: '85%', display: 'flex', flexWrap: 'wrap' }}>
                                {/*<div style={{ marginRight: '1%' }}>
                        <TextField
                            floatingLabelText="Full Name"
                            hintText="Full Name"
                            value={this.state.fullNameKey}
                            onChange={this.handleChange}
                            underlineStyle={styles.underlineStyle}
                        />
        </div>*/}
                                <div style={{ marginRight: '1%' }}>
                                    <TextField
                                        floatingLabelText="Course"
                                        hintText="Course"
                                        value={this.state.courseKey}
                                        onChange={this.handleCourseChange}
                                        underlineStyle={styles.underlineStyle}
                                    />
                                </div>
                                <div style={{ marginRight: '1%' }}>
                                    <TextField
                                        floatingLabelText="University"
                                        hintText="University"
                                        value={this.state.uniKey}
                                        onChange={this.handleUniChange}
                                        underlineStyle={styles.underlineStyle}
                                    />
                                </div>
                                <div style={{ marginRight: '1%' }}>
                                    <TextField
                                        floatingLabelText="Graduation Year"
                                        hintText="Graduation Year"
                                        value={this.state.yearKey}
                                        onChange={this.handleGraYear}
                                        underlineStyle={styles.underlineStyle}
                                    />
                                </div>
                                <div>
                                    <SelectField
                                        multiple={true}
                                        floatingLabelText="Skills"
                                        hintText="Skills"
                                        value={this.state.skillCanLearn}
                                        onChange={this.updateSkillCanLearn}
                                    >
                                        {this.state.skillList.map((e, idx) => (
                                            <MenuItem
                                                key={e}
                                                insetChildren={true}
                                                checked={this.state.skillCanLearn.indexOf(idx - 1) > -1}
                                                value={idx - 1}
                                                primaryText={e}
                                            />
                                        ))}
                                    </SelectField>
                                </div>
                                <div>
                                    <SelectField value={this.state.lookingFor} onChange={this.handleLookingFor} floatingLabelText="Looking For" >
                                        <MenuItem value={''} primaryText="" />
                                        <MenuItem value={1} primaryText="Part-time" />
                                        <MenuItem value={2} primaryText="Full-time" />
                                        <MenuItem value={3} primaryText="Internship" />
                                        <MenuItem value={4} primaryText="Volunteers" />
                                        <MenuItem value={5} primaryText="Freelance" />
                                    </SelectField>
                                </div>
                                {/*<div>
                        <text>Location</text><br />
                        <DropDownMenu value={this.state.stateMalaysia} onChange={this.handleMonstaXP}>
                            {this.state.listMenuItem}
                        </DropDownMenu>
                    </div>*/}
                            </td>
                            <td style={{ width: '15%' }}>
                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                    <RaisedButton label="Search" primary={true} onClick={this.handleSearch} />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <hr style={{ width: '100%' }} />
                    <text style={{ width: '100%', paddingBottom: '1%', fontWeight: 'bold' }}>{this.state.searchResult.length} found</text>
                    {
                        this.state.searchResult.length ?
                            this.state.pageOfItems.map(e =>
                                <div className={'clickDiv'} style={{
                                    width: (window.innerWidth < 1000 ? '100%' : '48%'), backgroundColor: 'white', margin: '0.5%',
                                    padding: '1%', borderRadius: 20
                                }} onClick={this.handleUserProfile(e.uid)}>
                                    <table>
                                        <tr>
                                            <td style={{ width: '10%' }}>
                                                <UserAvatar fileName={e.data.avatar} size={90} />
                                            </td>
                                            <td style={{ width: '20%' }}>
                                                <text style={{ fontWeight: 'bold' }}>{e.data.fullName}</text><br />
                                                <text style={{ fontWeight: 'bold', fontSize: 11 }}>Looking for: {
                                                    e.data.lookingFor === 1 ?
                                                        'Part-time'
                                                        :
                                                        e.data.lookingFor === 2 ?
                                                            'Full-time'
                                                            :
                                                            e.data.lookingFor === 3 ?
                                                                'Internship'
                                                                :
                                                                e.data.lookingFor === 4 ?
                                                                    'Volunteers'
                                                                    :
                                                                    e.data.lookingFor === 5 ?
                                                                        'Freelance'
                                                                        : ''}</text>
                                                {
                                                    !e.data.course ?
                                                        false
                                                        :
                                                        <div>
                                                            <Course />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{e.data.course}</text><br />
                                                        </div>
                                                }
                                                {
                                                    !e.data.university ?
                                                        false
                                                        :
                                                        <div>
                                                            <Uni />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{e.data.university}</text><br />
                                                        </div>
                                                }
                                                {
                                                    !e.data.graduationDate ?
                                                        false
                                                        :
                                                        <div>
                                                            <Calendar />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{moment(e.data.graduationDate).format('YYYY')}</text><br />
                                                        </div>
                                                }
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            )
                            : false
                    }
                </div>
                {
                    this.state.searchResult.length ?
                        <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                            <Pagination items={this.state.searchResult} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                        </div>
                        :
                        false
                }
            </div>
            </div>
            </div>
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        loadPeople: () => dispatch(userActions.dbGetPeopleInfo()),
        getImage: (name) => dispatch(imageGalleryActions.dbDownloadImage(name))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompPplSearch)