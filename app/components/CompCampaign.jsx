import React, { Component } from 'react';
import {
    Step,
    Stepper,
    StepLabel,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Checkbox from 'material-ui/Checkbox';
import { List, ListItem } from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Avatar from 'material-ui/Avatar';
import CurrencyInput from 'react-currency-input';
import { Redirect } from 'react-router-dom'
import '../styles/app.scss'
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import TimePicker from 'material-ui/TimePicker';
import Toggle from 'material-ui/Toggle';
import YouTube from 'react-youtube';
import UserAvatar from 'UserAvatar'
import Dialog from 'material-ui/Dialog';
import DialogTitle from 'DialogTitle'
import ImageGallery from 'ImageGallery'
import CameraIcon from 'react-icons/lib/fa/camera'
import CoverAvatar from 'CoverAvatar'

//import InputMask from 'react-input-mask';  <InputMask mask="(0)999 999 99 99" maskChar=" " />

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */

const todayDate = new Date();

class CompCampaign extends Component {

    // Constructor
    constructor(props) {
        super(props)

        // Default state
        this.state = {
            campKey: '',
            compName: '',
            finished: false,
            stepIndex: 0,
            typeOfCampaign: 1,
            typeOfSubCampaign: 1,
            monstaXp: 175,
            todayDate: todayDate,
            specializationList: [],
            aptitudeTest: false,
            skillCanLearn: [],
            requiredSkills: [],
            data: [],
            campaignList: '',
            minAmount: 0,
            maxAmount: 0,
            windowWidth: window.innerWidth,
            formWidth: '',
            listWidth: '',
            campaignTitle: '',
            campaignDesc: '',
            requirements: '',
            benefits: '',
            privacy: false,
            responsibilities: '',
            questionList: [],
            currentTest: 0,
            maxTestGenerate: 5,
            noAnswer: 0,
            maxAnswer: 4,
            eventTicketPrice: 0,
            eventDate: todayDate,
            eventTime: todayDate,
            isTShirt: false,
            surveyDesc: '',
            specialization: '',
            addReward: '',
            skillList: [],
            salaryType: '',
            minEducation: '',
            coursesList: [],
            requiredCourses: [],
            workLocation: [],
            opts: {
                height: '390',
                width: '640',
                playerVars: {
                    autoplay: 1
                }
            },
            videoId: '',
            isVideo: false,
            url: '',
            submitMethod: '',
            eventAdd1: '',
            eventAdd2: '',
            eventCity: '',
            eventState: '',
            eventZip: '',
            ticket: false,
            isName: false,
            isAge: false,
            isGender: false,
            isContact: false,
            isEmail: false,
            isCourse: false,
            isUni: false,
            selectedKey: '',
            isNavigate: false,
            companyInfo: '',
            errorCampaignTitle: false,
            errorCampaignDesc: false,
            errorMonstaXp: false,
            targetStudent: '',
            errorTagetStudent: false,
            errorSkillCanLearn: false,
            errorSpecialization: false,
            errorRequirements: false,
            errorResponsibilities: false,
            errorBenefits: false,
            credit: 0,
            isGreater: false,
            errorSalaryType: false,
            errorMinEducation: false,
            errorRequiredCourses: false,
            errorRequiredSkills: false,
            errorWorkLocation: [],
            errorQuestionList: [],
            errorIsVideo: false,
            errorSubmitMethod: false,
            errorEventAdd1: false,
            errorEventCity: false,
            errorEventState: false,
            errorEventZip: false,
            errorEventTicket: false,
            errorEndDate: false,
            isPurchase: false,
            avatar: '',
            openAvatar: false,
            companyAvatar: '',
            startDate: todayDate,
            endDate: todayDate
        }
        this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
    }

    componentWillMount() {
        if (window.innerWidth < 600) {
            this.setState({ windowWidth: window.innerWidth + 400, formWidth: '500px', listWidth: '520px' });
        }
        else if (window.innerWidth < 800) {
            this.setState({ formWidth: '500px', listWidth: '520px' });
        }
        else if (window.innerWidth < 1200) {
            this.setState({ formWidth: '700px', listWidth: '750px' });
        }
        else {
            this.setState({ formWidth: '800px', listWidth: '820px' });
        }

        var specialization = firebaseRef.child("staticData")
        specialization.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data.field)
            this.setState({ specializationList: dataKey, skillList: data.skills, coursesList: data.courses })
        }, this)

        var myCampaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0])
        myCampaignList.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            var array = []
            dataKey.forEach(function (e) {
                if (data[e].isDraft) {
                    this.setState({
                        campKey: e, Company: data[e].Company ? data[e].Company : this.state.compName,
                        campaignTitle: data[e].campaignTitle ? data[e].campaignTitle : this.state.campaignTitle,
                        campaignDesc: data[e].description ? data[e].description : this.state.campaignTitle,
                        typeOfCampaign: data[e].typeOfCampaign ? data[e].typeOfCampaign : this.state.typeOfCampaign,
                        typeOfSubCampaign: data[e].typeOfSubCampaign ? data[e].typeOfSubCampaign : this.state.typeOfSubCampaign,
                        monstaXp: data[e].monstaXP ? data[e].monstaXP : this.state.monstaXp,
                        addReward: data[e].additionalReward ? data[e].additionalReward : this.state.addReward,
                        targetStudent: data[e].targetStudent ? data[e].targetStudent : this.state.targetStudent,
                        specialization: data[e].specialization ? data[e].specialization : this.state.specialization,
                        skillCanLearn: data[e].skillCanLearn ? data[e].skillCanLearn : this.state.skillCanLearn,
                        requirements: data[e].requirements ? data[e].requirements : this.state.requirements,
                        responsibilities: data[e].responsibilities ? data[e].responsibilities : this.state.responsibilities,
                        benefits: data[e].benefits ? data[e].benefits : this.state.benefits,
                        minAmount: data[e].minPayout ? data[e].minPayout : this.state.minAmount,
                        maxAmount: data[e].maxPayout ? data[e].maxPayout : this.state.maxAmount,
                        privacy: data[e].isPrivacy ? data[e].isPrivacy : this.state.privacy,
                        salaryType: data[e].salaryType ? data[e].salaryType : this.state.salaryType,
                        minEducation: data[e].minEducation ? data[e].minEducation : this.state.minEducation,
                        requiredCourses: data[e].requiredCourses ? data[e].requiredCourses : this.state.requiredCourses,
                        requiredSkills: data[e].requiredSkills ? data[e].requiredSkills : this.state.requiredSkills,
                        workLocation: data[e].workLocation ? data[e].workLocation : this.state.workLocation,
                        aptitudeTest: data[e].aptitudeTest ? data[e].aptitudeTest : this.state.aptitudeTest,
                        questionList: data[e].questionList ? data[e].questionList : this.state.questionList,
                        isVideo: data[e].isVideo ? data[e].isVideo : this.state.isVideo,
                        videoId: data[e].videoId ? data[e].videoId : this.state.videoId,
                        submitMethod: data[e].submitMethod ? data[e].submitMethod : this.state.submitMethod,
                        eventTime: data[e].eventTime ? new Date(data[e].eventTime) : this.state.eventTime,
                        eventDate: data[e].eventDate ? new Date(data[e].eventDate) : this.state.eventDate,
                        eventAdd1: data[e].eventAdd1 ? data[e].eventAdd1 : this.state.eventAdd1,
                        eventAdd2: data[e].eventAdd2 ? data[e].eventAdd2 : this.state.eventAdd2,
                        eventCity: data[e].eventCity ? data[e].eventCity : this.state.eventCity,
                        eventState: data[e].eventState ? data[e].eventState : this.state.eventState,
                        eventZip: data[e].eventZip ? data[e].eventZip : this.state.eventZip,
                        ticket: data[e].isTicket ? data[e].isTicket : this.state.ticket,
                        eventTicketPrice: data[e].ticketPrice ? data[e].ticketPrice : this.state.eventTicketPrice,
                        avatar: data[e].campAvatar ? data[e].campAvatar : '',
                        companyAvatar: data[e].companyAvatar ? data[e].companyAvatar : '',
                        startDate: data[e].startDate ? new Date(data[e].startDate) : this.state.startDate,
                        endDate: data[e].endDate ? new Date(data[e].endDate) : this.state.endDate,
                    })
                }
                else {
                    array.push({ data: data[e], key: e })
                }
            }, this)
            this.setState({ campaignList: array })
        }, this)

        var companyInfo = firebaseRef.child("users").child(Object.keys(this.props.peopleInfo)[0])
        companyInfo.on('value', snapshot => {
            this.setState({
                companyInfo: snapshot.val().info, credit: snapshot.val().credit,
                companyAvatar: snapshot.val().info.avatar ? snapshot.val().info.avatar : '',
                compName: snapshot.val().info.company
            })
        })
    }

    handleWindowClose = (ev) => {
        ev.preventDefault();
        return ev.returnValue = 'Leaving this page will loose data';
    }

    handleOpenAvatarGallery = () => {
        this.setState({
            openAvatar: true
        })
    }

    handleCloseAvatarGallery = () => {
        this.setState({
            openAvatar: false
        })
    }

    handleRequestSetAvatar = (fileName) => {
        this.setState({
            avatar: fileName
        })
    }

    saveDraft = () => {
        console.log('a')
        if (this.state.campaignTitle && this.state.campaignDesc) {
            var todayDateArray = this.state.todayDate.toDateString().split(' ')
            var eventDateArray = this.state.eventDate.toDateString().split(' ')
            var dateString = new Date().toString()
            var eventDateString = eventDateArray[2] + ' ' + eventDateArray[1] + ' ' + eventDateArray[3]
            var campaigns

            if (this.state.campKey !== '') {
                campaigns = firebaseRef.child('campaigns').child(Object.keys(this.props.peopleInfo)[0]).child(this.state.campKey)

                if (this.state.typeOfCampaign === 1) {
                    campaigns.update({
                        companyAvatar: this.state.companyAvatar,
                        campAvatar: this.state.avatar,
                        Company: this.state.compName,
                        campaignTitle: this.state.campaignTitle,
                        typeOfCampaign: this.state.typeOfCampaign,
                        description: this.state.campaignDesc,
                        date: dateString,
                        typeOfSubCampaign: this.state.typeOfSubCampaign,
                        monstaXP: this.state.monstaXp,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        specialization: this.state.specialization,
                        skillCanLearn: this.state.skillCanLearn,
                        requirements: this.state.requirements,
                        responsibilities: this.state.responsibilities,
                        benefits: this.state.benefits,
                        minPayout: this.state.minAmount,
                        maxPayout: this.state.maxAmount,
                        isPrivacy: this.state.privacy,
                        salaryType: this.state.salaryType,
                        minEducation: this.state.minEducation,
                        requiredCourses: this.state.requiredCourses,
                        requiredSkills: this.state.requiredSkills,
                        workLocation: this.state.workLocation,
                        aptitudeTest: this.state.aptitudeTest,
                        questionList: this.state.questionList,
                        ParticipantList: [],
                        isDraft: true,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString(),
                    })
                }
                else if (this.state.typeOfCampaign === 2) {
                    if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                        campaigns.update({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            questionList: this.state.questionList,
                            isVideo: this.state.isVideo,
                            videoId: this.state.videoId,
                            ParticipantList: [],
                            isDraft: true,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    } else {
                        campaigns.update({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            submitMethod: this.state.submitMethod,
                            ParticipantList: [],
                            isDraft: true,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                }
                else if (this.state.typeOfCampaign === 3) {
                    campaigns.update({
                        companyAvatar: this.state.companyAvatar,
                        campAvatar: this.state.avatar,
                        Company: this.state.compName,
                        campaignTitle: this.state.campaignTitle,
                        typeOfCampaign: this.state.typeOfCampaign,
                        description: this.state.campaignDesc,
                        date: dateString,
                        typeOfSubCampaign: this.state.typeOfSubCampaign,
                        monstaXP: this.state.monstaXp,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        skillCanLearn: this.state.skillCanLearn,
                        eventDate: eventDateString,
                        eventTime: this.state.eventTime.toString(),
                        eventAdd1: this.state.eventAdd1,
                        eventAdd2: this.state.eventAdd2,
                        eventCity: this.state.eventCity,
                        eventState: this.state.eventState,
                        eventZip: this.state.eventZip,
                        isTicket: this.state.ticket,
                        ticketPrice: this.state.eventTicketPrice,
                        //isName: this.state.isName,
                        //isAge: this.state.isAge,
                        //isGender: this.state.isGender,
                        //isContact: this.state.isContact,
                        //isEmail: this.state.isEmail,
                        //isCourse: this.state.isCourse,
                        //isUni: this.state.isUni,
                        //isTShirt: this.state.isTShirt,
                        ParticipantList: [],
                        isDraft: true,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString(),
                    })
                }
            } else {
                campaigns = firebaseRef.child('campaigns').child(Object.keys(this.props.peopleInfo)[0])

                if (this.state.typeOfCampaign === 1) {
                    campaigns.push({
                        companyAvatar: this.state.companyAvatar,
                        campAvatar: this.state.avatar,
                        Company: this.state.compName,
                        campaignTitle: this.state.campaignTitle,
                        typeOfCampaign: this.state.typeOfCampaign,
                        description: this.state.campaignDesc,
                        date: dateString,
                        typeOfSubCampaign: this.state.typeOfSubCampaign,
                        monstaXP: this.state.monstaXp,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        specialization: this.state.specialization,
                        skillCanLearn: this.state.skillCanLearn,
                        requirements: this.state.requirements,
                        responsibilities: this.state.responsibilities,
                        benefits: this.state.benefits,
                        minPayout: this.state.minAmount,
                        maxPayout: this.state.maxAmount,
                        isPrivacy: this.state.privacy,
                        salaryType: this.state.salaryType,
                        minEducation: this.state.minEducation,
                        requiredCourses: this.state.requiredCourses,
                        requiredSkills: this.state.requiredSkills,
                        workLocation: this.state.workLocation,
                        aptitudeTest: this.state.aptitudeTest,
                        questionList: this.state.questionList,
                        ParticipantList: [],
                        isDraft: true,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString(),
                    })
                }
                else if (this.state.typeOfCampaign === 2) {
                    if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                        campaigns.push({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            questionList: this.state.questionList,
                            isVideo: this.state.isVideo,
                            videoId: this.state.videoId,
                            ParticipantList: [],
                            isDraft: true,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    } else {
                        campaigns.push({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            submitMethod: this.state.submitMethod,
                            ParticipantList: [],
                            isDraft: true,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                }
                else if (this.state.typeOfCampaign === 3) {
                    campaigns.push({
                        companyAvatar: this.state.companyAvatar,
                        campAvatar: this.state.avatar,
                        Company: this.state.compName,
                        campaignTitle: this.state.campaignTitle,
                        typeOfCampaign: this.state.typeOfCampaign,
                        description: this.state.campaignDesc,
                        date: dateString,
                        typeOfSubCampaign: this.state.typeOfSubCampaign,
                        monstaXP: this.state.monstaXp,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        skillCanLearn: this.state.skillCanLearn,
                        eventDate: eventDateString,
                        eventTime: this.state.eventTime.toString(),
                        eventAdd1: this.state.eventAdd1,
                        eventAdd2: this.state.eventAdd2,
                        eventCity: this.state.eventCity,
                        eventState: this.state.eventState,
                        eventZip: this.state.eventZip,
                        isTicket: this.state.ticket,
                        ticketPrice: this.state.eventTicketPrice,
                        //isName: this.state.isName,
                        //isAge: this.state.isAge,
                        //isGender: this.state.isGender,
                        //isContact: this.state.isContact,
                        //isEmail: this.state.isEmail,
                        //isCourse: this.state.isCourse,
                        //isUni: this.state.isUni,
                        //isTShirt: this.state.isTShirt,
                        ParticipantList: [],
                        isDraft: true,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString(),
                    })
                }
            }
            alert('Draft has been saved')
        }
        else{
            alert('The Title and Description cannot be empty')
        }
    }

    componentDidMount = () => {
        //window.addEventListener('onbeforeunload', this.handleWindowClose);
    }

    componentWillUnmount = () => {
        //window.removeEventListener('onbeforeunload', this.handleWindowClose);
    }

    handleMinAmount = (event, maskedvalue, floatvalue) => {
        this.setState({ minAmount: maskedvalue });
    }

    handleMaxAmount = (event, maskedvalue, floatvalue) => {
        this.setState({ maxAmount: maskedvalue });
    }

    handleChangeTicketPrice = (event, maskedvalue, floatvalue) => {
        this.setState({ eventTicketPrice: maskedvalue, errorEventTicket: false });
    }

    handleCampaignTitle = (event) => {
        var errorCampaignTitle = false
        if (event.target.value === '') {
            errorCampaignTitle = true
        }
        this.setState({
            campaignTitle: event.target.value,
            errorCampaignTitle: errorCampaignTitle
        });
    };

    handleURLChange = (event) => {
        this.setState({
            url: "https://www.youtube.com/watch?v=" + event.target.value,
            videoId: event.target.value,
            errorIsVideo: false
        });
    };
    handleTargetStudent = (event) => {
        var errorTagetStudent = false

        if (event.target.value === '' || isNaN(event.target.value) === true) {
            errorTagetStudent = true
        }
        this.setState({
            errorTagetStudent: errorTagetStudent
        })

        if (!isNaN(event.target.value)) {
            this.setState({
                targetStudent: event.target.value
            });
        }
    };


    handleMonstaXP = (event, index, value) => this.setState({ monstaXp: value })

    handleCampaignDesc = (event) => {
        var errorCampaignDesc = false
        if (event.target.value === '') {
            errorCampaignDesc = true
        }
        this.setState({
            campaignDesc: event.target.value,
            errorCampaignDesc: errorCampaignDesc
        });
    };

    handleAddReward = (event) => {
        this.setState({
            addReward: event.target.value,
        });
    };

    handleRequirements = (event) => {
        var errorRequirements = false
        if (event.target.value === '') {
            errorRequirements = true
        }
        this.setState({
            requirements: event.target.value, errorRequirements: errorRequirements
        });
    };

    handleResponsibilities = (event) => {
        var errorResponsibilities = false
        if (event.target.value === '') {
            errorResponsibilities = true
        }
        this.setState({
            responsibilities: event.target.value, errorResponsibilities: errorResponsibilities
        });
    };

    handleBenefits = (event) => {
        var errorBenefits = false
        if (event.target.value === '') {
            errorBenefits = true
        }
        this.setState({
            benefits: event.target.value, errorBenefits: errorBenefits
        });
    };

    handleSurveyDesc = (event) => {
        this.setState({
            surveyDesc: event.target.value,
        });
    };

    handleEventAdd1Change = (event) => {
        this.setState({
            eventAdd1: event.target.value, errorEventAdd1: false
        });
    };

    handleEventAdd2Change = (event) => {
        this.setState({
            eventAdd2: event.target.value,
        });
    };

    handleEventCityChange = (event) => {
        this.setState({
            eventCity: event.target.value, errorEventCity: false
        });
    };

    handleEventStateChange = (event, index, value) => this.setState({ eventState: value, errorEventState: false });

    handleEventZipChange = (event) => {
        this.setState({
            eventZip: event.target.value, errorEventZip: false
        });
    };

    handleNext = () => {
        var errorCampaignTitle = false
        var errorCampaignDesc = false
        var errorMonstaXp = false
        var errorTagetStudent = false
        var errorSpecialization = false
        var errorSkillCanLearn = false
        var errorRequirements = false
        var errorResponsibilities = false
        var errorBenefits = false
        var isGreater = false
        var errorSalaryType = false
        var errorMinEducation = false
        var errorRequiredCourses = false
        var errorRequiredSkills = false
        var errorWorkLocation = []
        var isErrorLocation = false
        var errorQuestionList = []
        var isErrorQuestion = false
        var errorIsVideo = false
        var errorSubmitMethod = false
        var errorEventAdd1 = false
        var errorEventCity = false
        var errorEventState = false
        var errorEventZip = false
        var errorEventTicket = false
        var errorEndDate = false

        const { stepIndex } = this.state;
        if (stepIndex < 3) {
            if (stepIndex === 0) {
                if (this.state.credit < this.state.monstaXp) {
                    var alertResult = confirm('Awarded monsta xp is exceed than you current credit, please proceed with purchase credit')
                    if (alertResult) {
                        this.saveDraft()
                        this.setState({ isPurchase: true })
                    }
                } else {
                    if (!this.state.campaignTitle) {
                        errorCampaignTitle = true
                    }
                    if (!this.state.campaignDesc) {
                        errorCampaignDesc = true
                    }
                    if (!this.state.monstaXp) {
                        errorMonstaXp = true
                    }
                    if (!this.state.targetStudent) {
                        errorTagetStudent = true
                    }

                    if (this.state.endDate < this.state.startDate) {
                        errorEndDate = true
                    }

                    if (this.state.typeOfCampaign === 1) {
                        if (this.state.specialization.length > 0) {
                            errorSpecialization = false
                        }
                        else {
                            errorSpecialization = true
                        }
                    }

                    if (this.state.skillCanLearn.length > 0) {
                        errorSkillCanLearn = false
                    }
                    else {
                        errorSkillCanLearn = true
                    }

                    this.setState({
                        errorCampaignTitle: errorCampaignTitle, errorCampaignDesc: errorCampaignDesc, errorMonstaXp: errorMonstaXp,
                        errorTagetStudent: errorTagetStudent, errorSpecialization: errorSpecialization, errorSkillCanLearn: errorSkillCanLearn,
                        errorEndDate: errorEndDate
                    })
                    if (!errorCampaignTitle && !errorCampaignDesc && !errorMonstaXp && !errorTagetStudent
                        && !errorSpecialization && !errorSkillCanLearn) {
                        if (this.state.typeOfCampaign === 2) {
                            if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                                this.setState({ stepIndex: stepIndex + 1 });
                            } else {
                                this.setState({ stepIndex: stepIndex + 2 });
                            }
                        } else {
                            this.setState({ stepIndex: stepIndex + 1 });
                        }
                    }
                }
            }
            else if (stepIndex === 1) {

                if (this.state.typeOfCampaign === 1) {
                    if (!this.state.requirements) {
                        errorRequirements = true
                    }
                    if (!this.state.responsibilities) {
                        errorResponsibilities = true
                    }
                    if (!this.state.benefits) {
                        errorBenefits = true
                    }
                    if (this.state.minAmount > this.state.maxAmount) {
                        isGreater = true
                    }
                    if (!this.state.salaryType) {
                        errorSalaryType = true
                    }
                    if (!this.state.minEducation) {
                        errorMinEducation = true
                    }
                    /*if (this.state.requiredCourses.length > 0) {
                        errorRequiredCourses = false
                    }
                    else {
                        errorRequiredCourses = true
                    }*/
                    if (this.state.requiredSkills.length > 0) {
                        errorRequiredSkills = false
                    }
                    else {
                        errorRequiredSkills = true
                    }

                    if (this.state.workLocation.length > 0) {
                        this.state.workLocation.forEach((e, idx) => {
                            var errorAdd1 = false
                            var errorCity = false
                            var errorState = false
                            var errorZip = false

                            if (!e.add1) {
                                errorAdd1 = true
                                isErrorLocation = true
                            }
                            if (!e.city) {
                                errorCity = true
                                isErrorLocation = true
                            }
                            if (!e.state) {
                                errorState = true
                                isErrorLocation = true
                            }
                            if (!e.zip) {
                                errorZip = true
                                isErrorLocation = true
                            }

                            errorWorkLocation.push({
                                errorAdd1: errorAdd1,
                                errorCity: errorCity,
                                errorState: errorState,
                                errorZip: errorZip,
                            })
                        })
                    }

                    this.setState({
                        errorRequirements: errorRequirements, errorResponsibilities: errorResponsibilities,
                        errorBenefits: errorBenefits, isGreater: isGreater, errorSalaryType: errorSalaryType,
                        errorMinEducation: errorMinEducation, //errorRequiredCourses: errorRequiredCourses,
                        errorRequiredSkills: errorRequiredSkills, errorWorkLocation: errorWorkLocation
                    })

                    if (!errorRequirements && !errorResponsibilities && !errorBenefits && !isGreater && !errorSalaryType &&
                        !errorMinEducation && !errorRequiredSkills && !isErrorLocation) {
                        this.setState({ stepIndex: stepIndex + 1 });
                    }
                }
                else if (this.state.typeOfCampaign === 2) {
                    if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                        var isErrorQues = []
                        var notComplate = false
                        if (this.state.questionList.length > 0) {
                            this.state.questionList.forEach((e, idx) => {
                                var errorQuestion = false
                                var errorAnswerList = false
                                var errorSelectedGroup = false
                                var errorQuesAns = []
                                var isErr = false
                                if (!e.question) {
                                    errorQuestion = true
                                    isErr = true
                                }
                                if (!e.selectedGroup) {
                                    errorSelectedGroup = true
                                    isErr = true
                                }
                                if (e.selectedGroup !== "textboxMultiLine" && e.selectedGroup !== "textboxSingleLine") {
                                    if (e.answerList.length === 0) {
                                        errorAnswerList = true
                                        isErr = true
                                    }
                                    else {
                                        e.answerList.forEach((ans, ansIdx) => {
                                            var errorAns = false
                                            if (!ans.answer) {
                                                errorAns = true
                                                isErr = true
                                            }

                                            errorQuesAns.push({
                                                errorAns: errorAns
                                            })
                                        })
                                    }
                                }

                                isErrorQues.push({
                                    isErr: isErr
                                })

                                errorQuestionList.push({
                                    errorQuestion: errorQuestion,
                                    errorAnswerList: errorAnswerList,
                                    errorQuesAns: errorQuesAns,
                                    errorSelectedGroup: errorSelectedGroup
                                })
                            })
                            this.setState({ errorQuestionList: errorQuestionList })
                            isErrorQues.forEach((e) => {
                                if (e.isErr) {
                                    notComplate = true
                                }
                            })

                            if (!notComplate) {
                                this.setState({ stepIndex: stepIndex + 1 });
                            }
                        }
                        else {
                            alert('Please add you question')
                        }
                    } else {
                        this.setState({ stepIndex: stepIndex + 1 });
                    }
                }
                else {

                    if (!this.state.eventAdd1) {
                        errorEventAdd1 = true
                    }
                    if (!this.state.eventCity) {
                        errorEventCity = true
                    }
                    if (!this.state.eventState) {
                        errorEventState = true
                    }
                    if (!this.state.eventZip) {
                        errorEventZip = true
                    }
                    if (this.state.ticket) {
                        if (this.state.eventTicketPrice === 0) {
                            errorEventTicket = true
                        }
                    }
                    this.setState({
                        errorEventAdd1: errorEventAdd1, errorEventCity: errorEventCity,
                        errorEventState: errorEventState, errorEventZip: errorEventZip,
                        errorEventTicket: errorEventTicket
                    })

                    if (!errorEventAdd1 && !errorEventCity && !errorEventState && !errorEventZip && !errorEventTicket) {
                        this.setState({ stepIndex: stepIndex + 2 });
                    }
                }
            }
            else {
                if (this.state.typeOfCampaign === 1) {
                    if (this.state.aptitudeTest) {
                        var isErrorQues = []
                        var notComplate = false
                        if (this.state.questionList.length === 0) {
                            alert('Please add you question')
                        }
                        else {
                            this.state.questionList.forEach((e, idx) => {
                                var errorQuestion = false
                                var errorAnswerList = false
                                var errorIsAnswer = true
                                var errorQuesAns = []
                                var isErr = false
                                if (!e.question) {
                                    errorQuestion = true
                                    isErr = true
                                }
                                if (e.answerList.length === 0) {
                                    errorAnswerList = true
                                    isErr = true
                                }
                                else {
                                    e.answerList.forEach((ans, ansIdx) => {
                                        var errorAns = false
                                        if (!ans.answer) {
                                            errorAns = true
                                            isErr = true
                                        }

                                        if (ans.isAnswer) {
                                            errorIsAnswer = false
                                        }

                                        errorQuesAns.push({
                                            errorAns: errorAns
                                        })
                                    })
                                }

                                if (errorIsAnswer) {
                                    isErr = true
                                }
                                else {
                                    isErr = false
                                }

                                isErrorQues.push({
                                    isErr: isErr
                                })

                                errorQuestionList.push({
                                    errorQuestion: errorQuestion,
                                    errorAnswerList: errorAnswerList,
                                    errorQuesAns: errorQuesAns,
                                    errorIsAnswer: errorIsAnswer
                                })
                            })
                            this.setState({ errorQuestionList: errorQuestionList })

                            isErrorQues.forEach((e) => {
                                if (e.isErr) {
                                    notComplate = true
                                }
                            })

                            if (!notComplate) {
                                this.setState({ stepIndex: stepIndex + 1 });
                            }
                        }
                    } else {
                        this.setState({ stepIndex: stepIndex + 1 });
                    }
                }
                else if (this.state.typeOfCampaign === 2) {
                    if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                        if (this.state.isVideo) {
                            if (!this.state.videoId) {
                                errorIsVideo = true
                            }
                            this.setState({ errorIsVideo: errorIsVideo })
                            if (!errorIsVideo) {
                                this.setState({ stepIndex: stepIndex + 1 });
                            }
                        } else {
                            this.setState({ stepIndex: stepIndex + 1 });
                        }
                    } else {
                        if (!this.state.submitMethod) {
                            errorSubmitMethod = true
                        }
                        this.setState({ errorSubmitMethod: errorSubmitMethod })
                        if (!errorSubmitMethod) {
                            this.setState({ stepIndex: stepIndex + 1 });
                        }
                    }
                }
                else {
                    this.setState({ stepIndex: stepIndex + 1 });
                }
            }
        }
        else {
            var alertResult = confirm('Confirm to create campaign?')
            if (alertResult) {
                var todayDateArray = this.state.todayDate.toDateString().split(' ')
                var eventDateArray = this.state.eventDate.toDateString().split(' ')
                var dateString = new Date().toString()
                var eventDateString = eventDateArray[2] + ' ' + eventDateArray[1] + ' ' + eventDateArray[3]
                var compCreditInfo = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}`)
                var campaigns

                if (this.state.campKey !== '') {
                    campaigns = firebaseRef.child('campaigns').child(Object.keys(this.props.peopleInfo)[0]).child(this.state.campKey)

                    if (this.state.typeOfCampaign === 1) {
                        campaigns.update({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            specialization: this.state.specialization,
                            skillCanLearn: this.state.skillCanLearn,
                            requirements: this.state.requirements,
                            responsibilities: this.state.responsibilities,
                            benefits: this.state.benefits,
                            minPayout: this.state.minAmount,
                            maxPayout: this.state.maxAmount,
                            isPrivacy: this.state.privacy,
                            salaryType: this.state.salaryType,
                            minEducation: this.state.minEducation,
                            requiredCourses: this.state.requiredCourses,
                            requiredSkills: this.state.requiredSkills,
                            workLocation: this.state.workLocation,
                            aptitudeTest: this.state.aptitudeTest,
                            questionList: this.state.questionList,
                            ParticipantList: [],
                            isDraft: false,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                    else if (this.state.typeOfCampaign === 2) {
                        if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                            campaigns.update({
                                companyAvatar: this.state.companyAvatar,
                                campAvatar: this.state.avatar,
                                Company: this.state.compName,
                                campaignTitle: this.state.campaignTitle,
                                typeOfCampaign: this.state.typeOfCampaign,
                                description: this.state.campaignDesc,
                                date: dateString,
                                typeOfSubCampaign: this.state.typeOfSubCampaign,
                                monstaXP: this.state.monstaXp,
                                additionalReward: this.state.addReward,
                                targetStudent: this.state.targetStudent,
                                skillCanLearn: this.state.skillCanLearn,
                                questionList: this.state.questionList,
                                isVideo: this.state.isVideo,
                                videoId: this.state.videoId,
                                ParticipantList: [],
                                isDraft: false,
                                startDate: this.state.startDate.toString(),
                                endDate: this.state.endDate.toString(),
                            })
                        } else {
                            campaigns.update({
                                companyAvatar: this.state.companyAvatar,
                                campAvatar: this.state.avatar,
                                Company: this.state.compName,
                                campaignTitle: this.state.campaignTitle,
                                typeOfCampaign: this.state.typeOfCampaign,
                                description: this.state.campaignDesc,
                                date: dateString,
                                typeOfSubCampaign: this.state.typeOfSubCampaign,
                                monstaXP: this.state.monstaXp,
                                additionalReward: this.state.addReward,
                                targetStudent: this.state.targetStudent,
                                skillCanLearn: this.state.skillCanLearn,
                                submitMethod: this.state.submitMethod,
                                ParticipantList: [],
                                isDraft: false,
                                startDate: this.state.startDate.toString(),
                                endDate: this.state.endDate.toString(),
                            })
                        }
                    }
                    else if (this.state.typeOfCampaign === 3) {
                        campaigns.update({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            eventDate: eventDateString,
                            eventTime: this.state.eventTime.toString(),
                            eventAdd1: this.state.eventAdd1,
                            eventAdd2: this.state.eventAdd2,
                            eventCity: this.state.eventCity,
                            eventState: this.state.eventState,
                            eventZip: this.state.eventZip,
                            isTicket: this.state.ticket,
                            ticketPrice: this.state.eventTicketPrice,
                            //isName: this.state.isName,
                            //isAge: this.state.isAge,
                            //isGender: this.state.isGender,
                            //isContact: this.state.isContact,
                            //isEmail: this.state.isEmail,
                            //isCourse: this.state.isCourse,
                            //isUni: this.state.isUni,
                            //isTShirt: this.state.isTShirt,
                            ParticipantList: [],
                            isDraft: false,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                } else {
                    campaigns = firebaseRef.child('campaigns').child(Object.keys(this.props.peopleInfo)[0])

                    if (this.state.typeOfCampaign === 1) {
                        campaigns.push({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            specialization: this.state.specialization,
                            skillCanLearn: this.state.skillCanLearn,
                            requirements: this.state.requirements,
                            responsibilities: this.state.responsibilities,
                            benefits: this.state.benefits,
                            minPayout: this.state.minAmount,
                            maxPayout: this.state.maxAmount,
                            isPrivacy: this.state.privacy,
                            salaryType: this.state.salaryType,
                            minEducation: this.state.minEducation,
                            requiredCourses: this.state.requiredCourses,
                            requiredSkills: this.state.requiredSkills,
                            workLocation: this.state.workLocation,
                            aptitudeTest: this.state.aptitudeTest,
                            questionList: this.state.questionList,
                            ParticipantList: [],
                            isDraft: false,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                    else if (this.state.typeOfCampaign === 2) {
                        if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                            campaigns.push({
                                companyAvatar: this.state.companyAvatar,
                                campAvatar: this.state.avatar,
                                Company: this.state.compName,
                                campaignTitle: this.state.campaignTitle,
                                typeOfCampaign: this.state.typeOfCampaign,
                                description: this.state.campaignDesc,
                                date: dateString,
                                typeOfSubCampaign: this.state.typeOfSubCampaign,
                                monstaXP: this.state.monstaXp,
                                additionalReward: this.state.addReward,
                                targetStudent: this.state.targetStudent,
                                skillCanLearn: this.state.skillCanLearn,
                                questionList: this.state.questionList,
                                isVideo: this.state.isVideo,
                                videoId: this.state.videoId,
                                ParticipantList: [],
                                isDraft: false,
                                startDate: this.state.startDate.toString(),
                                endDate: this.state.endDate.toString(),
                            })
                        } else {
                            campaigns.push({
                                companyAvatar: this.state.companyAvatar,
                                campAvatar: this.state.avatar,
                                Company: this.state.compName,
                                campaignTitle: this.state.campaignTitle,
                                typeOfCampaign: this.state.typeOfCampaign,
                                description: this.state.campaignDesc,
                                date: dateString,
                                typeOfSubCampaign: this.state.typeOfSubCampaign,
                                monstaXP: this.state.monstaXp,
                                additionalReward: this.state.addReward,
                                targetStudent: this.state.targetStudent,
                                skillCanLearn: this.state.skillCanLearn,
                                submitMethod: this.state.submitMethod,
                                ParticipantList: [],
                                isDraft: false,
                                startDate: this.state.startDate.toString(),
                                endDate: this.state.endDate.toString(),
                            })
                        }
                    }
                    else if (this.state.typeOfCampaign === 3) {
                        campaigns.push({
                            companyAvatar: this.state.companyAvatar,
                            campAvatar: this.state.avatar,
                            Company: this.state.compName,
                            campaignTitle: this.state.campaignTitle,
                            typeOfCampaign: this.state.typeOfCampaign,
                            description: this.state.campaignDesc,
                            date: dateString,
                            typeOfSubCampaign: this.state.typeOfSubCampaign,
                            monstaXP: this.state.monstaXp,
                            additionalReward: this.state.addReward,
                            targetStudent: this.state.targetStudent,
                            skillCanLearn: this.state.skillCanLearn,
                            eventDate: eventDateString,
                            eventTime: this.state.eventTime.toString(),
                            eventAdd1: this.state.eventAdd1,
                            eventAdd2: this.state.eventAdd2,
                            eventCity: this.state.eventCity,
                            eventState: this.state.eventState,
                            eventZip: this.state.eventZip,
                            isTicket: this.state.ticket,
                            ticketPrice: this.state.eventTicketPrice,
                            //isName: this.state.isName,
                            //isAge: this.state.isAge,
                            //isGender: this.state.isGender,
                            //isContact: this.state.isContact,
                            //isEmail: this.state.isEmail,
                            //isCourse: this.state.isCourse,
                            //isUni: this.state.isUni,
                            //isTShirt: this.state.isTShirt,
                            ParticipantList: [],
                            isDraft: false,
                            startDate: this.state.startDate.toString(),
                            endDate: this.state.endDate.toString(),
                        })
                    }
                }

                compCreditInfo.update({
                    credit: this.state.credit - this.state.monstaXp
                })

                this.setState({ finished: true })
                alert('The campaign has been created successfully')
            }
        }
    };

    _onReady(event) {
        // access to player in all event handlers via event.target
        event.target.pauseVideo();
    }

    handlePrev = () => {
        const { stepIndex } = this.state;
        if (stepIndex > 0) {
            if (stepIndex === 2) {
                if (this.state.typeOfCampaign === 2) {
                    if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                        this.setState({ stepIndex: stepIndex - 1 });
                    } else {
                        this.setState({ stepIndex: stepIndex - 2 });
                    }
                } else {
                    this.setState({ stepIndex: stepIndex - 1 });
                }
            }
            else if (stepIndex === 3) {
                if (this.state.typeOfCampaign === 3) {
                    this.setState({ stepIndex: stepIndex - 2 });
                } else {
                    this.setState({ stepIndex: stepIndex - 1 });
                }
            }
            else {
                this.setState({ stepIndex: stepIndex + -1 });
            }
        }
    };

    handleChangeDate = (event, date) => {
        this.setState({ todayDate: date, });
    };

    handleChangeStartDate = (event, date) => {
        this.setState({ startDate: date });
    };

    handleChangeEndDate = (event, date) => {
        this.setState({ endDate: date, errorEndDate: false });
    };

    handleChangeEventDate = (event, date) => {
        this.setState({ eventDate: date, });
    };

    handlePrivacySetting = (event, isPrivacy) => {
        this.setState({ privacy: isPrivacy, });
    };

    handleTicket = (event, isTicket) => {
        this.setState({ ticket: isTicket, eventTicketPrice: 0 });
    };

    handleIsName = (event, isName) => {
        this.setState({ isName: isName });
    };

    handleIsAge = (event, isAge) => {
        this.setState({ isAge: isAge });
    };

    handleIsGender = (event, isGender) => {
        this.setState({ isGender: isGender });
    };

    handleIsContact = (event, isContact) => {
        this.setState({ isContact: isContact });
    };

    handleIsEmail = (event, isEmail) => {
        this.setState({ isEmail: isEmail });
    };

    handleIsCourse = (event, isCourse) => {
        this.setState({ isCourse: isCourse });
    };

    handleIsUni = (event, isUni) => {
        this.setState({ isUni: isUni });
    };

    handleIsTShirt = (event, isTShirt) => {
        this.setState({ isTShirt: isTShirt });
    };


    handleVideo = (event, isVideo) => {
        this.setState({ isVideo: isVideo, videoId: '', url: '' });
    };

    handleTypeOfCampaign = (event, index, value) => {
        var monstaXp = 0
        if (value === 1) {
            monstaXp = 175
        }
        else if (value === 2) {
            monstaXp = 50
        }
        else {
            monstaXp = 275
        }

        this.setState({
            typeOfCampaign: value, typeOfSubCampaign: 1, campaignDesc: '', monstaXp: monstaXp, specialization: '', skillCanLearn: [], addReward: '', aptitudeTest: false,
            questionList: [], workLocation: [], currentTest: 0, requiredSkills: [], requiredCourses: [], minEducation: '', salaryType: '', privacy: false, minAmount: 0,
            maxAmount: 0, benefits: '', responsibilities: '', requirements: '', videoId: '', isVideo: false, url: '', submitMethod: '', eventAdd1: '', eventAdd2: '', eventCity: '',
            eventState: '', eventZip: '', ticket: false, isName: false, isAge: false, isGender: false, isContact: false, isEmail: false, isCourse: false, isUni: false,
            errorCampaignDesc: false, errorMonstaXp: false, targetStudent: ''
        })
    }

    handleTypeOfSubCampaign = (event, index, value) => {
        var monstaXp = 0
        if (this.state.typeOfCampaign === 1) {
            monstaXp = 175
        }
        else if (this.state.typeOfCampaign === 2) {
            if (value === 1) {
                monstaXp = 50
            }
            else if (value === 2) {
                monstaXp = 50
            }
            else if (value === 2) {
                monstaXp = 275
            }
            else {
                monstaXp = 275
            }
        }
        else {
            if (value === 1) {
                monstaXp = 275
            }
            else {
                monstaXp = 175
            }
        }

        this.setState({
            typeOfSubCampaign: value, monstaXp: monstaXp, specialization: '', skillCanLearn: [], addReward: '', aptitudeTest: false,
            questionList: [], currentTest: 0, requiredSkills: [], workLocation: [], requiredCourses: [], minEducation: '', salaryType: '', privacy: false, minAmount: 0, maxAmount: 0, benefits: '',
            responsibilities: '', requirements: '', videoId: '', isVideo: false, url: '', submitMethod: '', eventAdd1: '', eventAdd2: '', eventCity: '', eventState: '', eventZip: '', ticket: false,
            isName: false, isAge: false, isGender: false, isContact: false, isEmail: false, isCourse: false, isUni: false, errorMonstaXp: false, targetStudent: ''
        });
    }

    handleMinEducation = (event, index, value) => this.setState({ minEducation: value, errorMinEducation: false });

    handleSubmitMethod = (event, index, value) => this.setState({ submitMethod: value, errorSubmitMethod: false });

    handleSalaryType = (event, index, value) => this.setState({ salaryType: value, errorSalaryType: false });

    handleSpecialization = (event, index, value) => this.setState({ specialization: value, skillCanLearn: [], errorSpecialization: false });

    updateLeadSkill() {
        this.setState((oldState) => { return { leadSkill: !oldState.leadSkill, } })
    }

    updateSkillCanLearn = (event, index, values) => {
        var errorSkillCanLearn = false
        if (values.length > 0) {
            errorSkillCanLearn = false
        }
        else {
            errorSkillCanLearn = true
        }
        this.setState({ skillCanLearn: values, errorSkillCanLearn: errorSkillCanLearn });
    }

    updateRequiredSkills = (event, index, values) => {
        var errorRequiredSkills = false
        if (values.length > 0) {
            errorRequiredSkills = false
        }
        else {
            errorRequiredSkills = true
        }
        this.setState({ requiredSkills: values, errorRequiredSkills: errorRequiredSkills });
    }

    updateRequiredCourses = (event, index, values) => {
        var errorRequiredCourses = false
        if (values.length > 0) {
            errorRequiredCourses = false
        }
        else {
            errorRequiredCourses = true
        }
        this.setState({ requiredCourses: values, errorRequiredCourses: errorRequiredCourses });
    }

    updateAptitudeTest() {
        this.setState((oldState) => { return { aptitudeTest: !oldState.aptitudeTest, questionList: [], currentTest: this.state.currentTest + 1 } })
    }

    updatePmSkill() {
        this.setState((oldState) => { return { pmSkill: !oldState.pmSkill, } })
    }
    updateCommSkill() {
        this.setState((oldState) => { return { commSkill: !oldState.commSkill, } })
    }
    updateTeamSkill() {
        this.setState((oldState) => { return { teamSkill: !oldState.teamSkill, } })
    }
    updateOrgSkill() {
        this.setState((oldState) => { return { orgSkill: !oldState.orgSkill, } })
    }

    addNewQuestion() {
        if (this.state.currentTest < this.state.maxTestGenerate) {
            this.setState({
                questionList: this.state.questionList.concat([{ question: '', selectedGroup: this.state.typeOfCampaign === 1 ? 'multiSelect' : '', answerList: [] }]),
                currentTest: this.state.currentTest + 1
            });
        }
    }

    addNewLocation() {
        this.setState({
            workLocation: this.state.workLocation.concat([{ add1: '', add2: '', city: '', state: '', zip: '', country: '' }]),
        });
    }

    addNewAnswer = (idx) => (evt) => {
        var preData = this.state.questionList
        if (this.state.typeOfCampaign === 1) {
            preData[idx].answerList.push({ answer: '', isAnswer: false })
        } else {
            preData[idx].answerList.push({ answer: '' })
        }

        if (this.state.errorQuestionList.length) {
            if (this.state.errorQuestionList[idx]) {
                var newErrorQues = this.state.errorQuestionList.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorAnswerList: false };
                });
                this.setState({ questionList: preData, errorQuestionList: newErrorQues });
            } else {
                this.setState({ questionList: preData });
            }
        }
        else {
            this.setState({ questionList: preData });
        }
    }

    handleRemoveQuestion = (idx) => () => {
        this.setState({
            questionList: this.state.questionList.filter((s, sidx) => idx !== sidx),
            currentTest: this.state.currentTest - 1
        });
    }

    handleRemoveLocation = (idx) => () => {
        this.setState({
            workLocation: this.state.workLocation.filter((s, sidx) => idx !== sidx)
        });
    }

    navigateTo = (key) => () => {
        this.setState({ isNavigate: true, selectedKey: key })
    }

    handleRemoveAnswer = (idx, ansIdx) => () => {
        const removedAnswer = this.state.questionList
        var removeItem = removedAnswer[idx].answerList
        removedAnswer[idx].answerList = removeItem.filter((s, sidx) => ansIdx !== sidx)
        this.setState({ questionList: removedAnswer });
    }

    handleIsAnswer = (idx, ansIdx) => () => {
        const isAnswer = this.state.questionList
        var isAnswerData = isAnswer[idx].answerList
        if (isAnswer[idx].selectedGroup === "multiSelect") {
            isAnswerData[ansIdx].isAnswer = !isAnswerData[ansIdx].isAnswer
        } else {
            isAnswerData.map((ans, sidx) => {
                if (ansIdx !== sidx) {
                    ans.isAnswer = false
                }
                else {
                    ans.isAnswer = true
                }
            });
        }

        if (this.state.errorQuestionList.length) {
            if (this.state.errorQuestionList[idx]) {
                var newErrorQues = this.state.errorQuestionList.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorIsAnswer: false };
                });
                this.setState({ questionList: isAnswer, errorQuestionList: newErrorQues });
            } else {
                this.setState({ questionList: isAnswer });
            }
        }
        else {
            this.setState({ questionList: isAnswer });
        }
    }

    handleAnswerChange = (idx, ansIdx) => (evt) => {
        const newAnswer = this.state.questionList
        newAnswer[idx].answerList[ansIdx].answer = evt.target.value

        if (this.state.errorQuestionList.length) {
            const newErrorQues = this.state.errorQuestionList
            if (this.state.errorQuestionList[idx]) {
                if (this.state.errorQuestionList[idx].errorQuesAns) {
                    if (this.state.errorQuestionList[idx].errorQuesAns[ansIdx]) {
                        newErrorQues[idx].errorQuesAns[ansIdx].errorAns = false
                        this.setState({ questionList: newAnswer, errorQuestionList: newErrorQues });
                    }
                    else {
                        this.setState({ questionList: newAnswer });
                    }
                }
                else {
                    this.setState({ questionList: newAnswer });
                }
            } else {
                this.setState({ questionList: newAnswer });
            }
        }
        else {
            this.setState({ questionList: newAnswer });
        }
    }

    handleQuestionChange = (idx) => (evt) => {
        const newQuestion = this.state.questionList.map((ques, sidx) => {
            if (idx !== sidx) return ques;
            return { ...ques, question: evt.target.value };
        });

        if (this.state.errorQuestionList.length) {
            if (this.state.errorQuestionList[idx]) {
                var newErrorQues = this.state.errorQuestionList.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorQuestion: false };
                });
                this.setState({ questionList: newQuestion, errorQuestionList: newErrorQues });
            } else {
                this.setState({ questionList: newQuestion });
            }
        }
        else {
            this.setState({ questionList: newQuestion });
        }
    }

    handleAdd1Change = (idx) => (evt) => {
        const newAdd1 = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, add1: evt.target.value };
        });

        if (this.state.errorWorkLocation.length) {
            if (this.state.errorWorkLocation[idx]) {
                var newErrorAdd1 = this.state.errorWorkLocation.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorAdd1: false };
                });
                this.setState({ workLocation: newAdd1, errorWorkLocation: newErrorAdd1 });
            } else {
                this.setState({ workLocation: newAdd1 });
            }
        } else {
            this.setState({ workLocation: newAdd1 });
        }
    }

    handleAdd2Change = (idx) => (evt) => {
        const newAdd2 = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, add2: evt.target.value };
        });

        this.setState({ workLocation: newAdd2 });
    }

    handleZipChange = (idx) => (evt) => {
        const newZip = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, zip: evt.target.value };
        });

        if (this.state.errorWorkLocation.length) {
            if (this.state.errorWorkLocation[idx]) {
                var newErrorZip = this.state.errorWorkLocation.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorZip: false };
                });
                this.setState({ workLocation: newZip, errorWorkLocation: newErrorZip });
            } else {
                this.setState({ workLocation: newZip });
            }
        }
        else {
            this.setState({ workLocation: newZip });
        }
    }

    handleStateChange = (idx) => (event, index, value) => {
        const newState = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, state: value };
        });

        if (this.state.errorWorkLocation.length) {
            if (this.state.errorWorkLocation[idx]) {
                var newErrorState = this.state.errorWorkLocation.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorState: false };
                });
                this.setState({ workLocation: newState, errorWorkLocation: newErrorState });
            } else {
                this.setState({ workLocation: newState });
            }
        } else {
            this.setState({ workLocation: newState });
        }
    }

    handleCityChange = (idx) => (evt) => {
        const newCity = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, city: evt.target.value };
        });

        if (this.state.errorWorkLocation.length) {
            if (this.state.errorWorkLocation[idx]) {
                var newErrorCity = this.state.errorWorkLocation.map((e, sidx) => {
                    if (idx !== sidx) return e;
                    return { ...e, errorCity: false };
                });
                this.setState({ workLocation: newCity, errorWorkLocation: newErrorCity });
            } else {
                this.setState({ workLocation: newCity });
            }
        } else {
            this.setState({ workLocation: newCity });
        }
    }

    handleSelectionType = (idx) => (evt) => {
        const newQuestion = this.state.questionList.map((ques, sidx) => {
            if (idx !== sidx) return ques;
            return { ...ques, selectedGroup: evt.target.value, answerList: [] };
        });

        if (this.state.typeOfCampaign === 2) {
            if (this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2) {
                if (this.state.errorQuestionList.length) {
                    if (this.state.errorQuestionList[idx]) {
                        var newErrorQues = this.state.errorQuestionList.map((e, sidx) => {
                            if (idx !== sidx) return e;
                            return { ...e, errorSelectedGroup: false };
                        });
                        this.setState({ questionList: newQuestion, errorQuestionList: newErrorQues });
                    } else {
                        this.setState({ questionList: newQuestion });
                    }
                }
                else {
                    this.setState({ questionList: newQuestion });
                }
            } else {
                this.setState({ questionList: newQuestion });
            }
        } else {
            this.setState({ questionList: newQuestion });
        }
    }

    handleChangeEventTime = (event, date) => {
        this.setState({ eventTime: date });
    };

    getStepContent(stepIndex) {
        switch (stepIndex) {
            case 0:
                return (
                    <div>
                        <table>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Company/ Brand Name</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td style={{ width: '60%' }}>
                                    <text>{this.state.compName}</text>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Title</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <TextField
                                        style={{ width: '100%' }}
                                        hintText="Title of the Campaign"
                                        errorText={this.state.errorCampaignTitle ? 'This field is required' : false}
                                        value={this.state.campaignTitle}
                                        onChange={this.handleCampaignTitle}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Type</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <DropDownMenu value={this.state.typeOfCampaign} onChange={this.handleTypeOfCampaign}>
                                        <MenuItem value={1} primaryText="Career&amp;Opportunities" />
                                        <MenuItem value={2} primaryText="Online Events" />
                                        <MenuItem value={3} primaryText="Events" />
                                    </DropDownMenu>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Description</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <TextField
                                        style={{ width: '100%' }}
                                        hintText="Campaign Description"
                                        multiLine={true}
                                        errorText={this.state.errorCampaignDesc ? 'This field is required' : false}
                                        value={this.state.campaignDesc}
                                        onChange={this.handleCampaignDesc}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    {
                                        this.state.typeOfCampaign == 1 ?
                                            <text style={{ fontWeight: 'bold' }}>Type of Career Offer</text>
                                            :
                                            this.state.typeOfCampaign == 2 ?
                                                <text style={{ fontWeight: 'bold' }}>Type of Online Events</text>
                                                :
                                                <text style={{ fontWeight: 'bold' }}>Type of Event(s) </text>
                                    }
                                    <br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    {
                                        this.state.typeOfCampaign == 1 ?
                                            <DropDownMenu value={this.state.typeOfSubCampaign} onChange={this.handleTypeOfSubCampaign}>
                                                <MenuItem value={1} primaryText="Part-time" />
                                                <MenuItem value={2} primaryText="Full-time" />
                                                <MenuItem value={3} primaryText="Internship" />
                                                <MenuItem value={4} primaryText="Volunteers" />
                                                <MenuItem value={5} primaryText="Freelance" />
                                            </DropDownMenu>
                                            :
                                            this.state.typeOfCampaign == 2 ?
                                                <DropDownMenu value={this.state.typeOfSubCampaign} onChange={this.handleTypeOfSubCampaign}>
                                                   {/*  <MenuItem value={1} primaryText="Survey" />
                                                    <MenuItem value={2} primaryText="Feedback" /> */}
                                                    <MenuItem value={3} primaryText="Idea Bank" />
                                                    <MenuItem value={4} primaryText="Competition" />
                                                </DropDownMenu>
                                                :
                                                <DropDownMenu value={this.state.typeOfSubCampaign} onChange={this.handleTypeOfSubCampaign}>
                                                    <MenuItem value={1} primaryText="With Attendance" />
                                                    <MenuItem value={2} primaryText="Without Attendance" />
                                                </DropDownMenu>
                                    }
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Start Date</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <DatePicker
                                        onChange={this.handleChangeStartDate}
                                        minDate={this.state.todayDate}
                                        defaultDate={this.state.startDate} />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>End Date</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <DatePicker
                                        onChange={this.handleChangeEndDate}
                                        minDate={this.state.startDate}
                                        defaultDate={this.state.endDate} />
                                    {
                                        this.state.errorEndDate ?
                                            <text style={{ fontSize: 12, color: 'red' }}>This end date cannot earlier than start date</text>
                                            :
                                            false
                                    }
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Awarded Monsta XP</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <text> {this.state.monstaXp} XP</text><br />
                                    {/*<DropDownMenu value={this.state.monstaXp} onChange={this.handleMonstaXP}>
                                        <MenuItem value={'50'} primaryText="50" />
                                        <MenuItem value={'100'} primaryText="100" />
                                        <MenuItem value={'150'} primaryText="150" />
                                        <MenuItem value={'200'} primaryText="200" />
                                </DropDownMenu>*/}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Additional rewards</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <TextField
                                        style={{ width: '100%' }}
                                        floatingLabelText="Additional rewards"
                                        hintText="Additional rewards"
                                        multiLine={true}
                                        value={this.state.addReward}
                                        onChange={this.handleAddReward}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Target Student</text><br />
                                    <text style={{ fontSize: 10 }}>(number only)</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <TextField
                                        style={{ width: '100%' }}
                                        floatingLabelText="Target Student"
                                        hintText="Target Student"
                                        errorText={this.state.errorTagetStudent ? 'This field is required' : false}
                                        value={this.state.targetStudent}
                                        onChange={this.handleTargetStudent}
                                    />
                                </td>
                            </tr>
                            {
                                this.state.typeOfCampaign == 1 ?
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Specialization</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text>
                                        </td>
                                        <td>
                                            <DropDownMenu value={this.state.specialization} onChange={this.handleSpecialization}>
                                                {this.state.specializationList.map((e) => (
                                                    <MenuItem value={e} primaryText={e.split('_').join(' ')} />
                                                ))}
                                            </DropDownMenu>
                                            {
                                                this.state.errorSpecialization ?
                                                    <div>
                                                        <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                    </div>
                                                    :
                                                    false
                                            }
                                        </td>
                                    </tr>
                                    :
                                    ''
                            }
                            <tr>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}>Skills can be learned</text><br />
                                    <text style={{ fontSize: 10 }}>(can tick more than one)</text><br />
                                </td>
                                <td>
                                    <text style={{ fontWeight: 'bold' }}> : </text>
                                </td>
                                <td>
                                    <SelectField
                                        multiple={true}
                                        hintText="Select skills"
                                        value={this.state.skillCanLearn}
                                        onChange={this.updateSkillCanLearn}
                                        errorText={this.state.errorSkillCanLearn ? 'This field is required' : false}
                                    >
                                        {this.state.skillList.map((e, idx) => (
                                            <MenuItem
                                                key={e}
                                                insetChildren={true}
                                                checked={this.state.skillCanLearn.indexOf(idx) > -1}
                                                value={idx}
                                                primaryText={e}
                                            />
                                        ))}
                                    </SelectField>
                                </td>
                            </tr>
                        </table>
                    </div>
                );
            case 1:
                switch (this.state.typeOfCampaign) {
                    case 1:
                        return (
                            <div>
                                <text style={{ fontWeight: 'bold' }}>Description</text><br />
                                <hr className="hr-text" />
                                <table style={{ paddingLeft: '2%' }}>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>1. Requirements </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText="Requirements"
                                                floatingLabelText="Requirements"
                                                multiLine={true}
                                                value={this.state.requirements}
                                                onChange={this.handleRequirements}
                                                errorText={this.state.errorRequirements ? 'This field is required' : false}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>2. Responsibilities </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText="Responsibilities"
                                                floatingLabelText="Responsibilities"
                                                multiLine={true}
                                                value={this.state.responsibilities}
                                                onChange={this.handleResponsibilities}
                                                errorText={this.state.errorResponsibilities ? 'This field is required' : false}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>3. Benefits </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText="Benefits"
                                                floatingLabelText="Benefits"
                                                multiLine={true}
                                                value={this.state.benefits}
                                                onChange={this.handleBenefits}
                                                errorText={this.state.errorBenefits ? 'This field is required' : false}
                                            />
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <text style={{ fontWeight: 'bold' }}>Extra Info </text><br />
                                <hr className="hr-text" />
                                <table style={{ paddingLeft: '2%' }}>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Allowance/Payout </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <text>RM&nbsp;</text>
                                            <CurrencyInput style={{ textAlign: 'right' }} value={this.state.minAmount} onChangeEvent={this.handleMinAmount} />
                                            <text>&nbsp;-&nbsp;</text>
                                            <CurrencyInput style={{ textAlign: 'right' }} value={this.state.maxAmount} onChangeEvent={this.handleMaxAmount} />
                                            <br />
                                            {
                                                this.state.isGreater ?
                                                    <text style={{ fontSize: 12, color: 'red' }}>Minimum amount cannot bigger than maximum amount</text>
                                                    :
                                                    false
                                            }
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <text style={{ fontWeight: 'bold' }}>Advance Setting </text> <br />
                                <hr className="hr-text" />
                                <table style={{ paddingLeft: '2%' }}>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Privacy </text><br />
                                        </td>
                                        <td style={{ fontWeight: 'bold' }}> : </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.privacy === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.privacy}
                                                onToggle={this.handlePrivacySetting}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Salary Type </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <DropDownMenu value={this.state.salaryType} onChange={this.handleSalaryType}>
                                                <MenuItem value='PerHour' primaryText="Per Hour" />
                                                <MenuItem value='PerDay' primaryText="Per Day" />
                                                <MenuItem value='PerMonth' primaryText="Per Month" />
                                                <MenuItem value='PerCompletion' primaryText="Per Completion/Project" />
                                            </DropDownMenu>
                                            {
                                                this.state.errorSalaryType ?
                                                    <div>
                                                        <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                    </div>
                                                    :
                                                    false
                                            }
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Minimum Education </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <DropDownMenu value={this.state.minEducation} onChange={this.handleMinEducation}>
                                                <MenuItem value='PrimarySchool' primaryText="Primary School" />
                                                <MenuItem value='HighSchool' primaryText="High School" />
                                                <MenuItem value='Diploma' primaryText="Certificates/ Vocational/ Diploma" />
                                                <MenuItem value='Degree' primaryText="Degree" />
                                                <MenuItem value='Master' primaryText="Master/PHD" />
                                                <MenuItem value='NoLimit' primaryText="Not Required" />
                                            </DropDownMenu>
                                            {
                                                this.state.errorMinEducation ?
                                                    <div>
                                                        <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                    </div>
                                                    :
                                                    false
                                            }
                                        </td>
                                    </tr>
                           {/*          <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Required Courses </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <SelectField
                                                multiple={true}
                                                hintText="Required Courses"
                                                value={this.state.requiredCourses}
                                                onChange={this.updateRequiredCourses}
                                                errorText={this.state.errorRequiredCourses ? 'This field is required' : false}
                                            >
                                                {this.state.coursesList.map((e, idx) => (
                                                    <MenuItem
                                                        key={e}
                                                        insetChildren={true}
                                                        checked={this.state.requiredCourses.indexOf(idx) > -1}
                                                        value={idx}
                                                        primaryText={e}
                                                    />
                                                ))}
                                            </SelectField>
                                        </td>
                                    </tr> */}
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Required Skills </text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <SelectField
                                                multiple={true}
                                                hintText="Required Skills"
                                                value={this.state.requiredSkills}
                                                onChange={this.updateRequiredSkills}
                                                errorText={this.state.errorRequiredSkills ? 'This field is required' : false}
                                            >
                                                {this.state.skillList.map((e, idx) => (
                                                    <MenuItem
                                                        key={e}
                                                        insetChildren={true}
                                                        checked={this.state.requiredSkills.indexOf(idx) > -1}
                                                        value={idx}
                                                        primaryText={e}
                                                    />
                                                ))}
                                            </SelectField>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <text style={{ fontWeight: 'bold' }}>Work Location </text> <br />
                                <hr className="hr-text" />
                                {
                                    this.state.workLocation.map((location, idx) => (
                                        <div>
                                            <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveLocation(idx)} />
                                            <text style={{ fontWeight: 'bold' }}>{`Work Location ${idx + 1}`}</text><br />
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText='Street address 1'
                                                value={location.add1}
                                                onChange={this.handleAdd1Change(idx)}
                                                errorText={
                                                    this.state.errorWorkLocation.length > 0 ?
                                                        this.state.errorWorkLocation[idx] ?
                                                            this.state.errorWorkLocation[idx].errorAdd1 ?
                                                                'This field is required'
                                                                : false
                                                            : false
                                                        : false
                                                }
                                            />
                                            <br />
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText='Street address 2'
                                                value={location.add2}
                                                onChange={this.handleAdd2Change(idx)}
                                            />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <text>City</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            hintText='City'
                                                            value={location.city}
                                                            onChange={this.handleCityChange(idx)}
                                                            errorText={
                                                                this.state.errorWorkLocation.length > 0 ?
                                                                    this.state.errorWorkLocation[idx] ?
                                                                        this.state.errorWorkLocation[idx].errorCity ?
                                                                            'This field is required'
                                                                            : false
                                                                        : false
                                                                    : false
                                                            }
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text>State</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <DropDownMenu value={location.state} onChange={this.handleStateChange(idx)}>
                                                            <MenuItem value='Johor' primaryText="Johor" />
                                                            <MenuItem value='Kedah' primaryText="Kedah" />
                                                            <MenuItem value='Kelantan' primaryText="Kelantan" />
                                                            <MenuItem value='KualaLumpur' primaryText="Kuala Lumpur" />
                                                            <MenuItem value='Labuan' primaryText="Labuan" />
                                                            <MenuItem value='Malacca' primaryText="Malacca" />
                                                            <MenuItem value='NegeriSembilan' primaryText="Negeri Sembilan" />
                                                            <MenuItem value='Pahang' primaryText="Pahang" />
                                                            <MenuItem value='Penang' primaryText="Penang" />
                                                            <MenuItem value='Perak' primaryText="Perak" />
                                                            <MenuItem value='Perlis' primaryText="Perlis" />
                                                            <MenuItem value='Putrajaya' primaryText="Putrajaya" />
                                                            <MenuItem value='Sabah' primaryText="Sabah" />
                                                            <MenuItem value='Sarawak' primaryText="Sarawak" />
                                                            <MenuItem value='Selangor' primaryText="Selangor" />
                                                            <MenuItem value='Terengganu' primaryText="Terengganu" />
                                                        </DropDownMenu>
                                                        {
                                                            this.state.errorWorkLocation.length > 0 ?
                                                                this.state.errorWorkLocation[idx] ?
                                                                    this.state.errorWorkLocation[idx].errorState ?
                                                                        <div>
                                                                            <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                                        </div>
                                                                        : false
                                                                    : false
                                                                : false
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text>Zip/Postal Code</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            hintText='Zip/Postal Code'
                                                            value={location.zip}
                                                            onChange={this.handleZipChange(idx)}
                                                            errorText={
                                                                this.state.errorWorkLocation.length > 0 ?
                                                                    this.state.errorWorkLocation[idx] ?
                                                                        this.state.errorWorkLocation[idx].errorZip ?
                                                                            'This field is required'
                                                                            : false
                                                                        : false
                                                                    : false
                                                            }
                                                        />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </div>
                                    ))
                                }
                                {
                                    this.state.workLocation.length < 3 ?
                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                            <FlatButton
                                                label="Add Location"
                                                onClick={this.addNewLocation.bind(this)}
                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                            /><br />
                                        </div>
                                        :
                                        ''
                                }
                            </div >
                        );
                    case 2:
                        return (
                            this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2 ?
                                <div>
                                    {this.state.questionList.map((quesList, idx) => (
                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                            <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuestion(idx)} />
                                            <TextField
                                                style={{ width: '80%' }}
                                                hintText={`Enter your question${idx + 1} here`}
                                                floatingLabelText={`Question${idx + 1}`}
                                                value={quesList.question}
                                                onChange={this.handleQuestionChange(idx)}
                                                errorText={
                                                    this.state.errorQuestionList.length > 0 ?
                                                        this.state.errorQuestionList[idx] ?
                                                            this.state.errorQuestionList[idx].errorQuestion ?
                                                                'This field is required'
                                                                : false
                                                            : false
                                                        : false
                                                }
                                            />
                                            <br />
                                            <text style={{ fontWeight: 'bold' }}>Type of answer</text>
                                            {
                                                this.state.errorQuestionList.length > 0 ?
                                                    this.state.errorQuestionList[idx] ?
                                                        this.state.errorQuestionList[idx].errorSelectedGroup ?
                                                            <text style={{ fontSize: 12, color: 'red' }}> Please select the type of answer</text>
                                                            : false
                                                        : false
                                                    : false
                                            }
                                            <br />
                                            <RadioButtonGroup valueSelected={quesList.selectedGroup} onChange={this.handleSelectionType(idx)} style={{ padding: '2%' }}>
                                                <RadioButton
                                                    value='multiSelect'
                                                    label="Multiple Select"
                                                />
                                                <RadioButton
                                                    value='onlyOne'
                                                    label="Select only one"
                                                />
                                                <RadioButton
                                                    value='ratingScale'
                                                    label="Rating Scale"
                                                />
                                                <RadioButton
                                                    value='textboxMultiLine'
                                                    label="Textbox (multiple line)"
                                                />
                                                <RadioButton
                                                    value='textboxSingleLine'
                                                    label="Textbox (single line)"
                                                />
                                            </RadioButtonGroup>
                                            {
                                                quesList.selectedGroup !== 'textboxMultiLine' && quesList.selectedGroup !== 'textboxSingleLine' && quesList.selectedGroup !== '' ?
                                                    <div>
                                                        <text style={{ fontWeight: 'bold' }}>Answers</text>
                                                        {
                                                            this.state.errorQuestionList.length > 0 ?
                                                                this.state.errorQuestionList[idx] ?
                                                                    this.state.errorQuestionList[idx].errorAnswerList ?
                                                                        <div>
                                                                            <text style={{ fontSize: 12, color: 'red' }}> Answer cannot be empty</text>
                                                                        </div>
                                                                        :
                                                                        this.state.errorQuestionList[idx].errorIsAnswer ?
                                                                            <div>
                                                                                <text style={{ fontSize: 12, color: 'red' }}> Please select the correct answer</text>
                                                                            </div>
                                                                            : false
                                                                    : false
                                                                : false
                                                        }<br />
                                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                            {quesList.answerList.map((ansList, ansIdx) => (
                                                                <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', margin: '1%', padding: '1%' }}>
                                                                    <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveAnswer(idx, ansIdx)} />
                                                                    <TextField
                                                                        style={{ width: '80%' }}
                                                                        hintText={`Enter your answer${ansIdx + 1} here`}
                                                                        floatingLabelText={`Answer${ansIdx + 1}`}
                                                                        value={ansList.answer}
                                                                        onChange={this.handleAnswerChange(idx, ansIdx)}
                                                                        multiLine={true}
                                                                        errorText={
                                                                            this.state.errorQuestionList.length > 0 ?
                                                                                this.state.errorQuestionList[idx] ?
                                                                                    this.state.errorQuestionList[idx].errorQuesAns ?
                                                                                        this.state.errorQuestionList[idx].errorQuesAns[ansIdx] ?
                                                                                            this.state.errorQuestionList[idx].errorQuesAns[ansIdx].errorAns ?
                                                                                                'This field is required'
                                                                                                : false
                                                                                            : false
                                                                                        : false
                                                                                    : false
                                                                                : false
                                                                        }
                                                                    />
                                                                </div>
                                                            ))}
                                                            {
                                                                quesList.answerList.length < 4 ?
                                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                                        <FlatButton
                                                                            label="Add Answer"
                                                                            onClick={this.addNewAnswer(idx)}
                                                                            style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                                        /><br />
                                                                    </div>
                                                                    :
                                                                    ''
                                                            }
                                                        </div>
                                                    </div>
                                                    :
                                                    ''
                                            }
                                        </div>
                                    ))}
                                    {
                                        this.state.currentTest < 5 ?
                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                <FlatButton
                                                    label="Add Question"
                                                    onClick={this.addNewQuestion.bind(this)}
                                                    style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                /><br />
                                            </div>
                                            :
                                            ''
                                    }
                                </div>
                                :
                                ''
                        );
                    case 3:
                        return (
                            <div>
                                <table>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Event Date</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <DatePicker
                                                onChange={this.handleChangeEventDate}
                                                minDate={this.state.eventDate}
                                                defaultDate={this.state.eventDate} />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Event Time</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <TimePicker
                                                format="ampm"
                                                value={this.state.eventTime}
                                                onChange={this.handleChangeEventTime}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Event Venue</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText="Street address 1"
                                                floatingLabelText="Street address 1"
                                                value={this.state.eventAdd1}
                                                onChange={this.handleEventAdd1Change}
                                                errorText={this.state.errorEventAdd1 ? 'This field is required' : false}
                                            />
                                            <br />
                                            <TextField
                                                style={{ width: '100%' }}
                                                hintText='Street address 2'
                                                floatingLabelText="Street address 2"
                                                value={this.state.eventAdd2}
                                                onChange={this.handleEventAdd2Change}
                                            />
                                            <table>
                                                <tr>
                                                    <td>
                                                        <text>City</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            floatingLabelText="City"
                                                            hintText='City'
                                                            value={this.state.eventCity}
                                                            onChange={this.handleEventCityChange}
                                                            errorText={this.state.errorEventCity ? 'This field is required' : false}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text>State</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <DropDownMenu value={this.state.eventState} onChange={this.handleEventStateChange}>
                                                            <MenuItem value='Johor' primaryText="Johor" />
                                                            <MenuItem value='Kedah' primaryText="Kedah" />
                                                            <MenuItem value='Kelantan' primaryText="Kelantan" />
                                                            <MenuItem value='KualaLumpur' primaryText="Kuala Lumpur" />
                                                            <MenuItem value='Labuan' primaryText="Labuan" />
                                                            <MenuItem value='Malacca' primaryText="Malacca" />
                                                            <MenuItem value='NegeriSembilan' primaryText="Negeri Sembilan" />
                                                            <MenuItem value='Pahang' primaryText="Pahang" />
                                                            <MenuItem value='Penang' primaryText="Penang" />
                                                            <MenuItem value='Perak' primaryText="Perak" />
                                                            <MenuItem value='Perlis' primaryText="Perlis" />
                                                            <MenuItem value='Putrajaya' primaryText="Putrajaya" />
                                                            <MenuItem value='Sabah' primaryText="Sabah" />
                                                            <MenuItem value='Sarawak' primaryText="Sarawak" />
                                                            <MenuItem value='Selangor' primaryText="Selangor" />
                                                            <MenuItem value='Terengganu' primaryText="Terengganu" />
                                                        </DropDownMenu>
                                                        {
                                                            this.state.errorEventState ?
                                                                <div>
                                                                    <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                                </div>
                                                                :
                                                                false
                                                        }
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text>Zip/Postal Code</text>
                                                    </td>
                                                    <td> : </td>
                                                    <td>
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            floatingLabelText="Zip/Postal Code"
                                                            hintText='Zip/Postal Code'
                                                            value={this.state.eventZip}
                                                            onChange={this.handleEventZipChange}
                                                            errorText={this.state.errorEventZip ? 'This field is required' : false}
                                                        />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Ticket</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.ticket === true ?
                                                        "Paid"
                                                        :
                                                        "Free"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.ticket}
                                                onToggle={this.handleTicket}
                                            />
                                        </td>
                                    </tr>
                                    {
                                        this.state.ticket === true ?
                                            <tr>
                                                <td>
                                                    <text> </text>
                                                </td>
                                                <td>
                                                    <text> </text>
                                                </td>
                                                <td>
                                                    <text>RM </text>
                                                    <CurrencyInput style={{ textAlign: 'right' }} value={this.state.eventTicketPrice} onChangeEvent={this.handleChangeTicketPrice} /><br />
                                                    {
                                                        this.state.errorEventTicket ?
                                                            <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                            :
                                                            false
                                                    }
                                                </td>
                                            </tr>
                                            :
                                            ''
                                    }
                                </table>
                            </div>
                        );
                }
            case 2:
                switch (this.state.typeOfCampaign) {
                    case 1:
                        return (
                            <div>
                                <Checkbox
                                    label="Aptitude Test"
                                    checked={this.state.aptitudeTest}
                                    onCheck={this.updateAptitudeTest.bind(this)}
                                />
                                <hr className="hr-text" />
                                {this.state.questionList.map((quesList, idx) => (
                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuestion(idx)} />
                                        <TextField
                                            style={{ width: '80%' }}
                                            hintText={`Enter your question${idx + 1} here`}
                                            floatingLabelText={`Question${idx + 1}`}
                                            value={quesList.question}
                                            onChange={this.handleQuestionChange(idx)}
                                            errorText={
                                                this.state.errorQuestionList.length > 0 ?
                                                    this.state.errorQuestionList[idx] ?
                                                        this.state.errorQuestionList[idx].errorQuestion ?
                                                            'This field is required'
                                                            : false
                                                        : false
                                                    : false
                                            }
                                        />
                                        <br />
                                        <text style={{ fontWeight: 'bold' }}>Type of answer</text><br />
                                        <RadioButtonGroup valueSelected={quesList.selectedGroup} onChange={this.handleSelectionType(idx)} style={{ padding: '2%' }}>
                                            <RadioButton
                                                value='multiSelect'
                                                label="Multiple Select"
                                            />
                                            <RadioButton
                                                value='onlyOne'
                                                label="Select only one"
                                            />
                                        </RadioButtonGroup>
                                        <div>
                                            <text style={{ fontWeight: 'bold' }}>Answers</text>
                                            {
                                                this.state.errorQuestionList.length > 0 ?
                                                    this.state.errorQuestionList[idx] ?
                                                        this.state.errorQuestionList[idx].errorAnswerList ?
                                                            <div>
                                                                <text style={{ fontSize: 12, color: 'red' }}> Answer cannot be empty</text>
                                                            </div>
                                                            :
                                                            this.state.errorQuestionList[idx].errorIsAnswer ?
                                                                <div>
                                                                    <text style={{ fontSize: 12, color: 'red' }}> Please select the correct answer</text>
                                                                </div>
                                                                : false
                                                        : false
                                                    : false
                                            }
                                            <br />
                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                {quesList.answerList.map((ansList, ansIdx) => (
                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', margin: '1%', padding: '1%' }}>
                                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveAnswer(idx, ansIdx)} />
                                                        {quesList.selectedGroup === 'multiSelect' ?
                                                            <div style={{ float: 'right', color: '#818078', marginTop: '10px' }}>
                                                                <input type="checkbox" name="radAnswer" checked={ansList.isAnswer === true ? "checked" : ''} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                <text>Correct Answer</text>
                                                            </div>
                                                            :
                                                            <div style={{ float: 'right', color: '#818078', marginTop: '10px' }}>
                                                                <input type="radio" name="radAnswer" checked={ansList.isAnswer === true ? "checked" : ''} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                <text>Correct Answer</text>
                                                            </div>
                                                        }
                                                        <TextField
                                                            style={{ width: '80%' }}
                                                            hintText={`Enter your answer${ansIdx + 1} here`}
                                                            floatingLabelText={`Answer${ansIdx + 1}`}
                                                            value={ansList.answer}
                                                            onChange={this.handleAnswerChange(idx, ansIdx)}
                                                            multiLine={true}
                                                            errorText={
                                                                this.state.errorQuestionList.length > 0 ?
                                                                    this.state.errorQuestionList[idx] ?
                                                                        this.state.errorQuestionList[idx].errorQuesAns ?
                                                                            this.state.errorQuestionList[idx].errorQuesAns[ansIdx] ?
                                                                                this.state.errorQuestionList[idx].errorQuesAns[ansIdx].errorAns ?
                                                                                    'This field is required'
                                                                                    : false
                                                                                : false
                                                                            : false
                                                                        : false
                                                                    : false
                                                            }
                                                        />
                                                    </div>
                                                ))}
                                                {
                                                    quesList.answerList.length < 4 ?
                                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                            <FlatButton
                                                                label="Add Answer"
                                                                onClick={this.addNewAnswer(idx)}
                                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                            /><br />
                                                        </div>
                                                        :
                                                        ''
                                                }
                                            </div>
                                        </div>
                                    </div>
                                ))}
                                {
                                    this.state.aptitudeTest && this.state.currentTest < 5 ?
                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                            <FlatButton
                                                label="Add Question"
                                                onClick={this.addNewQuestion.bind(this)}
                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                            /><br />
                                        </div>
                                        :
                                        ''
                                }
                            </div>
                        );
                    case 2:
                        return (
                            this.state.typeOfSubCampaign === 1 || this.state.typeOfSubCampaign === 2 ?
                                <div>
                                    <table style={{ paddingLeft: '2%' }}>
                                        <tr>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}>Youtube </text><br />
                                            </td>
                                            <td style={{ fontWeight: 'bold' }}> : </td>
                                            <td>
                                                <Toggle
                                                    labelPosition="right"
                                                    toggled={this.state.isVideo}
                                                    onToggle={this.handleVideo}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td />
                                            <td />
                                            <td>
                                                {
                                                    this.state.isVideo === true ?
                                                        <div style={{ display: 'flex' }}>
                                                            <div style={{ width: '60%', paddingTop: '5%' }}>
                                                                <text >https://www.youtube.com/watch?v=</text>
                                                            </div>
                                                            <TextField
                                                                floatingLabelText="Youtube video id"
                                                                hintText={'Enter Youtube video id here'}
                                                                value={this.state.videoId}
                                                                onChange={this.handleURLChange}
                                                                errorText={this.state.errorIsVideo ? 'This field is required' : false}
                                                            />
                                                        </div>
                                                        :
                                                        ''
                                                }
                                            </td>
                                        </tr>
                                    </table>
                                    {
                                        this.state.videoId !== '' ?
                                            <YouTube
                                                videoId={this.state.videoId}
                                                opts={this.state.opts}
                                            //onReady={this._onReady}
                                            />
                                            :
                                            ''
                                    }
                                </div>
                                :
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}>Submit Method </text><br />
                                            </td>
                                            <td style={{ fontWeight: 'bold' }}> : </td>
                                            <td>
                                                <DropDownMenu value={this.state.submitMethod} onChange={this.handleSubmitMethod}>
                                                    <MenuItem value='submitLink' primaryText="Submit Link" />
                                                    {//<MenuItem value='attachFile' primaryText="Attach file" />
                                                    }
                                                </DropDownMenu>
                                                {
                                                    this.state.errorSubmitMethod ?
                                                        <div>
                                                            <text style={{ fontSize: 12, color: 'red' }}>This field is required</text>
                                                        </div>
                                                        : false
                                                }
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                        );
                    case 3:
                        return (
                            <div></div>
                            /*<div>
                                <text style={{ fontWeight: 'bold' }}>Options Standard Filled </text><br />
                                <hr className="hr-text" />
                                <table style={{ paddingLeft: '2%' }}>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Name</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isName === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isName}
                                                onToggle={this.handleIsName}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Age</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isAge === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isAge}
                                                onToggle={this.handleIsAge}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Gender</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isGender === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isGender}
                                                onToggle={this.handleIsGender}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Contact</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isContact === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isContact}
                                                onToggle={this.handleIsContact}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Email</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isEmail === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isEmail}
                                                onToggle={this.handleIsEmail}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>Course</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isCourse === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isCourse}
                                                onToggle={this.handleIsCourse}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>University</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isUni === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isUni}
                                                onToggle={this.handleIsUni}
                                            />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}>T-shirt size</text><br />
                                        </td>
                                        <td>
                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                        </td>
                                        <td>
                                            <Toggle
                                                label={
                                                    this.state.isTShirt === true ?
                                                        "Show"
                                                        :
                                                        "Hide"
                                                }
                                                labelPosition="right"
                                                toggled={this.state.isTShirt}
                                                onToggle={this.handleIsTShirt}
                                            />
                                        </td>
                                    </tr>
                                </table>
                            </div>*/
                        );
                }
            case 3:
                return (
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        <div style={{ width: '50%', float: 'left' }}>
                            <text style={{ fontWeight: 'bold' }}>Summary</text><br /><br />
                            <div style={{ position: 'relative' }}>
                                <CoverAvatar fileName={this.state.avatar} height={'180px'} />
                                <div style={{
                                    position: 'absolute', right: '5%', top: '10px',
                                    padding: '1%', backgroundColor: 'white', borderRadius: 30
                                }} onClick={this.handleOpenAvatarGallery}>
                                    <CameraIcon size={20} color='#045e7a' />
                                </div>
                            </div>
                            <br />
                            <table>
                                <tr>
                                    <td>
                                        <text style={{ fontWeight: 'bold' }}>Campaign Title</text><br />
                                    </td>
                                    <td>
                                        <text style={{ fontWeight: 'bold' }}> : </text>
                                    </td>
                                    <td style={{ width: '60%' }}>
                                        <text>{this.state.campaignTitle}</text>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <text style={{ fontWeight: 'bold' }}>Type of Campaign</text><br />
                                    </td>
                                    <td>
                                        <text style={{ fontWeight: 'bold' }}> : </text>
                                    </td>
                                    <td style={{ width: '60%' }}>
                                        {
                                            this.state.typeOfCampaign === 1 ?
                                                <text>Career&amp;Opportunities</text>
                                                :
                                                this.state.typeOfCampaign === 2 ?
                                                    <text>Online Events</text>
                                                    :
                                                    <text>Events</text>
                                        }
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        {
                                            this.state.typeOfCampaign == 1 ?
                                                <text style={{ fontWeight: 'bold' }}>Type of Career&amp;Opportunities</text>
                                                :
                                                this.state.typeOfCampaign == 2 ?
                                                    <text style={{ fontWeight: 'bold' }}>Type of Online Events</text>
                                                    :
                                                    <text style={{ fontWeight: 'bold' }}>Type of Event(s) </text>
                                        }
                                        <br />
                                    </td>
                                    <td>
                                        <text style={{ fontWeight: 'bold' }}> : </text>
                                    </td>
                                    <td>
                                        {
                                            this.state.typeOfCampaign == 1 ?
                                                this.state.typeOfSubCampaign === 1 ?
                                                    <text>Part-time</text>
                                                    :
                                                    this.state.typeOfSubCampaign === 2 ?
                                                        <text>Full-time</text>
                                                        :
                                                        this.state.typeOfSubCampaign === 3 ?
                                                            <text>Internship</text>
                                                            :
                                                            this.state.typeOfSubCampaign === 4 ?
                                                                <text>Volunteers</text>
                                                                :
                                                                this.state.typeOfSubCampaign === 5 ?
                                                                    <text>Freelance</text>
                                                                    :
                                                                    false
                                                :
                                                this.state.typeOfCampaign == 2 ?
                                                    this.state.typeOfSubCampaign === 1 ?
                                                        <text>Survey</text>
                                                        :
                                                        this.state.typeOfSubCampaign === 2 ?
                                                            <text>Feedback</text>
                                                            :
                                                            this.state.typeOfSubCampaign === 3 ?
                                                                <text>Idea Bank</text>
                                                                :
                                                                this.state.typeOfSubCampaign === 4 ?
                                                                    <text>Competition</text>
                                                                    :
                                                                    false
                                                    :
                                                    this.state.typeOfSubCampaign === 1 ?
                                                        <text>With Attendance</text>
                                                        :
                                                        this.state.typeOfSubCampaign === 2 ?
                                                            <text>Without Attendance</text>
                                                            :
                                                            false
                                        }
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style={{ width: '45%' }}>
                            <text style={{ fontWeight: 'bold' }}>Credit Summary</text><br /><br />
                            <div style={{ backgroundColor: '#F2F3F4', margin: '3%', padding: '5%', borderRadius: 20 }}>
                                <div>
                                    <text style={{ fontWeight: 'bold' }}>Current Credit</text><br />
                                    <div style={{ textAlign: 'right' }}>
                                        <text>{this.state.credit}</text><br />
                                    </div>
                                    <text style={{ fontWeight: 'bold' }}>Monsta XP</text><br />
                                    <div style={{ textAlign: 'right' }}>
                                        <text>{this.state.monstaXp}</text><br />
                                    </div>
                                    <text style={{ fontWeight: 'bold' }}>Balance Credit</text><br />
                                    <div style={{ textAlign: 'right' }}>
                                        <text>{this.state.credit - this.state.monstaXp}</text><br />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            default:
                return 'You\'re a long way from home sonny jim!';
        }
    }

    render() {
        const { finished, stepIndex } = this.state;
        if (this.state.finished) {
            return <Redirect push to="/" />;
        }

        if (this.state.isNavigate) {
            return <Redirect push to={`/companymycampaigninfo/${this.state.selectedKey}`} />;
        }

        if (this.state.isPurchase) {
            return <Redirect push to={`/PurchaseCredits`} />
        }

        return (
            <div>
                <Dialog
                    title={<DialogTitle title='Choose an image, recommended size 1366 * 768' onRequestClose={this.handleCloseAvatarGallery} />}
                    modal={false}
                    open={this.state.openAvatar}
                    onRequestClose={this.handleCloseAvatarGallery}
                    overlayStyle={{ background: "rgba(0,0,0,0.12)" }}
                    autoDetectWindowHeight={false}
                >
                    <ImageGallery set={this.handleRequestSetAvatar} close={this.handleCloseAvatarGallery} />
                </Dialog>
                <div style={{ margin: '20px 50px 20px 0', padding: '20px', overflow: 'hidden', width: this.state.windowWidth + 'px' }} >
                    <div style={{ width: this.state.formWidth, float: 'left' }}>
                        <div style={{ backgroundColor: 'white', padding: '20px', margin: '10px 30px 0 30px' }}>
                            <h1 style={{ textAlign: 'center' }}>Create a Campaign</h1>
                            <Stepper activeStep={stepIndex} style={{ width: '70%', marginLeft: '15%' }}>
                                <Step>
                                    <StepLabel></StepLabel>
                                </Step>
                                <Step>
                                    <StepLabel></StepLabel>
                                </Step>
                                <Step>
                                    <StepLabel></StepLabel>
                                </Step>
                                <Step>
                                    <StepLabel></StepLabel>
                                </Step>
                            </Stepper>
                            <Divider />
                            <div style={{ margin: '0 16px' }}>
                                <div>
                                    <p>{this.getStepContent(stepIndex)}</p>
                                    <div style={{ marginTop: 12, textAlign: 'center' }}>
                                        <FlatButton
                                            label="Back"
                                            disabled={stepIndex === 0}
                                            onClick={this.handlePrev}
                                            style={{ marginRight: 12 }}
                                        />
                                        <RaisedButton
                                            label="Save Draft"
                                            onClick={this.saveDraft}
                                            style={{ marginRight: 12 }}
                                        />
                                        <RaisedButton
                                            label={stepIndex === 3 ? 'Finish' : 'Next'}
                                            primary={true}
                                            onClick={this.handleNext}
                                            style={{ marginRight: 12 }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/*<div style={{ backgroundColor: 'white', padding: '20px', margin: '10px 30px 0 30px' }}>
                            <h1>About Us</h1>
                            <div style={{ backgroundColor: '#D7DBDD', padding: '10% 50% 10% 50%', margin: '0 0 5% 0' }}></div>
                            <div style={{ overflow: 'hidden' }}>
                                <div>
                                    <text style={{ fontWeight: 'bold' }}>{this.state.compName}</text><br />
                                    <p style={{ wordWrap: 'break-word' }}>
                                        {this.state.companyInfo.companyDesc}
                                    </p>
                                </div>
                            </div>
        </div>*/}
                    </div>
                    <div style={{ marginLeft: this.state.listWidth }}>
                        <List>
                            <Subheader>Campaign List</Subheader>
                            {
                                this.state.campaignList ?
                                    this.state.campaignList.map((e) => (
                                        <ListItem
                                            leftAvatar={<Avatar src="" />}
                                            secondaryTextLines="2"
                                            primaryText={e.data.campaignTitle}
                                            secondaryText={
                                                <text>{e.data.monstaXP} monsta XP</text>
                                            }
                                            onClick={this.navigateTo(e.key)}
                                        />
                                    ))
                                    :
                                    ''
                            }
                        </List>
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {

    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList,
        fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompCampaign)