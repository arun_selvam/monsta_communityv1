import React, { Component } from 'react'
import UserAvatar from 'UserAvatar'
import RaisedButton from 'material-ui/RaisedButton'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import '../styles/app.scss'
import TextField from 'material-ui/TextField'
import YouTube from 'react-youtube';
import TimePicker from 'material-ui/TimePicker';
import Profile from 'Profile'
import { Redirect } from 'react-router-dom'
import Map from 'GoogleMap'
import MapIcon from 'react-icons/lib/md/map'
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import FillBox from 'react-icons/lib/ti/media-stop'
import Box from 'react-icons/lib/ti/media-stop-outline'
import Checked from 'react-icons/lib/ti/input-checked'
import Workbook from 'react-excel-workbook'
import Pagination from 'Pagination';
import moment from 'moment'
import CoverAvatar from 'CoverAvatar'
import '../styles/div.scss'

var coursesList
var skillList
var users
var companyDesc
var campaignList

class CompMyCampaignInfo extends Component {

    constructor(props) {
        super(props)
        this.state = {
            monstaNetwork: [],
            redirect: false,
            avatarSize: window.innerWidth > 1000 ? window.innerWidth / 15 : (window.innerWidth > 700 ? window.innerWidth / 10 : window.innerWidth / 7),
            xpFontSize: window.innerWidth > 1000 ? 25 : 20,
            statusSize: window.innerWidth > 1000 ? 20 : 15,
            xpTitle: window.innerWidth > 1000 ? 25 : 15,
            width: window.innerWidth > 1000 ? '100%' : (window.innerWidth > 700 ? (window.innerWidth + 100) + 'px' : (window.innerWidth + 250) + 'px'),
            campaignList: [],
            skillList: [],
            coursesList: [],
            skills: [],
            requiredCourses: [],
            requiredSkills: [],
            questionList: [],
            opts: {
                height: window.innerHeight / 2,
                width: '100%',
                playerVars: {
                    autoplay: 1
                }
            },
            userList: [],
            participantList: [],
            isDirect: false,
            isEdit: false,
            uid: '',
            questionView: '',
            showDialog: false,
            sortedKeys: [],
            plist: [],
            isSelectedAll: false,
            isChecked: false,
            companyDesc: '',
            pageOfItems: [],
            postComment: '',
            displayPost: []
        }
        this.onChangePage = this.onChangePage.bind(this);
    }

    componentWillMount() {
        campaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0]).child(this.props.selectedKey)
        var specialization = firebaseRef.child("staticData")
        var user = firebaseRef.child("users")
        var requiredCourses = []
        var requiredSkills = []
        var skills = []

        specialization.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data.field)
            this.coursesList = data.courses
            this.skillList = data.skills
        })

        campaignList.on('value', snapshot => {
            user.on('value', snapshotUser => {
                snapshot.val().skillCanLearn ?
                    snapshot.val().skillCanLearn.map((e) => (
                        skills.push(this.skillList[e])
                    ))
                    :
                    ''

                snapshot.val().requiredCourses ?
                    snapshot.val().requiredCourses != '' ?
                        snapshot.val().requiredCourses.map((e) => (
                            requiredCourses.push(this.coursesList[e])
                        ))
                        :
                        ''
                    :
                    ''

                snapshot.val().requiredSkills ?
                    snapshot.val().requiredSkills != '' ?
                        snapshot.val().requiredSkills.map((e) => (
                            requiredSkills.push(this.skillList[e])
                        ))
                        :
                        ''
                    :
                    ''

                var quesList = []
                var partList = []
                var participantList = []
                if (snapshot.val().typeOfCampaign === 2) {
                    if (snapshot.val().typeOfSubCampaign === 1 || snapshot.val().typeOfSubCampaign === 2) {
                        quesList = snapshot.val().questionList
                        partList = snapshot.val().ParticipantList

                        quesList.forEach((ques, idx) => {
                            var max = 0
                            if (ques.selectedGroup === 'onlyOne' || ques.selectedGroup === 'multiSelect') {
                                ques.answerList.forEach((ans, sidx) => {
                                    var noSelected = 0
                                    if (partList) {
                                        var partKey = Object.keys(partList)
                                        partKey.forEach(key => {
                                            if (partList[key].userAnswer[idx]) {
                                                if (partList[key].userAnswer[idx].answerList[sidx].isAnswer) {
                                                    noSelected += 1
                                                }
                                            }
                                        }, this)
                                    }
                                    ans.noSelected = noSelected

                                    if (max < noSelected) {
                                        max = noSelected
                                    }
                                }, this)
                            }
                            else if (ques.selectedGroup === 'ratingScale') {
                                ques.answerList.forEach((ans, sidx) => {
                                    var sd = 0
                                    var d = 0
                                    var n = 0
                                    var a = 0
                                    var sa = 0
                                    var isAnswer = []
                                    if (partList) {
                                        var partKey = Object.keys(partList)
                                        partKey.forEach(key => {
                                            if (partList[key].userAnswer[idx]) {
                                                if (partList[key].userAnswer[idx].answerList[sidx].isAnswer === 'sd') {
                                                    sd++
                                                }
                                                else if (partList[key].userAnswer[idx].answerList[sidx].isAnswer === 'd') {
                                                    d++
                                                }
                                                else if (partList[key].userAnswer[idx].answerList[sidx].isAnswer === 'n') {
                                                    n++
                                                }
                                                else if (partList[key].userAnswer[idx].answerList[sidx].isAnswer === 'a') {
                                                    a++
                                                }
                                                else {
                                                    sa++
                                                }
                                            }
                                        }, this)
                                    }

                                    if (max < sd) {
                                        max = sd
                                    }
                                    if (max < d) {
                                        max = d
                                    }
                                    if (max < n) {
                                        max = n
                                    }
                                    if (max < a) {
                                        max = a
                                    }
                                    if (max < sa) {
                                        max = sa
                                    }

                                    ans.sd = sd
                                    ans.d = d
                                    ans.n = n
                                    ans.a = a
                                    ans.sa = sa
                                }, this)
                            } else {
                                var answer = []
                                if (partList) {
                                    var partKey = Object.keys(partList)
                                    partKey.forEach(key => {
                                        if (partList[key].userAnswer[idx]) {
                                            if (partList[key].userAnswer[idx].question === ques.question) {
                                                answer.push({
                                                    answer: partList[key].userAnswer[idx].isAnswer
                                                })
                                            }
                                        }
                                    }, this)
                                }
                                ques.answerList = answer
                            }
                            ques.maxNum = max
                        }, this)

                        if (partList) {
                            var partKey = Object.keys(partList)
                            partKey.forEach(userKey => {
                                participantList.push({
                                    name: snapshotUser.val()[userKey].info.fullName,
                                    university: snapshotUser.val()[userKey].info.university,
                                    email: snapshotUser.val()[userKey].info.email,
                                    mobileNumber: snapshotUser.val()[userKey].info.mobileNumber,
                                })
                            })
                        }

                    }
                }

                var plist = snapshot.val().ParticipantList
                if (plist) {
                    var pKeys = Object.keys(plist)
                    var sortedKeys = pKeys.sort((a, b) => {
                        if (plist[a].totalScore > plist[b].totalScore) {
                            return -1;
                        }
                        if (plist[a].totalScore < plist[b].totalScore) {
                            return 1;
                        }
                        return 0;
                    })

                    sortedKeys = sortedKeys.map(a => {
                        var obj = {}
                        obj.uid = a
                        obj.isSelected = false
                        return obj
                    })
                }

                var displayPost = []
                if (snapshot.val().broadcast) {
                    displayPost = snapshot.val().broadcast
                    displayPost.sort(function (a, b) {
                        return new Date(b.datePost) - new Date(a.datePost);
                    });
                }

                this.setState({
                    campaignList: snapshot.val(), requiredCourses: requiredCourses,
                    requiredSkills: requiredSkills, skills: skills, questionList: quesList,
                    sortedKeys: sortedKeys, plist: plist, displayPost: displayPost,
                    userList: snapshotUser.val(),
                    companyDesc: snapshotUser.val()[Object.keys(this.props.peopleInfo)[0]].info.companyDesc,
                    participantList: participantList
                })
            })
        })
    }

    componentDidMount() {
        /*var skill = []
            this.state.campaignList != '' ?
                this.state.campaignList.skillCanLearn ?
                    this.state.campaignList.skillCanLearn.map((e) => (
                        skill.push(this.state.skillList[e])
                    ))
                    :
                    ''
                :
                ''
            this.setState({ skills: skill })
    
            var requiredCourses = []
            this.state.campaignList != '' ?
                this.state.campaignList.requiredCourses ?
                    this.state.campaignList.requiredCourses != '' ?
                        this.state.campaignList.requiredCourses.map((e) => (
                            requiredCourses.push(this.state.coursesList[e])
                        ))
                        :
                        ''
                    :
                    ''
                :
                ''
            this.setState({ requiredCourses: requiredCourses })
    
            var requiredSkills = []
            this.state.campaignList != '' ?
                this.state.campaignList.requiredSkills ?
                    this.state.campaignList.requiredSkills != '' ?
                        this.state.campaignList.requiredSkills.map((e) => (
                            requiredSkills.push(this.state.skillList[e])
                        ))
                        :
                        ''
                    :
                    ''
                :
                ''
            this.setState({ requiredSkills: requiredSkills })

        var questionList = []
        this.state.campaignList != '' ?
            this.state.campaignList.questionList ?
                this.state.campaignList.questionList != '' ?
                    this.state.campaignList.questionList.map((e) => (
                        questionList.push(e)
                    ))
                    :
                    ''
                :
                ''
            :
            ''
        this.setState({ questionList: questionList })
        this.participantList()*/
    }

    /*participantList = () => {
        var pList = firebaseRef.child(`campaigns/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}/ParticipantList`)
        pList.on('value', snapshot => {
            var participantList = []
            var plist = snapshot.val()
            var pKeys = Object.keys(plist)
            var sortedKeys = pKeys.sort((a, b) => {
                if (plist[a].totalScore > plist[b].totalScore) {
                    return -1;
                }
                if (plist[a].totalScore < plist[b].totalScore) {
                    return 1;
                }
                return 0;
            })
            var correctAns = 0
            var totCorrectAns

            sortedKeys = sortedKeys.map(a => {
                var obj = {}
                obj.uid = a
                obj.isSelected = false
                return obj
            })

            this.setState({ sortedKeys: sortedKeys, plist: plist })
        })
    }*/

    handleCommanetChange = (event) => {
        this.setState({
            postComment: event.target.value,
        });
    };

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems, isSelectedAll: false });
    }

    handleAllRate = () => () => {
        var rateSelected = firebaseRef.child(`campaigns/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}/ParticipantList`)
        var users = firebaseRef.child('users')
        this.state.sortedKeys.forEach(e => {
            if (e.isSelected) {
                rateSelected.child(e.uid).update({
                    isRate: true
                })

                var monstaXP = 0
                var skills = []
                var addXP = users.child(`${e.uid}/info`)
                addXP.on('value', snapshot => {
                    snapshot.val().monstaXP ?
                        monstaXP = snapshot.val().monstaXP
                        :
                        0

                    if (snapshot.val().skills) {
                        skills = snapshot.val().skills
                        this.state.campaignList.skillCanLearn.forEach(skill => {
                            if (!snapshot.val().skills.includes(skill)) {
                                skills.push(skill)
                            }
                        })
                    } else {
                        skills = this.state.campaignList.skillCanLearn
                    }
                })

                addXP.update({
                    skills: skills,
                    monstaXP: monstaXP + this.state.campaignList.monstaXP
                })

                var notify = {
                    description: `Your application, ${this.state.campaignList.campaignTitle} has been approved by ${this.props.fullName}`,
                    url: `/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`,
                    notifierUserId: Object.keys(this.props.peopleInfo)[0],
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${e.uid}`).push(notify)
            }
        })
        //this.participantList()
        this.setState({ isSelectedAll: false, isChecked: false })
    }

    handleBroadcast = () => {
        var broadcast = []

        if (this.state.postComment) {
            if (this.state.campaignList.broadcast) {
                broadcast = this.state.campaignList.broadcast
                broadcast.push({
                    datePost: new Date().toString(),
                    message: this.state.postComment
                })

                campaignList.update({
                    broadcast: broadcast
                })

            } else {
                campaignList.update({
                    broadcast: [{
                        datePost: new Date().toString(),
                        message: this.state.postComment
                    }]
                })
            }

            /*if (this.state.sortedKeys) {
                if (this.state.sortedKeys.length > 0) {
                    this.state.sortedKeys.forEach((e) => {
                        if (this.state.userList[e.uid]) {
                            var notify = {
                                description: `${this.state.campaignList.Company} has been added by new post to the campaign, ${this.state.campaignList.campaignTitle}`,
                                url: `/CampaignInfo/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}`,
                                notifierUserId: Object.keys(this.props.peopleInfo)[0],
                                isSeen: false
                            }
                            var notifyRef = firebaseRef.child(`userNotifies/${e.uid}`).push(notify)
                        }
                    })
                }
            }*/

            this.setState({ postComment: '' })
        }
        else {
            alert('Nothing to broadcast')
        }




        /*if (this.state.campaignList.typeOfCampaign === 1) {
            campaignList.update({
                description: this.state.campaignList.campaignDesc,
                additionalReward: this.state.campaignList.addReward,
                targetStudent: this.state.campaignList.targetStudent,
                specialization: this.state.campaignList.specialization,
                skillCanLearn: this.state.campaignList.skillCanLearn,
                requirements: this.state.campaignList.requirements,
                responsibilities: this.state.campaignList.responsibilities,
                benefits: this.state.campaignList.benefits,
                minPayout: this.state.campaignList.minAmount,
                maxPayout: this.state.campaignList.maxAmount,
                isPrivacy: this.state.campaignList.privacy,
                salaryType: this.state.campaignList.salaryType,
                minEducation: this.state.campaignList.minEducation,
                requiredCourses: this.state.campaignList.requiredCourses,
                requiredSkills: this.state.campaignList.requiredSkills,
                workLocation: this.state.campaignList.workLocation ? this.state.campaignList.workLocation : [],
                aptitudeTest: this.state.campaignList.aptitudeTest,
                questionList: this.state.campaignList.questionList
            })
        }
        else if (this.state.campaignList.typeOfCampaign === 2) {
            if (this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2) {
                campaignList.update({
                    description: this.state.campaignList.campaignDesc,
                    additionalReward: this.state.campaignList.addReward,
                    targetStudent: this.state.campaignList.targetStudent,
                    skillCanLearn: this.state.campaignList.skillCanLearn,
                    questionList: this.state.campaignList.questionList,
                    isVideo: this.state.campaignList.isVideo,
                    videoId: this.state.campaignList.videoId
                })
            } else {
                campaignList.update({
                    description: this.state.campaignList.campaignDesc,
                    additionalReward: this.state.campaignList.addReward,
                    targetStudent: this.state.campaignList.targetStudent,
                    skillCanLearn: this.state.campaignList.skillCanLearn,
                    submitMethod: this.state.campaignList.submitMethod
                })
            }
        }
        else if (this.state.campaignList.typeOfCampaign === 3) {
            var eventDateArray = this.state.campaignList.eventDate.toDateString().split(' ')
            var eventDateString = eventDateArray[2] + ' ' + eventDateArray[1] + ' ' + eventDateArray[3]
            campaignList.update({
                description: this.state.campaignList.campaignDesc,
                additionalReward: this.state.campaignList.addReward,
                targetStudent: this.state.campaignList.targetStudent,
                skillCanLearn: this.state.campaignList.skillCanLearn,
                eventDate: eventDateString,
                eventTime: this.state.campaignList.eventTime.toString(),
                eventAdd1: this.state.campaignList.eventAdd1,
                eventAdd2: this.state.campaignList.eventAdd2,
                eventCity: this.state.campaignList.eventCity,
                eventState: this.state.campaignList.eventState,
                eventZip: this.state.campaignList.eventZip,
                isTicket: this.state.campaignList.ticket,
                ticketPrice: this.state.campaignList.eventTicketPrice,
                //isName: this.state.isName,
                //isAge: this.state.isAge,
                //isGender: this.state.isGender,
                //isContact: this.state.isContact,
                //isEmail: this.state.isEmail,
                //isCourse: this.state.isCourse,
                //isUni: this.state.isUni,
                //isTShirt: this.state.isTShirt
            })
        }*/
    }

    handleIsRate = (uid) => () => {

        var isRate = firebaseRef.child(`campaigns/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}/ParticipantList/${uid}`)
        isRate.update({
            isRate: true
        })

        var addXP = firebaseRef.child(`users/${uid}/info`)
        var monstaXP = 0
        var skills = []
        addXP.on('value', snapshot => {
            snapshot.val().monstaXP ?
                monstaXP = snapshot.val().monstaXP
                :
                0

            if (snapshot.val().skills) {
                skills = snapshot.val().skills
                this.state.campaignList.skillCanLearn.forEach(e => {
                    if (!snapshot.val().skills.includes(e)) {
                        skills.push(e)
                    }
                })
            } else {
                skills = this.state.campaignList.skillCanLearn
            }
        })

        addXP.update({
            skills: skills,
            monstaXP: monstaXP + this.state.campaignList.monstaXP
        })

        var notify = {
            description: `Your application, ${this.state.campaignList.campaignTitle} has been approved by ${this.props.fullName}`,
            url: `/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`,
            notifierUserId: Object.keys(this.props.peopleInfo)[0],
            isSeen: false,
            date: new Date().toString()
        }
        var notifyRef = firebaseRef.child(`userNotifies/${uid}`).push(notify)
        this.setState({ isSelectedAll: false, isChecked: false })
        //this.participantList()
    }

    handleAllAttend = () => () => {
        var attendSelected = firebaseRef.child(`campaigns/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}/ParticipantList`)
        var users = firebaseRef.child('users')
        this.state.pageOfItems.forEach(e => {
            if (e.isSelected) {
                attendSelected.child(e.uid).update({
                    isAttend: true
                })

                var monstaXP = 0
                var skills = []
                var addXP = users.child(`${e.uid}/info`)
                addXP.on('value', snapshot => {
                    snapshot.val().monstaXP ?
                        monstaXP = snapshot.val().monstaXP
                        :
                        0
                })
                addXP.update({
                    monstaXP: monstaXP + this.state.campaignList.monstaXP
                })

                var notify = {
                    description: `Your application, ${this.state.campaignList.campaignTitle} has been approved by ${this.props.fullName}`,
                    url: `/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`,
                    notifierUserId: Object.keys(this.props.peopleInfo)[0],
                    isSeen: false,
                    date: new Date().toString()
                }
                var notifyRef = firebaseRef.child(`userNotifies/${e.uid}`).push(notify)
            }
        })
        //this.participantList()
        this.setState({ isSelectedAll: false, isChecked: false })
    }

    handleIsAttend = (uid) => () => {
        var isAttend = firebaseRef.child(`campaigns/${Object.keys(this.props.peopleInfo)[0]}/${this.props.selectedKey}/ParticipantList/${uid}`)
        isAttend.update({
            isAttend: true
        })

        var addXP = firebaseRef.child(`users/${uid}/info`)
        var monstaXP = 0
        addXP.on('value', snapshot => {
            snapshot.val().monstaXP ?
                monstaXP = snapshot.val().monstaXP
                :
                0
        })
        addXP.update({
            monstaXP: monstaXP + this.state.campaignList.monstaXP
        })

        var notify = {
            description: `Your application, ${this.state.campaignList.campaignTitle} has been approved by ${this.props.fullName}`,
            url: `/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`,
            notifierUserId: Object.keys(this.props.peopleInfo)[0],
            isSeen: false,
            date: new Date().toString()
        }
        var notifyRef = firebaseRef.child(`userNotifies/${uid}`).push(notify)
        this.setState({ isSelectedAll: false, isChecked: false })
        //this.participantList()
    }

    handleShowAnswer = (uid, name) => () => {
        var quesList = this.state.campaignList.questionList
        var userAns = this.state.campaignList.ParticipantList[uid].userAnswer

        quesList.forEach(function (e, idx) {
            e.answerList.forEach(function (ans, sidx) {
                if (ans.isAnswer && userAns[idx].answerList[sidx].isAnswer) {
                    ans.color = 'green'
                    ans.isSelected = true
                }
                else if (ans.isAnswer) {
                    ans.color = 'green'
                    ans.isSelected = false
                }
                else if (userAns[idx].answerList[sidx].isAnswer) {
                    ans.color = 'red'
                    ans.isSelected = true
                }
                else {
                    ans.color = ''
                    ans.isSelected = false
                }
            }, this)
        }, this)

        var questionView = (
            <div>
                <text style={{ fontSize: '20', fontWeight: 'bold' }}>{name + "'s Answer"}</text>
                <br />
                <hr className="hr-text" />
                {
                    quesList.map((quesList) => (
                        <div style={{ paddingLeft: '2%' }}>
                            <text>{quesList.question}</text><br />
                            <div style={{ paddingLeft: '1%' }}>
                                {
                                    quesList.answerList.map((ans) => (
                                        quesList.selectedGroup === 'multiSelect' ?
                                            <div style={{ color: ans.color }}>
                                                <input type="checkbox" checked={ans.isSelected === true ? "checked" : ''} disabled />
                                                <text>&nbsp;{ans.answer}</text>
                                                <br />
                                            </div>

                                            :
                                            <div style={{ color: ans.color }}>
                                                <input type="radio" checked={ans.isSelected === true ? "checked" : ''} disabled />
                                                <text>&nbsp;{ans.answer}</text>
                                                <br />
                                            </div>
                                    ))
                                }
                            </div>
                            <br />
                        </div>
                    ))
                }
            </div>
        )
        //console.log(this.state.campaignList.ParticipantList[uid].userAnswer, this.state.questionList)
        this.setState({ questionView: questionView, showDialog: true })
    }

    handleCheckedAll = () => () => {
        if (this.state.pageOfItems.length > 0) {
            var selectedStu = false
            var pageItemKey = []
            this.state.pageOfItems.forEach(e => {
                if (this.state.plist[e.uid].isAttend) {
                    false
                }
                else {
                    selectedStu = true
                }
            })

            if (selectedStu) {
                var newSortedKey = this.state.pageOfItems
                newSortedKey = newSortedKey.map(a => {
                    var obj = {}
                    obj.uid = a.uid
                    obj.isSelected = this.state.isSelectedAll ? false : a.isSelected ? a.isSelected : !a.isSelected
                    return obj
                })

                this.setState({ isSelectedAll: !this.state.isSelectedAll, pageOfItems: newSortedKey })
            }
            else {
                alert('No Student can be selected')
                this.setState({ isSelectedAll: false })
            }
        }
        else {
            alert('No Student can be selected')
            this.setState({ isSelectedAll: false })
        }
    }

    handleIsChecked = (uid) => () => {
        var newSortedKey = this.state.pageOfItems
        var isChecked = false
        newSortedKey.forEach(e => {
            if (e.uid === uid) {
                e.isSelected = !e.isSelected
            }

            if (e.isSelected) {
                isChecked = true
            }
        })
        this.setState({ pageOfItems: newSortedKey, isChecked: isChecked })
    }

    handleOpenLink = (uid) => () => {
        if (this.state.campaignList.ParticipantList[uid].submitLink) {
            window.open(this.state.campaignList.ParticipantList[uid].submitLink, "_blank")
        }
        else {
            window.open('', "_blank")
        }

    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    handleEditCampaign = () => () => {
        this.setState({ uid: this.props.selectedKey, isEdit: true })
    }

    handleClose = () => {
        this.setState({ showDialog: false });
    };

    render() {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleClose}
            />,
        ];

        var setWidth = '30%'

        if (this.state.campaignList.typeOfCampaign !== 1) {
            setWidth = '65%'
        }

        if (this.state.isDirect) {
            return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }

        if (this.state.isEdit) {
            return <Redirect push to={`/companyeditcampaigninfo/${this.state.uid}`} />
        }

        var eventLoc = this.state.campaignList.eventAdd1 + this.state.campaignList.eventAdd2 + this.state.campaignList.eventZip + ',' + this.state.campaignList.eventCity + this.state.campaignList.eventState
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                <Dialog
                    actions={actions}
                    modal={true}
                    open={this.state.showDialog}
                    onRequestClose={this.handleClose}
                    autoScrollBodyContent={true}
                >
                    {this.state.questionView}
                </Dialog>
                <div style={{
                    margin: '1%', width: '66%',
                    height: 'auto', position: 'relative'
                }}>
                    <div>
                        <div style={{ position: 'absolute', left: '5%', top: '340px' }}>
                            <UserAvatar fileName={this.state.campaignList.companyAvatar}
                                size={90} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                        </div>
                        <CoverAvatar fileName={this.state.campaignList.campAvatar} height={'400px'} />
                        <div style={{
                            backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '5% 2% 2% 7%',
                            borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                        }}>
                            <div style={{ position: 'absolute', right: '5%', top: '410px' }}>
                                <RaisedButton backgroundColor='#F5B041' label="Edit" onClick={this.handleEditCampaign()} />
                            </div>
                            <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.campaignList.campaignTitle}</text><br />
                            <text style={{ fontSize: 13 }}>{this.state.campaignList.Company}</text><br />
                            <text style={{ fontSize: 13 }}>{`Published on ${moment(new Date(this.state.campaignList.date)).format('MMMM Do YYYY')}`} </text><br />

                            <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 14 }}>Start Date</text><br />
                                    <text style={{ fontSize: 13 }}>{moment(new Date(this.state.campaignList.startDate)).format('MMMM Do YYYY')}</text>
                                </div>
                                <div style={{ marginTop: '2%', width: '50%' }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 14 }}>End Date</text><br />
                                    <text style={{ fontSize: 13 }}>{moment(new Date(this.state.campaignList.endDate)).format('MMMM Do YYYY')}</text>
                                </div>
                            </div>
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <div style={{ marginTop: '2%' }}>
                                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN DETIALS </text><br />
                                        <table>
                                            <tr>
                                                <td><MapIcon /></td>
                                                <td style={{ display: 'flex' }}>
                                                    {
                                                        this.state.campaignList.workLocation ?
                                                            this.state.campaignList.workLocation.map((loc, idx) => (

                                                                <div style={{ backgroundColor: '#CACFD2', padding: '5%', borderRadius: 20, margin: '1%' }}>
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.add1}</text><br />
                                                                    {
                                                                        loc.add2 ?
                                                                            <div>
                                                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.add2}</text><br />
                                                                            </div>
                                                                            :
                                                                            false
                                                                    }
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.city}</text> <br />
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{loc.zip}, {loc.state}</text>
                                                                </div>
                                                            ))
                                                            :
                                                            <text style={{ fontSize: 13, paddingLeft: '1%' }}>no&nbsp;define</text>
                                                    }
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    :
                                    this.state.campaignList.typeOfCampaign === 3 ?
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN&nbsp;DETIALS </text><br />
                                            <table>
                                                <tr>
                                                    <td><Calender /></td>
                                                    <td>
                                                        <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventDate}</text>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><Time /></td>
                                                    <td>
                                                        <TimePicker
                                                            style={{ fontSize: 13, paddingLeft: '1%' }}
                                                            disabled={true}
                                                            format="ampm"
                                                            value={new Date(this.state.campaignList.eventTime)}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><MapIcon /></td>
                                                    <td>
                                                        <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventAdd1}</text><br />
                                                        {
                                                            this.state.campaignList.eventAdd2 ?
                                                                <div>
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventAdd2}</text><br />
                                                                </div>
                                                                :
                                                                false
                                                        }
                                                        <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventCity}</text> <br />
                                                        <text style={{ fontSize: 13, paddingLeft: '1%' }}>{this.state.campaignList.eventZip}, {this.state.campaignList.eventState}</text>
                                                    </td>
                                                </tr>
                                            </table>
                                            <div style={{ marginTop: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>TICKET</text><br />
                                                {
                                                    this.state.campaignList.isTicket ?
                                                        <text style={{ fontSize: 13 }}>RM&nbsp;{this.state.campaignList.ticketPrice}</text>
                                                        :
                                                        <text style={{ fontSize: 13 }}>Free</text>
                                                }
                                            </div>
                                        </div>
                                        : false
                            }
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontSize: 13, fontWeight: 'bold' }}>CAMPAIGN&nbsp;DESCRIPTION </text><br />
                                <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.description}</p>
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                {
                                    this.state.campaignList.typeOfCampaign === 1 ?
                                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;CAREER&amp;OPPORTUNITIES<br /></text>
                                        :
                                        this.state.campaignList.typeOfCampaign === 2 ?
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;DIGITAL&nbsp;CHALLENGE<br /></text>
                                            :
                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                <text style={{ fontSize: 13, fontWeight: 'bold' }}>TYPE&nbsp;OF&nbsp;EVENT<br /></text>
                                                : ''
                                }
                                {
                                    this.state.campaignList.typeOfCampaign === 1 ?
                                        this.state.campaignList.typeOfSubCampaign === 1 ?
                                            <text style={{ fontSize: 13 }}>Part-time</text>
                                            :
                                            this.state.campaignList.typeOfSubCampaign === 2 ?
                                                <text style={{ fontSize: 13 }}>Full-time</text>
                                                :
                                                this.state.campaignList.typeOfSubCampaign === 3 ?
                                                    <text style={{ fontSize: 13 }}>Internship</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 4 ?
                                                        <text style={{ fontSize: 13 }}>Volunteers</text>
                                                        :
                                                        this.state.campaignList.typeOfSubCampaign === 5 ?
                                                            <text style={{ fontSize: 13 }}>Freelance</text>
                                                            :
                                                            ''
                                        :
                                        this.state.campaignList.typeOfCampaign === 2 ?
                                            this.state.campaignList.typeOfSubCampaign === 1 ?
                                                <text style={{ fontSize: 13 }}>Survey</text>
                                                :
                                                this.state.campaignList.typeOfSubCampaign === 2 ?
                                                    <text style={{ fontSize: 13 }}>Feedback</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 3 ?
                                                        <text style={{ fontSize: 13 }}>Idea&nbsp;Bank</text>
                                                        :
                                                        this.state.campaignList.typeOfSubCampaign === 4 ?
                                                            <text style={{ fontSize: 13 }}>Competition</text>
                                                            :
                                                            ''
                                            :
                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                this.state.campaignList.typeOfSubCampaign === 1 ?
                                                    <text style={{ fontSize: 13 }}>With&nbsp;Attendance</text>
                                                    :
                                                    this.state.campaignList.typeOfSubCampaign === 2 ?
                                                        <text style={{ fontSize: 13 }}>Without&nbsp;Attendance</text>
                                                        :
                                                        ''
                                                :
                                                ''
                                }
                                <br />
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>ADDITIONAL&nbsp;REWARD</text><br />
                                <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>
                                    {this.state.campaignList.additionalReward ? this.state.campaignList.additionalReward : 'None'}
                                </p>
                            </div>
                            <div style={{ marginTop: '2%' }}>
                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>TARGET&nbsp;STUDENT</text><br />
                                <text style={{ fontSize: 13 }}>{this.state.campaignList.targetStudent ? this.state.campaignList.targetStudent : 'no define'}</text><br />
                            </div>
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>SPECIALIZATION</text><br />
                                            <text style={{ fontSize: 13 }}>{this.state.campaignList.specialization}</text>
                                        </div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIREMENT</text><br />
                                            <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.requirements}</p>
                                        </div>
                                        <div style={{ marginTop: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>RESPONSIBILITIES</text><br />
                                            <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.campaignList.responsibilities}</p>
                                        </div>
                                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>BENEFITS</text><br />
                                                <text style={{ fontSize: 13 }}>{this.state.campaignList.benefits}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIRE COURSES</text><br />
                                                <text style={{ fontSize: 13 }}>{this.state.requiredCourses.join()}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>ALLOWANCE/ PAYOUT</text><br />
                                                <text style={{ fontSize: 13 }}>{`RM ${this.state.campaignList.minPayout} - RM ${this.state.campaignList.maxPayout}`}</text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>REQUIRE SKILLS</text><br />
                                                <text style={{ fontSize: 13 }}>
                                                    {this.state.requiredSkills ? this.state.requiredSkills.join() : 'None'}
                                                </text>
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>SALARY TYPE</text><br />
                                                {
                                                    this.state.campaignList.salaryType === 'PerHour' ?
                                                        <text style={{ fontSize: 13 }}>Per&nbsp;Hour</text>
                                                        :
                                                        this.state.campaignList.salaryType === 'PerDay' ?
                                                            <text style={{ fontSize: 13 }}>Per&nbsp;Day</text>
                                                            :
                                                            this.state.campaignList.salaryType === 'PerMonth' ?
                                                                <text style={{ fontSize: 13 }}>Per&nbsp;Month</text>
                                                                :
                                                                this.state.campaignList.salaryType === 'PerCompletion' ?
                                                                    <text style={{ fontSize: 13 }}>Per&nbsp;Completion/Project</text>
                                                                    :
                                                                    ''
                                                }
                                            </div>
                                            <div style={{ marginTop: '2%', width: '50%' }}>
                                                <text style={{ fontWeight: 'bold', fontSize: 14 }}>MINIMUM EDUCATION</text><br />
                                                {
                                                    this.state.campaignList.minEducation === 'PrimarySchool' ?
                                                        <text style={{ fontSize: 13 }}>Primary&nbsp;School</text>
                                                        :
                                                        this.state.campaignList.minEducation === 'HighSchool' ?
                                                            <text style={{ fontSize: 13 }}>High&nbsp;School</text>
                                                            :
                                                            this.state.campaignList.minEducation === 'Diploma' ?
                                                                <text style={{ fontSize: 13 }}>Certificates/Vocational/Diploma</text>
                                                                :
                                                                this.state.campaignList.minEducation === 'Degree' ?
                                                                    <text style={{ fontSize: 13 }}>Degree</text>
                                                                    :
                                                                    this.state.campaignList.minEducation === 'Master' ?
                                                                        <text style={{ fontSize: 13 }}>Master/PHD</text>
                                                                        :
                                                                        this.state.campaignList.minEducation === 'NoLimit' ?
                                                                            <text style={{ fontSize: 13 }}>No&nbsp;Limit</text>
                                                                            :
                                                                            ''
                                                }
                                            </div>
                                        </div>
                                    </div>
                                    : false
                            }
                        </div>
                    </div>
                    <div style={{
                        marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                        height: 'auto', padding: '2% 2% 2% 4%'
                    }}>
                        <text style={{ fontSize: 13, fontWeight: 'bold' }}>ABOUT&nbsp;THE&nbsp;BRAND</text><br />
                        <p style={{ fontSize: 13, wordWrap: 'break-word', margin: 0 }}>{this.state.companyDesc}</p>
                    </div>
                    {
                        this.state.campaignList.isVideo ?
                            <div style={{
                                marginTop: '1%', backgroundColor: 'white', borderRadius: 20,
                                height: 'auto', padding: '2% 2% 2% 4%'
                            }}>
                                <YouTube
                                    videoId={this.state.campaignList.videoId}
                                    opts={this.state.opts}
                                />
                            </div>
                            :
                            false
                    }
                </div>
                <div style={{ margin: '1%', width: '30%', height: 'auto', position: 'relative' }}>
                    <div style={{ backgroundColor: 'white', borderRadius: 20, padding: '1% 1% 3% 1%' }}>
                        <div style={{ marginTop: '2%', textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Monsta&nbsp;Reward</text><br />
                            <text style={{ fontWeight: 'bold', fontSize: 25 }}>{`${this.state.campaignList.monstaXP ? this.state.campaignList.monstaXP : 0} XP`}</text>
                        </div>
                        <br />
                        <div style={{ marginTop: '2%', textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>TYPE</text><br />
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <text style={{ fontSize: 14 }}>Carrer</text>
                                    :
                                    this.state.campaignList.typeOfCampaign === 2 ?
                                        this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                            <text style={{ fontSize: 14 }}>Survey/&nbsp;Feedback</text>
                                            :
                                            <text style={{ fontSize: 14 }}>Submit&nbsp;Link</text>
                                        :
                                        this.state.campaignList.typeOfCampaign === 3 ?
                                            <text style={{ fontSize: 14 }}>Event</text>
                                            :
                                            ''
                            }
                        </div>
                        <br />
                        <div style={{ marginTop: '2%', textAlign: 'center', padding: '1%' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Skills&nbsp;Offer</text><br />
                            <div>
                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                    {
                                        this.state.skills.length > 0 ?
                                            this.state.skills.map((e, idx) => (
                                                <div style={{
                                                    backgroundColor: '#CACFD2', padding: '1%', width: `${e.length * 8 + 20}px`,
                                                    margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                }}>
                                                    <text style={{ fontSize: 13 }}>{e}</text>
                                                </div>
                                            ))
                                            :
                                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                <text style={{ fontSize: 13 }}>None</text>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <br />
                        <div style={{ textAlign: 'center' }}>
                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Latest&nbsp;Post</text><br />
                        </div>
                        <div style={{
                            margin: '2% 5% 2% 5%', padding: '1%', overflowY: 'auto', height: '250px',
                            backgroundColor: '#E5E7E9', borderRadius: 10
                        }}>
                            {
                                this.state.displayPost.map(e => (
                                    <div style={{ backgroundColor: 'white', borderRadius: 10, padding: '2%', marginBottom: '2%' }}>
                                        <text style={{ fontSize: 11 }}>
                                            {moment(new Date(e.datePost)).format('MMMM Do YYYY, h:mm:ss a')}
                                        </text>
                                        <p style={{ margin: 0, fontSize: 13, paddingLeft: '2%', wordWrap: 'break-word' }}>{e.message}</p>
                                    </div>
                                ))
                            }
                        </div>
                        <div style={{ padding: '1%', marginLeft: '2%' }}>
                            <TextField
                                style={{ width: '100%' }}
                                floatingLabelText="Enter your post"
                                multiLine={true}
                                value={this.state.postComment}
                                onChange={this.handleCommanetChange}
                            /><br />
                            <RaisedButton backgroundColor='#F5B041' label="Broadcast" style={{ width: '100%' }} onClick={this.handleBroadcast} />
                        </div>
                    </div>
                </div>
                {
                    this.state.campaignList.typeOfCampaign === 1 ?
                        this.state.campaignList.aptitudeTest ?
                            <div style={{
                                margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                height: 'auto', padding: '2% 2% 2% 4%'
                            }}>
                                <text style={{ fontSize: 22, fontWeight: 'bold' }}>Aptitude&nbsp;Test</text><br />
                                <div>
                                    <br />
                                    {this.state.campaignList.questionList.map((quesList, idx) => (
                                        <div style={{ paddingLeft: '2%' }}>
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                {idx + 1 + '. ' + quesList.question}
                                            </text><br />
                                            <div style={{ paddingLeft: '1%' }}>
                                                {
                                                    quesList.answerList.map((ans) => (
                                                        quesList.selectedGroup === 'multiSelect' ?
                                                            <div style={{
                                                                padding: '1%', backgroundColor: ans.isAnswer ? '#2ECC71' : '',
                                                                marginTop: '1%', borderRadius: 20
                                                            }}>
                                                                <input type="checkbox" disabled />
                                                                <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                <br />
                                                            </div>

                                                            :
                                                            <div style={{
                                                                padding: '1%', backgroundColor: ans.isAnswer ? '#2ECC71' : '',
                                                                marginTop: '1%', borderRadius: 20
                                                            }}>
                                                                <input type="radio" disabled />
                                                                <text style={{ fontSize: 13 }}>&nbsp;{ans.answer}</text>
                                                                <br />
                                                            </div>
                                                    ))
                                                }
                                            </div>
                                            <br />
                                        </div>
                                    ))}
                                </div>
                            </div>
                            : false
                        : this.state.campaignList.typeOfCampaign === 2 ?
                            this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                <div style={{
                                    margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                    height: 'auto', padding: '2% 2% 2% 4%'
                                }}>
                                    <Workbook filename={this.state.campaignList.campaignTitle + '.xlsx'} element={
                                        <RaisedButton backgroundColor='#F5B041' label="Export Data" style={{ fontWeight: 'bold', cursor: 'pointer', float: 'right', marginTop: '5px' }} />}>
                                        {
                                            this.state.questionList.map((ques, idx) => (
                                                ques.selectedGroup === 'onlyOne' || ques.selectedGroup === 'multiSelect' ?
                                                    <Workbook.Sheet data={ques.answerList} name={idx + 1 + '. ' + ques.question.replace(/[^a-zA-Z ]/g, "")}>
                                                        <Workbook.Column label="" value="answer" />
                                                        <Workbook.Column label="No of Student" value="noSelected" />
                                                    </Workbook.Sheet>
                                                    :
                                                    ques.selectedGroup === 'ratingScale' ?
                                                        <Workbook.Sheet data={ques.answerList} name={idx + 1 + '. ' + ques.question.replace(/[^a-zA-Z ]/g, "")}>
                                                            <Workbook.Column label="" value="answer" />
                                                            <Workbook.Column label="Strongly Disagree" value="sd" />
                                                            <Workbook.Column label="Disagree" value="d" />
                                                            <Workbook.Column label="Neutral" value="n" />
                                                            <Workbook.Column label="Agree" value="a" />
                                                            <Workbook.Column label="Strongly Agree" value="sa" />
                                                        </Workbook.Sheet>
                                                        :
                                                        <Workbook.Sheet data={ques.answerList} name={idx + 1 + '. ' + ques.question.replace(/[^a-zA-Z ]/g, "")}>
                                                            <Workbook.Column label="Answer" value="answer" />
                                                        </Workbook.Sheet>
                                            ))
                                        }
                                        {
                                            this.state.participantList ?
                                                <Workbook.Sheet data={this.state.participantList} name={'Students Participate'}>
                                                    <Workbook.Column label="Name" value="name" />
                                                    <Workbook.Column label="University" value="university" />
                                                    <Workbook.Column label="Email" value="email" />
                                                    <Workbook.Column label="Contact Number" value="mobileNumber" />
                                                </Workbook.Sheet>
                                                :
                                                false
                                        }
                                    </Workbook>
                                    <text style={{ fontSize: 22, fontWeight: 'bold' }}>Questions</text> <br />
                                    <br />
                                    {this.state.questionList.map((quesList, idx) => (
                                        <div style={{ paddingLeft: '2%', width: '100%' }}>
                                            <text style={{ fontSize: 13, fontWeight: 'bold' }}>
                                                {idx + 1 + '. ' + quesList.question}
                                            </text><br />
                                            <div style={{ paddingLeft: '1%' }}>
                                                {
                                                    quesList.selectedGroup === 'ratingScale' ?
                                                        <div>
                                                            <br />
                                                            <table style={{ borderCollapse: 'collapse' }}>
                                                                <tr>
                                                                    <th></th>
                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Disagree&nbsp;</text></th>
                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Disagree&nbsp;</text></th>
                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Neutral&nbsp;</text></th>
                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Agree&nbsp;</text></th>
                                                                    <th><text style={{ fontSize: 13 }}>&nbsp;Strongly&nbsp;Agree&nbsp;</text></th>
                                                                </tr>
                                                                {/*border: '1px solid black'*/}
                                                                {
                                                                    quesList.answerList ?
                                                                        quesList.answerList.map((ans) => (
                                                                            <tr>
                                                                                <td>
                                                                                    <text style={{ padding: '2%', fontSize: 13 }}>&nbsp;{ans.answer.trim()}&nbsp;</text>
                                                                                </td>
                                                                                <td >
                                                                                    <div style={{ textAlign: 'center', backgroundColor: '#E74C3C', margin: '1%', borderRadius: 20 }}>
                                                                                        <text style={{ fontSize: 13 }}>{ans.sd ? ans.sd : 0}</text>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div style={{ textAlign: 'center', backgroundColor: '#F39C12', margin: '1%', borderRadius: 20 }}>
                                                                                        <text style={{ fontSize: 13 }}>{ans.d ? ans.d : 0}</text>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div style={{ textAlign: 'center', backgroundColor: '#F4D03F', margin: '1%', borderRadius: 20 }}>
                                                                                        <text style={{ fontSize: 13 }}>{ans.n ? ans.n : 0}</text>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div style={{ textAlign: 'center', backgroundColor: '#2ECC71', margin: '1%', borderRadius: 20 }}>
                                                                                        <text style={{ fontSize: 13 }}>{ans.a ? ans.a : 0}</text>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <div style={{ textAlign: 'center', backgroundColor: '#5DADE2', margin: '1%', borderRadius: 20 }}>
                                                                                        <text style={{ fontSize: 13 }}>{ans.sa ? ans.sa : 0}</text>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        ))
                                                                        :
                                                                        ''
                                                                }
                                                            </table>
                                                        </div>
                                                        :
                                                        quesList.selectedGroup === 'multiSelect' || quesList.selectedGroup === 'onlyOne' ?
                                                            <table>
                                                                {
                                                                    quesList.answerList ?
                                                                        quesList.answerList.map((ans) => (
                                                                            quesList.selectedGroup === 'multiSelect' ?
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="checkbox" disabled />
                                                                                    </td>
                                                                                    <td>
                                                                                        <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                    </td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td>
                                                                                        <div style={{ textAlign: 'center', backgroundColor: '#5DADE2', margin: '1%', borderRadius: 20 }}>
                                                                                            <text style={{ fontSize: 13 }}>&nbsp;&nbsp;{ans.noSelected}&nbsp;&nbsp;</text>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                                :
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="radio" disabled />
                                                                                    </td>
                                                                                    <td>
                                                                                        <text style={{ fontSize: 13 }}>{ans.answer.trim()}</text>
                                                                                    </td>
                                                                                    <td>&nbsp;&nbsp;</td>
                                                                                    <td>
                                                                                        <div style={{ textAlign: 'center', backgroundColor: '#5DADE2', margin: '1%', borderRadius: 20 }}>
                                                                                            <text style={{ fontSize: 13 }}>&nbsp;&nbsp;{ans.noSelected}&nbsp;&nbsp;</text>
                                                                                        </div>
                                                                                    </td>
                                                                                </tr>
                                                                        ))
                                                                        : false
                                                                }
                                                            </table>
                                                            :
                                                            <table>
                                                                {
                                                                    quesList.selectedGroup === 'textboxMultiLine' ?
                                                                        <tr>
                                                                            <TextField
                                                                                disabled={true}
                                                                                hintText="Textbox (multiple line)"
                                                                            />
                                                                        </tr>
                                                                        :
                                                                        <tr>
                                                                            <TextField
                                                                                disabled={true}
                                                                                hintText="Textbox (single line)"
                                                                            />
                                                                        </tr>
                                                                }
                                                            </table>
                                                }
                                            </div>
                                            <br />
                                        </div>
                                    ))}
                                </div>
                                : false
                            : false
                }
                {
                    this.state.campaignList.typeOfCampaign === 1 ?
                        <div style={{
                            margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                            height: 'auto', padding: '2% 2% 2% 4%'
                        }}>
                            <text style={{ fontSize: 22, fontWeight: 'bold' }}>Students&nbsp;Applied</text><br /><br />
                            {
                                this.state.pageOfItems.length > 0 ?
                                    this.state.pageOfItems.map(e =>
                                        this.state.userList[e.uid] ?
                                            <div style={{ display: 'flex', backgroundColor: '#E5E7E9', padding: '2% 2% 2% 4%', borderRadius: 20, marginTop: '1%', width: '100%' }} >
                                                <div style={{ width: '40%' }}>
                                                    <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.userList[e.uid].info.fullName}</text>
                                                    <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                        {
                                                            this.state.userList[e.uid].info.monstaXP ?
                                                                ` ( Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1} )`
                                                                : ' ( Lvl 0 )'
                                                        }
                                                    </text>
                                                    <br />
                                                    <text style={{ fontSize: 14 }}>
                                                        {this.state.userList[e.uid].info.course ? this.state.userList[e.uid].info.course : '<------------>'}
                                                    </text><br /><br />
                                                    <text style={{ fontSize: 14 }}>
                                                        {this.state.userList[e.uid].info.university ? this.state.userList[e.uid].info.university : '<------------>'}
                                                    </text><br />
                                                    <text style={{ fontSize: 14 }}>
                                                        {this.state.userList[e.uid].info.state ? this.state.userList[e.uid].info.state : '<------------>'}
                                                    </text><br />
                                                    <text style={{ fontSize: 14 }}>{`Date Applied: ${
                                                        this.state.campaignList.ParticipantList[e.uid].dateApply ?
                                                            moment(new Date(this.state.campaignList.ParticipantList[e.uid].dateApply)).format('MMMM Do YYYY, h:mm:ss a')
                                                            : ''
                                                        }`}
                                                    </text><br />
                                                </div>
                                                {/*<div style={{ marginLeft: '10%', display: 'table', marginTop: 'auto', marginBottom: 'auto', textAlign: 'center' }}>
                                                    <text style={{ fontSize: 30 }}>
                                                        {
                                                            this.state.userList[e.uid].info.monstaXP ?
                                                                this.state.userList[e.uid].info.monstaXP > 1000 ?
                                                                    (this.state.userList[e.uid].info.monstaXP / 1000).toFixed(2) + 'K'
                                                                    :
                                                                    this.state.userList[e.uid].info.monstaXP
                                                                : 0
                                                        }
                                                    </text>
                                                    <text style={{ fontSize: 16, fontWeight: 'bold' }}>&nbsp;XP</text><br />
                                                    <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                        {
                                                            this.state.userList[e.uid].info.monstaXP ?
                                                                `Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1}`
                                                                : 'Lvl 0'
                                                        }
                                                    </text>
                                                    </div>*/}
                                                <div style={{ marginLeft: '10%', display: 'table', marginTop: 'auto', marginBottom: 'auto', textAlign: 'center' }}>
                                                    <text style={{ fontSize: 25 }}>
                                                        {
                                                            this.state.campaignList.ParticipantList[e.uid].totalScore ?
                                                                this.state.campaignList.ParticipantList[e.uid].totalScore + '%'
                                                                : '0%'
                                                        }
                                                    </text>
                                                </div>
                                                {
                                                    this.state.plist[e.uid].isRate ?
                                                        <div style={{ marginLeft: '5%', display: 'table', marginTop: 'auto', marginBottom: 'auto' }}>
                                                            <text style={{ fontSize: 25, fontWeight: 'bold' }}>Accepted</text>
                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Result" onClick={this.handleShowAnswer(e.uid, this.state.userList[e.uid].info.fullName)} />
                                                            </div>
                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Profile" onClick={this.handleUserProfile(e.uid)} />
                                                            </div>
                                                        </div>
                                                        :
                                                        <div style={{ marginLeft: '5%', display: 'table', marginTop: 'auto', marginBottom: 'auto' }}>
                                                            <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton backgroundColor='#F5B041' label="Accept" onClick={this.handleIsRate(e.uid)} />
                                                            </div>
                                                            {/*&nbsp;&nbsp;
                                                                <RaisedButton backgroundColor='#E74C3C' label="Decline" />*/}

                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Result" onClick={this.handleShowAnswer(e.uid, this.state.userList[e.uid].info.fullName)} />
                                                            </div>
                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Profile" onClick={this.handleUserProfile(e.uid)} />
                                                            </div>
                                                        </div>
                                                }
                                            </div>
                                            : false
                                    )
                                    :
                                    <div style={{ textAlign: 'center', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20 }}>
                                        <text style={{ fontSize: 20 }}>No&nbsp;student&nbsp;apply</text><br />
                                    </div>
                            }
                            {
                                this.state.sortedKeys ?
                                    this.state.sortedKeys.length > 0 ?
                                        <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                            <Pagination items={this.state.sortedKeys} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                                        </div>
                                        : false
                                    : false
                            }
                        </div>
                        :
                        this.state.campaignList.typeOfCampaign === 2 ?
                            this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                <div style={{
                                    margin: '1%', width: '100%', backgroundColor: 'white', borderRadius: 20,
                                    height: 'auto', padding: '2% 2% 2% 4%'
                                }}>
                                    <text style={{ fontSize: 22, fontWeight: 'bold' }}>Students&nbsp;Participate</text>
                                    <table>
                                        <tr>
                                            <td style={{ width: '10%' }} onClick={this.handleCheckedAll()}>
                                                {
                                                    this.state.isSelectedAll ?
                                                        <Checked size={40} color='green' />
                                                        :
                                                        <Box size={40} />
                                                }
                                            </td>
                                            <td>
                                                <div style={{ marginTop: 'auto', marginBottom: 'auto' }}>
                                                    <text>&nbsp;&nbsp;Select&nbsp;All</text>
                                                </div>
                                            </td>
                                            <td>
                                                <RaisedButton backgroundColor='#F5B041' label="Attended" onClick={this.handleAllAttend()} />
                                            </td>
                                        </tr>
                                    </table>
                                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                        {
                                            this.state.pageOfItems.length > 0 ?
                                                this.state.pageOfItems.map(e =>
                                                    this.state.userList[e.uid] ?
                                                        <div style={{
                                                            display: 'flex', marginTop: '1%',
                                                            width: (window.innerWidth < 1000 ? '100%' : '45%'), marginRight: '1%'
                                                        }} >
                                                            {
                                                                this.state.plist[e.uid].isAttend ?
                                                                    <div style={{ marginTop: 'auto', marginBottom: 'auto' }}>
                                                                        <Checked size={40} color='grey' />
                                                                    </div>
                                                                    :
                                                                    e.isSelected ?
                                                                        <div style={{ marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleIsChecked(e.uid)}>
                                                                            <Checked size={40} color='green' />
                                                                        </div>
                                                                        :
                                                                        <div style={{ marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleIsChecked(e.uid)}>
                                                                            <Box size={40} />
                                                                        </div>
                                                            }
                                                            <div
                                                                className={'clickDiv'}
                                                                style={{
                                                                    display: 'flex', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20, width: '80%',
                                                                    paddingLeft: '5%'
                                                                }} onClick={this.handleUserProfile(e.uid)}>
                                                                <div style={{ width: '80%' }}>
                                                                    <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.userList[e.uid].info.fullName}</text>
                                                                    <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                                        {
                                                                            this.state.userList[e.uid].info.monstaXP ?
                                                                                ` ( Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1} )`
                                                                                : ' ( Lvl 0 )'
                                                                        }
                                                                    </text>
                                                                    <br />
                                                                    <text style={{ fontSize: 14 }}>
                                                                        {this.state.userList[e.uid].info.university ? this.state.userList[e.uid].info.university : '<------------>'}
                                                                    </text><br />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        : false
                                                )
                                                :
                                                <div style={{ textAlign: 'center', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20, width: '100%' }}>
                                                    <text style={{ fontSize: 20 }}>No&nbsp;student&nbsp;apply</text><br />
                                                </div>
                                        }
                                    </div>
                                    {
                                        this.state.sortedKeys ?
                                            this.state.sortedKeys.length > 0 ?
                                                <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                    <Pagination items={this.state.sortedKeys} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                                                </div>
                                                : false
                                            : false
                                    }
                                </div>
                                :
                                <div style={{
                                    margin: '1%', width: '66%', backgroundColor: 'white', borderRadius: 20,
                                    height: 'auto', padding: '2% 2% 2% 4%'
                                }}>
                                    <text style={{ fontSize: 22, fontWeight: 'bold' }}>Students&nbsp;Joined</text>
                                    {
                                        this.state.pageOfItems.length > 0 ?
                                            this.state.pageOfItems.map(e =>
                                                this.state.userList[e.uid] ?
                                                    <div style={{ display: 'flex', backgroundColor: '#E5E7E9', padding: '2% 2% 2% 4%', borderRadius: 20, marginTop: '1%', width: '100%' }} >
                                                        <div style={{ width: '55%' }}>
                                                            <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.userList[e.uid].info.fullName}</text>
                                                            <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                                {
                                                                    this.state.userList[e.uid].info.monstaXP ?
                                                                        ` ( Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1} )`
                                                                        : ' ( Lvl 0 )'
                                                                }
                                                            </text>
                                                            <br />
                                                            <text style={{ fontSize: 14 }}>
                                                                {this.state.userList[e.uid].info.course ? this.state.userList[e.uid].info.course : '<------------>'}
                                                            </text><br /><br />
                                                            <text style={{ fontSize: 14 }}>
                                                                {this.state.userList[e.uid].info.university ? this.state.userList[e.uid].info.university : '<------------>'}
                                                            </text><br />
                                                            <text style={{ fontSize: 14 }}>
                                                                {this.state.userList[e.uid].info.state ? this.state.userList[e.uid].info.state : '<------------>'}
                                                            </text><br />
                                                            <text style={{ fontSize: 14 }}>{`Date Applied: ${
                                                                this.state.campaignList.ParticipantList[e.uid].dateApply ?
                                                                    moment(new Date(this.state.campaignList.ParticipantList[e.uid].dateApply)).format('MMMM Do YYYY, h:mm:ss a')
                                                                    : ''
                                                                }`}
                                                            </text><br />
                                                        </div>
                                                        {/*<div style={{ marginLeft: '10%', display: 'table', marginTop: 'auto', marginBottom: 'auto', width: '15%', textAlign: 'center' }}>
                                                        <text style={{ fontSize: 30 }}>
                                                            {
                                                                this.state.userList[e.uid].info.monstaXP ?
                                                                    this.state.userList[e.uid].info.monstaXP > 1000 ?
                                                                        (this.state.userList[e.uid].info.monstaXP / 1000).toFixed(2) + 'K'
                                                                        :
                                                                        this.state.userList[e.uid].info.monstaXP
                                                                    : 0
                                                            }
                                                        </text>
                                                        <text style={{ fontSize: 16, fontWeight: 'bold' }}>&nbsp;XP</text><br />
                                                        <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                            {
                                                                this.state.userList[e.uid].info.monstaXP ?
                                                                    `Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1}`
                                                                    : 'Lvl 0'
                                                            }
                                                        </text>
                                                        </div>*/}
                                                        <div style={{ marginLeft: '5%', display: 'table', marginTop: 'auto', marginBottom: 'auto' }}>
                                                            <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton backgroundColor='#2ECC71' label="Endorse" onClick={this.handleUserProfile(e.uid)} />
                                                            </div>
                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Submission" onClick={this.handleOpenLink(e.uid)} />
                                                            </div>
                                                            <div style={{ marginTop: '5%', display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                                                <RaisedButton primary={true} label="View Profile" onClick={this.handleUserProfile(e.uid)} />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    : false
                                            )
                                            :
                                            <div style={{ textAlign: 'center', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20 }}>
                                                <text style={{ fontSize: 20 }}>No&nbsp;student&nbsp;join</text><br />
                                            </div>
                                    }
                                    {
                                        this.state.sortedKeys ?
                                            this.state.sortedKeys.length > 0 ?
                                                <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                    <Pagination items={this.state.sortedKeys} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                                                </div>
                                                : false
                                            : false
                                    }
                                </div>
                            :
                            <div style={{
                                margin: '1%', width: '100%', backgroundColor: 'white', borderRadius: 20,
                                height: 'auto', padding: '2% 2% 2% 4%'
                            }}>
                                <text style={{ fontSize: 22, fontWeight: 'bold' }}>Students&nbsp;Participate</text>
                                {
                                    this.state.campaignList.typeOfSubCampaign === 1 ?
                                        <table>
                                            <tr>
                                                <td style={{ width: '10%' }} onClick={this.handleCheckedAll()}>
                                                    {
                                                        this.state.isSelectedAll ?
                                                            <Checked size={40} color='green' />
                                                            :
                                                            <Box size={40} />
                                                    }
                                                </td>
                                                <td>
                                                    <div style={{ marginTop: 'auto', marginBottom: 'auto' }}>
                                                        <text>&nbsp;&nbsp;Select&nbsp;All</text>
                                                    </div>
                                                </td>
                                                <td>
                                                    <RaisedButton backgroundColor='#F5B041' label="Attended" onClick={this.handleAllAttend()} />
                                                </td>
                                            </tr>
                                        </table>
                                        :
                                        false
                                }
                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                    {
                                        this.state.pageOfItems.length > 0 ?
                                            this.state.pageOfItems.map(e =>
                                                this.state.userList[e.uid] ?
                                                    <div style={{
                                                        display: 'flex', marginTop: '1%',
                                                        width: (window.innerWidth < 1000 ? '100%' : '45%'), marginRight: '1%'
                                                    }} >
                                                        {
                                                            this.state.campaignList.typeOfSubCampaign === 1 ?
                                                                this.state.plist[e.uid].isAttend ?
                                                                    <div style={{ marginTop: 'auto', marginBottom: 'auto' }}>
                                                                        <Checked size={40} color='grey' />
                                                                    </div>
                                                                    :
                                                                    e.isSelected ?
                                                                        <div style={{ marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleIsChecked(e.uid)}>
                                                                            <Checked size={40} color='green' />
                                                                        </div>
                                                                        :
                                                                        <div style={{ marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleIsChecked(e.uid)}>
                                                                            <Box size={40} />
                                                                        </div>
                                                                : false
                                                        }
                                                        <div
                                                            className={'clickDiv'}
                                                            style={{
                                                                display: 'flex', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20, width: '80%',
                                                                paddingLeft: '5%'
                                                            }} onClick={this.handleUserProfile(e.uid)}>
                                                            <div style={{ width: '80%' }}>
                                                                <text style={{ fontSize: 20, fontWeight: 'bold' }}>{this.state.userList[e.uid].info.fullName}</text>
                                                                <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                                    {
                                                                        this.state.userList[e.uid].info.monstaXP ?
                                                                            ` ( Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1} )`
                                                                            : ' ( Lvl 0 )'
                                                                    }
                                                                </text>
                                                                <br />
                                                                <text style={{ fontSize: 14 }}>
                                                                    {this.state.userList[e.uid].info.university ? this.state.userList[e.uid].info.university : '<------------>'}
                                                                </text><br />
                                                            </div>
                                                            {/*<div style={{ marginLeft: '10%', display: 'table', marginTop: 'auto', marginBottom: 'auto', textAlign: 'center' }}>
                                                                <text style={{ fontSize: 30 }}>
                                                                    {
                                                                        this.state.userList[e.uid].info.monstaXP ?
                                                                            this.state.userList[e.uid].info.monstaXP > 1000 ?
                                                                                (this.state.userList[e.uid].info.monstaXP / 1000).toFixed(2) + 'K'
                                                                                :
                                                                                this.state.userList[e.uid].info.monstaXP
                                                                            : 0
                                                                    }
                                                                </text>
                                                                <text style={{ fontSize: 16, fontWeight: 'bold' }}>&nbsp;XP</text><br />
                                                                <text style={{ fontSize: 16, fontWeight: 'bold' }}>
                                                                    {
                                                                        this.state.userList[e.uid].info.monstaXP ?
                                                                            `Lvl ${Math.floor(this.state.userList[e.uid].info.monstaXP / 1000) + 1}`
                                                                            : 'Lvl 0'
                                                                    }
                                                                </text>
                                                                </div>*/}
                                                        </div>
                                                    </div>
                                                    : false
                                            )
                                            :
                                            <div style={{ textAlign: 'center', backgroundColor: '#E5E7E9', padding: '2%', borderRadius: 20, width: '100%' }}>
                                                <text style={{ fontSize: 20 }}>No&nbsp;student&nbsp;apply</text><br />
                                            </div>
                                    }
                                </div>
                                {
                                    this.state.sortedKeys ?
                                        this.state.sortedKeys.length > 0 ?
                                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                                <Pagination items={this.state.sortedKeys} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                                            </div>
                                            : false
                                        : false
                                }
                            </div>
                }
            </div >
        );
    }
}

/**
 * Map dispatch to props
* @param  {func} dispatch is the function to dispatch action to reducers
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
        */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
* @param  {object} state is the obeject from redux store
* @param  {object} ownProps is the props belong to component
* @return {object}          props of component
*/
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList,
        fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompMyCampaignInfo)