import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router-dom'
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import renderHTML from 'react-render-html';
import { firebaseAuth, firebaseRef, firebaseApp } from 'app/firebase/'
import sha1 from 'js-sha1'
import moment from 'moment'
import PostPaymentPage from 'PostPaymentPage'
/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */

export default function PostPayment(props) {
    return <PostPaymentPage Uid={props.match.params.uid} PaymentPushKeyValue={props.match.params.paymentpushkey}/>
  }
