import React, { Component } from 'react';
import { connect } from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import { Redirect } from 'react-router-dom'
import { Card, CardActions, CardHeader, CardMedia, CardTitle, CardText } from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import renderHTML from 'react-render-html';
import { firebaseAuth, firebaseRef, firebaseApp } from 'app/firebase/'
import sha1 from 'js-sha1'
import moment from 'moment'
import DataTables from 'material-ui-datatables';

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
const styles = {
    underlineStyle: {
        borderColor: '#70E0F2',
    }
};

export default class PostPaymentPage extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            searchResult: [],
            allStudent: [],
            displayStudent: [],
            fullNameKey: '',
            courseKey: '',
            uniKey: '',
            stateMalaysia: 0,
            cityMalaysia: ['Selangor', 'Kuala Lumpur', 'Sarawak', 'Johor', 'Pulau Pinang', 'Sabah', 'Perak', 'Pahang', 'Negeri Sembilan', 'Kedah', 'Melaka', 'Terengganu', 'Kelantan', 'Perlis'],
            listMenuItem: [],
            uid: '',
            isDirect: false,
            amount: 1,
            amountSelected: false,
            userInfo: [],
            PurchaseHistory :[]
        }
    }

    componentWillMount() {
        var getUserInfo
        var check = firebaseApp.auth().currentUser
       /*  firebaseApp.database().ref().child(`users/${check.uid}/info`).once('value').then((snapshot) => {
            console.log(snapshot.val())
            this.state.userInfo = snapshot.val()
        }) */
        var arrHistory =[]
        firebaseApp.database().ref().child(`purchaseHistory/${check.uid}/`).on('value', snapshot => {
            snapshot.forEach(element => {
                arrHistory.push(element.val())
                this.setState({PurchaseHistory:arrHistory})
            });
        })
        arrHistory.reverse()
    }

 

    render() {
        var test = firebaseApp
        var checkUsers = firebaseApp.auth().currentUser
        const TABLE_COLUMNS=[{
            key: 'refNo',
            label: 'Refference No',
          },{
            key:'date',
            label:'Date'
          },{
            key:'status',
            label:'Transaction Status'
          },
          {
            key:'amount',
            label:'amount'
          },{
          key:'creditAfter',
          label:'Credit Balance'
          }
        ]
     /*    if(this.state.PurchaseHistory.length>0){
        if(this.state.PurchaseHistory[0].notified!=undefined ||false){
            var success = this.PurchaseHistory[0].statusCode==1?" is succesful":"has Failed"
            Alert('Your Recent Transcation '+this.PurchaseHistory[0].refNo+ 'for RM'+this.PurchaseHistory[0].amount+ ','+success)
            var test = firebaseApp */
           // const updateUserRef = firebaseApp.database().ref('purchaseHistory/'+checkUsers.uid+'/')

        //}
   // }
   //pageCount = this.state.PurchaseHistory.length > 1?  Math.ceil(this.state.PurchaseHistory.length/10):1
        return (            
            
            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
            <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') ,paddingRight:'5%' }}>
                  <DataTables
                    height={'auto'}
                    selectable={false}
                    showRowHover={true}
                    columns={TABLE_COLUMNS}
                    data={this.state.PurchaseHistory}
                    page={1}
                    count={10}
                />
            </div>
            </div>
        );
    }
}

