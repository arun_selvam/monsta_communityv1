// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Tabs, Tab } from 'material-ui/Tabs'
import Divider from 'material-ui/Divider'
import RaisedButton from 'material-ui/RaisedButton';
import { grey50, grey200, grey400, grey600, cyan500 } from 'material-ui/styles/colors'
import { push } from 'react-router-redux'
import { GridList, GridTile } from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import StarBorder from 'material-ui/svg-icons/toggle/star-border';
import { Table, Row, Cell } from 'react-responsive-table';
import Avatar from 'material-ui/Avatar';

// - Import app components
import HomeHeader from 'HomeHeader'
import UserAvatar from 'UserAvatar'

// - Import API


// - Import actions
import * as globalActions from 'globalActions'

/**
* Create component class
 */
export class JobDescription extends Component {

  static propTypes = {

  }

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props)

    //Defaul state
    this.state = {

    }

    // Binding functions to `this`

  }

  componentWillMount() {
    const { tab } = this.props.match.params
    switch (tab) {
      case undefined:
      case '':
        this.props.setHeaderTitle('My Profile')
        break;
      case 'circles':
        this.props.setHeaderTitle('Circles')
        break;
      case 'followers':
        this.props.setHeaderTitle('Followers')
        break;
      default:
        break;
    }

  }


  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {
    /**
   * Component styles
   */
    const styles = {
      root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
      },
      gridList: {
        width: 500,
        height: 450,
        overflowY: 'auto',
      },

    };

    const tilesData = [
      {
        img: 'images/grid-list/00-52-29-429_640.jpg',
        title: 'Job 1',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/burger-827309_640.jpg',
        title: 'Job 2',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/camera-813814_640.jpg',
        title: 'Job 3',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/morning-819362_640.jpg',
        title: 'Job 4',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/hats-829509_640.jpg',
        title: 'Job 5',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/honey-823614_640.jpg',
        title: 'Job 6',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/vegetables-790022_640.jpg',
        title: 'Job 7',
        author: 'Company Name',
      },
      {
        img: 'images/grid-list/water-plant-821293_640.jpg',
        title: 'Job 8',
        author: 'Company Name',
      },
    ];

    const { circlesLoaded } = this.props
    const { tab } = this.props.match.params

    return (

      <div style={{ marginBottom: '5px', display: 'flex', flexWrap: 'wrap', flexFlow: 'row wrap' }}>
        <div style={{ textAlign: 'center', minWidth: '64%', height: 'auto', backgroundColor: 'white', flexGrow: '1', marginRight: '3px', marginBottom: '3px' }}>
          <Row striped key="row2">
            <Cell minWidthPx={90} key="cell1"><Avatar size={80} src="images/uxceo-128.jpg" /></Cell>
            <Cell minWidthPx={800} style={{ textAlign: 'center', marginLeft: '10px' }} key="cell2"><h2>Sales Assistance</h2>
              <p>Brand Name Berhad</p>
              <p>RM40/day</p>
              <p>Part Time</p>
              <p>Kuala Lumpur</p>
              <p>Providing good services to client and monsta asia, monsta is the number 1 youth network in Asia.</p></Cell>
            <Cell minWidthPx={88} style={{}} key="cell3"><RaisedButton label="Apply" primary={true} /></Cell>
          </Row>
          {/* <Table>
                       <TableBody displayRowCheckbox={false}>
                           <TableRowColumn style={{width:'10%'}}></TableRowColumn>
                           <TableRowColumn style={{width:'60%'}}></TableRowColumn>
                                <TableRowColumn style={{width:'20%'}}>                
                            </TableRowColumn>
                            </TableBody>
                      </Table>  */}
          <Divider style={{ marginTop: '10px' }} />
          <p>Long Long Job descriptions</p>

        </div>
        <div style={{ textAlign: 'center', width: '34%', height: 'auto', backgroundColor: '', flexGrow: '1', marginLeft: '3px', marginBottom: '3px' }}>
          <div style={styles.root}>
            <GridList
              cellHeight={180}
              style={styles.gridList}
            >
              <Subheader>You might also interested: </Subheader>
              {tilesData.map((tile) => (
                <GridTile
                  key={tile.img}
                  title={tile.title}
                  subtitle={<span>by <b>{tile.author}</b></span>}
                >
                  <img src={tile.img} />
                </GridTile>
              ))}
            </GridList>
          </div>

        </div>
        <div style={{ textAlign: 'center', width: '100%', height: 'auto', backgroundColor: 'white', flexGrow: '1', marginTop: '3px', paddingBottom: '10px' }}>
          <div style={{ margin: '15px' }}>
            <h2>About Us</h2>
            <div style={{ backgroundColor: 'grey', width: '70%', margin: '0 auto', height: '200px' }}>
              <img stlye={{ width: '70%' }} />
            </div>
            <p>Company description long long </p>
            <RaisedButton label="Follow Us " primary={true} />
          </div>
        </div>

      </div>
    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {

  return {
    goTo: (url) => dispatch(push(url)),
    setHeaderTitle: (title) => dispatch(globalActions.setHeaderTitle(title))


  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {

  return {
    uid: state.authorize.uid,
    circlesLoaded: state.circle.loaded



  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(JobDescription))