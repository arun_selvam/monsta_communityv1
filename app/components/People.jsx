// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter, Route, Switch, Redirect, NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Tabs, Tab } from 'material-ui/Tabs'
import Divider from 'material-ui/Divider'
import RaisedButton from 'material-ui/RaisedButton';
import { grey50, grey200, grey400, grey600, cyan500 } from 'material-ui/styles/colors'
import { push } from 'react-router-redux'
import LinearProgress from 'material-ui/LinearProgress';
import UserAvatar from 'UserAvatar'
import Pagination from 'Pagination';
import { firebaseRef } from 'app/firebase/'
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import Map from 'react-icons/lib/md/map'
import TimePicker from 'material-ui/TimePicker';
import CoverAvatar from 'CoverAvatar'


// - Import app components
import FindPeople from 'FindPeople'
import Following from 'Following'
import Followers from 'Followers'
import YourCircles from 'YourCircles'
import Blog from 'Blog'
import '../styles/app.scss'
import '../styles/div.scss'

// - Import API
import CircleAPI from 'CircleAPI'


// - Import actions
import * as circleActions from 'circleActions'
import * as globalActions from 'globalActions'
import * as userActions from 'userActions'

import Responsive from 'react-responsive';


/**
* Create component class
 */
export class People extends Component {

  static propTypes = {

  }

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props)

    //Defaul state
    this.state = {
      currentExp: 50,
      usersInfo: [],
      pageOfItems: [],
      uid: '',
      role: '',
      isDirect: false,
      fullName: '',
      followingUser: [],
      isCampaign: false,
      totalItem: 0,
      numItem: 0,
      isProfile: false,
      isSearch: false,
      campaignList: [],
      campaignKey: '',
      redirect: false,
      viewMore: false,
      randomAvatar: []
    }
    // Binding functions to `this`
    this.onChangePage = this.onChangePage.bind(this);
    this.CampInfo = this.CampInfo.bind(this)

  }

  componentWillMount() {
    this.props.loadUserInfo()
    const { tab } = this.props.match.params
    switch (tab) {
      case undefined:
      case '':
        this.props.setHeaderTitle('People')
        break;
      case 'circles':
        this.props.setHeaderTitle('Circles')
        break;
      case 'followers':
        this.props.setHeaderTitle('Followers')
        break;
      default:
        break;
    }

    var user = firebaseRef.child("users")
    var userCircle = firebaseRef.child("userCircles").child(`${Object.keys(this.props.peopleInfo)[0]}/-Following/users/`)

    userCircle.on('value', circleSanp => {
      if (circleSanp.val()) {
        user.on('value', snapshot => {
          var userData = snapshot.val()
          var userKey = Object.keys(snapshot.val())
          var usersInfo = []
          var followingUser = []
          var totalItem = 0
          var numItem = 0
          var allCompKey = []
          var randomAvatar = []

          userKey.forEach(e => {
            if (circleSanp.val()[e]) {
              if (userData[e].info.role === 'student') {
                followingUser.push({
                  uid: e,
                  data: userData[e].info
                })
              } else {
                allCompKey.push(e)
              }
            }

            followingUser.sort(function (a, b) {
              var monstaXP_a = a.data.monstaXP ? a.data.monstaXP : 0
              var monstaXP_b = b.data.monstaXP ? b.data.monstaXP : 0
              return monstaXP_b - monstaXP_a
            });

            if (e !== Object.keys(this.props.peopleInfo)[0]) {
              if (userData[e].info) {
                usersInfo.push({
                  uid: e,
                  data: userData[e].info,
                  following: circleSanp.val()[e] ? true : false
                })
              }
            }
            else {
              Object.keys(userData[e].info).forEach(key => {
                if (key === 'address' || key === 'area' || key === 'avatar' || key === 'city' || key === 'course' ||
                  key === 'describeYourself' || key === 'graduationDate' || key === 'lookingFor' || key === 'mobileNumber' ||
                  key === 'postcode' || key === 'state' || key === 'summary' || key === 'university') {
                  if (userData[e].info[key]) {
                    numItem += 1
                  }
                  totalItem += 1
                }
              })
            }
          })

          if (allCompKey.length > 4) {
            while (i < 3) {
              var index = Math.floor(Math.random() * myArray.length)
              randomAvatar.push(userData[allCompKey[index]].info.avatar)
              allCompKey.splice(index, 1);
              i++;
            }
          }
          else {
            allCompKey.forEach(e => {
              randomAvatar.push(userData[e].info.avatar)
            })
          }

          this.setState({
            usersInfo: usersInfo, followingUser: followingUser,
            fullName: snapshot.val()[Object.keys(this.props.peopleInfo)[0]].info.fullName,
            numItem: numItem, totalItem: totalItem, randomAvatar: randomAvatar
          })
        })
      }
      else {
        user.on('value', snapshot => {
          var userData = snapshot.val()
          var userKey = Object.keys(snapshot.val())
          var usersInfo = []
          var totalItem = 0
          var numItem = 0

          userKey.forEach(e => {
            if (e !== Object.keys(this.props.peopleInfo)[0]) {
              if (userData[e].info) {
                usersInfo.push({
                  uid: e,
                  data: userData[e].info,
                  following: false
                })
              }
            }
            else {
              Object.keys(userData[e].info).forEach(key => {
                if (key === 'address' || key === 'area' || key === 'avatar' || key === 'city' || key === 'course' ||
                  key === 'describeYourself' || key === 'graduationDate' || key === 'lookingFor' || key === 'mobileNumber' ||
                  key === 'postcode' || key === 'state' || key === 'summary' || key === 'university') {
                  if (userData[e].info[key]) {
                    numItem += 1
                  }
                  totalItem += 1
                }
              })
            }
          })

          this.setState({
            usersInfo: usersInfo,
            fullName: snapshot.val()[Object.keys(this.props.peopleInfo)[0]].info.fullName,
            numItem: numItem, totalItem: totalItem
          })
        })
      }
    })

    var specialization = firebaseRef.child("staticData")
    var myCampaignList = firebaseRef.child("campaigns");
    var dataSort = []
    specialization.on('value', snapshotSpec => {
      myCampaignList.on('value', snapshot => {
        var data = snapshot.val()
        var dataKey = Object.keys(data)
        var arrayList = []
        dataKey.forEach(function (e) {
          if (!data[e].isDraft) {
            var dataCamp = data[e]
            var dataCampKey = Object.keys(dataCamp)
            dataCampKey.forEach(function (i) {
              if (dataCamp[i].startDate) {
                if (new Date(dataCamp[i].startDate).toDateString() <= new Date().toDateString() && new Date(dataCamp[i].endDate).toDateString() >= new Date().toDateString()) {
                  if (dataCamp[i].typeOfCampaign) {
                    dataCamp[i].compUid = e
                    dataCamp[i].campUid = i
                    dataSort.push({
                      data: dataCamp[i],
                      startDate: dataCamp[i].startDate
                    })
                  }
                }
              } else {
                if (dataCamp[i].typeOfCampaign) {
                  dataCamp[i].compUid = e
                  dataCamp[i].campUid = i
                  dataSort.push({
                    data: dataCamp[i],
                    startDate: new Date()
                  })
                }
              }
            })
          }
        })

        dataSort.sort(function (a, b) {
          return new Date(b.startDate) - new Date(a.startDate);
        });

        dataSort.forEach(function (e, idx) {
          if (idx < 3) {
            var campaignPath = e.data.compUid + "/" + e.data.campUid;
            var availableSlot = 0
            e.data.campaignPath = campaignPath;

            if (e.data.targetStudent) {
              if (e.data.ParticipantList) {
                var partKey = Object.keys(e.data.ParticipantList)
                availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
              } else {
                availableSlot = e.data.targetStudent
              }
            }

            arrayList.push(
              <div style={{
                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                height: 'auto', position: 'relative'
              }} onClick={this.CampInfo(e.data.campaignPath)}>
                <div className={'clickDiv'}>
                  <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                    <UserAvatar fileName={e.data.companyAvatar}
                      size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                  </div>
                  <div style={{
                    position: 'absolute', right: '5%', top: '210px',
                    backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                  }}>
                    <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                  </div>
                  <div style={{
                    position: 'absolute', left: '40%', top: '210px', padding: '1%'
                  }}>
                    {
                      e.data.targetStudent ?
                        availableSlot === 0 ?
                          <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                          :
                          <text style={{ fontSize: '15', color: 'green' }}>{`${availableSlot}`}&nbsp;slot&nbsp;available</text>
                        :
                        <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                    }
                  </div>
                  <CoverAvatar fileName={e.data.campAvatar} height={'200px'} />
                  <div style={{
                    backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                    borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                  }}>
                    <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                    <br />
                    <div style={{ marginTop: '2%', height: '90px' }}>
                      <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                        {
                          e.data.description.length > 200 ?
                            e.data.description.substring(0, 200) + '...'
                            :
                            e.data.description
                        }
                      </p>
                    </div>
                    <br />
                    {e.data.typeOfCampaign === 1 ?
                      <div>
                        <text style={{ fontWeight: 'bold', fontSize: 13 }}>Alloance : </text><br />
                        <text style={{ fontSize: 13 }}>RM {e.data.minPayout} - RM {e.data.maxPayout}</text>
                      </div>
                      :
                      e.data.typeOfCampaign === 2 ?
                        <div>
                          <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            {
                              e.data.skillCanLearn.length > 0 ?
                                e.data.skillCanLearn.map((e, idx) => (
                                  idx < 4 ?
                                    <div style={{
                                      backgroundColor: '#CACFD2', padding: '1%', width: `${snapshotSpec.val().skills[e].length * 8 + 20}px`,
                                      margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                    }}>
                                      <text style={{ fontSize: 13 }}>{snapshotSpec.val().skills[e]}</text>
                                    </div>
                                    : false
                                ))
                                : false
                            }
                          </div>
                        </div>
                        :
                        e.data.typeOfCampaign === 3 ?
                          <div>
                            <table>
                              <tr>
                                <td><Calender /></td>
                                <td>
                                  <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventDate}</text>
                                </td>
                              </tr>
                              <tr>
                                <td><Time /></td>
                                <td>
                                  <TimePicker
                                    style={{ fontSize: 13, paddingLeft: '1%' }}
                                    disabled={true}
                                    format="ampm"
                                    value={new Date(e.data.eventTime)}
                                  />
                                </td>
                              </tr>
                              <tr>
                                <td><Map /></td>
                                <td>
                                  {
                                    e.data.eventAdd2 ?
                                      <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                        {e.data.eventAdd1}, {e.data.eventAdd2}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                      </p>
                                      :
                                      <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                        {e.data.eventAdd1}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                      </p>
                                  }
                                </td>
                              </tr>
                            </table>
                          </div>
                          : false
                    }
                  </div>
                </div>
              </div>
            )
          }
        }, this)
        this.setState({ campaignList: arrayList });
      })
    })
  }

  CampInfo = (campaignKey) => () => {
    this.setState({ redirect: true, campaignKey: campaignKey })
  }

  handleViewMore = () => () => {
    this.setState({ viewMore: true })
  }

  onChangePage(pageOfItems) {
    // update state with new page of items
    this.setState({ pageOfItems: pageOfItems });
  }

  handleUserProfile = (uid, role) => () => {
    this.setState({ uid: uid, role: role, isDirect: true })
  }

  handleCampaign = () => () => {
    this.setState({ isCampaign: true })
  }

  handleCompleteProfile = () => () => {
    this.setState({ isProfile: true })
  }

  handleSearch = () => () => {
    this.setState({ isSearch: true })
  }

  handleFollow = (uid, role) => () => {
    var followUser = firebaseRef.child("userCircles")

    followUser.child(`${uid}/-Followers/users/${Object.keys(this.props.peopleInfo)[0]}`).update({
      followDate: new Date().toString()
    })

    followUser.child(`${Object.keys(this.props.peopleInfo)[0]}/-Following/users/${uid}`).update({
      followingDate: new Date().toString()
    })

    var notify = {
      description: `${this.state.fullName} has follow you`,
      url: `/MyProfile/${Object.keys(this.props.peopleInfo)[0]}`,
      notifierUserId: Object.keys(this.props.peopleInfo)[0],
      isSeen: false,
      date: new Date().toString()
    }
    var notifyRef = firebaseRef.child(`userNotifies/${uid}`).push(notify)
  }


  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {
    /**
   * Component styles
   */
    const styles = {
      people: {
        margin: '0 auto',
        width: '90%'
      },
      headline: {
        fontSize: 24,
        paddingTop: 16,
        marginBottom: 12,
        fontWeight: 400,
      },
      slide: {
        padding: 10,
      }
    }

    if (this.state.redirect) {
      return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />
    }

    if (this.state.isDirect) {
      if (this.state.role === 'student') {
        return <Redirect push to={`/MyProfile/${this.state.uid}`} />
      }
      else {
        return <Redirect push to={`/companyinfo/${this.state.uid}`} />
      }
    }

    if (this.state.isCampaign) {
      return <Redirect push to={'/mycampaigns'} />
    }

    if (this.state.isProfile) {
      return <Redirect push to={`/MyProfile/${Object.keys(this.props.peopleInfo)[0]}`} />
    }

    if (this.state.isSearch) {
      return <Redirect push to={'/SearchCompany'} />
    }

    if (this.state.viewMore) {
      return <Redirect push to={'/mycampaigns'} />
    }

    const { circlesLoaded } = this.props
    const { tab } = this.props.match.params
    const Desktop = props => <Responsive {...props} minWidth={1293} />;
    const Mid = props => <Responsive {...props} minWidth={869} maxWidth={1292} />;
    const Smaller = props => <Responsive {...props} maxWidth={868} />;

    return (
      <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
      <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
          <div style={{
            backgroundColor: 'white', margin: '1%', padding: '20px', overflow: 'hidden', display: 'flex',
            alignItems: 'center',justifyContent:'center', width: (window.innerWidth > 1000 ? '48%' : '100%'), borderRadius: 20
          }} >
            <div style={{ width: '50px', float: 'left' }}>
              <div style={{
                width: '100px', height: '100px',
                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2Fstudents%2Fico_opportunity.png?alt=media&token=2461b074-457b-4b2d-8916-e85689ea1663")`,
                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
              }}></div>
            </div>
            <div style={{ marginLeft: '100px' }}>
              <h1 >Discover Opportunities</h1>
              <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                {'Handpicked  Exciting Opportunities  For You'}
              </p><br />
              <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                <RaisedButton label="Discover" primary={true} onClick={this.handleCampaign()} />
              </div>
            </div>
          </div>
          <div style={{
            backgroundColor: 'white', margin: '1%', padding: '20px', overflow: 'hidden', display: 'flex',
            alignItems: 'center', width: (window.innerWidth > 1000 ? '48%' : '100%'), borderRadius: 20, textAlign: 'center'
          }} >
            <div style={{ width: '50px', float: 'left' }}>
              <div style={{
                width: '100px', height: '100px',
                borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2Fstudents%2Fico_culture%20profile.png?alt=media&token=42b81a58-f055-40b9-b528-57e43687d396")`,
                WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
              }}></div>
            </div>
            <div style={{ marginLeft: '100px' }}>
              <h1>Complete Your Profile</h1>
              <text>{`${this.state.numItem ? ((this.state.numItem / this.state.totalItem) * 100).toFixed(0) > 100 ? 100 : ((this.state.numItem / this.state.totalItem) * 100).toFixed(0) : 0}% profile complete`}</text>
              <LinearProgress style={{ marginLeft: '10%', marginBottom: '15px', width: '80%', height: '20px' }} mode="determinate"
                value={this.state.numItem ? ((this.state.numItem / this.state.totalItem) * 100).toFixed(0) > 100 ? 100 : ((this.state.numItem / this.state.totalItem) * 100).toFixed(0) : 0} />
              <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                <RaisedButton label="Complete Profile" primary={true} onClick={this.handleCompleteProfile()} />
              </div>
            </div>
          </div>
{/* width: (window.innerWidth > 800 ? '38%' : '100%')*/}
          <div style={{
            backgroundColor: 'white', margin: '1%', padding: '20px', width: '100%',
            height: 'auto', alignItems: 'center', borderRadius: 20,alignSelf:'center',textAlign: 'center', display: 'flex', justifyContent: 'center'
          }} >
            <div>
              <h1 style={{ fontSize: (window.innerWidth > 600 ? '' : '20px') }}>Search&nbsp;Companies</h1>
              <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                <div style={{ display: 'flex' }}>
                  {
                    this.state.randomAvatar.map(e => (
                      <div>
                        <UserAvatar fileName={e}
                          size={70} />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                    ))
                  }
                </div>
              </div>
              <br />
              <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                <RaisedButton label="Search" primary={true} onClick={this.handleSearch()} />
              </div>
            </div>
          </div>

   {/*        <div style={{
            backgroundColor: 'white', margin: '1%', padding: '20px',
            alignItems: 'center', width: (window.innerWidth > 1000 ? '58%' : '100%'), borderRadius: 20
          }} >
            <div style={{ textAlign: 'center' }}>
              <h1 >Monsta Scoreboard</h1>
            </div>
            <div style={{
              margin: '2%', padding: '1%', overflowY: 'auto', height: '300px', borderRadius: 10
            }}>
              {
                this.state.followingUser ?
                  this.state.followingUser.map(e =>
                    <div style={{
                      margin: '1%', padding: '1%', width: '98%',
                      backgroundColor: '#FFFFFF', borderRadius: 10, display: 'flex'
                    }}>
                      <div style={{ width: '10%' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                        <UserAvatar fileName={e.data.avatar} size={55} />
                      </div>
                      <div style={{ width: '50%', marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                        <text style={{ paddingLeft: '2%', fontSize: 14, fontWeight: 'bold' }}>{e.data.fullName}</text>
                      </div>
                      <div style={{ width: '20%', marginTop: 'auto', marginBottom: 'auto' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                        <text style={{ paddingLeft: '2%', fontSize: 14, fontWeight: 'bold' }}>{`${e.data.monstaXP ? e.data.monstaXP : 0} XP`}</text>
                      </div>
                      <div style={{ width: '10%', marginTop: 'auto', marginBottom: 'auto' }}>
                        <RaisedButton label="Message" primary={true} onClick={null} />
                      </div>
                    </div>
                  )
                  : false
              }
            </div>
          </div> */}
        </div>
        <hr className="hr-text" />
        <div style={{ margin: '0.5%', padding: '1%' }}>
          <h1>Discover</h1>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {this.state.campaignList}
          </div>
          <div style={{ display: 'table', marginTop: '1%', marginLeft: 'auto', marginRight: 'auto' }}>
            <RaisedButton label="view more" backgroundColor='#F5B041' onClick={this.handleViewMore()} />
          </div>
        </div>
        <hr className="hr-text" data-content="Explore monsta network" />
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {this.state.pageOfItems.map(e =>
            <div className={'clickDiv'} style={{
              width: (window.innerWidth < 1000 ? '100%' : '32%'), backgroundColor: 'white',
              margin: '0.5%', padding: '1%', borderRadius: 20
            }}>
              <table>
                <tr>
                  <td style={{ width: '10%' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>
                    <UserAvatar fileName={e.data.avatar} size={90} />
                  </td>
                  {
                    e.data.role === 'student' ?
                      <td style={{ width: '20%' }}>
                        <text style={{ fontWeight: 'bold' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>{e.data.fullName}</text><br />
                        {
                        /*   e.following ?
                            <div style={{ display: 'table', marginTop: '1%', marginLeft: 'auto', marginRight: 'auto' }}>
                              <RaisedButton label="Following" primary={true} disabled />
                            </div>
                            :
                            <div style={{ display: 'table', marginTop: '1%', marginLeft: 'auto', marginRight: 'auto' }}>
                              <RaisedButton label="Follow" primary={true} onClick={this.handleFollow(e.uid, e.data.role)} />
                            </div> */
                        }
                      </td>
                      :
                      <td style={{ width: '20%' }}>
                        <text style={{ fontWeight: 'bold' }} onClick={this.handleUserProfile(e.uid, e.data.role)}>{e.data.company}</text><br />
                        {
                         /*  e.following ?
                            <div style={{ display: 'table', marginTop: '1%', marginLeft: 'auto', marginRight: 'auto' }}>
                              <RaisedButton label="Following" primary={true} disabled />
                            </div>
                            :
                            <div style={{ display: 'table', marginTop: '1%', marginLeft: 'auto', marginRight: 'auto' }}>
                              <RaisedButton label="Follow" primary={true} onClick={this.handleFollow(e.uid, e.data.role)} />
                            </div> */
                        }
                      </td>
                  }
                </tr>
              </table>
            </div>
          )}
        </div>
        <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
          <Pagination items={this.state.usersInfo} onChangePage={this.onChangePage} numOfItem={12} numOfPage={9} />
        </div>
        {/*         <div className="blog"><Blog displayWriting={true} posts={this.props.mergedPosts} /></div>
 */}      </div>
    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  const { userId } = ownProps.match.params

  return {
    goTo: (url) => dispatch(push(url)),
    setHeaderTitle: (title) => dispatch(globalActions.setHeaderTitle(title)),
    //loadPosts: () => dispatch(postActions.dbGetPostsByUserId(userId)),
    loadUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(userId, 'header'))


  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */

const mapStateToProps = (state, ownProps) => {
  const { userId } = ownProps.match.params
  const { uid } = state.authorize
  let mergedPosts = {}
  const circles = state.circle ? (state.circle.userCircles[uid] || {}) : {}
  const followingUsers = CircleAPI.getFollowingUsers(circles)
  const posts = state.post.userPosts ? state.post.userPosts[state.authorize.uid] : {}
  Object.keys(followingUsers).forEach((userId) => {
    let newPosts = state.post.userPosts ? state.post.userPosts[userId] : {}
    _.merge(mergedPosts, newPosts)
  })
  _.merge(mergedPosts, posts)
  return {
    uid: state.authorize.uid,
    circlesLoaded: state.circle.loaded,
    mergedPosts,
    peopleInfo: state.user.info,
  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(People))