import React, { Component } from 'react';
import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import AppBar from 'material-ui/AppBar';
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import LinearProgress from 'material-ui/LinearProgress'
import RaisedButton from 'material-ui/RaisedButton'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import { firebaseRef } from 'app/firebase/'
import Calendar from 'react-icons/lib/go/calendar'
//import Pin from 'react-icons/lib/fa/map-marker'
import Uni from 'react-icons/lib/go/home'
import '../styles/app.scss'
import FlatButton from 'material-ui/FlatButton';
import Workbook from 'react-excel-workbook'
import UserAvatar from 'UserAvatar'
import moment from 'moment'
import Course from 'react-icons/lib/go/book'
import Pagination from 'Pagination';
import '../styles/div.scss'

var compCredit

class CompanyDashboard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: [{ "name": "Ricky Robin", "uni": "University Science Malaysia", "level": "Monsta Level 2" }, { "name": "Ricky Robin", "uni": "University Science Malaysia", "level": "Monsta Level 2" }, { "name": "Ricky Robin", "uni": "University Science Malaysia", "level": "Monsta Level 2" }],
            monstaNetwork: [],
            redirect: false,
            uid: '',
            isDirect: false,
            isUpdateProfile: false,
            pageOfItems: [],
            isPurchase: false,
            isSearch: false,
            numItem: 0,
            randomAvatar: []
        }
        this.onChangePage = this.onChangePage.bind(this);
        this.createCampaign = this.createCampaign.bind(this)
        this.updateProfile = this.updateProfile.bind(this)
        this.purchaseCredit = this.purchaseCredit.bind(this)
        this.searchPpl = this.searchPpl.bind(this)
    }

    renderStu() {
        var allUser = firebaseRef.child(`users`)
        var myCampaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0])
        var arrayData = []

        allUser.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            var compInfo = data[Object.keys(this.props.peopleInfo)[0]].info
            var compInfoKey = Object.keys(compInfo)
            var numItem = 0
            var allStudentKey = []
            var randomAvatar = []

            compInfoKey.forEach(e => {
                if (e !== 'companyFanPage' && e !== 'companyIndustry' && e !== 'companySSM' && e !== 'companyType'
                    && e !== 'emailSubFrequency' && e !== 'password' && e !== 'role' && e !== 'validated') {
                    if (e === 'quotes') {
                        if (compInfo[e].length) {
                            numItem += 1
                        }
                    }
                    else if (compInfo[e]) {
                        numItem += 1
                    }
                }
            })
            dataKey.forEach(function (e) {
                if (data[e].info) {
                    if (data[e].info.role) {
                        if (data[e].info.role === 'student') {
                            allStudentKey.push(e)
                            arrayData.push({ uid: e, data: data[e].info })
                        }
                    }
                }
                this.compCredit = data[Object.keys(this.props.peopleInfo)[0]].credit
            }, this)

            if (allStudentKey.length > 5) {
                while (i < 4) {
                    var index = Math.floor(Math.random() * myArray.length)
                    randomAvatar.push(data[allStudentKey[index]].info.avatar)
                    allStudentKey.splice(index, 1);
                    i++;
                }
            }
            else {
                allStudentKey.forEach(e => {
                    randomAvatar.push(data[e].info.avatar)
                })
            }

            this.setState({ monstaNetwork: arrayData, numItem: numItem, randomAvatar: randomAvatar })
        })
    }

    componentWillMount() {
        this.props.loadPeople()
        this.renderStu()
    }

    /*handleUnlock = (e) => () => {
        if (this.compCredit < 50) {
            alert('Your current credit is not sufficient to unlock the student')
        } else {
            var result = confirm('Confirm to unlock the student? If yes, 50 amount of credit will be deducted from the account.')
            if (result) {
                var compInfo = firebaseRef.child(`users/${Object.keys(this.props.peopleInfo)[0]}`)
                compInfo.update({
                    credit: this.compCredit - 50
                })
                var unlockStudent = compInfo.child(`/info/unlockStudent/${e}`)
                unlockStudent.update({
                    date: moment().format('LLL').toString()
                })
                this.renderStu()
                alert('You have successfully unlock the student')
            }
        }
    }*/

    createCampaign = () => {
        this.setState({ redirect: true })
    }

    searchPpl = () => {
        this.setState({ isSearch: true })
    }

    purchaseCredit = () => {
        this.setState({ isPurchase: true })
    }

    updateProfile = () => {
        this.setState({ isUpdateProfile: true })
    }

    handleUserProfile = (uid) => () => {
        this.setState({ uid: uid, isDirect: true })
    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    render() {

        const { circlesLoaded } = this.props

        if (this.state.redirect) {
            return <Redirect push to="/companycampaign" />;
        }

        if (this.state.isDirect) {
            return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }

        if (this.state.isUpdateProfile) {
            return <Redirect push to={`/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`} />
        }

        if (this.state.isPurchase) {
            return <Redirect push to={`/PurchaseCredits`} />
        }

        if (this.state.isSearch) {
            return <Redirect push to={`/companypplsearch`} />
        }

        return (
            <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                    <div>
                        <hr className="hr-text" data-content="Getting Started" />
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            <div style={{
                                backgroundColor: 'white', margin: '1%', padding: '20px', overflow: 'hidden', display: 'flex', alignItems: 'center',
                                width: (window.innerWidth > 800 ? '48%' : '100%'), borderRadius: 20, justifyContent: 'center'
                            }} >
                                <div style={{ width: '50px', float: 'left' }}>
                                    <div style={{
                                        width: '100px', height: '100px',
                                        borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2Fcompany%2Fico_opportunity.png?alt=media&token=c9a93791-eb83-4536-b039-0b7584c82463")`,
                                        WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                    }}></div>
                                </div>
                                <div style={{ marginLeft: '100px', width: '65%' ,textAlign: 'center' }}>
                                    <h1 style={{ fontSize: (window.innerWidth > 600 ? '' : '20px') }}>Create Opportunities</h1>
                                    <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                        {'Create Campaign to engage thousands of students with your custom digital campaign, events or more!'}
                                    </p><br />
                                    <RaisedButton label="Create Now" primary={true} onClick={this.createCampaign} />
                                </div>
                            </div>
                            <div style={{
                                backgroundColor: 'white', margin: '1%', padding: '20px', overflow: 'hidden', display: 'flex', alignItems: 'center',
                                width: (window.innerWidth > 800 ? '48%' : '100%'), borderRadius: 20, justifyContent: 'center'
                            }} >
                                <div style={{ width: '50px', float: 'left' }}>
                                    <div style={{
                                        width: '100px', height: '100px',
                                        borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2Fcompany%2Fico_culture%20profile.png?alt=media&token=5e9c97a4-c5bb-4f0b-b084-0ddb2e6dee3b")`,
                                        WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                    }}></div>
                                </div>
                                <div style={{ marginLeft: '100px', width: '65%',textAlign: 'center' }}>
                                    <h1 style={{ fontSize: (window.innerWidth > 600 ? '' : '20px') }}>My Cultural Profile</h1>
                                    <text>{`${this.state.numItem === 0 ? 0 : ((this.state.numItem / 17) * 100).toFixed(0) > 100 ? 100 : ((this.state.numItem / 17) * 100).toFixed(0)}% profile complete`}</text>
                                        <LinearProgress style={{ marginLeft: '10%', marginBottom: '15px', width: '80%', height: '20px' }} mode="determinate" value={this.state.numItem === 0 ? 0 : ((this.state.numItem / 17) * 100).toFixed(0)} />
                                        <RaisedButton label="Complete Profile" primary={true} onClick={this.updateProfile} />
                                </div>
                            </div>
                        </div>
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            <div style={{
                                backgroundColor: 'white', margin: '1%', padding: '20px', overflow: 'hidden', display: 'flex', alignItems: 'center',
                                width: (window.innerWidth > 800 ? '48%' : '100%'), borderRadius: 20, justifyContent: 'center'
                            }} >
                                <div style={{ width: '50px', float: 'left' }}>
                                    <div style={{
                                        width: '100px', height: '100px',
                                        borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundImage: `url("https://firebasestorage.googleapis.com/v0/b/react-monsta.appspot.com/o/IconPacks%2Fcompany%2Fico_purchase%20credit.png?alt=media&token=75b7fba9-15f5-41b6-8f0f-dc1264f209c8")`,
                                        WebkitBackgroundSize: 'cover', MozBackgroundSize: 'center', OBackgroundSize: 'center', backgroundSize: 'center'
                                    }}></div>
                                </div>
                                <div style={{ marginLeft: '100px', width: '65%',textAlign: 'center'  }}>
                                    <h1 style={{ fontSize: (window.innerWidth > 600 ? '' : '20px') }}>Purchase Credit</h1>
                                    <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                        {'By purchasing the credit you will be able to engage with students'}
                                    </p><br />
                                    <RaisedButton label="Purchase Now" primary={true} onClick={this.purchaseCredit} />
                                </div>
                            </div>
                            <div style={{
                                backgroundColor: 'white', margin: '1%', padding: '20px', width: (window.innerWidth > 800 ? '48%' : '100%'),
                                height: 'auto', alignItems: 'center', borderRadius: 20, textAlign: 'center', justifyContent: 'center'
                            }} >
                                <h1 style={{ fontSize: (window.innerWidth > 600 ? '' : '20px') }}>People Search</h1>
                                <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                    <div style={{ display: 'flex' }}>
                                        {
                                            this.state.randomAvatar.map(e => (
                                                <div>
                                                    <UserAvatar fileName={e}
                                                        size={70} />
                                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>
                                            ))
                                        }
                                    </div>
                                </div>
                                <br />
                                <div style={{ display: 'table', marginLeft: 'auto', marginRight: 'auto' }}>
                                    <RaisedButton label="Search" primary={true} onClick={this.searchPpl} />
                                </div>
                            </div>
                        </div>

                        <hr className="hr-text" data-content="Explore monsta network" />
                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                            {this.state.pageOfItems.map(e =>
                                <div className={'clickDiv'} style={{
                                    width: (window.innerWidth < 1000 ? '100%' : '48%'), backgroundColor: 'white', margin: '0.5%',
                                    padding: '1%', borderRadius: 20
                                }} onClick={this.handleUserProfile(e.uid)}>
                                    <table>
                                        <tr>
                                            <td style={{ width: '10%' }}>
                                                <UserAvatar fileName={e.data.avatar} size={90} />
                                            </td>
                                            <td style={{ width: '20%' }}>
                                                <text style={{ fontWeight: 'bold' }}>{e.data.fullName}</text><br />
                                                {
                                                    !e.data.course ?
                                                        false
                                                        :
                                                        <div>
                                                            <Course />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{e.data.course}</text><br />
                                                        </div>
                                                }
                                                {
                                                    !e.data.university ?
                                                        false
                                                        :
                                                        <div>
                                                            <Uni />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{e.data.university}</text><br />
                                                        </div>
                                                }
                                                {
                                                    !e.data.graduationDate ?
                                                        false
                                                        :
                                                        <div>
                                                            <Calendar />
                                                            <text style={{ marginLeft: '5%', fontSize: 11 }}>{moment(e.data.graduationDate).format('YYYY')}</text><br />
                                                        </div>
                                                }
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            )}
                        </div>
                        {
                            this.state.monstaNetwork ?
                                <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                    <Pagination items={this.state.monstaNetwork} onChangePage={this.onChangePage} numOfItem={10} numOfPage={9} />
                                </div>
                                :
                                false
                        }
                    </div>
                </div>
            </div>

        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {

    return {
        goTo: (url) => dispatch(push(url)),
        setHeaderTitle: (title) => dispatch(globalActions.setHeaderTitle(title)),
        loadPeople: () => dispatch(userActions.dbGetPeopleInfo()),
        getImage: (name) => dispatch(imageGalleryActions.dbDownloadImage(name))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {

    return {
        uid: state.authorize.uid,
        circlesLoaded: state.circle.loaded,
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(CompanyDashboard))