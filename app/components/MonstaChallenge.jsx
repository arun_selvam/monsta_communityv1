import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import { Table, Row, Cell } from 'react-responsive-table';
import Avatar from 'material-ui/Avatar'
import UserAvatar from 'UserAvatar'
import '../styles/app.scss'
import '../styles/div.scss'

var skills = []

class MonstaChallenge extends Component {

    constructor(props) {
        super(props)
        this.state = {
            campaignList: [],
            redirect: false,
            selectedKey: '',
            campaignKey: '',
            arrayList2: [],
            skills: []

        }
        this.CampInfo = this.CampInfo.bind(this)
    }

    componentWillMount() {

        var specialization = firebaseRef.child("staticData")
        var myCampaignList = firebaseRef.child("campaigns");
        var dataSort = []
        specialization.on('value', snapshotSpec => {
            myCampaignList.on('value', snapshot => {
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        var dataCamp = data[e]
                        var dataCampKey = Object.keys(dataCamp)
                        dataCampKey.forEach(function (i) {
                            if (dataCamp[i].typeOfCampaign) {
                                if (dataCamp[i].typeOfCampaign == 2) {
                                    dataCamp[i].compUid = e
                                    dataCamp[i].campUid = i
                                    dataSort.push({
                                        data: dataCamp[i]
                                    })
                                }
                            }
                        })
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(b.data.date) - new Date(a.data.date);
                });

                dataSort.forEach(function (e) {
                    var campaignPath = e.data.compUid + "/" + e.data.campUid;
                    var availableSlot = 0
                    e.data.campaignPath = campaignPath;

                    if (e.data.targetStudent) {
                        if (e.data.ParticipantList) {
                            var partKey = Object.keys(e.data.ParticipantList)
                            availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                        } else {
                            availableSlot = e.data.targetStudent
                        }
                    }

                    arrayList.push(
                        <div style={{
                            margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                            height: 'auto', position: 'relative'
                        }} onClick={this.CampInfo(e.data.campaignPath)}>
                            <div className={'clickDiv'}>
                                <div style={{ position: 'absolute', left: '5%', top: '38px' }}>
                                    <UserAvatar fileName={e.data.campAvatar}
                                        size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                </div>
                                <div style={{
                                    position: 'absolute', right: '5%', top: '55px',
                                    backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                                }}>
                                    <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                                </div>
                                <div style={{
                                    position: 'absolute', right: '5%', top: '100px', padding: '1%'
                                }}>
                                    {
                                        e.data.targetStudent ?
                                            availableSlot === 0 ?
                                                <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                                                :
                                                <text style={{ fontSize: '15', color: 'green' }}>{`${availableSlot}`}&nbsp;slot&nbsp;available</text>
                                            :
                                            <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                                    }
                                </div>
                                <div style={{
                                    backgroundColor: '#d3d3d3', width: '100%', height: '100px',
                                    borderTopLeftRadius: 20, borderTopRightRadius: 20
                                }} ></div>
                                <div style={{
                                    backgroundColor: '#FFFFFF', width: '100%', height: 'auto', padding: '10% 2% 2% 7%',
                                    borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                                    <br />
                                    <div style={{ marginTop: '2%' }}>
                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                            {
                                                e.data.description.length > 200 ?
                                                    e.data.description.substring(0, 200) + '...'
                                                    :
                                                    e.data.description
                                            }
                                        </p>
                                    </div>
                                    <br />
                                    <div>
                                        <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                            {
                                                e.data.skillCanLearn.length > 0 ?
                                                    e.data.skillCanLearn.map((e, idx) => (
                                                        idx < 4 ?
                                                            <div style={{
                                                                backgroundColor: '#CACFD2', padding: '1%', width: `${snapshotSpec.val().skills[e].length * 8 + 20}px`,
                                                                margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                            }}>
                                                                <text style={{ fontSize: 13 }}>{snapshotSpec.val().skills[e]}</text>
                                                            </div>
                                                            : false
                                                    ))
                                                    : false
                                            }
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }, this)
                this.setState({ campaignList: arrayList });
            })

        })
    }

    CampInfo = (abc) => () => {
        this.setState({ redirect: true, campaignKey: abc })
    }

    render() {

        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />;
        }

        return (
            <div>
                <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                    <text style={{ fontSize: 20, fontWeight: 'bold' }}>Monsta&amp;Challenge</text>
                    <hr className="hr-text" />
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {this.state.campaignList}
                    </div>
                </div>
            </div>
        );
    }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(MonstaChallenge)