import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
//import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Redirect} from 'react-router-dom'
import {
	Card,
	CardActions,
	CardHeader,
	CardMedia,
	CardTitle,
	CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {firebaseAuth, firebaseRef, firebaseApp} from 'app/firebase/'
import moment from 'moment'
import {
	ThemeProvider,
	Avatar,
	Subtitle,
	ChatListItem,
	ChatList,
	Row,
	Column,
	Title
} from '@livechat/ui-kit'
import InstantMessengerMsgList from './InstantMessengerBox'

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
const styles = {
	underlineStyle: {
		borderColor: '#70E0F2'
	}
};

export default class InstantMessenger extends Component {

	// Constructor
	constructor(props) {
		super(props)
		// Default state
		this.state = {
			Users: [],
			userData: [],
			userDataToRender: [],
			selectedChatRoomUser: ''
		}
	}

	componentWillMount() {

		/* 		var getUserInfo
								var check = firebaseApp
												.auth()
												.currentUser
								  firebaseApp.database().ref().child(`users/${check.uid}/info`).once('value').then((snapshot) => {
            console.log(snapshot.val())
            this.state.userInfo = snapshot.val()

								var arrHistory = []
								firebaseApp
												.database()
												.ref()
												.child(`purchaseHistory/${check.uid}/`)
												.on('value', snapshot => {
																snapshot.forEach(element => {
																				arrHistory.push(element.val())
																				this.setState({PurchaseHistory: arrHistory})
																});
												})
								arrHistory.reverse() */
		this.renderUsers()
	}

	setActive = (uid) => () => {
		this.setState({selectedChatRoomUser: uid})
		this.checkAndCreateChatRoom(uid)
		this.renderUsers()
	}

	checkAndCreateChatRoom = (uid) => {
		var check = firebaseApp.auth().currentUser
		var chatRoomRef = firebaseApp.database().ref("conversations")
		var exist=false
		chatRoomRef.on('value',snapshot=>{
			var data= snapshot.val()
			var chatRoomData= Object.keys(data)
			chatRoomData.forEach((e) => {
				if(data[e].users.users[0]==uid||data[e].users.users[1]==uid && data[e].users.users[0]==check.uid||data[e].users.users[1]==check.uid)//check and create the chat room
				{
					console.log("exist with key :" + e )
					this.setState({chatRoomID:e})
					exist=true
				}
		})
		if(!exist){
			chatRoomRef.push({
				 users:{users:[check.uid,uid],
				 DateCreated:moment().unix()	
					 }
				}) 
			}		
	})


	}

	renderUsers = () => {
		var allUser = firebaseRef.child(`users`)
		var Users = []
		var userData = []
		var userDataToRender = []
		allUser.on('value', snapshot => {
			var data = snapshot.val()
			var dataKey = Object.keys(data)
			dataKey.forEach((e) => {
				if (data[e].info) {
					userData.push({uid: e, data: data[e].info})
					if (this.state.selectedChatRoomUser == e) 
						userDataToRender.push(
							<div onClick={this.setActive(e)}>
								<ChatListItem active>
									<Avatar
										imgUrl={data[e].info.fullName.avatar
										? data[e].info.fullName.avatar
										: ''}
										letter={data[e]
										.info
										.fullName
										.toString()
										.substring(1, 1)}/>
									<Column fill>
										<Row justify>
											<Title ellipsis>{data[e].info.fullName}</Title>
											<Subtitle nowrap>{''}</Subtitle>
										</Row>
										<Subtitle ellipsis>
											{''}
										</Subtitle>
									</Column>
								</ChatListItem>
							</div>
						)
					else {
						userDataToRender.push(
							<div onClick={this.setActive(e)}>
								<ChatListItem>
									<Avatar
										imgUrl={data[e].info.fullName.avatar
										? data[e].info.fullName.avatar
										: ''}
										letter={data[e]
										.info
										.fullName
										.toString()
										.substring(1, 1)}/>
									<Column fill>
										<Row justify>
											<Title ellipsis>{data[e].info.fullName}</Title>
											<Subtitle nowrap>{''}</Subtitle>
										</Row>
										<Subtitle ellipsis>
											{''}
										</Subtitle>
									</Column>
								</ChatListItem>
							</div>
						)
					}
				}
				this.setState({Users: Users, userData: userData, userDataToRender: userDataToRender})
			})
		})
	}

	render() {
		var selectedUserChatData = this.state.userData.find(x=>x.uid==this.state.selectedChatRoomUser)
		return (
			<ThemeProvider>
				<div style={{height:"100vh", width:"100vw",paddingRight:'5%'}}>
				<p>Click on a user to start chatting</p>
					<div style={{
						float: 'left',
						overflow:'scroll',
						height:'90%',
						width:'auto'
					}}>
						<ChatList style={{
							maxWidth: 400
						}}>
							{this.state.userDataToRender}
						</ChatList>
					</div>
					<div style={{
						float: 'right',
						height:'90%',
						width:'70%',
						marginRight:'10%',
						backgroundColor:'white'
					}}>
					<InstantMessengerMsgList  selectedUserChatData={selectedUserChatData} chatRoomID={this.state.chatRoomID}/>
					</div>
				</div>
			</ThemeProvider>
		)
	}
}