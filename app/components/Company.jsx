// - Import react components
import React, { Component } from 'react'
import _ from 'lodash'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { push } from 'react-router-redux'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import SvgArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import SvgHome from 'material-ui/svg-icons/action/home'
import SvgFeedback from 'material-ui/svg-icons/action/feedback'
import SvgSettings from 'material-ui/svg-icons/action/settings'
import SvgAccountCircle from 'material-ui/svg-icons/action/account-circle'
import SvgPeople from 'material-ui/svg-icons/social/people'
import EventListener, { withOptions } from 'react-event-listener'
import RaisedButton from 'material-ui/RaisedButton'
import { firebaseRef } from 'app/firebase/'
import HomeIcon from 'react-icons/lib/ti/home-outline'
import PeopleIcon from 'react-icons/lib/md/person-outline'
import ProfileIcon from 'react-icons/lib/go/file-text'
import CampIcon from 'react-icons/lib/io/fireball'
import PCreditIcon from 'react-icons/lib/md/bookmark-outline'
import PHisIcon from 'react-icons/lib/md/history'
import MssgIcon from 'react-icons/lib/ti/messages'
import SettingIcon from 'react-icons/lib/md/settings'
import '../styles/components/_hr.scss'
import FeedIcon from 'react-icons/lib/io/android-send'

// - Import app components
import Sidebar from 'Sidebar'
import Blog from 'Blog'
import HomeHeader from 'HomeHeader'
import SidebarContent from 'SidebarContent'
import SidebarMain from 'SidebarMain'
import MyProfile from 'MyProfile'
import PostPage from 'PostPage'
import ImgCover from 'ImgCover'
import EditProfile from 'EditProfile'
import UserAvatar from 'UserAvatar'
import CompanyDashboard from 'CompanyDashboard'
import CompCampaign from 'CompCampaign'
import CompMyCampaign from 'CompMyCampaign'
import CompanyInfoView from 'CompanyInfoView'
import MyCampInfo from 'CompMyCampaignInfo'
import EditCampInfo from 'CompEditCampaignInfo'
import CompPplSearch from 'CompPplSearch'
import CompPageBuilder from 'CompPageBuilder'
import Followers from 'Followers'
import PurchaseCredits from 'PurchaseCredits'
import PostPaymentPage from 'PostPaymentPage'
import InstantMessenger from 'InstantMessenger'
import CompCampView from 'CompCampView'
import Footer from 'Footer'

// - Import API
import CircleAPI from 'CircleAPI'

// - Import actions
import * as globalActions from 'globalActions'
import * as userActions from 'userActions'


// - Create Company component class
export class Company extends Component {
    static propTypes = {

        /**
         * User avatar address
         */
        avatar: PropTypes.string,
        /**
         * User avatar address
         */
        banner: PropTypes.string,
        /**
         * User full name
         */
        fullName: PropTypes.string.isRequired,
        /**
         * The number of followers
         */
        followerCount: PropTypes.number,
        /**
         * User identifier
         */
        userId: PropTypes.string,
        /**
         * If the user profile identifier of param is equal to the user authed identifier 
         */
        isAuthedUser: PropTypes.bool,

        university: PropTypes.string

    }

    // Constructor
    constructor(props) {
        super(props)

        // Default state
        this.state = {
            sidebarOpen: () => _,
            sidebarStatus: true,
            sidebaOverlay: false,
            noParticipantList: [],
            followers: 0,
            credit: 0,
            dashColor: '#002c54',
            searchColor: '',
            profileColor: '',
            campColor: '',
            pCreditColor: '',
            pHisColor: '',
            mssgColor: '',
            avatar: '',
            company: ''
        }

        // Binding function to `this`
        this.sidebar = this.sidebar.bind(this)
        this.sidebarStatus = this.sidebarStatus.bind(this)
        this.sidebarOverlay = this.sidebarOverlay.bind(this)
        this.handleCloseSidebar = this.handleCloseSidebar.bind(this)

    }

    componentWillMount() {
        this.props.loadUserInfo()
        if (Object.keys(this.props.peopleInfo)[0]) {
            var circleData = firebaseRef.child(`userCircles`).child(Object.keys(this.props.peopleInfo)[0])
            circleData.on('value', snapshot => {
                var followersCount = 0
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                dataKey.forEach(e => {
                    if (e === '-Followers') {
                        followersCount += Object.keys(data[e].users).length
                    }
                })
                this.setState({ followers: followersCount })
            })
        }

        var userInfo = firebaseRef.child(`users`).child(Object.keys(this.props.peopleInfo)[0]).child('info')
        userInfo.on('value', snapshot => {
            this.setState({ avatar: snapshot.val().avatar, company: snapshot.val().company,  })
        })

        var windowPath = window.location.pathname.split('/')[1]

        if (windowPath === 'companypplsearch' || windowPath === 'MyProfile') {
            this.handleSearchClick()
        }
        else if (windowPath === 'companyinfo' || windowPath === 'comppagebuilder') {
            this.handleProfileClick()
        }
        else if (windowPath === 'companymycampaign' || windowPath === 'companymycampaigninfo' || windowPath === 'companyeditcampaigninfo') {
            this.handleCampClick()
        }
        else if (windowPath === 'PurchaseCredits') {
            this.handlePCreditClick()
        }
        else if (windowPath === 'PostPaymentPage') {
            this.handlePHisClick()
        }
        else if (windowPath === 'InstantMessenger') {
            this.handleMssgClick()
        }
        else {
            this.handleDashClick()
        }
    }
    handleFeedbackClick = () => {
        window.location='http://gomonsta.asia/contact/'
    }
    componentDidMount() {
        var participantList = []
        var campaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0])
        var companyInfo = firebaseRef.child("users").child(this.props.uid)

        companyInfo.on('value', snapshot => {
            var data = snapshot.val()
            console.log(data)
            console.log('here')
            this.setState({ credit: snapshot.val().credit })
        })

        var test = this.state.credit

        campaignList.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            dataKey.forEach(function (e) {
                if (data[e].ParticipantList) {
                    var plist = data[e].ParticipantList
                    var pKeys = Object.keys(plist)
                    pKeys.forEach(function (idx) {
                        participantList.includes(idx) === false ?
                            participantList.push(idx)
                            :
                            ''
                    })
                }
            })
            this.setState({ noParticipantList: participantList })
        })
    }

    handleDashClick = () => this.setState({
        dashColor: '#002c54', searchColor: '', profileColor: '', campColor: '',
        pCreditColor: '', pHisColor: '', mssgColor: ''
    })
    handleSearchClick = () => this.setState({
        dashColor: '', searchColor: '#002c54', profileColor: '', campColor: '',
        pCreditColor: '', pHisColor: '', mssgColor: ''
    })
    handleProfileClick = () => this.setState({
        dashColor: '', searchColor: '', profileColor: '#002c54', campColor: '',
        pCreditColor: '', pHisColor: '', mssgColor: ''
    })
    handleCampClick = () => this.setState({
        dashColor: '', searchColor: '', profileColor: '', campColor: '#002c54',
        pCreditColor: '', pHisColor: '', mssgColor: ''
    })
    handlePCreditClick = () => this.setState({
        dashColor: '', searchColor: '', profileColor: '', campColor: '',
        pCreditColor: '#002c54', pHisColor: '', mssgColor: ''
    })
    handlePHisClick = () => this.setState({
        dashColor: '', searchColor: '', profileColor: '', campColor: '',
        pCreditColor: '', pHisColor: '#002c54', mssgColor: ''
    })
    handleMssgClick = () => this.setState({
        dashColor: '', searchColor: '', profileColor: '', campColor: '',
        pCreditColor: '', pHisColor: '', mssgColor: '#002c54'
    })

    /**
     * handle close sidebar
     */
    handleCloseSidebar = () => {
        this.state.sidebarOpen(false, 'overlay')
    }

    /**
     * Change sidebar overlay status
     * @param  {boolean} status if is true, the sidebar is on overlay status
     */
    sidebarOverlay = (status) => {
        this.setState({
            sidebarOverlay: status
        })
    }


    /**
     * Pass function to change sidebar status
     * @param  {boolean} open  is a function callback to change sidebar status out of sidebar component
     */
    sidebar = (open) => {

        this.setState({
            sidebarOpen: open
        })
    }


    /**
     * Change sidebar status if is open or not
     * @param  {boolean} status is true, if the sidebar is open
     */
    sidebarStatus = (status) => {
        this.setState({
            sidebarStatus: status
        })
    }

    /**
     * Render DOM component
     * 
     * @returns DOM
     * 
     * @memberof Company
     */
    render() {

        const styles = {
            avatar: {
                border: '2px solid rgb(255, 255, 255)'
            },
            iconButton: {
                fill: 'rgb(255, 255, 255)',
                height: '24px',
                width: '24px',

            },
            iconButtonSmall: {
                fill: 'rgb(0, 0, 0)',
                height: '24px',
                width: '24px',
            },

            editButton: {

                marginLeft: '20px'

            },
            editButtonSmall: {

                marginLeft: '20px',
                color: 'white',
                fill: 'blue'

            },
            aboutButton: {
                color: 'white'
            },
            aboutButtonSmall: {
                color: 'black'
            }
        }

        const { isAuthedUser } = this.props

        return (
            <div id="home">
                <HomeHeader sidebar={this.state.sidebarOpen} sidebarStatus={this.state.sidebarStatus} />
                <Sidebar overlay={this.sidebarOverlay} open={this.sidebar} status={this.sidebarStatus}>
                    <SidebarContent>
                        <div className={this.state.isSmall ? 'profile__head-info-s' : 'profile__head-info'}>
                            <EventListener
                                target="window"
                                onResize={this.handleResize}
                            />
                            <div style={{ display: 'table', marginRight: 'auto', marginLeft: 'auto' }}>
                                <UserAvatar fileName={this.state.avatar} size={100} style={styles.avatar} />
                                {isAuthedUser ? (
                                    <div style={this.state.isSmall ? styles.editButtonSmall : styles.editButton}>
                                        <RaisedButton label="EDIT PROFILE" onClick={this.props.openEditor} />
                                    </div>) : ''}
                            </div>
                        </div>
                        <div style={{ padding: ' 5% 0% 15% 0%', display: 'table', marginRight: 'auto', marginLeft: 'auto', textAlign: 'center' }}>
                            <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>WELCOME</text><br />
                            <text style={{ color: "white", fontSize: 20 }}>{this.state.company}</text><br />
                            <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>{`CREDIT : ${this.state.credit} CREDITS`} </text><br />
                      {/*       <NavLink to='/Followers'>
                                <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>{this.state.followers} Students Following</text> <br />
                            </NavLink>
                            <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>{this.state.noParticipantList.length} Students participated </text><br />
                        */} </div>
                        <hr className="hr-text" />
                        <Menu>
                            {this.state.sidebarOverlay
                                ?
                                <div>
                                    <MenuItem onClick={this.handleCloseSidebar} primaryText={<span style={{ color: "white" }}
                                        className="sidebar__title">Monsta</span>}
                                        rightIcon={<SvgArrowLeft viewBox="0 3 24 24" style={{ color: "#fff", marginLeft: "15px", width: "32px", height: "32px", cursor: "pointer" }} />} />
                                    <Divider />
                                </div>
                                : ""
                            }
                            <NavLink to='/'>
                                <MenuItem onClick={this.handleDashClick} primaryText="Dashboard"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.dashColor }}
                                    leftIcon={<HomeIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            <NavLink to='/companypplsearch'>
                                <MenuItem onClick={this.handleSearchClick} primaryText="Search Students"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.searchColor }}
                                    leftIcon={<PeopleIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            {/*<NavLink to='/comppagebuilder'><MenuItem primaryText="Company Cultural Profile" style={{ color: "rgb(117, 117, 117)" }} /></NavLink><Divider />*/}
                            <NavLink to={`/companyinfo/${Object.keys(this.props.peopleInfo)[0]}`}>
                                <MenuItem onClick={this.handleProfileClick} primaryText="My Culture Profile"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.profileColor }}
                                    leftIcon={<ProfileIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            <NavLink to='/companymycampaign'>
                                <MenuItem onClick={this.handleCampClick} primaryText="My Campaigns/ Events"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.campColor }}
                                    leftIcon={<CampIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            {/*<NavLink to='/#'><MenuItem primaryText="Student Scoreboard" style={{ color: "rgb(117, 117, 117)" }} /></NavLink><Divider />*/}
                            <NavLink to='/PurchaseCredits'>
                                <MenuItem onClick={this.handlePCreditClick} primaryText="Purchase Credits"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.pCreditColor }}
                                    leftIcon={<PCreditIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            <NavLink to='/PostPaymentPage'>
                                <MenuItem onClick={this.handlePHisClick} primaryText="Purchase History"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.pHisColor }}
                                    leftIcon={<PHisIcon style={{ color: 'white' }} />} />
                            </NavLink>
                          {/*         <NavLink to='/InstantMessenger'>
                                <MenuItem onClick={this.handleMssgClick} primaryText="Messenger"
                                    style={{ color: "white", fontSize: 14, backgroundColor: this.state.mssgColor }}
                                    leftIcon={<MssgIcon style={{ color: 'white' }} />} />
                            </NavLink>  */}
                            <NavLink to='/settings'>
                                <MenuItem primaryText="Settings" style={{ color: "white", fontSize: 14 }}
                                    leftIcon={<SettingIcon style={{ color: 'white' }} />} />
                            </NavLink>
                            <NavLink to='#'>
                                 <MenuItem primaryText="Send feedback" onClick={this.handleFeedbackClick}
                                     style={{ color: "white", fontSize: 14 }}
                                     leftIcon={<FeedIcon style={{ color: 'white' }} />} />
                             </NavLink>

                        </Menu>
                    </SidebarContent>

                    <SidebarMain>
                        <Switch>
                            <Route path="/PostPaymentPage" component={PostPaymentPage} />
                            <Route path="/Followers" component={Followers} />
                            <Route path="/InstantMessenger" component={InstantMessenger} />
                            <Route path="/companypplsearch" render={() => {
                                return (
                                    this.props.authed
                                        ? <CompPplSearch />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/compcampview/:companyKey/:selectedKey" render={({ match }) => {
                                return (
                                    this.props.authed
                                        ? <CompCampView companyKey={match.params.companyKey} selectedKey={match.params.selectedKey} />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/companymycampaigninfo/:selectedKey" render={({ match }) => {
                                return (
                                    this.props.authed
                                        ? <MyCampInfo selectedKey={match.params.selectedKey} />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/companyeditcampaigninfo/:selectedKey" render={({ match }) => {
                                return (
                                    this.props.authed
                                        ? <EditCampInfo selectedKey={match.params.selectedKey} />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/comppagebuilder/" render={() => {
                                return (
                                    this.props.authed
                                        ? <CompPageBuilder />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/companymycampaign/" render={() => {
                                return (
                                    this.props.authed
                                        ? <CompMyCampaign />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/companycampaign/:tab?" render={() => {
                                return (
                                    this.props.authed
                                        ? <CompCampaign />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/companyinfo/:selectedKey" render={({ match }) => {
                                return (
                                    this.props.authed
                                        ? <CompanyInfoView selectedKey={match.params.selectedKey} />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/tag/:tag" render={({ match }) => {

                                return (
                                    this.props.authed
                                        ? <div className="blog"><Blog displayWriting={false} homeTitle={`#${match.params.tag}`} posts={this.props.mergedPosts} /></div>
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/:userId/posts/:postId/:tag?" component={PostPage} />
                            <Route path="/MyProfile/:userId"
                                component={({ match }) => <MyProfile userId={match.params.userId} />} />
                            <Route path="/PurchaseCredits" render={() => {
                                return (
                                    this.props.authed
                                        ? <PurchaseCredits />
                                        : <Redirect to="/login" />
                                )
                            }} />
                            <Route path="/" render={() => {

                                return (
                                    this.props.authed
                                        ? <CompanyDashboard />
                                        : <Redirect to="/login" />
                                )
                            }} />
                        </Switch>
                    </SidebarMain>
                </Sidebar>
                <Footer style={{position:'absolute',bottom:0}} sidebarStatus={this.state.sidebarStatus}/>
            </div>

        )
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    const { userId } = ownProps.match.params
    return {
        loadUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(userId, 'header'))
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    const { uid } = state.authorize
    let mergedPosts = {}
    const circles = state.circle ? (state.circle.userCircles[uid] || {}) : {}
    const followingUsers = CircleAPI.getFollowingUsers(circles)
    const posts = state.post.userPosts ? state.post.userPosts[state.authorize.uid] : {}
    Object.keys(followingUsers).forEach((userId) => {
        let newPosts = state.post.userPosts ? state.post.userPosts[userId] : {}
        _.merge(mergedPosts, newPosts)
    })
    _.merge(mergedPosts, posts)
    return {
        avatar: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].avatar : '',
        fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
        university: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].university : '',
        monstaXP: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].monstaXP : '',
        peopleInfo: state.user.info,
        authed: state.authorize.authed,
        mainStyle: state.global.sidebarMainStyle,
        mergedPosts
    }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Company))
