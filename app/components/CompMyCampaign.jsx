import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import UserAvatar from 'UserAvatar'
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import Map from 'react-icons/lib/md/map'
import TimePicker from 'material-ui/TimePicker';
import CoverAvatar from 'CoverAvatar'
import '../styles/app.scss'
import '../styles/div.scss'
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

var skills = []

class CompMyCampaign extends Component {

    constructor(props) {
        super(props)
        this.state = {
            campaignList: [],
            specializationList: [],
            skillList: [],
            coursesList: [],
            redirect: false,
            selectedKey: '',
            userList: [],
            skills: [],
            specialization: [],
            myCampaignList: [],
            dropDownValue:'all',
            dropDownValueCurrentPast:'current'
        }
        this.CampInfo = this.CampInfo.bind(this)
        this.genCampList = this.genCampList.bind(this)
    }

    genCampList = (campSelect,pastEvent)  => {
        console.log("campselect",campSelect)
        var dataSort = []
        this.state.specialization.on('value', snapshot => {
            this.state.myCampaignList.on('value', snapshotCamp => {
                var data = snapshotCamp.val()
                var dataKey = Object.keys(data)
                var arrayList = []

                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        if (data[e].startDate) {
                            if(pastEvent=="current"){
                                if (new Date(data[e].startDate).toDateString() <= new Date().toDateString() && new Date(data[e].endDate).toDateString() >= new Date().toDateString()) {
                                if (data[e].typeOfCampaign) {
                                    if (campSelect === 'job') {
                                        if (data[e].typeOfCampaign == 1) {
                                            dataSort.push({
                                                uid: e
                                            })
                                        }
                                    }
                                    else if (campSelect === 'survey') {
                                        if (data[e].typeOfCampaign == 2) {
                                            dataSort.push({
                                                uid: e
                                            })
                                        }
                                    }
                                    else if (campSelect === 'event') {
                                        if (data[e].typeOfCampaign == 3) {
                                            dataSort.push({
                                                uid: e
                                            })
                                        }
                                    }
                                    else {
                                        dataSort.push({
                                            uid: e
                                        })
                                    }
                                }
                                }
                            }
                            else{
                                    if (data[e].typeOfCampaign) {
                                        if (campSelect === 'job') {
                                            if (data[e].typeOfCampaign == 1) {
                                                dataSort.push({
                                                    uid: e
                                                })
                                            }
                                        }
                                        else if (campSelect === 'survey') {
                                            if (data[e].typeOfCampaign == 2) {
                                                dataSort.push({
                                                    uid: e
                                                })
                                            }
                                        }
                                        else if (campSelect === 'event') {
                                            if (data[e].typeOfCampaign == 3) {
                                                dataSort.push({
                                                    uid: e
                                                })
                                            }
                                        }
                                        else {
                                            dataSort.push({
                                                uid: e
                                            })
                                        }
                                    }
                            }
                        }
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(data[b.uid].date) - new Date(data[a.uid].date);
                });

                dataSort.forEach(function (e) {
                    if (!data[e.uid].isDraft) {
                        arrayList.push(
                            <div style={{
                                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                                height: 'auto', position: 'relative'
                            }} onClick={this.CampInfo(e.uid)}>
                                <div className={'clickDiv'}>
                                    <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                                        <UserAvatar fileName={data[e.uid].companyAvatar}
                                            size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                    </div>
                                    <div style={{
                                        position: 'absolute', right: '5%', top: '210px',
                                        backgroundColor: '#212F3C', borderRadius: 10, padding: '1%'
                                    }}>
                                        <text style={{ fontSize: 13, color: 'white' }}>
                                            {data[e.uid].ParticipantList ? " " + Object.keys(data[e.uid].ParticipantList).length : " 0"}
                                            {data[e.uid].typeOfCampaign === 1 ?
                                                " Applied "
                                                :
                                                data[e.uid].typeOfCampaign === 2 ?
                                                    " Joined "
                                                    :
                                                    data[e.uid].typeOfCampaign === 3 ?
                                                        " Attend "
                                                        : false}
                                        </text>
                                    </div>
                                    <CoverAvatar fileName={data[e.uid].campAvatar} height={'200px'} />
                                    <div style={{
                                        backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                    }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 16 }}>{data[e.uid].campaignTitle}</text>
                                        <br />
                                        <div style={{ marginTop: '2%', height: '90px' }}>
                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                                {
                                                    data[e.uid].description.length > 200 ?
                                                        data[e.uid].description.substring(0, 200) + '...'
                                                        :
                                                        data[e.uid].description
                                                }
                                            </p>
                                        </div>
                                        <br />
                                        {data[e.uid].typeOfCampaign === 1 ?
                                            <div>
                                                <text style={{ fontWeight: 'bold', fontSize: 13 }}>Allowance : </text><br />
                                                <text style={{ fontSize: 13 }}>RM {data[e.uid].minPayout} - RM {data[e.uid].maxPayout}</text>
                                            </div>
                                            :
                                            data[e.uid].typeOfCampaign === 2 ?
                                                <div>
                                                    <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                        {
                                                            data[e.uid].skillCanLearn.length > 0 ?
                                                                data[e.uid].skillCanLearn.map((e, idx) => (
                                                                    idx < 4 ?
                                                                        <div style={{
                                                                            backgroundColor: '#CACFD2', padding: '1%', width: `${snapshot.val().skills[e].length * 8 + 20}px`,
                                                                            margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                                        }}>
                                                                            <text style={{ fontSize: 13 }}>{snapshot.val().skills[e]}</text>
                                                                        </div>
                                                                        : false
                                                                ))
                                                                : false
                                                        }
                                                    </div>
                                                </div>
                                                :
                                                data[e.uid].typeOfCampaign === 3 ?
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td><Calender /></td>
                                                                <td>
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{data[e.uid].eventDate}</text>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><Time /></td>
                                                                <td>
                                                                    <TimePicker
                                                                        style={{ fontSize: 13, paddingLeft: '1%' }}
                                                                        disabled={true}
                                                                        format="ampm"
                                                                        value={new Date(data[e.uid].eventTime)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><Map /></td>
                                                                <td>
                                                                    {
                                                                        data[e.uid].eventAdd2 ?
                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                {data[e.uid].eventAdd1}, {data[e.uid].eventAdd2}, {data[e.uid].eventCity}, {data[e.uid].eventZip}, {data[e.uid].eventState}
                                                                            </p>
                                                                            :
                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                {data[e.uid].eventAdd1}, {data[e.uid].eventCity}, {data[e.uid].eventZip}, {data[e.uid].eventState}
                                                                            </p>
                                                                    }
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    : false
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }, this)
                this.setState({ campaignList: arrayList })
            })
        })
    }

    componentWillMount() {
        var specialization = firebaseRef.child("staticData")
        var myCampaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0])
        specialization.on('value', snapshot => {
            myCampaignList.on('value', snapshotCamp => {
                var data = snapshotCamp.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.sort(function (a, b) {
                    return new Date(data[b].date) - new Date(data[a].date);
                });

                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        if (new Date(data[e].startDate).toDateString() <= new Date().toDateString() && new Date(data[e].endDate).toDateString() >= new Date().toDateString()) {
                        arrayList.push(
                            <div style={{
                                margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                                height: 'auto', position: 'relative'
                            }} onClick={this.CampInfo(e)}>
                                <div className={'clickDiv'}>
                                    <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                                        <UserAvatar fileName={data[e].companyAvatar}
                                            size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                    </div>
                                    <div style={{
                                        position: 'absolute', right: '5%', top: '210px',
                                        backgroundColor: '#212F3C', borderRadius: 10, padding: '1%'
                                    }}>
                                        <text style={{ fontSize: 13, color: 'white' }}>
                                            {data[e].ParticipantList ? " " + Object.keys(data[e].ParticipantList).length : " 0"}
                                            {data[e].typeOfCampaign === 1 ?
                                                " Applied "
                                                :
                                                data[e].typeOfCampaign === 2 ?
                                                    " Joined "
                                                    :
                                                    data[e].typeOfCampaign === 3 ?
                                                        " Attend "
                                                        : false}
                                        </text>
                                    </div>
                                    <CoverAvatar fileName={data[e].campAvatar} height={'200px'} />
                                    <div style={{
                                        backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                                        borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                    }}>
                                        <text style={{ fontWeight: 'bold', fontSize: 16 }}>{data[e].campaignTitle}</text>
                                        <br />
                                        <div style={{ marginTop: '2%', height: '90px' }}>
                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                                {
                                                    data[e].description.length > 200 ?
                                                        data[e].description.substring(0, 200) + '...'
                                                        :
                                                        data[e].description
                                                }
                                            </p>
                                        </div>
                                        <br />
                                        {data[e].typeOfCampaign === 1 ?
                                            <div>
                                                <text style={{ fontWeight: 'bold', fontSize: 13 }}>Allowance : </text><br />
                                                <text style={{ fontSize: 13 }}>RM {data[e].minPayout} - RM {data[e].maxPayout}</text>
                                            </div>
                                            :
                                            data[e].typeOfCampaign === 2 ?
                                                <div>
                                                    <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                        {
                                                            data[e].skillCanLearn.length > 0 ?
                                                                data[e].skillCanLearn.map((e, idx) => (
                                                                    idx < 4 ?
                                                                        <div style={{
                                                                            backgroundColor: '#CACFD2', padding: '1%', width: `${snapshot.val().skills[e].length * 8 + 20}px`,
                                                                            margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                                        }}>
                                                                            <text style={{ fontSize: 13 }}>{snapshot.val().skills[e]}</text>
                                                                        </div>
                                                                        : false
                                                                ))
                                                                : false
                                                        }
                                                    </div>
                                                </div>
                                                :
                                                data[e].typeOfCampaign === 3 ?
                                                    <div>
                                                        <table>
                                                            <tr>
                                                                <td><Calender /></td>
                                                                <td>
                                                                    <text style={{ fontSize: 13, paddingLeft: '1%' }}>{data[e].eventDate}</text>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><Time /></td>
                                                                <td>
                                                                    <TimePicker
                                                                        style={{ fontSize: 13, paddingLeft: '1%' }}
                                                                        disabled={true}
                                                                        format="ampm"
                                                                        value={new Date(data[e].eventTime)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td><Map /></td>
                                                                <td>
                                                                    {
                                                                        data[e].eventAdd2 ?
                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                {data[e].eventAdd1}, {data[e].eventAdd2}, {data[e].eventCity}, {data[e].eventZip}, {data[e].eventState}
                                                                            </p>
                                                                            :
                                                                            <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                                {data[e].eventAdd1}, {data[e].eventCity}, {data[e].eventZip}, {data[e].eventState}
                                                                            </p>
                                                                    }
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    : false
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
            }, this)
                this.setState({ campaignList: arrayList, myCampaignList: myCampaignList, specialization: specialization })
            })
        })

        var user = firebaseRef.child("users")
        user.on('value', snapshot => {
            this.setState({ userList: snapshot.val() })
        })
    }

    CampInfo = (objectKey) => () => {
        this.setState({ redirect: true, selectedKey: objectKey })
    }
    handleChange = (event, index, value) =>{
        this.setState({dropDownValue:value})
        this.genCampList(value,this.state.dropDownValueCurrentPast)
        this.forceUpdate()
    } 

    handleChangeDate=(event,index,value)=>{
        this.setState({dropDownValueCurrentPast:value})
        this.genCampList(this.state.dropDownValue,value)
        this.forceUpdate()
    }

    render() {

        if (this.state.redirect) {
            return <Redirect push to={`/companymycampaigninfo/${this.state.selectedKey}`} />;
        }

        return (
            <div>
                <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                    <div style={{ display: 'flex',justifyContent:'space-between', flexWrap: 'wrap', width: '40%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                    {/*     <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="All" onClick={this.genCampList('all')} />
                        </div>
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Online Events" onClick={this.genCampList('survey')} />
                        </div>
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Event" onClick={this.genCampList('event')} />
                        </div>
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Career &amp; Opportunities" onClick={this.genCampList('job')} />
                        </div> */}
                        <SelectField floatingLabelText="Categories" value={this.state.dropDownValue} onChange={this.handleChange}>
                             <MenuItem value={'all'} primaryText="All" />
                             <MenuItem value={'survey'} primaryText="Online Events" />
                             <MenuItem value={'event'} primaryText="Event" />
                             <MenuItem value={'job'} primaryText="Career &amp; Opportunities" />
                        </SelectField>
                        <SelectField floatingLabelText="Show" value={this.state.dropDownValueCurrentPast} onChange={this.handleChangeDate}>
                             <MenuItem value={'current'} primaryText="Ongoing" />
                             <MenuItem value={'past'} primaryText="Past" />
                        </SelectField>
                    </div>
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {this.state.campaignList}
                    </div>
                </div>
            </div>
        );
    }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompMyCampaign)