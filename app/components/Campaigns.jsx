import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'

class Campaigns extends Component {

    constructor(props) {
        super(props)
        this.state = {
            campaignList: [],
            redirect: false,
            selectedKey: '',
            campaignKey: ''
        }
        this.CampInfo = this.CampInfo.bind(this)
    }

    componentWillMount() {
        var myCampaignList = firebaseRef.child("campaigns");
        myCampaignList.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data)
            var arrayList = []
            var i = 0;
            dataKey.forEach(function (e) {
                var secondList = firebaseRef.child("campaigns/" + dataKey[i]);
                var key1 = dataKey[i];
                i++;
                console.log(i);
                secondList.on('value', snapshot1 => {
                    var data1 = snapshot1.val()
                    var dataKey1 = Object.keys(data1)
                    var x = 0;
                    dataKey1.forEach(function (e) {
                        var campaignPath = key1 + "/" + dataKey1[x];
                        data1[e].campaignPath = campaignPath;
                        x++;

                        if (data1[e].typeOfCampaign == 1) {
                            arrayList.push(
                                <div style={{ border: '1px solid black ', margin: '2% 2% 0 2%', padding: '2%', overflow: 'hidden', display: 'flex' }}  >
                                    <div style={{ float: 'left', width: '100%' }}>
                                        <text style={{ fontSize: '20', fontWeight: 'bold' }}>{data1[e].campaignTitle}</text><br />
                                        <p style={{ fontSize: '12', wordWrap: 'break-word' }}>{data1[e].description}</p>
                                    </div>
                                    <div style={{ marginLeft: '10%', display: 'flex', alignItems: 'center' }}>
                                        <div style={{ textAlign: 'center' }}>
                                            <text style={{ fontSize: '30', fontWeight: 'bold' }}>{data1[e].monstaXP}</text><br />
                                            <text style={{ fontSize: '20' }}>Monsta XP</text>
                                            <RaisedButton label="Join Now" primary={true} onClick={this.CampInfo(data1[e].campaignPath)} />
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    }, this)
                    this.setState({ campaignList: arrayList })
                })
            }, this)
        })
    }

    CampInfo = (abc) => () => {
        this.setState({ redirect: true, campaignKey: abc })
    }

    render() {

        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />;
        }

        return (
            <div>
                <div style={{ backgroundColor: 'white', width: '100%', margin: '2%', padding: '2%' }}>
                    <text>Campaigns <text style={{ fontSize: '20', fontWeight: 'bold' }}></text></text>
                    {this.state.campaignList}
                </div>
            </div>
        );
    }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(Campaigns)