import React, { Component } from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import UserAvatar from 'UserAvatar'
import Avatar from 'material-ui/Avatar'
import { Table, Row, Cell } from 'react-responsive-table';
import Calender from 'react-icons/lib/fa/calendar'
import Time from 'react-icons/lib/md/access-time'
import Map from 'react-icons/lib/md/map'
import TimePicker from 'material-ui/TimePicker';
import CoverAvatar from 'CoverAvatar'
import '../styles/app.scss'
import '../styles/div.scss'
import DropDownMenu from 'material-ui/DropDownMenu';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

class CareerOpportunities extends Component {

    constructor(props) {
        super(props)
        this.state = {
            campaignList: [],
            redirect: false,
            selectedKey: '',
            campaignKey: '',
            arrayList2: [],
            avatarURL: 'http://gomonsta.asia/wp-content/uploads/2017/11/monsta_logo.png',
            specialization: [],
            myCampaignList: [],
            dropDownValue:'all'

        }
        this.CampInfo = this.CampInfo.bind(this)
        this.genCampList = this.genCampList.bind(this)
    }

    genCampList = (campSelect)  => {
        var dataSort = []
        this.state.specialization.on('value', snapshotSpec => {
            this.state.myCampaignList.on('value', snapshot => {
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        var dataCamp = data[e]
                        var dataCampKey = Object.keys(dataCamp)
                        dataCampKey.forEach(function (i) {
                            if (dataCamp[i].startDate) {
                                if (new Date(dataCamp[i].startDate).toDateString() <= new Date().toDateString() && new Date(dataCamp[i].endDate).toDateString() >= new Date().toDateString()) {
                                    if (dataCamp[i].typeOfCampaign) {
                                        if (campSelect === 'job') {
                                            if (dataCamp[i].typeOfCampaign == 1) {
                                                dataCamp[i].compUid = e
                                                dataCamp[i].campUid = i
                                                dataSort.push({
                                                    data: dataCamp[i],
                                                    startDate: dataCamp[i].startDate
                                                })
                                            }
                                        }
                                        else if (campSelect === 'survey') {
                                            if (dataCamp[i].typeOfCampaign == 2) {
                                                dataCamp[i].compUid = e
                                                dataCamp[i].campUid = i
                                                dataSort.push({
                                                    data: dataCamp[i],
                                                    startDate: dataCamp[i].startDate
                                                })
                                            }
                                        }
                                        else if (campSelect === 'event') {
                                            if (dataCamp[i].typeOfCampaign == 3) {
                                                dataCamp[i].compUid = e
                                                dataCamp[i].campUid = i
                                                dataSort.push({
                                                    data: dataCamp[i],
                                                    startDate: dataCamp[i].startDate
                                                })
                                            }
                                        }
                                        else {
                                            dataCamp[i].compUid = e
                                            dataCamp[i].campUid = i
                                            dataSort.push({
                                                data: dataCamp[i],
                                                startDate: dataCamp[i].startDate
                                            })
                                        }
                                    }
                                }
                            } else {
                                if (dataCamp[i].typeOfCampaign) {
                                    if (campSelect === 'job') {
                                        if (dataCamp[i].typeOfCampaign == 1) {
                                            dataCamp[i].compUid = e
                                            dataCamp[i].campUid = i
                                            dataSort.push({
                                                data: dataCamp[i],
                                                startDate: new Date()
                                            })
                                        }
                                    }
                                    else if (campSelect === 'survey') {
                                        if (dataCamp[i].typeOfCampaign == 2) {
                                            dataCamp[i].compUid = e
                                            dataCamp[i].campUid = i
                                            dataSort.push({
                                                data: dataCamp[i],
                                                startDate: new Date()
                                            })
                                        }
                                    }
                                    else if (campSelect === 'event') {
                                        if (dataCamp[i].typeOfCampaign == 3) {
                                            dataCamp[i].compUid = e
                                            dataCamp[i].campUid = i
                                            dataSort.push({
                                                data: dataCamp[i],
                                                startDate: new Date()
                                            })
                                        }
                                    }
                                    else {
                                        dataCamp[i].compUid = e
                                        dataCamp[i].campUid = i
                                        dataSort.push({
                                            data: dataCamp[i],
                                            startDate: new Date()
                                        })
                                    }

                                }
                            }
                        })
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(b.startDate) - new Date(a.startDate);
                });

                dataSort.forEach(function (e) {
                    var campaignPath = e.data.compUid + "/" + e.data.campUid;
                    var availableSlot = 0
                    e.data.campaignPath = campaignPath;

                    if (e.data.targetStudent) {
                        if (e.data.ParticipantList) {
                            var partKey = Object.keys(e.data.ParticipantList)
                            availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                        } else {
                            availableSlot = e.data.targetStudent
                        }
                    }

                    arrayList.push(
                        <div style={{
                            margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                            height: 'auto', position: 'relative'
                        }} onClick={this.CampInfo(e.data.campaignPath)}>
                            <div className={'clickDiv'}>
                                <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                                    <UserAvatar fileName={e.data.companyAvatar}
                                        size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                </div>
                                <div style={{
                                    position: 'absolute', right: '5%', top: '210px',
                                    backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                                }}>
                                    <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                                </div>
                                <div style={{
                                    position: 'absolute', left: '40%', top: '210px', padding: '1%'
                                }}>
                                    {
                                        e.data.targetStudent ?
                                            availableSlot === 0 ?
                                                <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                                                :
                                                <text style={{ fontSize: '15', color: 'green' }}>{`${availableSlot}`}&nbsp;slot&nbsp;available</text>
                                            :
                                            <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                                    }
                                </div>
                                <CoverAvatar fileName={e.data.campAvatar} height={'200px'} />
                                <div style={{
                                    backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                                    borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                                    <br />
                                    <div style={{ marginTop: '2%', height: '90px' }}>
                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                            {
                                                e.data.description.length > 200 ?
                                                    e.data.description.substring(0, 200) + '...'
                                                    :
                                                    e.data.description
                                            }
                                        </p>
                                    </div>
                                    <br />
                                    {e.data.typeOfCampaign === 1 ?
                                        <div>
                                            <text style={{ fontWeight: 'bold', fontSize: 13 }}>Allowance : </text><br />
                                            <text style={{ fontSize: 13 }}>RM {e.data.minPayout} - RM {e.data.maxPayout}</text>
                                        </div>
                                        :
                                        e.data.typeOfCampaign === 2 ?
                                            <div>
                                                <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                    {
                                                        e.data.skillCanLearn.length > 0 ?
                                                            e.data.skillCanLearn.map((e, idx) => (
                                                                idx < 4 ?
                                                                    <div style={{
                                                                        backgroundColor: '#CACFD2', padding: '1%', width: `${snapshotSpec.val().skills[e].length * 8 + 20}px`,
                                                                        margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                                    }}>
                                                                        <text style={{ fontSize: 13 }}>{snapshotSpec.val().skills[e]}</text>
                                                                    </div>
                                                                    : false
                                                            ))
                                                            : false
                                                    }
                                                </div>
                                            </div>
                                            :
                                            e.data.typeOfCampaign === 3 ?
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td><Calender /></td>
                                                            <td>
                                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventDate}</text>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><Time /></td>
                                                            <td>
                                                                <TimePicker
                                                                    style={{ fontSize: 13, paddingLeft: '1%' }}
                                                                    disabled={true}
                                                                    format="ampm"
                                                                    value={new Date(e.data.eventTime)}
                                                                />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><Map /></td>
                                                            <td>
                                                                {
                                                                    e.data.eventAdd2 ?
                                                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                            {e.data.eventAdd1}, {e.data.eventAdd2}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                        </p>
                                                                        :
                                                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                            {e.data.eventAdd1}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                        </p>
                                                                }
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                : false
                                    }
                                </div>
                            </div>
                        </div>
                    )
                }, this)
                this.setState({ campaignList: arrayList });
            })
        })
    }

    componentWillMount() {
        var specialization = firebaseRef.child("staticData")
        var myCampaignList = firebaseRef.child("campaigns");
        var dataSort = []
        specialization.on('value', snapshotSpec => {
            myCampaignList.on('value', snapshot => {
                var data = snapshot.val()
                var dataKey = Object.keys(data)
                var arrayList = []
                dataKey.forEach(function (e) {
                    if (!data[e].isDraft) {
                        var dataCamp = data[e]
                        var dataCampKey = Object.keys(dataCamp)
                        dataCampKey.forEach(function (i) {
                            if (dataCamp[i].startDate) {
                                if (new Date(dataCamp[i].startDate).toDateString() <= new Date().toDateString() && new Date(dataCamp[i].endDate).toDateString() >= new Date().toDateString()) {
                                    if (dataCamp[i].typeOfCampaign) {
                                        dataCamp[i].compUid = e
                                        dataCamp[i].campUid = i
                                        dataSort.push({
                                            data: dataCamp[i],
                                            startDate: dataCamp[i].startDate
                                        })
                                    }
                                }
                            } else {
                                if (dataCamp[i].typeOfCampaign) {
                                    dataCamp[i].compUid = e
                                    dataCamp[i].campUid = i
                                    dataSort.push({
                                        data: dataCamp[i],
                                        startDate: new Date()
                                    })
                                }
                            }
                        })
                    }
                })

                dataSort.sort(function (a, b) {
                    return new Date(b.startDate) - new Date(a.startDate);
                });

                dataSort.forEach(function (e) {
                    var campaignPath = e.data.compUid + "/" + e.data.campUid;
                    var availableSlot = 0
                    e.data.campaignPath = campaignPath;

                    if (e.data.targetStudent) {
                        if (e.data.ParticipantList) {
                            var partKey = Object.keys(e.data.ParticipantList)
                            availableSlot = e.data.targetStudent - partKey.length <= 0 ? 0 : e.data.targetStudent - partKey.length
                        } else {
                            availableSlot = e.data.targetStudent
                        }
                    }

                    arrayList.push(
                        <div style={{
                            margin: '1%', width: (window.innerWidth < 1000 ? '100%' : '30%'),
                            height: 'auto', position: 'relative'
                        }} onClick={this.CampInfo(e.data.campaignPath)}>
                            <div className={'clickDiv'}>
                                <div style={{ position: 'absolute', left: '5%', top: '150px' }}>
                                    <UserAvatar fileName={e.data.companyAvatar}
                                        size={80} style={{ border: '2px solid rgb(255, 255, 255)' }} />
                                </div>
                                <div style={{
                                    position: 'absolute', right: '5%', top: '210px',
                                    backgroundColor: '#F5B041', borderRadius: 10, padding: '1%'
                                }}>
                                    <text style={{ fontSize: 13, color: 'white' }}>{`${e.data.monstaXP} XP`}</text>
                                </div>
                                <div style={{
                                    position: 'absolute', left: '40%', top: '210px', padding: '1%'
                                }}>
                                    {
                                        e.data.targetStudent ?
                                            availableSlot === 0 ?
                                                <text style={{ fontSize: '15', color: 'red' }}>No&nbsp;Slot&nbsp;available</text>
                                                :
                                                <text style={{ fontSize: '15', color: 'green' }}>{`${availableSlot}`}&nbsp;slot&nbsp;available</text>
                                            :
                                            <text style={{ fontSize: '15' }}>Not&nbsp;limit&nbsp;define</text>
                                    }
                                </div>
                                <CoverAvatar fileName={e.data.campAvatar} height={'200px'} />
                                <div style={{
                                    backgroundColor: '#FFFFFF', width: '100%', height: '340px', padding: '10% 2% 2% 7%',
                                    borderBottomLeftRadius: 20, borderBottomRightRadius: 20
                                }}>
                                    <text style={{ fontWeight: 'bold', fontSize: 16 }}>{e.data.campaignTitle}</text>
                                    <br />
                                    <div style={{ marginTop: '2%', height: '90px' }}>
                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13 }}>
                                            {
                                                e.data.description.length > 200 ?
                                                    e.data.description.substring(0, 200) + '...'
                                                    :
                                                    e.data.description
                                            }
                                        </p>
                                    </div>
                                    <br />
                                    {e.data.typeOfCampaign === 1 ?
                                        <div>
                                            <text style={{ fontWeight: 'bold', fontSize: 13 }}>Allowance : </text><br />
                                            <text style={{ fontSize: 13 }}>RM {e.data.minPayout} - RM {e.data.maxPayout}</text>
                                        </div>
                                        :
                                        e.data.typeOfCampaign === 2 ?
                                            <div>
                                                <text style={{ fontWeight: 'bold', fontSize: 13 }}>Skill Offer : </text><br />
                                                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                                    {
                                                        e.data.skillCanLearn.length > 0 ?
                                                            e.data.skillCanLearn.map((e, idx) => (
                                                                idx < 4 ?
                                                                    <div style={{
                                                                        backgroundColor: '#CACFD2', padding: '1%', width: `${snapshotSpec.val().skills[e].length * 8 + 20}px`,
                                                                        margin: '1% 1% 0 1%', textAlign: 'center', borderRadius: 20
                                                                    }}>
                                                                        <text style={{ fontSize: 13 }}>{snapshotSpec.val().skills[e]}</text>
                                                                    </div>
                                                                    : false
                                                            ))
                                                            : false
                                                    }
                                                </div>
                                            </div>
                                            :
                                            e.data.typeOfCampaign === 3 ?
                                                <div>
                                                    <table>
                                                        <tr>
                                                            <td><Calender /></td>
                                                            <td>
                                                                <text style={{ fontSize: 13, paddingLeft: '1%' }}>{e.data.eventDate}</text>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><Time /></td>
                                                            <td>
                                                                <TimePicker
                                                                    style={{ fontSize: 13, paddingLeft: '1%' }}
                                                                    disabled={true}
                                                                    format="ampm"
                                                                    value={new Date(e.data.eventTime)}
                                                                />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><Map /></td>
                                                            <td>
                                                                {
                                                                    e.data.eventAdd2 ?
                                                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                            {e.data.eventAdd1}, {e.data.eventAdd2}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                        </p>
                                                                        :
                                                                        <p style={{ wordWrap: 'break-word', margin: 0, fontSize: 13, paddingLeft: '1%' }}>
                                                                            {e.data.eventAdd1}, {e.data.eventCity}, {e.data.eventZip}, {e.data.eventState}
                                                                        </p>
                                                                }
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                : false
                                    }
                                </div>
                            </div>
                        </div>
                    )
                }, this)
                this.setState({ campaignList: arrayList, myCampaignList: myCampaignList, specialization: specialization });
            })
        })
    }

    CampInfo = (abc) => () => {
        this.setState({ redirect: true, campaignKey: abc })
    }
    handleChange = (event, index, value) =>{
        this.setState({dropDownValue:value})
       this.genCampList(value)
        this.forceUpdate()
    } 

    render() {

        if (this.state.redirect) {
            return <Redirect push to={`/CampaignInfo/${this.state.campaignKey}`} />

        }

        return (
            <div>
                <div style={{ width: '100%', margin: '2%', padding: '2%' }}>
                    <div style={{ display: 'flex', flexWrap: 'wrap', width: '100%', paddingLeft: (window.innerWidth < 1100 ? '0%' : '0%') }}>
                       {/* <div style={{ margin: '1%' }}>
                             <RaisedButton backgroundColor='#F5B041' label="All" onClick={this.genCampList('all')} />
                        </div>
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Digital Submission" onClick={this.genCampList('survey')} />
                        </div> 
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Event" onClick={this.genCampList('event')} />
                        </div>
                        <div style={{ margin: '1%' }}>
                            <RaisedButton backgroundColor='#F5B041' label="Career &amp; Opportunities" onClick={this.genCampList('job')} />
                        </div> */}
                            <SelectField floatingLabelText="Categories" value={this.state.dropDownValue} onChange={this.handleChange}>
                             <MenuItem value={'all'} primaryText="All" />
                             <MenuItem value={'survey'} primaryText="Online Events" />
                             <MenuItem value={'event'} primaryText="Event" />
                             <MenuItem value={'job'} primaryText="Career &amp; Opportunities" />
                        </SelectField>
                    </div>
                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                        {this.state.campaignList}
                    </div>
                </div>
            </div>
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CareerOpportunities)