// - Import react components
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { push } from 'react-router-redux'
import { NavLink, withRouter } from 'react-router-dom'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import FlatButton from 'material-ui/FlatButton'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import { firebaseRef } from 'app/firebase/'
import Checkbox from 'material-ui/Checkbox';
import ExpandTransition from 'material-ui/internal/ExpandTransition';
import {
  Step,
  Stepper,
  StepButton,
  StepLabel
} from 'material-ui/Stepper';



// - Import actions
import *  as authorizeActions from 'authorizeActions'
import * as globalActions from 'globalActions'




// - Create Signup componet class
export class Signup extends Component {

  /**
   * Component constructor
   * @param  {object} props is an object properties of component
   */
  constructor(props) {
    super(props);

    this.state = {
      company: '',
      companyType: '',
      companySite: '',
      companySSM: '',
      companyFanPage: '',
      companyDesc: '',
      companyMobile: '',
      companyEmail: '',
      companyIndustry: '',
      otherCompanyIndustry: '',
      emailSubFrequency: '0',

      fullNameInput: '',
      fullNameInputError: '',
      emailInput: '',
      emailInputError: '',
      passwordInput: '',
      passwordInputError: '',
      companyRegNameError: '',
      companyDisNameError: '',
      notificationEmailError: '',
      confirmInput: '',
      confirmInputError: '',
      mobileNumber: '',
      role: 'company',
      websiteLink:'',

      address: '',
      area: '',
      city: '',
      postcode: '',
      state: '',
      country: 'Malaysia',
      loading: false,
      finished: false,
      stepIndex: 0,
      checked: false,

    }
    // Binding function to `this`
    this.handleForm = this.handleForm.bind(this)

  }

  /**
   * Handle data on input change
   * @param  {event} evt is an event of inputs of element on change
   */
  handleInputChange = (evt) => {
    const target = evt.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({
      [name]: value
    })

    if (name === 'companyDisName') {
      if (value) {
        this.setState({ companyDisNameError: '' })
      }
    }
    else if (name === 'companyRegName') {
      if (value) {
        this.setState({ companyRegNameError: '' })
      }
    }
    else if (name === 'notificationEmail') {
      if (value) {
        this.setState({ notificationEmailError: '' })
      }
    }

    switch (name) {
      case 'fullNameInput':
        this.setState({
          fullNameInputError: '',
        })
        break
      case 'emailInput':
        this.setState({
          emailInputError: ''
        })
        break
      case 'passwordInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
      case 'confirmInput':
        this.setState({
          confirmInputError: '',
          passwordInputError: ''
        })
        break
      case 'checkInput':
        this.setState({
          checkInputError: ''
        })
        break;
      default:
    }
  }

  dummyAsync = (cb) => {
    this.setState({loading: true}, () => {
      this.asyncTimer = setTimeout(cb, 500);
    });
  };

  handleNext = () => {
    const {stepIndex} = this.state;
    if (!this.state.loading) {
      this.dummyAsync(() => this.setState({
        loading: false,
        stepIndex: stepIndex + 1,
        finished: stepIndex >= 2,
      }));
    }
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (!this.state.loading) {
      this.dummyAsync(() => this.setState({
        loading: false,
        stepIndex: stepIndex - 1,
      }));
    }
  };

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return (
          <div>
            <TextField
                onChange={this.handleInputChange}
                errorText={this.state.confirmInputError}
                name="company"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Company"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.companyRegNameError}
                name="companyRegName"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Company Registered Name"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.companyDisNameError}
                name="companyDisName"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Company Display Name"
                type="text"
              /><br />
               <TextField
                onChange={this.handleInputChange}
                errorText={this.state.confirmInputError}
                name="companySSM"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="SSM Registration Number"
                type="text"
              /><br />
                <SelectField style={{ width: '80%' }} value={this.state.companyType} onChange={this.handleChangeDropdownCompanyType} autoWidth={true} floatingLabelText="Company Type" >
                <MenuItem value="Recruitment Agency" primaryText="Recruitment Agency" />
                <MenuItem value="SME" primaryText="SME" />
                <MenuItem value="MNC" primaryText="MNC" />
                <MenuItem value="Non-Profit Organisation" primaryText="Non-Profit Organisation" />
                <MenuItem value="Government" primaryText="Government" />
              </SelectField><br />
              <SelectField style={{ width: '80%' }} value={this.state.companyIndustry} onChange={this.handleChangeDropdownCompanyIndustry} autoWidth={true} floatingLabelText="Industry" >
                <MenuItem value="Business Administrative" primaryText="Business Administrative" />
                <MenuItem value="Culinary Arts" primaryText="Culinary Arts" />
                <MenuItem value="Engineering" primaryText="Engineering" />
                <MenuItem value="Event Management" primaryText="Event Management" />
                <MenuItem value="Hospitality" primaryText="Hospitality" />
                <MenuItem value="Information Technology" primaryText="Information Technology" />
                <MenuItem value="Logistic" primaryText="Logistic" />
                <MenuItem value="Marketing" primaryText="Marketing" />
                <MenuItem value="Mass Communication" primaryText="Mass Communication" />
                <MenuItem value="Science" primaryText="Science" />
                <MenuItem value="Psychology" primaryText="Psychology" />
                <MenuItem value="Others" primaryText="Others" />
              </SelectField><br />
              <TextField style={this.state.companyIndustry != "Others" ? { display: 'none' } : {}}
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="otherCompanyIndustry"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Please Specify"
                type="text"
              /><br />
                            <TextField
                onChange={this.handleInputChange}
                name="address"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Address"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                name="area"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Area"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                name="postcode"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="postcode"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                name="city"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="City"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                name="state"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="State"
                type="text"
              /><br />
              <SelectField style={{ width: '80%' }} value={this.state.country} labelStyle={{ textAlign: 'left', color: "gray" }} onChange={this.handleChangeDropdownCountry} autoWidth={true} floatingLabelText="Country" >
              <MenuItem value="Malaysia" primaryText="Malaysia" />
                <MenuItem value="Singapore" primaryText="Singapore" />
                <MenuItem value="China" primaryText="China" />
                <MenuItem value="Japan" primaryText="Japan" />
                <MenuItem value="South Korea" primaryText="South Korea" />
                <MenuItem value="Cambodia" primaryText="Cambodia" />
                <MenuItem value="Thailand" primaryText="Thailand" />
                <MenuItem value="Indonesia" primaryText="Indonesia" />
                <MenuItem value="Vietnam" primaryText="Vietnam" />
                <MenuItem value="Australia" primaryText="Australia" />
                <MenuItem value="New Zealand" primaryText="New Zealand"/>
              </SelectField><br />
          </div>
        );
      case 1:
        return (
          <div>
             <TextField
                name="companyDesc"
                floatingLabelText="Write a short description of your company"
                floatingLabelStyle={{ fontSize: "15px" }}
                multiLine={true}
                rows={2}
                rowsMax={8}
                onChange={this.handleInputChange}
              /><br />
                 <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="websiteLink"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Website"
                type="text"
              /><br />
               <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="companyFanPage"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Company Social Media Page"
                type="text"
              /><br />
            

          </div>
        );
      case 2:
        return (
          <div>
          <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="fullNameInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Your Name"
                type="text"
              /><br />
                           <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="mobileNumber"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Your Mobile"
                type="text"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.emailInputError}
                name="emailInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Email"
                type="email"
              /><br />
               <TextField
                errorText={this.state.notificationEmailError}
                onChange={this.handleInputChange}
                name="notificationEmail"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Notification Email"
                type="email"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.passwordInputError}
                name="passwordInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Password"
                type="password"
              /><br />
              <TextField
                onChange={this.handleInputChange}
                errorText={this.state.confirmInputError}
                name="confirmInput"
                floatingLabelStyle={{ fontSize: "15px" }}
                floatingLabelText="Confirm Password"
                type="password"
              /><br />
              {/*Contact Person Details */}
              <br />
              <Checkbox
          label={<div><a href='http://gomonsta.asia/terms-and-conditions/'>Terms and Conditions</a></div>}
          checked={this.state.checked}
          onCheck={this.updateCheck.bind(this)}
        />
      </div>
        );
      default:
        return 'Somethings Wrong!';
    }
  }

  updateCheck=() =>{
    this.setState((oldState) => {
      return {
        checked: !oldState.checked,
      };
    });
  }

  renderContent() {
    const {finished, stepIndex} = this.state;
    const contentStyle = {margin: '0 16px', overflow: 'hidden'};

    if (finished) {
      return (
        <div style={contentStyle}>
          <p>
            <a
              href="#"
              onClick={(event) => {
                event.preventDefault();
                this.setState({stepIndex: 0, finished: false});
              }}
            >
            </a>
          </p>
        </div>
      );
    }

    return (
      <div style={contentStyle}>
        <div>{this.getStepContent(stepIndex)}</div>
        <div style={{marginTop: 24, marginBottom: 12}}>
          <FlatButton
            label="Back"
            onClick={stepIndex === 0?this.props.loginPage: this.handlePrev}
            style={{marginRight: 12}}
          />
          <RaisedButton
            label={stepIndex === 2 ? 'Create' : 'Next'}
            primary={true}
            onClick={stepIndex === 2 ? this.handleForm:this.handleNext}
          />
        </div>
      </div>
    );
  }

  handleChangeDropdownCompanyType = (event, index, companyType) => this.setState({ companyType });
  handleChangeDropdownCompanyIndustry = (event, index, companyIndustry) => this.setState({ companyIndustry });
  handleChangeDropdownemailSubFrequency = (event, index, emailSubFrequency) => this.setState({ emailSubFrequency });
  handleChangeDropdownCountry = (event, index, country) => this.setState({ country });
  /**
   * Handle register form
   */
  handleForm = () => {
    if(!this.state.checked){
      alert("Please agree to the Terms and Conditions")
    }

    var error = false
    if (this.state.fullNameInput === '') {
      this.setState({
        fullNameInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }
    if (this.state.emailInput === '') {
      this.setState({
        emailInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})


    }
    if (this.state.passwordInput === '') {
      this.setState({
        passwordInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})


    }
    if (this.state.confirmInput === '') {
      this.setState({
        confirmInputError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }
    if (!this.state.companyRegName) {
      this.setState({
        companyRegNameError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }

    if (!this.state.companyDisName) {
      this.setState({
        companyDisNameError: 'This field is required'
      })
      error = true
      this.setState({stepIndex:0})

    }

    if (!this.state.notificationEmail) {
      this.setState({
        notificationEmailError: 'This field is required'
      })
      this.setState({stepIndex:0})
    }

    else if (this.state.confirmInput !== this.state.passwordInput) {
      this.setState({
        passwordInputError: 'This field sould be equal to confirm password',
        confirmInputError: 'This field sould be equal to password'
      })
      error = true
      this.setState({stepIndex:0})


    }
    if (!error) {
      this.props.register({
        company: this.state.company,
        companyType: this.state.companyType,
        companySite: this.state.companySite,
        companySSM: this.state.companySSM,
        companyFanPage: this.state.companyFanPage,
        websiteLink:this.state.websiteLink,
        companyDesc: this.state.companyDesc,
        companyMobile: this.state.companyMobile,
        companyEmail: this.state.companyEmail,
        companyRegName: this.state.companyRegName,
        companyDisName: this.state.companyDisName,
        notificationEmail: this.state.notificationEmail,
        companyIndustry: this.state.companyIndustry == "Others" ? this.state.otherCompanyIndustry : this.state.companyIndustry,
        emailSubFrequency: this.state.emailSubFrequency,


        email: this.state.emailInput,
        password: this.state.passwordInput,
        fullName: this.state.fullNameInput,
        mobileNumber: this.state.mobileNumber,
        role: this.state.role,

        address: this.state.address,
        area: this.state.area,
        city: this.state.city,
        postcode: this.state.postcode,
        state: this.state.state,
        country: this.state.country,
        credit:30000,
        validated: false
      })
    }

  }

  /**
   * Reneder component DOM
   * @return {react element} return the DOM which rendered by component
   */
  render() {

    const {loading, stepIndex} = this.state;

    const contentStyle = {margin: '0 16px'};

    const paperStyle = {
      minHeight: 'auto',
      padding:'5%',
      width: '60%',
      margin: 20,
      textAlign: 'left',
      display: 'flex',
      margin: "auto"
    };


    return (
      <div>
        <h1 style={{
          textAlign: "center",
          padding: "20px",
          fontSize: "30px",
          fontWeight: 500,
          lineHeight: "32px",
          margin: "auto",
          color: "rgba(138, 148, 138, 0.2)"
        }}>Monsta</h1>

        <div className="animate-bottom">
          <Paper style={paperStyle} zDepth={1} rounded={false} >
          <div style={{width: '100%', margin: 'auto'}}>
          <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Company Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>Company Description</StepLabel>
          </Step>
          <Step>
            <StepLabel>Contact Person's Detail</StepLabel>
          </Step>
        </Stepper>
        <ExpandTransition loading={loading} open={true}>
          {this.renderContent()}
        </ExpandTransition>
      </div>
          </Paper>
        </div>
      </div>

    )
  }
}


/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    showError: (message) => {
      dispatch(action(types.SHOW_ERROR_MESSAGE_GLOBAL)(error.message))
    },
    register: (data) => {
      dispatch(authorizeActions.dbSignup(data))
    },
    loginPage: () => {
      dispatch(push("/login"))
    }
  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
  return {

  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Signup))
