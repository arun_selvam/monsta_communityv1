import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import 'react-chat-elements/dist/main.css'
import {Input} from 'react-chat-elements'
import {Button} from 'react-chat-elements'
import trim from 'trim'

//import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Redirect} from 'react-router-dom'
import {
		Card,
		CardActions,
		CardHeader,
		CardMedia,
		CardTitle,
		CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import {firebaseAuth, firebaseRef, firebaseApp} from 'app/firebase/'
import moment from 'moment'
import {
		ThemeProvider,
		Avatar,
		Subtitle,
		ChatListItem,
		ChatList,
		Row,
		Column,
		Title,
		MessageMedia,
		MessageTitle,
		MessageText,
		MessageButtons,
		MessageButton
} from '@livechat/ui-kit'
import {MessageList} from 'react-chat-elements'

export default class InstantMessengerInput extends Component {

		// Constructor
		constructor(props) {
				super(props)
				this.onChange = this
						.onChange
						.bind(this)
				this.onKeyup = this
						.onKeyup
						.bind(this)
				this.state = {
						message: ''
				}

		}

		componentWillMount() {}

		onChange(e) {
				this.setState({message: e.target.value})
		}
		onKeyup(e) {
				if (e.keyCode === 13 && trim(e.target.value) !== '') {
						e.preventDefault()
						let dbCon = firebaseApp
								.database()
								.ref(`/conversations/${this.props.chatRoomID}/messages`)
						dbCon.push({
								message: trim(e.target.value),
								date:moment().unix(),
								sender:firebaseApp.auth().currentUser.uid
						})
						this.setState({message: ''})
				}
		}

		render() {
				return (
						<div style={{
								height: '10%',
								backgroundColor: 'grey'
						}}>
								<form>
										<textarea
												placeholder="Type a message"
												cols="100"
												style={{}}
												onChange={this.onChange}
												onKeyUp={this.onKeyup}
												value={this.state.message}></textarea>
								</form>

						</div>
				)
		}
}