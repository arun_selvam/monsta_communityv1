// - Import react components
import React, { Component } from 'react'
import _ from 'lodash'
import { Route, Switch, withRouter, Redirect, NavLink } from 'react-router-dom'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { push } from 'react-router-redux'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'
import Divider from 'material-ui/Divider'
import SvgArrowLeft from 'material-ui/svg-icons/hardware/keyboard-arrow-left'
import SvgHome from 'material-ui/svg-icons/action/home'
import SvgFeedback from 'material-ui/svg-icons/action/feedback'
import SvgSettings from 'material-ui/svg-icons/action/settings'
import SvgAccountCircle from 'material-ui/svg-icons/action/account-circle'
import SvgPeople from 'material-ui/svg-icons/social/people'
import EventListener, { withOptions } from 'react-event-listener'
import RaisedButton from 'material-ui/RaisedButton'
import LinearProgress from 'material-ui/LinearProgress';
import { firebaseRef } from 'app/firebase/'
import HomeIcon from 'react-icons/lib/ti/home-outline'
import PeopleIcon from 'react-icons/lib/md/person-outline'
import ProfileIcon from 'react-icons/lib/go/file-text'
import CampIcon from 'react-icons/lib/io/fireball'
import PCreditIcon from 'react-icons/lib/md/bookmark-outline'
import PHisIcon from 'react-icons/lib/md/history'
import MssgIcon from 'react-icons/lib/ti/messages'
import SettingIcon from 'react-icons/lib/md/settings'
import FeedIcon from 'react-icons/lib/io/android-send'
// - Import app components
import Sidebar from 'Sidebar'
import Blog from 'Blog'
import HomeHeader from 'HomeHeader'
import SidebarContent from 'SidebarContent'
import SidebarMain from 'SidebarMain'
import Profile from 'Profile'
import PostPage from 'PostPage'
import People from 'People'
import ImgCover from 'ImgCover'
import EditProfile from 'EditProfile'
import UserAvatar from 'UserAvatar'
import MonstaChallenge from 'MonstaChallenge'
import MyProfile from 'MyProfile'
import CareerOpportunities from 'CareerOpportunities'
import Scoreboard from 'Scoreboard'
import JobDescription from 'JobDescription'
import Campaigns from 'Campaigns'
import CampaignInfo from 'CampaignInfo'
import MonstaEvents from 'MonstaEvents'
import CompanyInfoView from 'CompanyInfoView'
import Messenger from 'Messenger'
import Following from 'Following'
import Followers from 'Followers'
import StuPplSearch from 'StuPplSearch'
import InstantMessenger from 'InstantMessenger'
import Footer from 'Footer'



// - Import API
import CircleAPI from 'CircleAPI'

// - Import actions
import * as globalActions from 'globalActions'
import * as circleActions from 'circleActions'
import * as userActions from 'userActions'



// - Create Home component class
export class Home extends Component {
  static propTypes = {

    /**
     * User avatar address
     */
    avatar: PropTypes.string,
    /**
     * User avatar address
     */
    banner: PropTypes.string,
    /**
     * User full name
     */
    fullName: PropTypes.string.isRequired,
    /**
     * The number of followers
     */
    followerCount: PropTypes.number,
    /**
     * User identifier
     */
    userId: PropTypes.string,
    /**
     * If the user profile identifier of param is equal to the user authed identifier 
     */
    isAuthedUser: PropTypes.bool,

    university: PropTypes.string

  }

  // Constructor
  constructor(props) {
    super(props)

    // Default state
    this.state = {
      sidebarOpen: () => _,
      sidebarStatus: true,
      sidebaOverlay: false,
      following: 0,
      followers: 0,
      dashColor: '#002c54',
      searchColor: '',
      profileColor: '',
      campColor: '',
      feedbackColor: '',
      mssgColor: '',
      avatar: '',
      fullName: ''
    }

    // Binding function to `this`
    this.sidebar = this.sidebar.bind(this)
    this.sidebarStatus = this.sidebarStatus.bind(this)
    this.sidebarOverlay = this.sidebarOverlay.bind(this)
    this.handleCloseSidebar = this.handleCloseSidebar.bind(this)

  }

  componentWillMount() {

    this.props.loadUserInfo()
    if (Object.keys(this.props.peopleInfo)[0]) {
      var circleData = firebaseRef.child(`userCircles`).child(Object.keys(this.props.peopleInfo)[0])
      circleData.on('value', snapshot => {
        var followersCount = 0
        var followingCount = 0
        var data = snapshot.val()
        var dataKey = Object.keys(data)
        dataKey.forEach(e => {
          if (e === '-Followers') {
            followersCount += Object.keys(data[e].users).length
          } else {
            followingCount += Object.keys(data[e].users).length
          }
        })
        this.setState({ followers: followersCount, following: followingCount })
      })
    }

    var userInfo = firebaseRef.child(`users`).child(Object.keys(this.props.peopleInfo)[0]).child('info')
    userInfo.on('value', snapshot => {
      this.setState({ avatar: snapshot.val().avatar, fullName: snapshot.val().fullName })
    })

    var windowPath = window.location.pathname.split('/')[1]

    if (windowPath === 'SearchCompany') {
      this.handleSearchClick()
    }
    else if (windowPath === 'MyProfile') {
      this.handleProfileClick()
    }
    else if (windowPath === 'mycampaigns' || windowPath === 'CampaignInfo') {
      this.handleCampClick()
    }
    else if (windowPath === 'feedback') {
      this.handleFeedbackClick()
    }
    else {
      this.handleDashClick()
    }
  }

  handleDashClick = () => this.setState({
    dashColor: '#002c54', searchColor: '', profileColor: '', campColor: '',
    feedbackColor: '', mssgColor: ''
  })
  handleSearchClick = () => this.setState({
    dashColor: '', searchColor: '#002c54', profileColor: '', campColor: '',
    feedbackColor: '', mssgColor: ''
  })
  handleProfileClick = () => this.setState({
    dashColor: '', searchColor: '', profileColor: '#002c54', campColor: '',
    feedbackColor: '', mssgColor: ''
  })
  handleCampClick = () => this.setState({
    dashColor: '', searchColor: '', profileColor: '', campColor: '#002c54',
    feedbackColor: '', mssgColor: ''
  })
  handleFeedbackClick = () => {
    window.location='http://gomonsta.asia/contact/'
}
  handleMssgClick = () => this.setState({
    dashColor: '', searchColor: '', profileColor: '', campColor: '',
    feedbackColor: '', mssgColor: '#002c54'
  })

  /**
   * handle close sidebar
   */
  handleCloseSidebar = () => {
    this.state.sidebarOpen(false, 'overlay')
  }

  /**
   * Change sidebar overlay status
   * @param  {boolean} status if is true, the sidebar is on overlay status
   */
  sidebarOverlay = (status) => {
    this.setState({
      sidebarOverlay: status
    })
  }


  /**
   * Pass function to change sidebar status
   * @param  {boolean} open  is a function callback to change sidebar status out of sidebar component
   */
  sidebar = (open) => {

    this.setState({
      sidebarOpen: open
    })
  }


  /**
   * Change sidebar status if is open or not
   * @param  {boolean} status is true, if the sidebar is open
   */
  sidebarStatus = (status) => {
    this.setState({
      sidebarStatus: status
    })
  }

  /**
   * Render DOM component
   * 
   * @returns DOM
   * 
   * @memberof Home
   */
  render() {

    const styles = {
      avatar: {
        border: '2px solid rgb(255, 255, 255)'
      },
      iconButton: {
        fill: 'rgb(255, 255, 255)',
        height: '24px',
        width: '24px',

      },
      iconButtonSmall: {
        fill: 'rgb(0, 0, 0)',
        height: '24px',
        width: '24px',
      },

      editButton: {

        marginLeft: '20px'

      },
      editButtonSmall: {

        marginLeft: '20px',
        color: 'white',
        fill: 'blue'

      },
      aboutButton: {
        color: 'white'
      },
      aboutButtonSmall: {
        color: 'black'
      }
    }

    const level = Math.floor(this.props.monstaXP / 100);
    const expBar = this.props.monstaXP % 100;
    const { isAuthedUser } = this.props
    return (
      <div id="home">
        <HomeHeader sidebar={this.state.sidebarOpen} sidebarStatus={this.state.sidebarStatus} />
        <Sidebar overlay={this.sidebarOverlay} open={this.sidebar} status={this.sidebarStatus}>
          <SidebarContent>
            <div>
              <div style={{ padding: '5%' }}>
                {/* User avatar*/}

                <div style={{ display: 'flex', justifyContent: 'center' }}>
                  <UserAvatar fileName={this.state.avatar} size={100} style={styles.avatar} />
                </div>
                <br />

                <div style={{ padding: ' 5% 0% 15% 0%', display: 'table', marginRight: 'auto', marginLeft: 'auto', textAlign: 'center' }}>
                  <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>WELCOME</text><br />
                  <text style={{ color: "white", fontSize: 20 }}>{this.state.fullName}</text><br />
                  <text style={{ color: "white", fontSize: 13, fontWeight: 'bold' }}>{this.props.university}</text><br />
                </div>
              </div>
           {/*    <div style={{ display: 'flex', textAlign: 'center', padding: '2%', marginLeft: '1%' }}>
                <div style={{ width: '50%', borderRight: '1px solid white ' }}>
                  <NavLink to='/Followers'>
                    <text style={{ color: "white", fontWeight: 'bold' }}>Followers</text><br />
                    <text style={{ color: "white", fontSize: 13 }}>{this.state.followers}</text>
                  </NavLink>
                </div>
                <div style={{ width: '50%' }}>
                  <NavLink to='/Following'>
                    <text style={{ color: "white", fontWeight: 'bold' }}>Following</text><br />
                    <text style={{ color: "white", fontSize: 13 }}>{this.state.following}</text>
                  </NavLink>
                </div>
              </div> */}
            </div>
            <br />

            <Menu>
              {this.state.sidebarOverlay
                ? <div><MenuItem onClick={this.handleCloseSidebar} primaryText={<span style={{ color: "rgb(117, 117, 117)" }} className="sidebar__title">Monsta</span>} rightIcon={<SvgArrowLeft viewBox="0 3 24 24" style={{ color: "#fff", marginLeft: "15px", width: "32px", height: "32px", cursor: "pointer" }} />} /><Divider /></div>
                : ""
              }
              <NavLink to='/people'>
                <MenuItem style={{ color: "white", fontSize: 14, backgroundColor: this.state.dashColor }}
                  onClick={this.handleDashClick} primaryText="Dashboard" leftIcon={<HomeIcon style={{ color: 'white' }} />} />
              </NavLink>
              <NavLink to={`/MyProfile/${Object.keys(this.props.peopleInfo)[0]}`}>
                <MenuItem primaryText="My Profile" onClick={this.handleProfileClick}
                  style={{ color: "white", fontSize: 14, backgroundColor: this.state.profileColor }}
                  leftIcon={<ProfileIcon style={{ color: 'white' }} />} />
              </NavLink>
              <NavLink to='/SearchCompany'>
                <MenuItem primaryText="Discover Companies" onClick={this.handleSearchClick}
                  style={{ color: "white", fontSize: 14, backgroundColor: this.state.searchColor }}
                  leftIcon={<PeopleIcon style={{ color: 'white' }} />} />
              </NavLink>
              <NavLink to='/mycampaigns'>
                <MenuItem primaryText="Campaigns / Events" onClick={this.handleCampClick}
                  style={{ color: "white", fontSize: 14, backgroundColor: this.state.campColor }}
                  leftIcon={<CampIcon style={{ color: 'white' }} />} />
              </NavLink>
              {/*<NavLink to='/MonstaChallenge'><MenuItem primaryText="Monsta Challenges" style={{ color: "white", fontSize: 14 }} /></NavLink>
              <NavLink to='/MonstaEvents'><MenuItem primaryText="Monsta Events" style={{ color: "white", fontSize: 14 }} /></NavLink>*/}
       {/*        <NavLink to='/InstantMessenger'>
                <MenuItem primaryText="Messenger" onClick={this.handleMssgClick}
                  style={{ color: "white", fontSize: 14, backgroundColor: this.state.mssgColor }}
                  leftIcon={<MssgIcon style={{ color: 'white' }} />} />
              </NavLink>  */}
              <NavLink to='/settings'>
                <MenuItem primaryText="Settings" style={{ color: "white", fontSize: 14 }}
                  leftIcon={<SettingIcon style={{ color: 'white' }} />} />
              </NavLink>
              <NavLink to='#'>
                <MenuItem primaryText="Send feedback" onClick={this.handleFeedbackClick}
                  style={{ color: "white", fontSize: 14, backgroundColor: this.state.feedbackColor }}
                  leftIcon={<FeedIcon style={{ color: 'white' }} />} />
              </NavLink>

            </Menu>
          </SidebarContent>

          <SidebarMain>
            <Switch>
              <Route path="/Following" component={Following} />
              <Route path="/Followers" component={Followers} />
              <Route path="/MonstaChallenge" component={MonstaChallenge} />
              <Route path="/mycampaigns" component={CareerOpportunities} />
              <Route path="/Scoreboard" component={Scoreboard} />
              <Route path="/JobDescription" component={JobDescription} />
              <Route path="/MonstaEvents" component={MonstaEvents} />
              <Route path="/InstantMessenger" component={InstantMessenger} />
              <Route path="/Campaigns" component={Campaigns} />
              <Route path="/SearchCompany" render={() => {
                return (
                  this.props.authed
                    ? <StuPplSearch />
                    : <Redirect to="/login" />
                )
              }} />
              <Route path="/CampaignInfo/:companyKey/:userKey" render={({ match }) => {
                return (
                  this.props.authed
                    ? <CampaignInfo companyKey={match.params.companyKey} userKey={match.params.userKey} />
                    : <Redirect to="/login" />
                )
              }} />

              <Route path="/people/:tab?" render={() => {
                return (
                  this.props.authed
                    ? <People data={this.props.monstaXP} />
                    : <Redirect to="/login" />
                )
              }} /*component={{data:"hashd"}}*/ />
              <Route path="/tag/:tag" render={({ match }) => {

                return (
                  this.props.authed
                    ? <div className="blog"><Blog displayWriting={false} homeTitle={`#${match.params.tag}`} posts={this.props.mergedPosts} /></div>
                    : <Redirect to="/login" />
                )
              }} />
              <Route path="/companyinfo/:selectedKey" render={({ match }) => {
                return (
                  this.props.authed
                    ? <CompanyInfoView selectedKey={match.params.selectedKey} />
                    : <Redirect to="/login" />
                )
              }} />
              <Route path="/:userId/posts/:postId/:tag?" component={PostPage} />
              <Route path="/MyProfile/:userId"
                component={({ match }) => <MyProfile userId={match.params.userId} />} />
              <Route path="/" render={() => {

                return (
                  this.props.authed
                    ?
                    <div>
                      <Redirect to="/people" />
                      {/* <div style={{marginBottom:'5px', display:'flex', flexWrap:'wrap'}}>                   
                       <div style={{textAlign:'center', width:'64%' , height:'auto', backgroundColor:'white', flexGrow:'1', marginRight:'3px', marginBottom:'3px'}}>
                        <UserAvatar fileName={this.props.avatar} size={60} style={{marginTop:'0.5%', border: '2px solid rgb(255, 255, 255)'}} />  
                        <h2 style={{textAlign:'center'}}>Melati Wang Wang Ling </h2>
                        <p style={{textAlign:'center'}}>Level: 10</p>
                        <p style={{textAlign:'center'}}>University: Monsta University</p>
                        <p style={{textAlign:'center'}}>Join Date:2015-10-5</p>
                        <Divider/>
                        <p style={{textAlign:'center'}}>Course: How to be a Monsta</p>
                        <p style={{textAlign:'center'}}>Years Of Study: 5</p>
                        <Divider/>
                        <p style={{textAlign:'center'}}>Quotes: Once a monsta, always a monsta</p>
                      </div>
                      <div style={{textAlign:'center', width:'34%' , height:'auto', backgroundColor:'white', flexGrow:'1', marginLeft:'3px', marginBottom:'3px'}}>
                        <h2 style={{textAlign:'center'}}>Place latest featured company / Event here </h2>
                        <p style={{textAlign:'center'}}></p>
                        <RaisedButton label="Read More" primary={true}/>

                        </div>
                      <div style={{textAlign:'center', width:'34%' , height:'auto', backgroundColor:'white', flexGrow:'1', marginRight:'3px', marginTop:'3px',marginBottom:'3px', paddingBottom:'10px' }}>
                        <h2 style={{textAlign:'center'}}>Endorsements</h2>
                        <p style={{textAlign:'center'}}>You have no endorsements records in Monsta</p>
                        <RaisedButton label="Earn More " primary={true}/>

                      </div>
                      <div style={{textAlign:'center', width:'64%' , height:'auto', backgroundColor:'white',flexGrow:'1', marginLeft:'3px', marginTop:'3px', marginBottom:'3px',paddingBottom:'10px' }}>
                        <h2 style={{textAlign:'center'}}>Experinces</h2>
                        <p style={{textAlign:'center'}}>You have no experinces records in Monsta</p>

                        <RaisedButton label="Gain More " primary={true}/>
                        </div>
                      <div style={{textAlign:'center', width:'100%' , height:'auto', backgroundColor:'white',flexGrow:'1', marginLeft:'3px', marginTop:'3px',paddingBottom:'10px' }}>
                        <h2 style={{textAlign:'center'}}>Skills</h2>
                        <p style={{textAlign:'center'}}>Programming</p>
                        <p style={{textAlign:'center'}}>Skype</p>
                        <p style={{textAlign:'center'}}>Whatsapp </p>
                        <p style={{textAlign:'center'}}>Microsoft Excel</p>
                        <p style={{textAlign:'center'}}>Microsoft Word</p>

                        <RaisedButton label="Add More " primary={true}/>
                      </div>
                       
                      </div> */}

                      {/*  <div style={styles.people}>
      <div style={{marginBottom:'5px'}}>
          <div style={{textAlign:'center', width:'64%' , height:'200px', backgroundColor:'white', display:'inline-block', marginRight:'3px'}}>
            <h2 style={{textAlign:'center'}}>Discover Opportunities</h2>
            <p style={{textAlign:'center'}}>Hundreds of industry opportunity waiting for you</p>
            <RaisedButton label="Discover" primary={true}/>
          </div>
          <div style={{textAlign:'center', width:'34%' , height:'200px', backgroundColor:'white', display:'inline-block', marginLeft:'3px'}}>
            <h2 style={{textAlign:'center'}}>Gain Your XP</h2>
            <p style={{textAlign:'center'}}>Complete Monsta Challenge to gain your XP</p>
            <RaisedButton label="Accept Challenges" primary={true}/>

            </div>
      </div>
      <div>
          <div style={{textAlign:'center', width:'34%' , height:'200px', backgroundColor:'white', display:'inline-block', marginRight:'3px', }}>
            <h2 style={{textAlign:'center'}}>My Profile</h2>
            <p style={{textAlign:'center'}}>X% Complete</p>
            <RaisedButton label="Complete Profile" primary={true}/>

          </div>
          <div style={{textAlign:'center', width:'64%' , height:'200px', backgroundColor:'white', display:'inline-block', marginLeft:'3px', }}>
            <h2 style={{textAlign:'center'}}>Stories</h2>
            <p style={{textAlign:'center'}}>Discover stories of When We Are Young Individual</p>
            <RaisedButton label="Search" primary={true}/>

            
          </div>
      </div>
      <div>
          {circlesLoaded ? <FindPeople /> : ''}
        </div>
      </div> */}
                    </div>

                    : <Redirect to="/login" />
                )
              }} />
            </Switch>
          </SidebarMain>
        </Sidebar>
        <Footer style={{position:'absolute',bottom:0}} sidebarStatus={this.state.sidebarStatus}/>
     </div >

    )
  }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  const { userId } = ownProps.match.params

  return {
    userId: userId,
    loadUserInfo: () => dispatch(userActions.dbGetUserInfoByUserId(userId, 'header')),
    openEditor: () => dispatch(userActions.openEditProfile())

  }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
  const { uid } = state.authorize
  let mergedPosts = {}
  const circles = state.circle ? (state.circle.userCircles[uid] || {}) : {}
  const followingUsers = CircleAPI.getFollowingUsers(circles)
  const posts = state.post.userPosts ? state.post.userPosts[state.authorize.uid] : {}
  Object.keys(followingUsers).forEach((userId) => {
    let newPosts = state.post.userPosts ? state.post.userPosts[userId] : {}
    _.merge(mergedPosts, newPosts)
  })
  _.merge(mergedPosts, posts)
  return {
    peopleInfo: state.user.info,
    avatar: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].avatar : '',
    fullName: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].fullName : '',
    university: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].university : '',
    monstaXP: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].monstaXP : '',
    course: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].course : '',
    year: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].year : '',
    joinDate: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].joinDate : '',
    tagLine: state.user.info && state.user.info[state.authorize.uid] ? state.user.info[state.authorize.uid].tagLine : '',

    //fullName: state.user.info && state.user.info[userId] ? state.user.info[userId].fullName || '' : '',    
    authed: state.authorize.authed,
    mainStyle: state.global.sidebarMainStyle,
    mergedPosts,

  }
}

// - Connect component to redux store
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home))
