import React, {Component} from 'react';
import {connect} from 'react-redux'
import * as userActions from 'userActions'
import * as imageGalleryActions from 'imageGalleryActions'
import Avatar from 'material-ui/Avatar'
import Pin from 'react-icons/lib/fa/map-marker'
import DollarSign from 'react-icons/lib/fa/dollar'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import {Redirect} from 'react-router-dom'
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import BackButton from 'material-ui/svg-icons/navigation/chevron-left';
import {
    Card,
    CardActions,
    CardHeader,
    CardMedia,
    CardTitle,
    CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import renderHTML from 'react-render-html';
import ReactHtmlParser, { processNodes, convertNodeToElement, htmlparser2 } from 'react-html-parser';
import {firebaseAuth, firebaseRef, firebaseApp} from 'app/firebase/'
import sha1 from 'js-sha1'
import moment from 'moment'
import {
    blue300,
    indigo900,
    orange200,
    deepOrange300,
    pink400,
    purple500,
  } from 'material-ui/styles/colors';
  import ActionFlightTakeoff from 'material-ui/svg-icons/action/flight-takeoff';

  
  const style = {margin: 5};

/**
 * Horizontal steppers are ideal when the contents of one step depend on an earlier step.
 * Avoid using long step names in horizontal steppers.
 *
 * Linear steppers require users to complete one step in order to move on to the next.
 */
const styles = {
    underlineStyle: {
        borderColor: '#70E0F2'
    }
};

export default class PurchaseCredits extends Component {

    // Constructor
    constructor(props) {
        super(props)
        // Default state
        this.state = {
            searchResult: [],
            allStudent: [],
            displayStudent: [],
            fullNameKey: '',
            courseKey: '',
            uniKey: '',
            stateMalaysia: 0,
            cityMalaysia: [
                'Selangor',
                'Kuala Lumpur',
                'Sarawak',
                'Johor',
                'Pulau Pinang',
                'Sabah',
                'Perak',
                'Pahang',
                'Negeri Sembilan',
                'Kedah',
                'Melaka',
                'Terengganu',
                'Kelantan',
                'Perlis'
            ],
            listMenuItem: [],
            uid: '',
            isDirect: false,
            amount: 1,
            amountSelected: false,
            userInfo: [],
            credit: 0,
            array: []
        }
    }

    componentWillMount() {
        var getUserInfo
        var check = firebaseApp
            .auth()
            .currentUser
        firebaseApp
            .database()
            .ref()
            .child(`users/${check.uid}/info`)
            .once('value')
            .then((snapshot) => {
                console.log(snapshot.val())
                this.state.userInfo = snapshot.val()
            })

        var companyInfo = firebaseRef
            .child("users")
            .child(check.uid)

        companyInfo.on('value', snapshot => {
            var data = snapshot.val()
            this.setState({
                credit: snapshot
                    .val()
                    .credit
            })
        })

        var paymentmenPackageArray = [
            {
                packageName: "Starter",
                credit: 500,
                price: 500,
                freeCredit: 50,
                totalCredit: 550,
                creditValidity: 1
            }, {
                packageName: "Basic",
                credit: 1500,
                price: 1500,
                freeCredit: 200,
                totalCredit: 1700,
                creditValidity: 1
            }, {
                packageName: "Super",
                credit: 3000,
                price: 3000,
                freeCredit: 500,
                totalCredit: 3500,
                creditValidity: 1
            }, {
                packageName: "Epic",
                credit: 5000,
                price: 5000,
                freeCredit: 1000,
                totalCredit: 6000,
                creditValidity: 1
            }
        ]
        var array = []
        paymentmenPackageArray.forEach(data => {
            array.push(
                <Card style={{
                    margin: '3%', width: (window.innerWidth < 1000 ? '100%' : '15%'),
                    height: 'auto',padding:10, position: 'relative',textAlign:'center'
                }}>
                    <strong>{data.packageName}</strong>
                    <br/>
                    <Avatar
                        icon={<FontIcon className="muidocs-icon-communication-voicemail" />}
                        color={blue300}
                        backgroundColor={indigo900}
                        size={30}
                        style={style}
                    />
                    <br/>
                    <strong>{data.credit} Credit</strong>
                    <p>
                        <span style={{color:'gray'}}>Price</span>
                        <br/>
                       RM {data.price}
                    </p>
                    <br/>
                    <p><span style={{color:'gray'}}>Free Credit</span>
                    <br/>
                       RM {data.freeCredit}
                    </p>
                    <br/>
                    <p><span style={{color:'gray'}}>Total Credit</span>
                        <br/>
                       RM {data.totalCredit}
                    </p>
                    <br/>
                    <br/>
                    <CardActions>
                         <RaisedButton backgroundColor='orange' style={{marginRight:20,marginLeft:20}}  onClick={() => { this.submit(data.price) }}size="large"><span style={{color:'white'}}>Purchase</span></RaisedButton>
                    </CardActions>
                </Card>
            )
            this.setState({array: array})
        });

    }

    submit = (amount) => {
        this.setState({amount: amount})
        this.setState({amountSelected: true})
    }

   
    render() {
        var date = moment
        var merchantKey = "aCqY7i4lns"
        var merchantCode = "M13335"
        var RefNo = moment()
            .unix()
            .toString()
        var amount = this.state.amount.toString()+".00"
        var amountString = this.state.amount.toString()+"00MYR"
        var rawSign = merchantKey + merchantCode + RefNo + amountString
        const hex2bin = str => str
            .match(/.{1,2}/g)
            .reduce((str, hex) => str += String.fromCharCode(parseInt(hex, 16)), '');
        var signature = btoa(hex2bin(sha1(rawSign)));
        // var signature = 'nbcIL4eKDHgHce/XyJ3AwCt3rHY='        sha1( merchantKey +
        // merchantCode + RefNo + "1" + "MYR")
        var remark = firebaseApp
            .auth()
            .currentUser
            .uid + "," + this.state.amount + ',' + this.state.credit
        var formHtml = `<HTML><BODY><FORM method="post" name="ePayment" id="paymentForm" action="https://payment.ipay88.com.my/ePayment/entry.asp"><INPUT type="hidden" name="MerchantCode" value=${merchantCode}><INPUT type="hidden" name="PaymentId" value=""><INPUT type="hidden" name="RefNo" value=${RefNo}><INPUT type="hidden" name="Amount" value=${amount}><INPUT type="hidden" name="Currency" value="MYR"><INPUT type="hidden" name="ProdDesc" value="Monsta Credit"><INPUT type="hidden" name="UserName" value=${this.state.userInfo.name}><INPUT type="hidden" name="UserEmail" value="${this.state.userInfo.email}"><INPUT type="hidden" name="UserContact" value=${this.state.userInfo.mobileNumber}><INPUT type="hidden" name="Remark" value=${remark}><INPUT type="hidden" name="Lang" value="UTF-8"><INPUT type="hidden" name="Signature" value=${signature}><INPUT type="hidden" name="ResponseURL" value="http://139.162.40.8:8080/api/payment"><INPUT type="hidden" name="BackendURL"value="http://www.YourBackendURL.com/payment/backend_response.asp"><INPUT type="submit" value="Proceed with Payment" name="Submit"></FORM><script>document.getElementById("paymentForm").submit();</script></BODY></HTML><script>`
        return (
            <div>
                {this.state.amountSelected
                    ? 
                    <div>
                                        <div style={{display:'flex',alignContent:'center',alignItems:'center'}} onClick={() => this.setState({amountSelected: false})}>
                <BackButton style={{height:30,width:30}} />
                <p>Back</p>
                </div>
                    <div
                            style={{
                            display: 'flex',
                            justifyContent: 'center',
                            padding: 20
                        }}>
                            <div>
                                <Card>
                                    <CardHeader
                                        title="Summary"
                                        subtitle={"Purchase of " + this.state.amount + " Credits"}/>
                                    <CardTitle title="Checkout Details"/>
                                    <CardText>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>:{this.state.userInfo.company}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>:{this.state.userInfo.email}</td>
                                                </tr>
                                                <tr>
                                                    <td>Amount</td>
                                                    <td>:RM 1.00</td>
                                                </tr>
                                                <tr>
                                                    <td>Billing Address</td>
                                                    <td
                                                        style={{
                                                        wordWrap: 'break-word',
                                                        width: '15em'
                                                    }}>:{this.state.userInfo.address},{this.state.userInfo.area}, {this.state.userInfo.city},{this.state.userInfo.postcode}, {this.state.userInfo.country}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Refference No</td>
                                                    <td>:71632471264</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </CardText>
                                    <CardActions>
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                    {ReactHtmlParser(formHtml)}
                                                    </td>
                                                    <td>
                                                        <button onClick={() => this.setState({amountSelected: false})}>Cancel</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </CardActions>
                                </Card>
                            </div>
                            <br/>
                            <div></div>
                        </div>
                        </div>
                    :<div style={{ display: 'flex', flexWrap: 'wrap',alignItems:'center',justifyContent:'center' }}>
                        {  this.state.array}
                    </div>
                }
            </div>

        );
    }
}
    
