import React, { Component } from 'react'
import Avatar from 'material-ui/Avatar'
import RaisedButton from 'material-ui/RaisedButton'
import { firebaseRef } from 'app/firebase/'
import { connect } from 'react-redux'
import '../styles/app.scss'
import TextField from 'material-ui/TextField'
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import YouTube from 'react-youtube';
import TimePicker from 'material-ui/TimePicker';
import Profile from 'Profile'
import { Redirect } from 'react-router-dom'
import CurrencyInput from 'react-currency-input';
import Toggle from 'material-ui/Toggle';
import FlatButton from 'material-ui/FlatButton';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import Checkbox from 'material-ui/Checkbox';
import DatePicker from 'material-ui/DatePicker';
import UserAvatar from 'UserAvatar'
import Dialog from 'material-ui/Dialog';
import DialogTitle from 'DialogTitle'
import ImageGallery from 'ImageGallery'
import CameraIcon from 'react-icons/lib/fa/camera'
import CoverAvatar from 'CoverAvatar'
import moment from 'moment'

var campaignList

class CompEditCampaignInfo extends Component {

    constructor(props) {
        super(props)
        this.state = {
            monstaNetwork: [],
            redirect: false,
            avatarSize: window.innerWidth > 1000 ? window.innerWidth / 15 : (window.innerWidth > 700 ? window.innerWidth / 10 : window.innerWidth / 7),
            xpFontSize: window.innerWidth > 1000 ? '30' : '25',
            statusSize: window.innerWidth > 1000 ? '25' : '20',
            xpTitle: window.innerWidth > 1000 ? '25' : '15',
            width: window.innerWidth > 1000 ? '100%' : (window.innerWidth > 700 ? (window.innerWidth + 100) + 'px' : (window.innerWidth + 250) + 'px'),
            campaignList: [],
            specializationList: [],
            specialization: '',
            skillList: [],
            coursesList: [],
            skills: [],
            requiredCourses: [],
            requiredSkills: [],
            workLocation: [],
            questionList: [],
            opts: {
                height: window.innerHeight / 2,
                width: '100%',
                playerVars: {
                    autoplay: 1
                }
            },
            userList: [],
            participantList: [],
            isDirect: false,
            isSave: false,
            uid: '',
            campaignDesc: '',
            addReward: '',
            skillCanLearn: [],
            requirements: '',
            responsibilities: '',
            benefits: '',
            minAmount: 0,
            maxAmount: 0,
            privacy: false,
            salaryType: '',
            minEducation: '',
            requiredCourses: [],
            requiredSkills: [],
            aptitudeTest: false,
            questionList: [],
            isVideo: false,
            videoId: '',
            url: '',
            submitMethod: '',
            eventDate: '',
            eventTime: '',
            eventAdd1: '',
            eventAdd2: '',
            eventCity: '',
            eventState: '',
            eventZip: '',
            ticket: '',
            eventTicketPrice: 0,
            isName: false,
            isAge: false,
            isGender: false,
            isContact: false,
            isEmail: false,
            isCourse: false,
            isUni: false,
            isTShirt: false,
            questionView: '',
            showDialog: false,
            targetStudent: '',
            avatar: '',
            openAvatar: false,
            startDate: new Date(),
            endDate: new Date(),
            todayDate: new Date(),
            errorEndDate: false,
        }
        this.handleRequestSetAvatar = this.handleRequestSetAvatar.bind(this)
    }

    componentWillMount() {
        campaignList = firebaseRef.child("campaigns").child(Object.keys(this.props.peopleInfo)[0]).child(this.props.selectedKey)
        campaignList.on('value', snapshot => {
            console.log(snapshot.val())
            this.setState({
                campaignList: snapshot.val(), campaignDesc: snapshot.val().description, addReward: snapshot.val().additionalReward,
                specialization: snapshot.val().specialization, skillCanLearn: snapshot.val().skillCanLearn, requirements: snapshot.val().requirements, responsibilities: snapshot.val().responsibilities,
                benefits: snapshot.val().benefits, minAmount: snapshot.val().minPayout, maxAmount: snapshot.val().maxPayout, privacy: snapshot.val().isPrivacy, salaryType: snapshot.val().salaryType,
                minEducation: snapshot.val().minEducation, requiredCourses: snapshot.val().requiredCourses, requiredSkills: snapshot.val().requiredSkills, workLocation: snapshot.val().workLocation,
                aptitudeTest: snapshot.val().aptitudeTest, questionList: snapshot.val().questionList, isVideo: snapshot.val().isVideo, videoId: snapshot.val().videoId, url: snapshot.val().url,
                submitMethod: snapshot.val().submitMethod, eventDate: new Date(snapshot.val().eventDate), eventTime: new Date(snapshot.val().eventTime), eventAdd1: snapshot.val().eventAdd1, eventAdd2: snapshot.val().eventAdd2,
                eventCity: snapshot.val().eventCity, eventState: snapshot.val().eventState, eventZip: snapshot.val().eventZip, ticket: snapshot.val().isTicket, eventTicketPrice: snapshot.val().ticketPrice,
                targetStudent: snapshot.val().targetStudent ? snapshot.val().targetStudent : '', avatar: snapshot.val().campAvatar ? snapshot.val().campAvatar : '',
                startDate: snapshot.val().startDate ? new Date(snapshot.val().startDate) : new Date(), endDate: snapshot.val().endDate ? new Date(snapshot.val().endDate) : new Date()
                /*isName: snapshot.val().isName, isAge: snapshot.val().isAge, isGender: snapshot.val().isGender, isContact: snapshot.val().isContact, isEmail: snapshot.val().isEmail, isCourse: snapshot.val().isCourse,
                isUni: snapshot.val().isUni, isTShirt: snapshot.val().isTShirt*/
            })
        })
        var specialization = firebaseRef.child("staticData")
        specialization.on('value', snapshot => {
            var data = snapshot.val()
            var dataKey = Object.keys(data.field)
            this.setState({ specializationList: dataKey, skillList: data.skills, coursesList: data.courses })
        })

        var user = firebaseRef.child("users")
        user.on('value', snapshot => {
            this.setState({ userList: snapshot.val() })
        })
    }

    componentDidMount() {
        var questionList = []
        this.state.campaignList != '' ?
            this.state.campaignList.questionList ?
                this.state.campaignList.questionList != '' ?
                    this.state.campaignList.questionList.map((e) => (
                        questionList.push(e)
                    ))
                    :
                    ''
                :
                ''
            :
            ''
        this.setState({ questionList: questionList })
    }

    handleCampaignDesc = (event) => {
        this.setState({
            campaignDesc: event.target.value,
        });
    };

    handleAddReward = (event) => {
        this.setState({
            addReward: event.target.value,
        });
    };

    handleSpecialization = (event, index, value) => this.setState({ specialization: value })

    updateSkillCanLearn = (event, index, values) => this.setState({ skillCanLearn: values })

    handleRequirements = (event) => {
        this.setState({
            requirements: event.target.value,
        });
    };

    handleResponsibilities = (event) => {
        this.setState({
            responsibilities: event.target.value,
        });
    };

    handleBenefits = (event) => {
        this.setState({
            benefits: event.target.value,
        });
    };

    handleMinAmount = (event, maskedvalue, floatvalue) => {
        this.setState({ minAmount: maskedvalue });
    }

    handleMaxAmount = (event, maskedvalue, floatvalue) => {
        this.setState({ maxAmount: maskedvalue });
    }

    handlePrivacySetting = (event, isPrivacy) => {
        this.setState({ privacy: isPrivacy, });
    };

    handleMinEducation = (event, index, value) => this.setState({ minEducation: value });

    handleSalaryType = (event, index, value) => this.setState({ salaryType: value });

    updateRequiredSkills = (event, index, values) => this.setState({ requiredSkills: values });

    updateRequiredCourses = (event, index, values) => this.setState({ requiredCourses: values });

    addNewLocation() {
        this.state.workLocation ?
            this.setState({
                workLocation: this.state.workLocation.concat([{ add1: '', add2: '', city: '', state: '', zip: '', country: '' }])
            })
            :
            this.setState({
                workLocation: [{ add1: '', add2: '', city: '', state: '', zip: '', country: '' }]
            })
    }

    handleAdd1Change = (idx) => (evt) => {
        const newAdd1 = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, add1: evt.target.value };
        });

        this.setState({ workLocation: newAdd1 });
    }

    handleRemoveLocation = (idx) => () => {
        this.setState({
            workLocation: this.state.workLocation.filter((s, sidx) => idx !== sidx)
        });
    }

    handleAdd2Change = (idx) => (evt) => {
        const newAdd2 = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, add2: evt.target.value };
        });

        this.setState({ workLocation: newAdd2 });
    }

    handleZipChange = (idx) => (evt) => {
        const newZip = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, zip: evt.target.value };
        });

        this.setState({ workLocation: newZip });
    }

    handleStateChange = (idx) => (event, index, value) => {
        const newState = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, state: value };
        });

        this.setState({ workLocation: newState });
    }

    handleCityChange = (idx) => (evt) => {
        const newCity = this.state.workLocation.map((loc, sidx) => {
            if (idx !== sidx) return loc;
            return { ...loc, city: evt.target.value };
        });

        this.setState({ workLocation: newCity });
    }

    addNewQuestion() {
        this.setState({
            questionList: this.state.questionList.concat([{ question: '', selectedGroup: this.state.typeOfCampaign === 1 ? 'multiSelect' : '', answerList: [] }]),
        });
    }

    addNewAnswer = (idx) => (evt) => {
        var preData = this.state.questionList
        if (this.state.typeOfCampaign === 1) {
            preData[idx].answerList.push({ answer: '', isAnswer: false })
        } else {
            preData[idx].answerList.push({ answer: '' })
        }

        this.setState({ questionList: preData })
    }

    handleAnswerChange = (idx, ansIdx) => (evt) => {
        const newAnswer = this.state.questionList
        newAnswer[idx].answerList[ansIdx].answer = evt.target.value
        this.setState({ questionList: newAnswer });
    }

    handleIsAnswer = (idx, ansIdx) => () => {
        const isAnswer = this.state.questionList
        var isAnswerData = isAnswer[idx].answerList
        if (isAnswer[idx].selectedGroup === "multiSelect") {
            isAnswerData[ansIdx].isAnswer = !isAnswerData[ansIdx].isAnswer
        } else {
            isAnswerData.map((ans, sidx) => {
                if (ansIdx !== sidx) {
                    ans.isAnswer = false
                }
                else {
                    ans.isAnswer = true
                }
            });
        }
        this.setState({ questionList: isAnswer });
    }

    handleSelectionType = (idx) => (evt) => {
        const newQuestion = this.state.questionList.map((ques, sidx) => {
            if (idx !== sidx) return ques;
            return { ...ques, selectedGroup: evt.target.value, answerList: [] };
        });

        this.setState({ questionList: newQuestion });
    }

    handleQuestionChange = (idx) => (evt) => {
        const newQuestion = this.state.questionList.map((ques, sidx) => {
            if (idx !== sidx) return ques;
            return { ...ques, question: evt.target.value };
        });

        this.setState({ questionList: newQuestion });
    }

    handleRemoveQuestion = (idx) => () => {
        this.setState({
            questionList: this.state.questionList.filter((s, sidx) => idx !== sidx),
            currentTest: this.state.currentTest - 1
        });
    }

    updateAptitudeTest() {
        this.setState((oldState) => { return { aptitudeTest: !oldState.aptitudeTest, questionList: [] } })
    }

    handleRemoveAnswer = (idx, ansIdx) => () => {
        const removedAnswer = this.state.questionList
        var removeItem = removedAnswer[idx].answerList
        removedAnswer[idx].answerList = removeItem.filter((s, sidx) => ansIdx !== sidx)
        this.setState({ questionList: removedAnswer });
    }

    handleURLChange = (event) => {

        var url = event.target.value.split('?')
        var getVideoKey = url[1].split('=')
        this.setState({
            url: event.target.value,
            videoId: getVideoKey[1]
        });
    };

    handleVideo = (event, isVideo) => {
        this.setState({ isVideo: isVideo, videoId: '', url: '' });
    };

    handleSubmitMethod = (event, index, value) => this.setState({ submitMethod: value });

    handleChangeEventDate = (event, date) => {
        this.setState({ eventDate: date, });
    };

    handleChangeEventTime = (event, date) => {
        this.setState({ eventTime: date });
    };

    handleEventAdd1Change = (event) => {
        this.setState({
            eventAdd1: event.target.value,
        });
    };

    handleEventAdd2Change = (event) => {
        this.setState({
            eventAdd2: event.target.value,
        });
    };

    handleEventCityChange = (event) => {
        this.setState({
            eventCity: event.target.value,
        });
    };

    handleEventStateChange = (event, index, value) => this.setState({ eventState: value });

    handleEventZipChange = (event) => {
        this.setState({
            eventZip: event.target.value,
        });
    };

    handleTicket = (event, isTicket) => {
        this.setState({ ticket: isTicket, eventTicketPrice: 0 });
    };

    handleChangeTicketPrice = (event, maskedvalue, floatvalue) => {
        this.setState({ eventTicketPrice: maskedvalue });
    }

    handleIsName = (event, isName) => {
        this.setState({ isName: isName });
    };

    handleIsAge = (event, isAge) => {
        this.setState({ isAge: isAge });
    };

    handleIsGender = (event, isGender) => {
        this.setState({ isGender: isGender });
    };

    handleIsContact = (event, isContact) => {
        this.setState({ isContact: isContact });
    };

    handleIsEmail = (event, isEmail) => {
        this.setState({ isEmail: isEmail });
    };

    handleIsCourse = (event, isCourse) => {
        this.setState({ isCourse: isCourse });
    };

    handleIsUni = (event, isUni) => {
        this.setState({ isUni: isUni });
    };

    handleIsTShirt = (event, isTShirt) => {
        this.setState({ isTShirt: isTShirt });
    };

    handleSaveCampaign = () => () => {
        if (this.state.endDate < this.state.startDate) {
            this.setState({ errorEndDate: true })
        }
        else {
            if (this.state.campaignList.typeOfCampaign === 1) {
                campaignList.update({
                    campAvatar: this.state.avatar,
                    description: this.state.campaignDesc,
                    additionalReward: this.state.addReward,
                    targetStudent: this.state.targetStudent,
                    specialization: this.state.specialization,
                    skillCanLearn: this.state.skillCanLearn,
                    requirements: this.state.requirements,
                    responsibilities: this.state.responsibilities,
                    benefits: this.state.benefits,
                    minPayout: this.state.minAmount,
                    maxPayout: this.state.maxAmount,
                    isPrivacy: this.state.privacy,
                    salaryType: this.state.salaryType,
                    minEducation: this.state.minEducation,
                    requiredCourses: this.state.requiredCourses,
                    requiredSkills: this.state.requiredSkills,
                    workLocation: this.state.workLocation ? this.state.workLocation : [],
                    aptitudeTest: this.state.aptitudeTest,
                    questionList: this.state.questionList ? this.state.questionList : [],
                    startDate: this.state.startDate.toString(),
                    endDate: this.state.endDate.toString()
                })
            }
            else if (this.state.campaignList.typeOfCampaign === 2) {
                if (this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2) {
                    campaignList.update({
                        campAvatar: this.state.avatar,
                        description: this.state.campaignDesc,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        skillCanLearn: this.state.skillCanLearn,
                        questionList: this.state.questionList ? this.state.questionList : [],
                        isVideo: this.state.isVideo,
                        videoId: this.state.videoId,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString()
                    })
                } else {
                    campaignList.update({
                        campAvatar: this.state.avatar,
                        description: this.state.campaignDesc,
                        additionalReward: this.state.addReward,
                        targetStudent: this.state.targetStudent,
                        skillCanLearn: this.state.skillCanLearn,
                        submitMethod: this.state.submitMethod,
                        startDate: this.state.startDate.toString(),
                        endDate: this.state.endDate.toString()
                    })
                }
            }
            else if (this.state.campaignList.typeOfCampaign === 3) {
                var eventDateArray = this.state.eventDate.toDateString().split(' ')
                var eventDateString = eventDateArray[2] + ' ' + eventDateArray[1] + ' ' + eventDateArray[3]
                campaignList.update({
                    campAvatar: this.state.avatar,
                    description: this.state.campaignDesc,
                    additionalReward: this.state.addReward,
                    targetStudent: this.state.targetStudent,
                    skillCanLearn: this.state.skillCanLearn,
                    eventDate: eventDateString,
                    eventTime: this.state.eventTime.toString(),
                    eventAdd1: this.state.eventAdd1,
                    eventAdd2: this.state.eventAdd2,
                    eventCity: this.state.eventCity,
                    eventState: this.state.eventState,
                    eventZip: this.state.eventZip,
                    isTicket: this.state.ticket,
                    ticketPrice: this.state.eventTicketPrice,
                    startDate: this.state.startDate.toString(),
                    endDate: this.state.endDate.toString()
                    //isName: this.state.isName,
                    //isAge: this.state.isAge,
                    //isGender: this.state.isGender,
                    //isContact: this.state.isContact,
                    //isEmail: this.state.isEmail,
                    //isCourse: this.state.isCourse,
                    //isUni: this.state.isUni,
                    //isTShirt: this.state.isTShirt
                })
            }
            this.setState({ isSave: true })
        }
    }

    handleOpenAvatarGallery = () => {
        this.setState({
            openAvatar: true
        })
    }

    handleChangeStartDate = (event, date) => {
        this.setState({ startDate: date });
    };

    handleChangeEndDate = (event, date) => {
        this.setState({ endDate: date, errorEndDate: false });
    };

    handleCloseAvatarGallery = () => {
        this.setState({
            openAvatar: false
        })
    }

    handleRequestSetAvatar = (fileName) => {
        this.setState({
            avatar: fileName
        })
    }

    handleTargetStudent = (event) => {
        if (!isNaN(event.target.value)) {
            this.setState({
                targetStudent: event.target.value
            });
        }
    };

    handleClose = () => {
        this.setState({ showDialog: false });
    };

    render() {
        const actions = [
            <FlatButton
                label="Close"
                primary={true}
                keyboardFocused={true}
                onClick={this.handleClose}
            />,
        ];

        if (this.state.isDirect) {
            return <Redirect push to={`/MyProfile/${this.state.uid}`} />
        }

        if (this.state.isSave) {
            return <Redirect push to={`/companymycampaigninfo/${this.props.selectedKey}`} />
        }

        return (
            <div style={{ overflow: 'hidden', display: 'flex', width: '100%' }}>
                <Dialog
                    title={<DialogTitle title='Choose an image, recommended size 1366 * 768' onRequestClose={this.handleCloseAvatarGallery} />}
                    modal={false}
                    open={this.state.openAvatar}
                    onRequestClose={this.handleCloseAvatarGallery}
                    overlayStyle={{ background: "rgba(0,0,0,0.12)" }}
                    autoDetectWindowHeight={false}

                >
                    <ImageGallery set={this.handleRequestSetAvatar} close={this.handleCloseAvatarGallery} />
                </Dialog>
                <div style={{ width: '100%', margin: '2%', float: 'left' }}>
                    <div style={{ backgroundColor: 'white', padding: '2%', margin: '2% 0 0 0' }}>
                        <table style={{ width: '100%' }}>
                            <tr>
                                <td style={{ width: '70%' }}>
                                    <div style={{ position: 'relative' }}>
                                        <CoverAvatar fileName={this.state.avatar} height={'400px'} />
                                        <div style={{
                                            position: 'absolute', right: '3%', top: '10px',
                                            padding: '1%', background: 'white', borderRadius: 30
                                        }} onClick={this.handleOpenAvatarGallery}>
                                            <CameraIcon size={20} color='#045e7a' />
                                        </div>
                                    </div>
                                    <br />
                                    <text style={{ fontSize: '20', fontWeight: 'bold' }}>{this.state.campaignList.campaignTitle}</text><br />
                                    <text style={{ fontWeight: 'bold' }}>{this.state.campaignList.Company}</text><br />
                                    <text>{this.state.campaignList.date}</text><br />
                                    <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                                        <div style={{ marginTop: '2%', width: '50%', paddingRight: '2%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>Start Date</text><br />
                                            <DatePicker
                                                onChange={this.handleChangeStartDate}
                                                minDate={this.state.todayDate}
                                                defaultDate={this.state.startDate} />
                                        </div>
                                        <div style={{ marginTop: '2%', width: '50%' }}>
                                            <text style={{ fontWeight: 'bold', fontSize: 14 }}>End Date</text><br />
                                            <DatePicker
                                                onChange={this.handleChangeEndDate}
                                                minDate={this.state.startDate}
                                                defaultDate={this.state.endDate} />
                                            {
                                                this.state.errorEndDate ?
                                                    <text style={{ fontSize: 12, color: 'red' }}>This end date cannot earlier than start date</text>
                                                    :
                                                    false
                                            }
                                        </div>
                                    </div>
                                    <TextField
                                        style={{ width: '100%' }}
                                        floatingLabelText="Compaign Description"
                                        value={this.state.campaignDesc}
                                        onChange={this.handleCampaignDesc}
                                        multiLine={true}
                                        rows={2}
                                    />
                                </td>
                                <td style={{ width: '30%' }}>
                                    <div style={{ textAlign: 'center' }}>
                                        <text style={{ fontSize: this.state.xpFontSize, fontWeight: 'bold' }}>{this.state.campaignList.monstaXP}</text><br />
                                        <text style={{ fontSize: this.state.xpTitle }}>Monsta XP</text><br />
                                        <RaisedButton label="Save" primary={true} onClick={this.handleSaveCampaign()} />
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style={{ width: '20%' }}>
                                    <text> </text>
                                </td>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}>Target&nbsp;Student<br /></text>
                                            </td>
                                            <td >
                                                <text style={{ fontWeight: 'bold' }}> : </text>
                                            </td>
                                            <td>
                                                <TextField
                                                    style={{ width: '100%' }}
                                                    hintText="Target Student"
                                                    value={this.state.targetStudent}
                                                    onChange={this.handleTargetStudent}
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                {
                                                    this.state.campaignList.typeOfCampaign === 1 ?
                                                        <text style={{ fontWeight: 'bold' }}>Type&nbsp;of&nbsp;CAREER&amp;OPPORTUNITIES<br /></text>
                                                        :
                                                        this.state.campaignList.typeOfCampaign === 2 ?
                                                            <text style={{ fontWeight: 'bold' }}>Type&nbsp;of&nbsp;DIGITAL&nbsp;CHALLENGE<br /></text>
                                                            :
                                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                                <text style={{ fontWeight: 'bold' }}>Type&nbsp;of&nbsp;Event(s) <br /></text>
                                                                : ''
                                                }
                                            </td>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}> : </text>
                                            </td>
                                            <td>
                                                {
                                                    this.state.campaignList.typeOfCampaign === 1 ?
                                                        this.state.campaignList.typeOfSubCampaign === 1 ?
                                                            <text style={{ paddingLeft: '1%' }}>Part-time</text>
                                                            :
                                                            this.state.campaignList.typeOfSubCampaign === 2 ?
                                                                <text style={{ paddingLeft: '1%' }}>Full-time</text>
                                                                :
                                                                this.state.campaignList.typeOfSubCampaign === 3 ?
                                                                    <text style={{ paddingLeft: '1%' }}>Internship</text>
                                                                    :
                                                                    this.state.campaignList.typeOfSubCampaign === 4 ?
                                                                        <text style={{ paddingLeft: '1%' }}>Volunteers</text>
                                                                        :
                                                                        this.state.campaignList.typeOfSubCampaign === 5 ?
                                                                            <text style={{ paddingLeft: '1%' }}>Freelance</text>
                                                                            :
                                                                            ''
                                                        :
                                                        this.state.campaignList.typeOfCampaign === 2 ?
                                                            this.state.campaignList.typeOfSubCampaign === 1 ?
                                                                <text style={{ paddingLeft: '1%' }}>Survey</text>
                                                                :
                                                                this.state.campaignList.typeOfSubCampaign === 2 ?
                                                                    <text style={{ paddingLeft: '1%' }}>Feedback</text>
                                                                    :
                                                                    this.state.campaignList.typeOfSubCampaign === 3 ?
                                                                        <text style={{ paddingLeft: '1%' }}>Idea&nbsp;Bank</text>
                                                                        :
                                                                        this.state.campaignList.typeOfSubCampaign === 4 ?
                                                                            <text style={{ paddingLeft: '1%' }}>Competition</text>
                                                                            :
                                                                            ''
                                                            :
                                                            this.state.campaignList.typeOfCampaign === 3 ?
                                                                this.state.campaignList.typeOfSubCampaign === 1 ?
                                                                    <text style={{ paddingLeft: '1%' }}>With&nbsp;Attendance</text>
                                                                    :
                                                                    this.state.campaignList.typeOfSubCampaign === 2 ?
                                                                        <text style={{ paddingLeft: '1%' }}>Without&nbsp;Attendance</text>
                                                                        :
                                                                        ''
                                                                :
                                                                ''
                                                }
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}>Additional Rewards</text><br />
                                            </td>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}> : </text>
                                            </td>
                                            <td>
                                                <TextField
                                                    style={{ width: '100%' }}
                                                    value={this.state.addReward}
                                                    hintText="Additional Rewards"
                                                    onChange={this.handleAddReward}
                                                    multiLine={true}
                                                    rows={1}
                                                />
                                            </td>
                                        </tr>
                                        {
                                            this.state.campaignList.specialization ?
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Specialization</text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text>
                                                    </td>
                                                    <td>
                                                        <DropDownMenu value={this.state.specialization} onChange={this.handleSpecialization}>
                                                            {this.state.specializationList.map((e) => (
                                                                <MenuItem value={e} primaryText={e.split('_').join(' ')} />
                                                            ))}
                                                        </DropDownMenu>
                                                    </td>
                                                </tr>
                                                :
                                                ''
                                        }
                                        <tr>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}>Skills&nbsp;can&nbsp;be&nbsp;learned</text><br />
                                            </td>
                                            <td>
                                                <text style={{ fontWeight: 'bold' }}> : </text>
                                            </td>
                                            <td>
                                                <SelectField
                                                    multiple={true}
                                                    hintText="Select skills"
                                                    value={this.state.skillCanLearn}
                                                    onChange={this.updateSkillCanLearn}
                                                >
                                                    {this.state.skillList.map((e, idx) => (
                                                        this.state.skillCanLearn ?
                                                            <MenuItem
                                                                key={e}
                                                                insetChildren={true}
                                                                checked={this.state.skillCanLearn.indexOf(idx) > -1}
                                                                value={idx}
                                                                primaryText={e}
                                                            />
                                                            :
                                                            <MenuItem
                                                                key={e}
                                                                insetChildren={true}
                                                                checked={false}
                                                                value={idx}
                                                                primaryText={e}
                                                            />
                                                    ))}
                                                </SelectField>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <hr className="hr-text" />
                        <div>
                            {
                                this.state.campaignList.typeOfCampaign === 1 ?
                                    <div>
                                        <text style={{ fontWeight: 'bold' }}>Job description</text><br />
                                        <hr className="hr-text" />
                                        <div>
                                            <TextField
                                                style={{ width: '100%' }}
                                                floatingLabelText="1. Requirements"
                                                multiLine={true}
                                                value={this.state.requirements}
                                                onChange={this.handleRequirements}
                                            />
                                            <br />
                                            <TextField
                                                style={{ width: '100%' }}
                                                floatingLabelText="2. Responsibilities"
                                                multiLine={true}
                                                value={this.state.responsibilities}
                                                onChange={this.handleResponsibilities}
                                            />
                                            <br />
                                            <TextField
                                                style={{ width: '100%' }}
                                                floatingLabelText="3. Benefits "
                                                multiLine={true}
                                                value={this.state.benefits}
                                                onChange={this.handleBenefits}
                                            />
                                            <br />
                                        </div>
                                        <br />
                                        <text style={{ fontWeight: 'bold' }}>Extra Info </text><br />
                                        <hr className="hr-text" />
                                        <div style={{ paddingLeft: '1%' }}>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Allowance/Payout </text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                                    <td>
                                                        <text>RM </text>
                                                        <CurrencyInput style={{ textAlign: 'right' }} value={this.state.minAmount} onChangeEvent={this.handleMinAmount} />
                                                        <text> - </text>
                                                        <CurrencyInput style={{ textAlign: 'right' }} value={this.state.maxAmount} onChangeEvent={this.handleMaxAmount} />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div>
                                            <br />
                                            <text style={{ fontWeight: 'bold' }}>Advance Setting </text> <br />
                                            <hr className="hr-text" />
                                            <table style={{ paddingLeft: '2%' }}>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Privacy</text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                                    <td>
                                                        <Toggle
                                                            label={
                                                                this.state.privacy === true ?
                                                                    "Show"
                                                                    :
                                                                    "Hide"
                                                            }
                                                            labelPosition="right"
                                                            toggled={this.state.privacy}
                                                            onToggle={this.handlePrivacySetting}
                                                        />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Salary Type </text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                                    <td>
                                                        <DropDownMenu value={this.state.salaryType} onChange={this.handleSalaryType}>
                                                            <MenuItem value='PerHour' primaryText="Per Hour" />
                                                            <MenuItem value='PerDay' primaryText="Per Day" />
                                                            <MenuItem value='PerMonth' primaryText="Per Month" />
                                                            <MenuItem value='PerCompletion' primaryText="Per Completion/Project" />
                                                        </DropDownMenu>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Minimum Education </text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                                    <td>
                                                        <DropDownMenu value={this.state.minEducation} onChange={this.handleMinEducation}>
                                                            <MenuItem value='PrimarySchool' primaryText="Primary School" />
                                                            <MenuItem value='HighSchool' primaryText="High School" />
                                                            <MenuItem value='Diploma' primaryText="Certificates/ Vocational/ Diploma" />
                                                            <MenuItem value='Degree' primaryText="Degree" />
                                                            <MenuItem value='Master' primaryText="Master/PHD" />
                                                            <MenuItem value='NoLimit' primaryText="Not Required" />
                                                        </DropDownMenu>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Required Courses </text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                            {/*         <td>
                                                        <SelectField
                                                            multiple={true}
                                                            hintText="Required Courses"
                                                            value={this.state.requiredCourses}
                                                            onChange={this.updateRequiredCourses}
                                                        >
                                                            {this.state.coursesList.map((e, idx) => (
                                                                <MenuItem
                                                                    key={e}
                                                                    insetChildren={true}
                                                                    checked={this.state.requiredCourses.indexOf(idx) > -1}
                                                                    value={idx}
                                                                    primaryText={e}
                                                                />
                                                            ))}
                                                        </SelectField>
                                                    </td> */}
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}>Required Skills </text><br />
                                                    </td>
                                                    <td>
                                                        <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                    </td>
                                                    <td>
                                                        <SelectField
                                                            multiple={true}
                                                            hintText="Required Skills"
                                                            value={this.state.requiredSkills}
                                                            onChange={this.updateRequiredSkills}
                                                        >
                                                            {this.state.skillList.map((e, idx) => (
                                                                <MenuItem
                                                                    key={e}
                                                                    insetChildren={true}
                                                                    checked={this.state.requiredSkills.indexOf(idx) > -1}
                                                                    value={idx}
                                                                    primaryText={e}
                                                                />
                                                            ))}
                                                        </SelectField>
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                        </div>
                                        <text style={{ fontWeight: 'bold' }}>Work Location </text><br />
                                        <hr className="hr-text" />
                                        {
                                            this.state.workLocation ?
                                                this.state.workLocation.map((location, idx) => (
                                                    <div style={{ padding: '1%' }}>
                                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveLocation(idx)} />
                                                        <text style={{ fontWeight: 'bold' }}>{`Work Location ${idx + 1}`}</text><br />
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            hintText='Street address 1'
                                                            value={location.add1}
                                                            onChange={this.handleAdd1Change(idx)}
                                                        />
                                                        <br />
                                                        <TextField
                                                            style={{ width: '100%' }}
                                                            hintText='Street address 2'
                                                            value={location.add2}
                                                            onChange={this.handleAdd2Change(idx)}
                                                        />
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <text>City</text>
                                                                </td>
                                                                <td> : </td>
                                                                <td>
                                                                    <TextField
                                                                        style={{ width: '100%' }}
                                                                        hintText='City'
                                                                        value={location.city}
                                                                        onChange={this.handleCityChange(idx)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <text>State</text>
                                                                </td>
                                                                <td> : </td>
                                                                <td>
                                                                    <DropDownMenu value={location.state} onChange={this.handleStateChange(idx)}>
                                                                        <MenuItem value='Johor' primaryText="Johor" />
                                                                        <MenuItem value='Kedah' primaryText="Kedah" />
                                                                        <MenuItem value='Kelantan' primaryText="Kelantan" />
                                                                        <MenuItem value='KualaLumpur' primaryText="Kuala Lumpur" />
                                                                        <MenuItem value='Labuan' primaryText="Labuan" />
                                                                        <MenuItem value='Malacca' primaryText="Malacca" />
                                                                        <MenuItem value='NegeriSembilan' primaryText="Negeri Sembilan" />
                                                                        <MenuItem value='Pahang' primaryText="Pahang" />
                                                                        <MenuItem value='Penang' primaryText="Penang" />
                                                                        <MenuItem value='Perak' primaryText="Perak" />
                                                                        <MenuItem value='Perlis' primaryText="Perlis" />
                                                                        <MenuItem value='Putrajaya' primaryText="Putrajaya" />
                                                                        <MenuItem value='Sabah' primaryText="Sabah" />
                                                                        <MenuItem value='Sarawak' primaryText="Sarawak" />
                                                                        <MenuItem value='Selangor' primaryText="Selangor" />
                                                                        <MenuItem value='Terengganu' primaryText="Terengganu" />
                                                                    </DropDownMenu>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <text>Zip/Postal Code</text>
                                                                </td>
                                                                <td> : </td>
                                                                <td>
                                                                    <TextField
                                                                        style={{ width: '100%' }}
                                                                        hintText='Zip/Postal Code'
                                                                        value={location.zip}
                                                                        onChange={this.handleZipChange(idx)}
                                                                    />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <br />
                                                    </div>
                                                ))
                                                :
                                                ''
                                        }
                                        {
                                            this.state.workLocation ?
                                                this.state.workLocation.length < 3 ?
                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                        <FlatButton
                                                            label="Add Location"
                                                            onClick={this.addNewLocation.bind(this)}
                                                            style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                        /><br />
                                                    </div>
                                                    :
                                                    ''
                                                :
                                                <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                    <FlatButton
                                                        label="Add Location"
                                                        onClick={this.addNewLocation.bind(this)}
                                                        style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                    /><br />
                                                </div>
                                        }
                                        <Checkbox
                                            label="Aptitude Test"
                                            checked={this.state.aptitudeTest}
                                            onCheck={this.updateAptitudeTest.bind(this)}
                                        />
                                        <hr className="hr-text" />
                                        {
                                            this.state.questionList ?
                                                this.state.questionList.map((quesList, idx) => (
                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuestion(idx)} />
                                                        <TextField
                                                            hintText={`Enter your question${idx + 1} here`}
                                                            value={quesList.question}
                                                            onChange={this.handleQuestionChange(idx)}
                                                        />
                                                        <br />
                                                        <text style={{ fontWeight: 'bold' }}>Type of answer</text><br />
                                                        <RadioButtonGroup valueSelected={quesList.selectedGroup} onChange={this.handleSelectionType(idx)} style={{ padding: '2%' }}>
                                                            <RadioButton
                                                                value='multiSelect'
                                                                label="Multiple Select"
                                                            />
                                                            <RadioButton
                                                                value='onlyOne'
                                                                label="Select only one"
                                                            />
                                                        </RadioButtonGroup>
                                                        <div>
                                                            <text style={{ fontWeight: 'bold' }}>Answers</text><br />
                                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                                {quesList.answerList.map((ansList, ansIdx) => (
                                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', margin: '1%', padding: '1%' }}>
                                                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveAnswer(idx, ansIdx)} />
                                                                        {quesList.selectedGroup === 'multiSelect' ?
                                                                            <div style={{ float: 'right', color: '#818078', marginTop: '10px' }}>
                                                                                <input type="checkbox" name="radAnswer" checked={ansList.isAnswer === true ? "checked" : ''} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                <text>Correct Answer</text>
                                                                            </div>
                                                                            :
                                                                            <div style={{ float: 'right', color: '#818078', marginTop: '10px' }}>
                                                                                <input type="radio" name="radAnswer" checked={ansList.isAnswer === true ? "checked" : ''} onClick={this.handleIsAnswer(idx, ansIdx)} />
                                                                                <text>Correct Answer</text>
                                                                            </div>
                                                                        }
                                                                        <TextField
                                                                            style={{ width: '80%' }}
                                                                            hintText={`Enter your answer${ansIdx + 1} here`}
                                                                            value={ansList.answer}
                                                                            onChange={this.handleAnswerChange(idx, ansIdx)}
                                                                        />
                                                                    </div>
                                                                ))}
                                                                {
                                                                    quesList.answerList.length < 4 ?
                                                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                                            <FlatButton
                                                                                label="Add Answer"
                                                                                onClick={this.addNewAnswer(idx)}
                                                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                                            /><br />
                                                                        </div>
                                                                        :
                                                                        ''
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                                :
                                                ''
                                        }
                                        {
                                            this.state.aptitudeTest && this.state.questionList.length < 5 ?
                                                <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                    <FlatButton
                                                        label="Add Question"
                                                        onClick={this.addNewQuestion.bind(this)}
                                                        style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                    /><br />
                                                </div>
                                                :
                                                ''
                                        }
                                    </div>
                                    :
                                    this.state.campaignList.typeOfCampaign === 2 ?
                                        this.state.campaignList.typeOfSubCampaign === 1 || this.state.campaignList.typeOfSubCampaign === 2 ?
                                            <div>
                                                {
                                                    this.state.questionList ?
                                                        this.state.questionList.map((quesList, idx) => (
                                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                                <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveQuestion(idx)} />
                                                                <TextField
                                                                    hintText={`Enter your question${idx + 1} here`}
                                                                    value={quesList.question}
                                                                    onChange={this.handleQuestionChange(idx)}
                                                                />
                                                                <br />
                                                                <text style={{ fontWeight: 'bold' }}>Type of answer</text><br />
                                                                <RadioButtonGroup valueSelected={quesList.selectedGroup} onChange={this.handleSelectionType(idx)} style={{ padding: '2%' }}>
                                                                    <RadioButton
                                                                        value='multiSelect'
                                                                        label="Multiple Select"
                                                                    />
                                                                    <RadioButton
                                                                        value='onlyOne'
                                                                        label="Select only one"
                                                                    />
                                                                    <RadioButton
                                                                        value='ratingScale'
                                                                        label="Rating Scale"
                                                                    />
                                                                    <RadioButton
                                                                        value='textboxMultiLine'
                                                                        label="Textbox (multiple line)"
                                                                    />
                                                                    <RadioButton
                                                                        value='textboxSingleLine'
                                                                        label="Textbox (single line)"
                                                                    />
                                                                </RadioButtonGroup>
                                                                {
                                                                    quesList.selectedGroup !== 'textboxMultiLine' && quesList.selectedGroup !== 'textboxSingleLine' && quesList.selectedGroup !== '' ?
                                                                        <div>
                                                                            <text style={{ fontWeight: 'bold' }}>Answers</text><br />
                                                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', padding: '1%', margin: '1%' }}>
                                                                                {quesList.answerList.map((ansList, ansIdx) => (
                                                                                    <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', margin: '1%', padding: '1%' }}>
                                                                                        <FlatButton label="Remove" style={{ fontWeight: 'bold', color: '#818078', cursor: 'pointer', float: 'right', marginTop: '5px', width: '20px' }} onClick={this.handleRemoveAnswer(idx, ansIdx)} />
                                                                                        <TextField
                                                                                            style={{ width: '80%' }}
                                                                                            hintText={`Enter your answer${ansIdx + 1} here`}
                                                                                            value={ansList.answer}
                                                                                            onChange={this.handleAnswerChange(idx, ansIdx)}
                                                                                        />
                                                                                    </div>
                                                                                ))}
                                                                                {
                                                                                    quesList.answerList.length < 4 ?
                                                                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                                                            <FlatButton
                                                                                                label="Add Answer"
                                                                                                onClick={this.addNewAnswer(idx)}
                                                                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                                                            /><br />
                                                                                        </div>
                                                                                        :
                                                                                        ''
                                                                                }
                                                                            </div>
                                                                        </div>
                                                                        :
                                                                        ''
                                                                }
                                                            </div>
                                                        ))
                                                        :
                                                        ''
                                                }
                                                {
                                                    this.state.questionList ?
                                                        this.state.questionList.length < 5 ?
                                                            <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                                <FlatButton
                                                                    label="Add Question"
                                                                    onClick={this.addNewQuestion.bind(this)}
                                                                    style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                                /><br />
                                                            </div>
                                                            :
                                                            ''
                                                        :
                                                        <div style={{ borderStyle: 'dotted', borderWidth: 1, borderColor: '#818078', textAlign: 'center', margin: '1%' }}>
                                                            <FlatButton
                                                                label="Add Question"
                                                                onClick={this.addNewQuestion.bind(this)}
                                                                style={{ fontWeight: 'bold', color: '#818078', width: '100%' }}
                                                            /><br />
                                                        </div>
                                                }
                                                {
                                                    this.state.campaignList.isVideo !== null || this.state.campaignList.isVideo !== undefined ?
                                                        <div>
                                                            <table style={{ paddingLeft: '2%' }}>
                                                                <tr>
                                                                    <td>
                                                                        <text style={{ fontWeight: 'bold' }}>Youtube </text><br />
                                                                    </td>
                                                                    <td style={{ fontWeight: 'bold' }}> : </td>
                                                                    <td>
                                                                        <Toggle
                                                                            labelPosition="right"
                                                                            toggled={this.state.isVideo}
                                                                            onToggle={this.handleVideo}
                                                                        />
                                                                    </td>
                                                                    <td style={{ width: '80%' }}>
                                                                        {
                                                                            this.state.isVideo === true ?
                                                                                <TextField
                                                                                    style={{ width: '100%' }}
                                                                                    hintText={'Enter youtube URL here'}
                                                                                    value={this.state.url}
                                                                                    onChange={this.handleURLChange}
                                                                                />
                                                                                :
                                                                                ''
                                                                        }
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            {
                                                                this.state.videoId !== '' ?
                                                                    <YouTube
                                                                        videoId={this.state.videoId}
                                                                        opts={this.state.opts}
                                                                    //onReady={this._onReady}
                                                                    />
                                                                    :
                                                                    ''
                                                            }
                                                        </div>
                                                        :
                                                        ''
                                                }
                                            </div>
                                            :
                                            <div>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Submit Method </text><br />
                                                        </td>
                                                        <td style={{ fontWeight: 'bold' }}> : </td>
                                                        <td>
                                                            <DropDownMenu value={this.state.submitMethod} onChange={this.handleSubmitMethod}>
                                                                <MenuItem value='submitLink' primaryText="Submit Link" />
                                                            </DropDownMenu>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        :
                                        this.state.campaignList.typeOfCampaign === 3 ?
                                            <div>
                                                <table style={{ paddingLeft: '2%' }}>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Start Date</text> <br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>:</text> <br />
                                                        </td>
                                                        <td>
                                                            <DatePicker
                                                                onChange={this.handleChangeEventDate}
                                                                minDate={this.state.eventDate}
                                                                defaultDate={this.state.eventDate} />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Start Time</text> <br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>:</text> <br />
                                                        </td>
                                                        <td>
                                                            <TimePicker
                                                                format="ampm"
                                                                value={this.state.eventTime}
                                                                onChange={this.handleChangeEventTime}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Event location</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                floatingLabelText='Street address 1'
                                                                value={this.state.eventAdd1}
                                                                onChange={this.handleEventAdd1Change}
                                                            />
                                                            <br />
                                                            <TextField
                                                                style={{ width: '100%' }}
                                                                floatingLabelText='Street address 2'
                                                                value={this.state.eventAdd2}
                                                                onChange={this.handleEventAdd2Change}
                                                            />
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <text>City</text>
                                                                    </td>
                                                                    <td> : </td>
                                                                    <td>
                                                                        <TextField
                                                                            style={{ width: '100%' }}
                                                                            hintText='City'
                                                                            value={this.state.eventCity}
                                                                            onChange={this.handleEventCityChange}
                                                                        />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <text>State</text>
                                                                    </td>
                                                                    <td> : </td>
                                                                    <td>
                                                                        <DropDownMenu value={this.state.eventState} onChange={this.handleEventStateChange}>
                                                                            <MenuItem value='Johor' primaryText="Johor" />
                                                                            <MenuItem value='Kedah' primaryText="Kedah" />
                                                                            <MenuItem value='Kelantan' primaryText="Kelantan" />
                                                                            <MenuItem value='KualaLumpur' primaryText="Kuala Lumpur" />
                                                                            <MenuItem value='Labuan' primaryText="Labuan" />
                                                                            <MenuItem value='Malacca' primaryText="Malacca" />
                                                                            <MenuItem value='NegeriSembilan' primaryText="Negeri Sembilan" />
                                                                            <MenuItem value='Pahang' primaryText="Pahang" />
                                                                            <MenuItem value='Penang' primaryText="Penang" />
                                                                            <MenuItem value='Perak' primaryText="Perak" />
                                                                            <MenuItem value='Perlis' primaryText="Perlis" />
                                                                            <MenuItem value='Putrajaya' primaryText="Putrajaya" />
                                                                            <MenuItem value='Sabah' primaryText="Sabah" />
                                                                            <MenuItem value='Sarawak' primaryText="Sarawak" />
                                                                            <MenuItem value='Selangor' primaryText="Selangor" />
                                                                            <MenuItem value='Terengganu' primaryText="Terengganu" />
                                                                        </DropDownMenu>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <text>Zip/Postal Code</text>
                                                                    </td>
                                                                    <td> : </td>
                                                                    <td>
                                                                        <TextField
                                                                            style={{ width: '100%' }}
                                                                            hintText='Zip/Postal Code'
                                                                            value={this.state.eventZip}
                                                                            onChange={this.handleEventZipChange}
                                                                        />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Ticket</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.ticket === true ?
                                                                        "Paid"
                                                                        :
                                                                        "Free"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.ticket}
                                                                onToggle={this.handleTicket}
                                                            />
                                                        </td>
                                                    </tr>
                                                    {
                                                        this.state.ticket === true ?
                                                            <tr>
                                                                <td>
                                                                    <text> </text>
                                                                </td>
                                                                <td>
                                                                    <text> </text>
                                                                </td>
                                                                <td>
                                                                    <text>RM </text>
                                                                    <CurrencyInput style={{ textAlign: 'right' }} value={this.state.eventTicketPrice} onChangeEvent={this.handleChangeTicketPrice} />
                                                                </td>
                                                            </tr>
                                                            :
                                                            ''
                                                    }
                                                </table>
                                                <br />
                                                {/*
                                                <text style={{ fontWeight: 'bold' }}>Options Standard Filled </text><br />
                                                <hr className="hr-text" />
                                                <table style={{ paddingLeft: '2%' }}>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Name</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isName === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isName}
                                                                onToggle={this.handleIsName}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Age</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isAge === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isAge}
                                                                onToggle={this.handleIsAge}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Gender</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isGender === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isGender}
                                                                onToggle={this.handleIsGender}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Contact</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isContact === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isContact}
                                                                onToggle={this.handleIsContact}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Email</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isEmail === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isEmail}
                                                                onToggle={this.handleIsEmail}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>Course</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isCourse === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isCourse}
                                                                onToggle={this.handleIsCourse}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>University</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isUni === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isUni}
                                                                onToggle={this.handleIsUni}
                                                            />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}>T-shirt size</text><br />
                                                        </td>
                                                        <td>
                                                            <text style={{ fontWeight: 'bold' }}> : </text><br />
                                                        </td>
                                                        <td>
                                                            <Toggle
                                                                label={
                                                                    this.state.isTShirt === true ?
                                                                        "Show"
                                                                        :
                                                                        "Hide"
                                                                }
                                                                labelPosition="right"
                                                                toggled={this.state.isTShirt}
                                                                onToggle={this.handleIsTShirt}
                                                            />
                                                        </td>
                                                    </tr>
                                                </table>*/
                                                }
                                            </div>
                                            :
                                            ''
                            }
                        </div>
                    </div>
                    {/*<div style={{ backgroundColor: 'white', padding: '2%', margin: '2% 0 0 0' }}>
                        <text>Students Participated</text>
                        {this.state.participantList}
                        </div>*/}
                </div>
            </div>
        );
    }
}

/**
 * Map dispatch to props
 * @param  {func} dispatch is the function to dispatch action to reducers
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

/**
 * Map state to props
 * @param  {object} state is the obeject from redux store
 * @param  {object} ownProps is the props belong to component
 * @return {object}          props of component
 */
const mapStateToProps = (state, ownProps) => {
    return {
        peopleInfo: state.user.info,
        avatarURL: state.imageGallery.imageURLList
    }
}

// - Connect component to redux store
export default connect(mapStateToProps, mapDispatchToProps)(CompEditCampaignInfo)