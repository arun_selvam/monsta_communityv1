/* var express = require('express');
var morgan = require('morgan');
var path = require('path');

// Create our app
var app = express();
const PORT = process.env.PORT || 3000;

// Setup logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));


app.use(function (req, res, next){
  if (req.headers['x-forwarded-proto'] === 'https') {
    res.redirect('http://' + req.hostname + req.url);
  } else {
    next();
  }
});

app.use(function(req, res, next) {
  var userAgent = req.get('User-Agent');
  console.log(userAgent);
  next();
});

app.use(express.static('public'));


// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
  res.sendFile(path.resolve('public', 'index.html'));

});


app.listen(PORT, function () {
  console.log('Express server is up on port ' + PORT);
});
 uncomment this block for productions*/

//this block for development with hot reload
var path = require('path');
var webpack = require('webpack');
var express = require('express');
var config = require('./webpack.config');

var app = express();
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});

app.listen(3000, function(err) {
  if (err) {
    return console.error(err);
  }

  console.log('Listening at http://localhost:3000/');
});
//this block for development with hot reload
